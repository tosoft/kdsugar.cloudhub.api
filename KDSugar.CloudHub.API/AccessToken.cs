﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API
{
    /// <summary>
    /// Access访问|发起
    /// </summary>
    public class AccessToken : PostRequest
    {
        /// <summary>
        /// appId
        /// </summary>
        public string appId { get; set; }

        /// <summary>
        /// 团队EID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// SecretKey
        /// </summary>
        public string secret { get; set; }

        /// <summary>
        /// (枚举)访问类型
        /// </summary>
        [JsonIgnore]
        public Constant.ScopeType _scope;

        /// <summary>
        /// 访问类型
        /// </summary>
        public string scope
        {
            get
            {
                return Enum.GetName(typeof(Constant.ScopeType), _scope);
            }
        }

        /// <summary>
        /// (日期时间实例)时间戳
        /// </summary>
        [JsonIgnore]
        public DateTime _timestamp = DateTime.Now;

        /// <summary>
        /// 时间戳
        /// </summary>
        public long timestamp
        {
            get
            {
                return Helper.ToUnixTime(_timestamp);
            }
        }

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="scopeType">访问类型</param>
        /// <param name="dateTime">时间</param>
        public AccessToken(Constant.ScopeType scopeType, DateTime? dateTime = null)
        {
            _scope = scopeType;
            if (dateTime != null)
                _timestamp = dateTime.Value;
        }

        private const string URL = "https://www.yunzhijia.com/gateway/oauth2/token/getAccessToken";

        /// <summary>
        /// 获取AccessToken
        /// </summary>
        /// <returns></returns>
        public _AccessToken GetToken()
        {
            return base.Post<_AccessToken>(URL);
        }
    }

    /// <summary>
    /// 刷新Token
    /// </summary>
    public class RefreshToken : PostRequest
    {
        /// <summary>
        /// AppId
        /// </summary>
        public string appId { get; set; }

        /// <summary>
        /// 团队EID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 刷新Token
        /// </summary>
        public string refreshToken { get; set; }

        /// <summary>
        /// (枚举)调用类型
        /// </summary>
        [JsonIgnore]
        public Constant.ScopeType _scope;

        /// <summary>
        /// 调用类型
        /// </summary>
        public string scope
        {
            get
            {
                return Enum.GetName(typeof(Constant.ScopeType), _scope);
            }
        }

        /// <summary>
        /// (日期时间实例)时间戳
        /// </summary>
        [JsonIgnore]
        public DateTime _timestamp = DateTime.Now;

        /// <summary>
        /// 时间戳
        /// </summary>
        public long timestamp
        {
            get
            {
                return Helper.ToUnixTime(_timestamp);
            }
        }

        /// <summary>
        /// 刷新Token|初始化
        /// </summary>
        /// <param name="scopeType">访问类型</param>
        /// <param name="dateTime">时间</param>
        public RefreshToken(Constant.ScopeType scopeType, DateTime? dateTime = null)
        {
            _scope = scopeType;
            if (dateTime != null)
                _timestamp = dateTime.Value;
        }

        private const string URL = "https://www.yunzhijia.com/gateway/oauth2/token/refreshToken";

        /// <summary>
        /// 获取AccessToken
        /// </summary>
        /// <returns></returns>
        public _AccessToken GetRefreshToken()
        {
            return base.Post<_AccessToken>(URL);
        }
    }

    /// <summary>
    /// Accetoken返回信息
    /// </summary>
    public class _AccessToken : BaseResult
    {
        /// <summary>
        /// Accetoken返回数据类
        /// </summary>
        public class _AccessTokenData
        {
            /// <summary>
            /// token字符串
            /// </summary>
            public string accessToken { get; set; }

            /// <summary>
            /// 有效时间(秒)
            /// </summary>
            public int expireIn { get; set; }

            /// <summary>
            /// 刷新token字符串
            /// </summary>
            public string refreshToken { get; set; }
        }

        /// <summary>
        /// Accetoken返回数据
        /// </summary>
        public _AccessTokenData data { get; set; }
    }
}
