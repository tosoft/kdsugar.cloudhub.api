﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList
{
    /// <summary>
    /// 基本组织部门信息
    /// </summary>
    public class OBaseOrg
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 部门ID
        /// </summary>
        public string orgId { get; set; }

        /// <summary>
        /// 上级部门ID
        /// </summary>
        public string parentId { get; set; }
    }
}
