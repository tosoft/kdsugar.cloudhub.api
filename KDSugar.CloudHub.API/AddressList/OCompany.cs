﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.AddressList
{
    /// <summary>
    /// 通讯录-基本工作圈信息
    /// </summary>
    public class OCompany
    {
        /// <summary>
        /// 工作圈名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 工作圈企业EID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 工作圈用户数
        /// </summary>
        [Obsolete("推荐使用UserCount属性")]
        public string userCount { get; set; }

        /// <summary>
        /// 工作圈用户数
        /// </summary>
        public int UserCount
        {
            get
            {
                int result = 0;
                int.TryParse(userCount, out result);
                return result;
            }
        }
    }
}
