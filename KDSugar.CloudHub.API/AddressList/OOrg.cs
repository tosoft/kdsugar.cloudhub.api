﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList
{
    /// <summary>
    /// 通讯录-基本组织信息
    /// </summary>
    public class OOrg
    {
        /// <summary>
        /// 部门名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 部门ID
        /// </summary>
        public string orgId { get; set; }

        /// <summary>
        /// 上级部门ID
        /// </summary>
        public string parentId { get; set; }

        /// <summary>
        /// 负责人信息
        /// </summary>
        public List<OPerson> inChargers { get; set; }
    }
}
