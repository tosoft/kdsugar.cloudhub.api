﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList
{
    /// <summary>
    /// 通讯录-基本人员信息
    /// </summary>
    public class OPerson
    {
        /// <summary>
        /// 人员的openid
        /// </summary>
        public string openId { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 头像URL
        /// </summary>
        public string photoUrl { get; set; }

        /// <summary>
        /// 手机号码（未开放）
        /// </summary>
        public string phone { get; set; }

        /// <summary>
        /// 邮箱（未开放）
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// 部门
        /// </summary>
        public string department { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string globalId { get; set; }

        /// <summary>
        /// 用户所在部门ID
        /// </summary>
        public string orgId { get; set; }

        /// <summary>
        /// 职位
        /// </summary>
        public string jobTitle { get; set; }

        /// <summary>
        /// 性别, 0: 不确定; 1: 男; 2: 女
        /// </summary>
        public string gender { get; set; }

        /// <summary>
        /// 是否管理员， 1: 是； 0:不是，拥有工作圈的最高权限（与创建者一致）
        /// </summary>
        public string isAdmin { get; set; }

        /// <summary>
        /// 是否在职， 1：在职；0: 离职
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// 工号
        /// </summary>
        public string jobNo { get; set; }
    }
}
