﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.OrgData
{
    /// <summary>
    /// 添加合作伙伴|初始化
    /// </summary>
    public class CreatePartners : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/opendata-control/partner/createPartners?accessToken={0}";

        /// <summary>
        /// 企业EID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 本人OID
        /// </summary>
        public string myOid { get; set; }

        /// <summary>
        /// 合作伙伴eid
        /// </summary>
        public string partnerEid { get; set; }

        /// <summary>
        /// 合作伙伴手机号
        /// </summary>
        public string partnerPhone { get; set; }

        /// <summary>
        /// 添加合作伙伴|初始化
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_myOid">本人OID</param>
        /// <param name="_partnerEid">合作伙伴EID</param>
        /// <param name="_partnerPhone">合作伙伴手机号</param>
        public CreatePartners(string _eid, string _myOid, string _partnerEid, string _partnerPhone)
        {
            eid = _eid;
            myOid = _myOid;
            partnerEid = _partnerEid;
            partnerPhone = _partnerPhone;
        }

        /// <summary>
        /// 添加合作伙伴
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public BaseResult Create(string _token)
        {
            return PostByUrlencoded<BaseResult>(URL, _token);
        }
    }
}
