﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.OrgData
{
    /// <summary>
    /// 删除合作伙伴联系人|发起
    /// </summary>
    public class DeletePartContacts : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/opendata-control/partner/delPartContacts?accessToken={0}";

        /// <summary>
        /// 企业EID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 合作伙伴EID
        /// </summary>
        public string partnerEid { get; set; }

        /// <summary>
        /// 联系人ID
        /// </summary>
        public string contactId { get; set; }

        /// <summary>
        /// 删除合作伙伴联系人|初始化
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_partnerEid">合作伙伴EID</param>
        /// <param name="_contactId">联系人ID</param>
        public DeletePartContacts(string _eid,string _partnerEid,string _contactId)
        {
            eid = _eid;
            partnerEid = _partnerEid;
            contactId = _contactId;
        }

        /// <summary>
        /// 删除合作伙伴联系人
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public BaseResult Delete(string _token)
        {
            return PostByUrlencoded<BaseResult>(URL, _token);
        }
    }
}
