﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.AddressList.OrgData
{
    /// <summary>
    /// 通过工作圈eid获取管理员oid|发起
    /// </summary>
    public class GetAdminOIDsByEID : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/opendata-control/data/getAdminOidsByEid?accessToken={0}";

        /// <summary>
        /// 企业EID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 通过工作圈eid获取管理员oid|初始化
        /// </summary>
        /// <param name="_eid">企业EID</param>
        public GetAdminOIDsByEID(string _eid)
        {
            eid = _eid;
        }

        /// <summary>
        /// 通过工作圈eid获取管理员oid
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _GetAdminOIDsByEID Query(string _token)
        {
            return PostByUrlencoded<_GetAdminOIDsByEID>(URL, _token);
        }
    }

    /// <summary>
    /// 通过工作圈eid获取管理员oid|返回
    /// </summary>
    public class _GetAdminOIDsByEID : BaseResult
    {
        /// <summary>
        /// 管理员OID信息
        /// </summary>
        public AdminOIDsInfo data { get; set; }

        /// <summary>
        /// 管理员OID信息类
        /// </summary>
        public class AdminOIDsInfo
        {
            /// <summary>
            /// 管理员OID信息
            /// </summary>
            public string oids { get; set; }

            /// <summary>
            /// 管理员OID信息
            /// </summary>
            [JsonIgnore]
            public List<string> OIDs
            {
                get
                {
                    List<string> result = null;
                    try
                    {
                        if (!string.IsNullOrWhiteSpace(oids))
                        {
                            var arrys = oids.Split(',');
                            if (arrys != null)
                                result = arrys.ToList();
                        }
                    }
                    catch
                    {
                        result = null;
                    }
                    return result;
                }
            }
        }
    }
}
