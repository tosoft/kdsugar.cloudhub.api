﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.OrgData
{
    /// <summary>
    /// 获取所有部门列表|发起
    /// </summary>
    public class GetAllDepts : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/opendata-control/data/getallorgs?accessToken={0}";

        /// <summary>
        /// 企业EID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 分页起始位置，从0开始
        /// </summary>
        public int? begin { get; set; }

        /// <summary>
        /// 分页记录条数，这个值不要大于1000
        /// </summary>
        public int? count { get; set; }

        /// <summary>
        /// 获取所有部门列表|初始化
        /// </summary>
        /// <param name="_eid"></param>
        public GetAllDepts(string _eid)
        {
            eid = _eid;
        }

        /// <summary>
        /// 获取所有部门列表
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _GetAllDepts Query(string _token)
        {
            return PostByUrlencoded<_GetAllDepts>(URL, _token);
        }
    }

    /// <summary>
    /// 获取所有部门列表|返回
    /// </summary>
    public class _GetAllDepts : BaseResult
    {
        /// <summary>
        /// 部门列表
        /// </summary>
        public List<OOrg> data { get; set; }
    }
}
