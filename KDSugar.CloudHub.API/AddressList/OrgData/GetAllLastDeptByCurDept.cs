﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.OrgData
{
    /// <summary>
    /// 获取当前部门的所有上级部门列表|发起
    /// </summary>
    public class GetAllLastDeptByCurDept : GetDeptInfoByOrgIdAndEID
    {
        private const string URL = "https://yunzhijia.com/gateway/opendata-control/data/getancestororgs?accessToken={0}";

        /// <summary>
        /// 获取当前部门的所有上级部门列表|初始化
        /// </summary>
        /// <param name="_orgId">部门ID</param>
        /// <param name="_eid">企业EID</param>
        public GetAllLastDeptByCurDept(string _orgId,string _eid)
            : base(_orgId, _eid)
        {

        }

        /// <summary>
        /// 获取当前部门的所有上级部门列表
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _GetAllLastDeptByCurDept Query(string _token)
        {
            return PostByUrlencoded<_GetAllLastDeptByCurDept>(URL, _token);
        }
    }

    /// <summary>
    /// 获取当前部门的所有上级部门列表|返回
    /// </summary>
    public class _GetAllLastDeptByCurDept : BaseResult
    {
        /// <summary>
        /// 上级部门列表
        /// </summary>
        public List<OOrg> data { get; set; }
    }
}
