﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.OrgData
{
    /// <summary>
    /// 获取当前部门所有下级部门列表|发起
    /// </summary>
    public class GetAllNextDeptByCurDept : GetDeptInfoByOrgIdAndEID
    {
        private const string URL = "https://yunzhijia.com/gateway/opendata-control/data/getsuborgs?accessToken={0}";

        /// <summary>
        /// 获取当前部门所有下级部门列表|初始化
        /// </summary>
        /// <param name="_orgId">组织ID</param>
        /// <param name="_eid">企业EID</param>
        public GetAllNextDeptByCurDept(string _orgId,string _eid)
            : base(_orgId, _eid)
        {

        }

        /// <summary>
        /// 获取当前部门所有下级部门列表
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _GetAllNextDeptByCurDept Query(string _token)
        {
            return PostByUrlencoded<_GetAllNextDeptByCurDept>(URL, _token);
        } 
    }

    /// <summary>
    /// 获取当前部门所有下级部门列表|返回
    /// </summary>
    public class _GetAllNextDeptByCurDept : BaseResult
    {
        /// <summary>
        /// 当前部门所有下级部门列表
        /// </summary>
        public List<OOrg> data { get; set; }
    }
}
