﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.OrgData
{
    /// <summary>
    /// 获取企业所有组织人员|发起
    /// </summary>
    public class GetAllPersons : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/opendata-control/data/getallpersons?accessToken={0}";

        /// <summary>
        /// 拉取数量最大值
        /// </summary>
        public const int MAX_COUNT = 1000;

        /// <summary>
        ///企业EID 
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 查询时间，查询这个时刻之后所有变更的数据
        /// </summary>
        public string time { get; set; }

        /// <summary>
        /// 起始数，默认0
        /// </summary>
        public int? begin { get; set; }

        /// <summary>
        /// 拉取数量，默认1000
        /// <para>每次拉取限制1000条以内（包括1000条）</para>
        /// <para>如果返回条数不足count条，表示分页拉取已结束</para>
        /// </summary>
        public int? count { get; set; }

        /// <summary>
        /// 获取企业所有组织人员|初始化
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_dt">
        /// 查询时间，查询这个时刻之后所有变更的数据
        /// <para>若赋值为null，则视为第一次拉取</para>
        /// </param>
        /// <param name="_begin">起始数</param>
        /// <param name="_count">拉取数量</param>
        public GetAllPersons(string _eid,DateTime? _dt,int? _begin,int? _count)
        {
            eid = _eid;
            time = _dt != null ? _dt.Value.ToString("yyyy-MM-dd HH:mm:ss") :
                new DateTime(2008, 8, 2, 1, 40, 38).ToString("yyyy-MM-dd HH:mm:ss");
            begin = _begin;
            count = _count;
        }

        /// <summary>
        /// 获取企业所有组织人员
        /// </summary>
        /// <param name="_token"></param>
        /// <returns></returns>
        public _GetAllPersons Query(string _token)
        {
            return PostByUrlencoded<_GetAllPersons>(URL, _token);
        }
    }

    /// <summary>
    /// 获取企业所有组织人员|返回
    /// </summary>
    public class _GetAllPersons : BaseResult
    {
        /// <summary>
        /// 人员数据
        /// </summary>
        public PersonData data { get; set; }

        /// <summary>
        /// 人员数据类
        /// </summary>
        public class PersonData
        {
            /// <summary>
            /// 基本人员信息列表
            /// </summary>
            public List<OPerson> persons { get; set; }

            /// <summary>
            /// 本次拉取数据的截止时刻
            /// </summary>
            public string time { get; set; }
        }
    }
}
