﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.OrgData
{
    /// <summary>
    /// 获取企业基本信息|发起
    /// </summary>
    public class GetCompany : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/opendata-control/data/getcompany?accessToken={0}";

        /// <summary>
        /// 企业EID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 获取企业基本信息|初始化
        /// </summary>
        /// <param name="_eid">企业EID</param>
        public GetCompany(string _eid)
        {
            eid = _eid;
        }

        /// <summary>
        /// 获取企业基本信息
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _GetCompany Query(string _token)
        {
            return PostByUrlencoded<_GetCompany>(URL, _token);
        }
    }

    /// <summary>
    /// 获取企业基本信息|返回
    /// </summary>
    public class _GetCompany : BaseResult
    {
        /// <summary>
        /// 企业信息
        /// </summary>
        public OCompany data { get; set; }
        //public CompanyInfo data { get; set; }

        /// <summary>
        /// 企业信息类
        /// </summary>
        public class CompanyInfo
        {
            /// <summary>
            /// 企业EID
            /// </summary>
            public string eid { get; set; }

            /// <summary>
            /// 用户数量
            /// </summary>
            public string userCount { get; set; }

            /// <summary>
            /// 企业名称
            /// </summary>
            public string name { get; set; }
        }
    }
}
