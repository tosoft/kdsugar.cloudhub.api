﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.OrgData
{
    /// <summary>
    /// 查询部门信息|发起
    /// </summary>
    public class GetDeptInfo : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/opendata-control/data/getorginfos?accessToken={0}";

        /// <summary>
        /// 企业EID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 部门ID
        /// </summary>
        public string orgIds { get; set; }

        /// <summary>
        /// 分页开始
        /// </summary>
        public int? begin { get; set; }

        /// <summary>
        /// 每页数量
        /// </summary>
        public int? count { get; set; }

        /// <summary>
        /// 是否查询子部门信息
        /// </summary>
        public bool hasChild { get; set; }

        /// <summary>
        /// 查询单个部门信息|初始化
        /// </summary>
        /// <param name="_orgId">组织ID</param>
        /// <param name="_eid">企业EID</param>
        /// <param name="_hasChild">是否查询子部门信息</param>
        /// <param name="_begin">
        /// 分页开始
        /// <para>为null,默认0</para>
        /// </param>
        /// <param name="_count">
        /// 每页数量
        /// <para>为null,默认1000</para>
        /// </param>
        public GetDeptInfo(string _orgId, string _eid, bool _hasChild, int? _begin, int? _count)
        {
            orgIds = _orgId;
            eid = _eid;
            hasChild = _hasChild;
            begin = _begin;
            count = _count;
        }

        /// <summary>
        /// 查询多个部门信息|初始化
        /// </summary>
        /// <param name="_orgIds">组织ID</param>
        /// <param name="_eid">企业EID</param>
        /// <param name="_hasChild">是否查询子部门信息</param>
        /// <param name="_begin">
        /// 分页开始
        /// <para>为null,默认0</para>
        /// </param>
        /// <param name="_count">
        /// 每页数量
        /// <para>为null,默认1000</para>
        /// </param>
        public GetDeptInfo(List<string> _orgIds,string _eid, bool _hasChild, int? _begin, int? _count)
        {
            string tmpIds = "";
            if (_orgIds != null)
            {
                foreach (string id in _orgIds)
                {
                    if (!string.IsNullOrWhiteSpace(id))
                        tmpIds += string.Format("{0}_", id);
                }
            }
            if (tmpIds.Length > 0)
                tmpIds = tmpIds.Substring(0, tmpIds.Length - 1);
            orgIds = tmpIds;
            eid = _eid;
            hasChild = _hasChild;
            begin = _begin;
            count = _count;
        }

        /// <summary>
        /// 查询部门信息
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _GetDeptInfo Query(string _token)
        {
            return Post<_GetDeptInfo>(URL, _token);
        }
    }

    /// <summary>
    /// 查询部门信息|返回
    /// </summary>
    public class _GetDeptInfo : BaseResult
    {
        /// <summary>
        /// 部门信息列表
        /// </summary>
        public List<DeptInfo> data { get; set; }

        /// <summary>
        /// 部门信息类
        /// </summary>
        public class DeptInfo
        {
            /// <summary>
            /// 部门基本信息
            /// </summary>
            public OBaseOrg org { get; set; }

            /// <summary>
            /// 子部门信息
            /// </summary>
            public List<OBaseOrg> subOrgs { get; set; }

            /// <summary>
            /// 部门成员信息
            /// </summary>
            public List<OPerson> members { get; set; }
        }
    }
}
