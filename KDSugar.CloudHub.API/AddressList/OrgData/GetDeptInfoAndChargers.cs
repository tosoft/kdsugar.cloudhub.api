﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.OrgData
{
    /// <summary>
    /// 获取当前部门基本信息或部门负责人|发起
    /// </summary>
    public class GetDeptInfoAndChargers : GetDeptInfoByOrgIdAndEID
    {
        private const string URL = "https://yunzhijia.com/gateway/opendata-control/data/getorg?accessToken={0}";

        /// <summary>
        /// 获取当前部门基本信息或部门负责人|初始化
        /// </summary>
        /// <param name="_orgId">部门ID</param>
        /// <param name="_eid">企业EID</param>
        public GetDeptInfoAndChargers(string _orgId, string _eid)
            : base(_orgId, _eid)
        {

        }

        /// <summary>
        /// 获取当前部门基本信息或部门负责人
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _GetDeptInfoAndChargers Query(string _token)
        {
            return PostByUrlencoded<_GetDeptInfoAndChargers>(URL, _token);
        }
    }

    /// <summary>
    /// 获取当前部门基本信息或部门负责人|返回
    /// </summary>
    public class _GetDeptInfoAndChargers : BaseResult
    {
        /// <summary>
        /// 部门信息及负责人
        /// </summary>
        public DeptInfoChargers data { get; set; }

        /// <summary>
        /// 部门信息及负责人类
        /// </summary>
        public class DeptInfoChargers
        {
            /// <summary>
            /// 部门名称
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// 部门ID
            /// </summary>
            public string orgId { get; set; }

            /// <summary>
            /// 上级部门ID
            /// </summary>
            public string parentId { get; set; }

            /// <summary>
            /// 负责人信息
            /// </summary>
            public List<OPerson> inChargers { get; set; }
        }
    }
}
