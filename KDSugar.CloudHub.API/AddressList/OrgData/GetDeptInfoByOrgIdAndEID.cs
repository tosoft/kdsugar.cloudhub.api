﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.OrgData
{
    /// <summary>
    /// 仅根据部门ID与企业EID为参数获取数据（用于继承）
    /// </summary>
    public class GetDeptInfoByOrgIdAndEID : PostRequest
    {
        /// <summary>
        /// 部门ID
        /// </summary>
        public string orgId { get; set; }

        /// <summary>
        /// 企业EID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="_orgId">部门ID</param>
        /// <param name="_eid">企业EID</param>
        public GetDeptInfoByOrgIdAndEID(string _orgId,string _eid)
        {
            orgId = _orgId;
            eid = _eid;
        }
    }
}
