﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.OrgData
{
    /// <summary>
    /// 获取当前部门下一层级的所有部门基本信息列表|发起
    /// </summary>
    public class GetNextLevDeptByCurDept : GetDeptInfoByOrgIdAndEID
    {
        private const string URL = "https://yunzhijia.com/gateway/opendata-control/data/getsublevelorgs?accessToken={0}";

        /// <summary>
        /// 获取当前部门下一层级的所有部门基本信息列表|初始化
        /// </summary>
        /// <param name="_orgId">组织ID</param>
        /// <param name="_eid">企业EID</param>
        public GetNextLevDeptByCurDept(string _orgId, string _eid)
            : base(_orgId, _eid)
        {

        }

        /// <summary>
        /// 获取当前部门下一层级的所有部门基本信息列表
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _GetNextLevDeptByCurDept Query(string _token)
        {
            return PostByUrlencoded<_GetNextLevDeptByCurDept>(URL, _token);
        }
    }

    /// <summary>
    /// 获取当前部门下一层级的所有部门基本信息列表|返回
    /// </summary>
    public class _GetNextLevDeptByCurDept : BaseResult
    {
        /// <summary>
        /// 当前部门下一层级的所有部门基本信息列表
        /// </summary>
        public List<OOrg> data { get; set; }
    }
}
