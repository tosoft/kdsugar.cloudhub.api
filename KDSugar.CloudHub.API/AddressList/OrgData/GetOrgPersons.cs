﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.OrgData
{
    /// <summary>
    /// 获取当前部门成员或部门负责人信息|发起
    /// </summary>
    public class GetOrgPersons : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/opendata-control/data/getorgpersons?accessToken={0}";

        /// <summary>
        /// 企业EID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 组织ID
        /// </summary>
        public string orgId { get; set; }

        /// <summary>
        /// 默认0
        /// </summary>
        public int? begin { get; set; }

        /// <summary>
        /// 默认1000，每次拉取限制1000条以内（包括1000条）
        /// </summary>
        public int? count { get; set; }

        /// <summary>
        /// 获取当前部门成员或部门负责人信息|初始化
        /// </summary>
        /// <param name="_eid"></param>
        /// <param name="_orgId"></param>
        public GetOrgPersons(string _eid,string _orgId)
        {
            eid = _eid;
            orgId = _orgId;
        }

        /// <summary>
        /// 获取当前部门成员或部门负责人信息
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _GetOrgPersons Query(string _token)
        {
            return PostByUrlencoded<_GetOrgPersons>(URL, _token);
        }
    }

    /// <summary>
    /// 获取当前部门成员或部门负责人信息|返回
    /// </summary>
    public class _GetOrgPersons : BaseResult
    {
        /// <summary>
        /// 当前部门成员或部门负责人信息
        /// </summary>
        public OrgPersonInfo data { get; set; }

        /// <summary>
        /// 当前部门成员或部门负责人信息-类
        /// </summary>
        public class OrgPersonInfo
        {
            /// <summary>
            /// 部门负责人
            /// </summary>
            public List<OPerson> inChargers { get; set; }

            /// <summary>
            /// 部门成员
            /// </summary>
            public List<OPerson> members { get; set; }
        }
    }
}
