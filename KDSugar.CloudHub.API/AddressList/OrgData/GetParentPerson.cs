﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.OrgData
{
    /// <summary>
    ///  获取用户的默认上级或默认汇报上级或指定上级|发起
    /// </summary>
    public class GetParentPerson : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/opendata-control/data/getparentperson?accessToken={0}";

        /// <summary>
        /// 企业EID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 人员OID
        /// </summary>
        public string openId { get; set; }

        /// <summary>
        ///  获取用户的默认上级或默认汇报上级或指定上级|初始化
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_openId">人员OID</param>
        public GetParentPerson(string _eid,string _openId)
        {
            eid = _eid;
            openId = _openId;
        }

        /// <summary>
        ///  获取用户的默认上级或默认汇报上级或指定上级
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _GetParentPerson Query(string _token)
        {
            return PostByUrlencoded<_GetParentPerson>(URL, _token);
        }
    }

    /// <summary>
    ///  获取用户的默认上级或默认汇报上级或指定上级|返回
    /// </summary>
    public class _GetParentPerson : BaseResult
    {
        /// <summary>
        /// 用户的默认上级或默认汇报上级或指定上级
        /// </summary>
        public ParentPersonInfo data { get; set; }

        /// <summary>
        /// 上级信息
        /// </summary>
        public class ParentPersonInfo
        {
            /// <summary>
            /// 默认上级
            /// </summary>
            public List<OPerson> defaultParentPersons { get; set; }

            /// <summary>
            /// 默认汇报上级
            /// </summary>
            public List<OPerson> defaultParentSecPersons { get; set; }

            /// <summary>
            /// 指定上级
            /// </summary>
            public List<OPerson> selectParentPersons { get; set; }
        }
    }
}
