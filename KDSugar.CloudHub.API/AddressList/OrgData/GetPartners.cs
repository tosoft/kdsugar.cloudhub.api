﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.OrgData
{
    /// <summary>
    /// 根据企业eid查询全部合作伙伴信息|发起
    /// </summary>
    public class GetPartners : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/opendata-control/partner/getPartners?accessToken={0}";

        /// <summary>
        /// 根据企业eid查询全部合作伙伴信息
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 时间戳（精确到毫秒）;传空或者不传表示全部查询
        /// </summary>
        public string time { get; set; }

        /// <summary>
        /// 起始条数，默认为0
        /// </summary>
        public int? begin { get; set; }

        /// <summary>
        /// 默认1000，查询使用分页机制，每次查询总数不能超过1000条；如果输出条数不足count条，表示分页拉取已结束
        /// </summary>
        public int? count { get; set; }

        /// <summary>
        /// 根据企业eid查询全部合作伙伴信息|初始化
        /// </summary>
        /// <param name="_eid">企业EID</param>
        public GetPartners(string _eid)
        {
            eid = _eid;
        }

        /// <summary>
        /// 根据企业eid查询全部合作伙伴信息
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _GetPartners Query(string _token)
        {
            return PostByUrlencoded<_GetPartners>(URL, _token);
        }
    }

    /// <summary>
    /// 根据企业eid查询全部合作伙伴信息|返回
    /// </summary>
    public class _GetPartners : BaseResult
    {
        /// <summary>
        /// 合作伙伴
        /// </summary>
        public PartnerInfo data { get; set; }

        /// <summary>
        /// 合伙伙伴信息类
        /// </summary>
        public class PartnerInfo
        {
            /// <summary>
            /// 合作伙伴id
            /// </summary>
            public string partnerId { get; set; }

            /// <summary>
            /// 合作伙伴编号
            /// </summary>
            public string code { get; set; }

            /// <summary>
            /// 合作伙伴管理员名称
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// 0,未认证；1，已认证
            /// </summary>
            public int hasCertified { get; set; }

            /// <summary>
            /// 合作伙伴对应的云之家团队eid
            /// </summary>
            public string eid { get; set; }

            /// <summary>
            /// 合作伙伴名称
            /// </summary>
            public string companyName { get; set; }

            /// <summary>
            /// 联系人信息
            /// </summary>
            public List<ContactInfo> contacts { get; set; }

            /// <summary>
            /// 联系人信息类
            /// </summary>
            public class ContactInfo
            {
                /// <summary>
                /// 联系人名称
                /// </summary>
                public string name { get; set; }

                /// <summary>
                /// 联系人手机号码
                /// </summary>
                public string phone { get; set; }

                /// <summary>
                /// 联系人云之家对应openid
                /// </summary>
                public string openid { get; set; }

                /// <summary>
                /// 联系人id
                /// </summary>
                public string contactId { get; set; }
            }
        }
    }
}
