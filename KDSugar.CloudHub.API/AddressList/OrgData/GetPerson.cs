﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.OrgData
{
    /// <summary>
    /// 获取个人信息|发起
    /// </summary>
    public class GetPerson : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/opendata-control/data/getperson?accessToken={0}";

        /// <summary>
        /// 人员openId
        /// </summary>
        public string openId { get; set; }

        /// <summary>
        /// 企业EID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 获取个人信息|初始化
        /// </summary>
        /// <param name="_openId">人员openId</param>
        /// <param name="_eid">企业EID</param>
        public GetPerson(string _openId,string _eid)
        {
            openId = _openId;
            eid = _eid;
        }

        /// <summary>
        /// 获取个人信息
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _GetPerson Query(string _token)
        {
            return PostByUrlencoded<_GetPerson>(URL, _token);
        }
    }

    /// <summary>
    /// 获取个人信息|返回
    /// </summary>
    public class _GetPerson : BaseResult
    {
        /// <summary>
        /// 个人信息
        /// </summary>
        public List<OPerson> data { get; set; }
    }
}
