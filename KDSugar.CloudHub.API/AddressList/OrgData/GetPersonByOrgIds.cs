﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.OrgData
{
    /// <summary>
    /// 根据orgIds获取人员信息|发起
    /// </summary>
    public class GetPersonByOrgIds : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/opendata-control/data/org/getUserInfoRelyOrgIds?accessToken={0}";

        /// <summary>
        /// 企业EID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 部门ID
        /// <para>多个部门ID之间用逗号隔开</para>
        /// </summary>
        public string orgIds { get; set; }

        /// <summary>
        /// 是否包含下级部门人员信息
        /// </summary>
        public bool isIncludeSub { get; set; }

        /// <summary>
        /// 分页开始
        /// <para>默认0</para>
        /// </summary>
        public int? begin { get; set; }

        /// <summary>
        /// 每页数量
        /// <para>最大限制5000</para>
        /// </summary>
        public int? count { get; set; }

        /// <summary>
        /// 根据orgIds获取人员信息|初始化
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_orgIds">
        /// 部门ID
        /// <para>多个部门ID之间用逗号隔开</para>
        /// </param>
        /// <param name="_isIncSub">是否包含下级部门人员信息</param>
        public GetPersonByOrgIds(string _eid, string _orgIds, bool _isIncSub)
        {
            eid = _eid;
            orgIds = _orgIds;
            isIncludeSub = _isIncSub;
        }

        /// <summary>
        /// 根据orgIds获取人员信息|初始化
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_orgIds">部门ID</param>
        /// <param name="_isIncSub">是否包含下级部门人员信息</param>
        public GetPersonByOrgIds(string _eid,List<string> _orgIds,bool _isIncSub)
        {
            eid = _eid;
            isIncludeSub = _isIncSub;
            string tmpOrgIds = null;
            if (_orgIds != null)
            {
                tmpOrgIds = "";
                foreach(string oid in _orgIds)
                {
                    tmpOrgIds += string.Format("{0},", oid);
                }
                if (!string.IsNullOrWhiteSpace(tmpOrgIds))
                    tmpOrgIds = tmpOrgIds.Substring(0, tmpOrgIds.Length - 1);
            }
            orgIds = tmpOrgIds;
        }

        /// <summary>
        /// 根据orgIds获取人员信息
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _GetPersonByOrgIds Query(string _token)
        {
            return PostByUrlencoded<_GetPersonByOrgIds>(URL, _token);
        }
    }

    /// <summary>
    /// 根据orgIds获取人员信息|返回
    /// </summary>
    public class _GetPersonByOrgIds : BaseResult
    {
        /// <summary>
        /// 人员列表
        /// </summary>
        public List<OPerson> data { get; set; }
    }
}
