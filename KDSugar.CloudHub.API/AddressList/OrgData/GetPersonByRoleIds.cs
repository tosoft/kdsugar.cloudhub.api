﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.OrgData
{
    /// <summary>
    /// 按角色id获取人员信息|发起
    /// </summary>
    public class GetPersonByRoleIds : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/opendata-control/data/getpersonByRoleIds?accessToken={0}";

        /// <summary>
        /// 企业EID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 角色ID
        /// <para>多个角色ID之间用下划线"_"连接</para>
        /// </summary>
        public string roleIds { get; set; }

        /// <summary>
        /// 分页开始
        /// <para>默认0</para>
        /// </summary>
        public int? begin { get; set; }

        /// <summary>
        /// 每页数量
        /// <para>默认1000</para>
        /// </summary>
        public int? count { get; set; }

        /// <summary>
        /// 按角色id获取人员信息|初始化
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_roleIds">角色ID</param>
        public GetPersonByRoleIds(string _eid,string _roleIds)
        {
            eid = _eid;
            roleIds = _roleIds;
        }

        /// <summary>
        /// 按角色id获取人员信息|初始化
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_roleIds">角色ID</param>
        public GetPersonByRoleIds(string _eid, List<string> _roleIds)
        {
            eid = _eid;
            string tmpRoleIds = null;
            if (_roleIds != null)
            {
                tmpRoleIds = "";
                foreach(string role in _roleIds)
                {
                    tmpRoleIds += string.Format("{0}_", role);
                }
                if (!string.IsNullOrWhiteSpace(tmpRoleIds))
                    tmpRoleIds = tmpRoleIds.Substring(0, tmpRoleIds.Length - 1);
            }
            roleIds = tmpRoleIds;
        }

        /// <summary>
        /// 按角色id获取人员信息
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _GetPersonByRoleIds Query(string _token)
        {
            return PostByUrlencoded<_GetPersonByRoleIds>(URL, _token);
        }
    }

    /// <summary>
    /// 按角色id获取人员信息|返回
    /// </summary>
    public class _GetPersonByRoleIds : BaseResult
    {
        /// <summary>
        /// 角色列表
        /// </summary>
        public List<RoleInfo> data { get; set; }

        /// <summary>
        /// 角色信息类
        /// </summary>
        public class RoleInfo
        {
            /// <summary>
            /// 企业EID
            /// </summary>
            public string eid { get; set; }

            /// <summary>
            /// 角色ID
            /// </summary>
            public string roleId { get; set; }

            /// <summary>
            /// 角色名称
            /// </summary>
            public string roleName { get; set; }

            /// <summary>
            /// 成员列表
            /// </summary>
            public List<MemberInfo> members { get; set; }

            /// <summary>
            /// 成员信息类
            /// </summary>
            public class MemberInfo
            {
                /// <summary>
                /// UID
                /// </summary>
                public string uid { get; set; }

                /// <summary>
                /// 头像URL
                /// </summary>
                public string photoUrl { get; set; }

                /// <summary>
                /// 用户OID
                /// </summary>
                public string openId { get; set; }

                /// <summary>
                /// 用户姓名
                /// </summary>
                public string name { get; set; }
            }
        }
    }
}
