﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.OrgData
{
    /// <summary>
    /// 根据手机号码和工作圈名称查询工作圈信息|发起
    /// </summary>
    public class SearchNetwork : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/opendata-control/partner/searchNetwork?accessToken={0}";

        /// <summary>
        /// 企业EID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string phone { get; set; }

        /// <summary>
        /// 团队名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 根据手机号码和工作圈名称查询工作圈信息|初始化
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_phone">手机号</param>
        /// <param name="_name">团队名称</param>
        public SearchNetwork(string _eid, string _phone, string _name)
        {
            eid = _eid;
            phone = _phone;
            name = _name;
        }

        /// <summary>
        /// 根据手机号码和工作圈名称查询工作圈信息
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _SearchNetwork Query(string _token)
        {
            return PostByUrlencoded<_SearchNetwork>(URL, _token);
        }
    }

    /// <summary>
    /// 根据手机号码和工作圈名称查询工作圈信息|返回
    /// </summary>
    public class _SearchNetwork : BaseResult
    {
        /// <summary>
        /// 工作圈数据
        /// </summary>
        public NetWorkData data { get; set; }

        /// <summary>
        /// 工作圈数据类
        /// </summary>
        public class NetWorkData
        {
            /// <summary>
            /// 工作圈列表
            /// </summary>
            public List<NetWorkInfo> newtorks { get; set; }

            /// <summary>
            /// 工作圈信息类
            /// </summary>
            public class NetWorkInfo
            {
                /// <summary>
                /// 企业EID
                /// </summary>
                public string eid { get; set; }

                /// <summary>
                /// 工作圈networkId
                /// </summary>
                public string networkId { get; set; }

                /// <summary>
                /// 工作圈名称
                /// </summary>
                public string name { get; set; }
            }
        }
    }
}
