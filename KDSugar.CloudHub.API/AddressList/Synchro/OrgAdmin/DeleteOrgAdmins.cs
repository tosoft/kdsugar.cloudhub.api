﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.OrgAdmin
{
    /// <summary>
    /// 批量删除部门负责人|发起
    /// </summary>
    public class DeleteOrgAdmins : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/company/deleteOrgAdmins?accessToken={0}";

        /// <summary>
        /// 是否删除所有负责人
        /// </summary>
        public bool? deleteAll { get; set; }

        /// <summary>
        /// 批量删除信息列表
        /// </summary>
        public List<DeleteOrgAdminInfo> list { get; set; }

        /// <summary>
        /// 批量删除信息
        /// </summary>
        public class DeleteOrgAdminInfo
        {
            /// <summary>
            /// 部门长名称，格式："一级部门\\二级部门\\三级部门"，如 ： "研发中心\\移动平台产品部\\开发部"
            /// </summary>
            public string department { get; set; }

            /// <summary>
            /// 用户openId
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 唯一标识一条数据，建议使用自增序列（返回时，可用来判断一条记录是否成功）
            /// </summary>
            public string commitId { get; set; }
        }

        /// <summary>
        /// 删除所有的部门负责人
        /// </summary>
        public DeleteOrgAdmins()
        {
            deleteAll = true;
        }

        /// <summary>
        /// 根据删除信息删除部门负责人
        /// </summary>
        /// <param name="_list">删除信息</param>
        public DeleteOrgAdmins(List<DeleteOrgAdminInfo> _list)
        {
            deleteAll = false;
            list = _list;
        }

        /// <summary>
        /// 批量删除信息
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public DeleteOrgAdminsError Operate(string _eid,string _token,string _nonce)
        {
            return PostData<DeleteOrgAdminsError>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 根据组织id批量删除部门负责人|发起
    /// </summary>
    public class DeleteOrgAdminsById : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/company/deleteOrgAdminsById?accessToken={0}";

        /// <summary>
        /// 删除信息列表(指定人员删除时，最多允许一次删除1000条记录)
        /// </summary>
        public List<DeleteOrgAdminByIdInfo> list { get; set; }

        /// <summary>
        /// 删除信息
        /// </summary>
        public class DeleteOrgAdminByIdInfo
        {
            /// <summary>
            /// 部门id
            /// </summary>
            public string orgId { get; set; }

            /// <summary>
            /// 人员openId
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 唯一标识一条数据，建议使用自增序列（返回时，可用来判断一条记录是否成功）
            /// </summary>
            public string commitId { get; set; }
        }

        /// <summary>
        /// 根据组织id批量删除部门负责人|初始化
        /// </summary>
        /// <param name="_list">删除信息</param>
        public DeleteOrgAdminsById(List<DeleteOrgAdminByIdInfo> _list)
        {
            list = _list;
        }

        /// <summary>
        /// 根据组织id批量删除部门负责人
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public DeleteOrgAdminsError Operate(string _eid,string _token,string _nonce)
        {
            return PostData<DeleteOrgAdminsError>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 删除组织负责人|发起
    /// </summary>
    public class DeleteOrgAdminsByParent : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/relationship/deleteOrgAdmins?accessToken={0}";

        /// <summary>
        /// 是否删除所有
        /// </summary>
        public bool? deleteAll { get; set; }

        /// <summary>
        /// 删除信息列表(指定人员删除时，最多允许一次删除1000条记录)
        /// </summary>
        public List<DeleteOrgAdminByParentInfo> list { get; set; }

        /// <summary>
        /// 删除信息
        /// </summary>
        public class DeleteOrgAdminByParentInfo
        {
            /// <summary>
            /// 部门id
            /// </summary>
            public string orgId { get; set; }

            /// <summary>
            /// 人员openId
            /// </summary>
            public string openId { get; set; }
        }

        /// <summary>
        /// 删除所有组织负责人
        /// </summary>
        public DeleteOrgAdminsByParent()
        {
            deleteAll = true;
        }

        /// <summary>
        /// 根据删除信息删除组织负责人
        /// </summary>
        /// <param name="_list">删除信息</param>
        public DeleteOrgAdminsByParent(List<DeleteOrgAdminByParentInfo> _list)
        {
            deleteAll = false;
            list = _list;
        }

        /// <summary>
        /// 删除组织负责人
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public BaseResult Operate(string _eid,string _token,string _nonce)
        {
            return PostData<BaseResult>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 删除负责人信息报错
    /// </summary>
    public class DeleteOrgAdminsError : BaseResult
    {
        /// <summary>
        /// 错误信息列表
        /// </summary>
        public List<DeleteOrgAdminMsg> data { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        public class DeleteOrgAdminMsg
        {
            /// <summary>
            /// 错误码
            /// </summary>
            public int? errorCode { get; set; }

            /// <summary>
            /// 提交id
            /// </summary>
            public string commitId { get; set; }

            /// <summary>
            /// 错误信息
            /// </summary>
            public string error { get; set; }
        }
    }
}
