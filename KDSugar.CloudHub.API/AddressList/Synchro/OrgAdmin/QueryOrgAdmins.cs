﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.OrgAdmin
{
    /// <summary>
    /// 查询所有部门负责人|发起
    /// </summary>
    public class QueryOrgAdmins : RequestDataBaseByPage
    {
        private const string URL = "/gateway/openimport/open/company/queryOrgAdmins?accessToken={0}";

        /// <summary>
        /// 查询所有部门负责人
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _QueryOrgAdmins GetOrgAdmins(string _eid,string _token,string _nonce)
        {
            return PostData<_QueryOrgAdmins>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 查询所有部门负责人|返回
    /// </summary>
    public class _QueryOrgAdmins : BaseResult
    {
        /// <summary>
        /// 部门负责人信息列表
        /// </summary>
        public List<QueryOrgAdminInfo> data { get; set; }

        /// <summary>
        /// 部门负责人信息
        /// </summary>
        public class QueryOrgAdminInfo
        {
            /// <summary>
            /// 用户openId
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 部门ID
            /// </summary>
            public string departmentId { get; set; }

            /// <summary>
            /// 部门名称
            /// </summary>
            public string department { get; set; }
        }
    }
}
