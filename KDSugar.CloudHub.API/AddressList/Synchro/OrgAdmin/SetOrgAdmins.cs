﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.AddressList.Synchro.OrgAdmin
{
    /// <summary>
    /// 批量设置部门负责人|发起
    /// </summary>
    public class SetOrgAdmins : RequestBase
    {
        private const string URL = "/gateway/openimport/open/company/setOrgAdmins?accessToken={0}";

        /// <summary>
        /// 设置信息
        /// </summary>
        public class SetOrgAdminInfo
        {
            /// <summary>
            /// 部门长名称，格式："一级部门\\二级部门\\三级部门"，如 ： "研发中心\\移动平台产品部\\开发部"
            /// </summary>
            public string department { get; set; }

            /// <summary>
            /// 人员openId
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 可选，部门负责人排序权重，权重越小，排序越前.若不填则以人员权重排序
            /// </summary>
            public int? weights { get; set; }

            /// <summary>
            /// 唯一标识一条设置数据，建议使用自增序列（返回时，可用来判断一条记录是否成功）
            /// </summary>
            public string commitId { get; set; }
        }

        /// <summary>
        /// 批量设置部门负责人|初始化
        /// </summary>
        /// <param name="infos">设置信息</param>
        /// <param name="_eid">企业EID</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        public SetOrgAdmins(List<SetOrgAdminInfo> infos, string _eid, string _nonce = null)
            : base(_eid, _nonce)
        {
            data = JsonConvert.SerializeObject(infos);
        }

        /// <summary>
        /// 批量设置部门负责人
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public SetOrgAdminsError Operate(string _token)
        {
            return GetDataPack<SetOrgAdminsError>(Constant.ROOTURL + URL, _token);
        }
    }

    /// <summary>
    /// 根据组织id批量设置部门负责人|发起
    /// </summary>
    public class SetOrgAdminsById : RequestBase
    {
        private const string URL = "/gateway/openimport/open/company/setOrgAdminsById?accessToken={0}";

        /// <summary>
        /// 设置信息
        /// </summary>
        public class SetOrgAdminByIdInfo
        {
            /// <summary>
            /// 部门id
            /// </summary>
            public string orgId { get; set; }

            /// <summary>
            /// 用户openId
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 可选，排序权重，权重越小，排序越前
            /// </summary>
            public int? weights { get; set; }

            /// <summary>
            /// 唯一标识一条设置数据，建议使用自增序列（返回时，可用来判断一条记录是否成功）
            /// </summary>
            public string commitId { get; set; }
        }

        /// <summary>
        /// 根据组织id批量设置部门负责人|初始化
        /// </summary>
        /// <param name="infos">设置信息</param>
        /// <param name="_eid">企业EID</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        public SetOrgAdminsById(List<SetOrgAdminByIdInfo> infos,string _eid,string _nonce=null)
            : base(_eid, _nonce)
        {
            data = JsonConvert.SerializeObject(infos);
        }

        /// <summary>
        /// 根据组织id批量设置部门负责人
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public SetOrgAdminsError Operate(string _token)
        {
            return GetDataPack<SetOrgAdminsError>(Constant.ROOTURL + URL, _token);
        }
    }

    /// <summary>
    /// 设置错误信息
    /// </summary>
    public class SetOrgAdminsError : BaseResult
    {
        /// <summary>
        /// 错误信息列表
        /// </summary>
        public List<SetOrgAdminErrorMsg> data { get; set; }
        
        /// <summary>
        /// 错误信息
        /// </summary>
        public class SetOrgAdminErrorMsg
        {
            /// <summary>
            /// 提交ID
            /// </summary>
            public string commitId { get; set; }

            /// <summary>
            /// 错误信息
            /// </summary>
            public string errorMsg { get; set; }
        }
    }
}
