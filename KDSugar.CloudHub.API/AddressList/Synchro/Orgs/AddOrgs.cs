﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Orgs
{
    /// <summary>
    /// 新增组织|发起
    /// </summary>
    public class AddOrgs : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/dept/add?accessToken={0}";

        /// <summary>
        /// 必填，组织长名称数组
        /// <para>单个组织长名称格式："一级部门\\二级部门\\三级部门"，如 ： "研发中心\\移动平台产品部\\开发部"</para>
        /// </summary>
        public List<string> departments { get; set; } = new List<string>();

        /// <summary>
        /// 必填，保证weights与departments长度一致
        /// <para>如果不传根部门，只传了子部门，根部门的排序码会依据子部门的排序码来生成 ，子部门排序码越小，生成的根部门排序码就也越小。</para>
        /// </summary>
        public List<string> weights { get; set; } = new List<string>();

        /// <summary>
        /// 新增组织
        /// </summary>
        /// <param name="_dept"></param>
        /// <param name="_weight"></param>
        public void AddOrgInfo(string _dept,string _weight)
        {
            //if (departments == null)
            //    departments = new List<string>();
            //if (weights == null)
            //    weights = new List<string>();
            departments.Add(_dept);
            weights.Add(_weight);
        }

        /// <summary>
        /// 新增组织操作
        /// </summary>
        /// <param name="_eid">团队EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _AddOrgs AddOrgsOperate(string _eid, string _token, string _nonce = null)
        {
            return PostData<_AddOrgs>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 新增组织|返回
    /// </summary>
    public class _AddOrgs : BaseResult
    {
        /// <summary>
        /// 错误返回信息
        /// </summary>
        public List<MsgInfo> data { get; set; }
    }
}
