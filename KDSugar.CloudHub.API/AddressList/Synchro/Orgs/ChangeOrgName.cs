﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Orgs
{
    /// <summary>
    /// 更新组织名称|发起
    /// </summary>
    public class ChangeOrgName : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/dept/update?accessToken={0}";

        /// <summary>
        /// 更新组织名称信息
        /// </summary>
        public class ChangeOrgNameInfo
        {
            /// <summary>
            /// 原组织长名称
            /// </summary>
            public string department { get; set; }

            /// <summary>
            /// 新组织长名称
            /// </summary>
            public string todepartment { get; set; }

            /// <summary>
            /// 更新组织名称信息构造
            /// </summary>
            /// <param name="_oldOrgName">旧组织长名称</param>
            /// <param name="_newOrgName">新组织长名称</param>
            public ChangeOrgNameInfo(string _oldOrgName,string _newOrgName)
            {
                department = _oldOrgName;
                todepartment = _newOrgName;
            }
        }

        /// <summary>
        /// 更新列表
        /// </summary>
        public List<ChangeOrgNameInfo> departments { get; set; } = new List<ChangeOrgNameInfo>();

        /// <summary>
        /// 新增更新组织信息
        /// </summary>
        /// <param name="_info"></param>
        public void AddChangeOrgNameInfo(ChangeOrgNameInfo _info)
        {
            departments.Add(_info);
        }

        /// <summary>
        /// 更新组织名称信息
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _ChangeOrgName ChangeOrgNamesOperate(string _eid,string _token,string _nonce = null)
        {
            return PostData<_ChangeOrgName>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 根据orgId更新组织名称|发起
    /// </summary>
    public class ChangeOrgNameByOrgId : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/dept/updateById?accessToken={0}";

        /// <summary>
        /// 更新信息类
        /// </summary>
        public class ChangeInfo
        {
            /// <summary>
            /// 原组织id
            /// </summary>
            public string orgId { get; set; }

            /// <summary>
            /// 新组织名称,不是长名称
            /// </summary>
            public string todepartment { get; set; }

            /// <summary>
            /// 初始化更新信息
            /// </summary>
            /// <param name="_orgId">原组织id</param>
            /// <param name="_todepartment">新组织名称,不是长名称</param>
            public ChangeInfo(string _orgId,string _todepartment)
            {
                orgId = _orgId;
                todepartment = _todepartment;
            }
        }

        /// <summary>
        /// 更新信息列表
        /// </summary>
        public List<ChangeInfo> departments { get; set; } = new List<ChangeInfo>();

        /// <summary>
        /// 新增更新信息
        /// </summary>
        /// <param name="_info"></param>
        public void AddChangeInfo(ChangeInfo _info)
        {
            departments.Add(_info);
        }

        /// <summary>
        /// 更新组织名称信息
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _ChangeOrgName ChangeOrgNamesOperate(string _eid,string _token,string _nonce = null)
        {
            return PostData<_ChangeOrgName>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 更新组织名称|返回
    /// </summary>
    public class _ChangeOrgName : BaseResult
    {
        /// <summary>
        /// 错误返回信息
        /// </summary>
        public List<MsgInfo> data { get; set; }
    }
}
