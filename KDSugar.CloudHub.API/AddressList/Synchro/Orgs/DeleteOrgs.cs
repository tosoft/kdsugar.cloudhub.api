﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Orgs
{
    /// <summary>
    /// 删除组织|发起
    /// </summary>
    public class DeleteOrgs : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/dept/delete?accessToken={0}";

        /// <summary>
        /// 必填,要删除的组织长名称数组
        /// </summary>
        public List<string> departments { get; set; } = new List<string>();

        /// <summary>
        /// 新增删除组织
        /// </summary>
        /// <param name="_org">组织长名称</param>
        public void AddDeleteOrg(string _org)
        {
            departments.Add(_org);
        }

        /// <summary>
        /// 删除组织操作
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _DeleteOrgs DeleteOrgsOperate(string _eid, string _token, string _nonce = null)
        {
            return PostData<_DeleteOrgs>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 根据orgId删除组织|发起
    /// </summary>
    public class DeleteOrgsByOrgId : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/dept/deleteById?accessToken={0}";

        /// <summary>
        /// 要删除的组织id数组
        /// </summary>
        public List<string> departments { get; set; } = new List<string>();

        /// <summary>
        /// 新增组织Id
        /// </summary>
        /// <param name="_id"></param>
        public void AddOrgId(string _id)
        {
            departments.Add(_id);
        }

        /// <summary>
        /// 删除组织操作
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _DeleteOrgs DeleteOrgsOperate(string _eid,string _token,string _nonce = null)
        {
            return PostData<_DeleteOrgs>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 删除组织|返回
    /// </summary>
    public class _DeleteOrgs : BaseResult
    {
        /// <summary>
        /// 错误返回信息
        /// </summary>
        public List<MsgInfo> data { get; set; }
    }
}
