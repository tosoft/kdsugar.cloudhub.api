﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Orgs
{
    /// <summary>
    /// 跨层次部门挪动|发起
    /// </summary>
    public class MoveOrg : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/dept/moveOrg?accessToken={0}";

        /// <summary>
        /// 待挪动部门ID
        /// </summary>
        public string orgId { get; set; }

        /// <summary>
        /// 挪动到的部门ID
        /// </summary>
        public string moveToOrgId { get; set; }

        /// <summary>
        /// 跨层次部门挪动|初始化
        /// </summary>
        /// <param name="_orgId">待挪动部门ID</param>
        /// <param name="_moveToOrgId">挪动到的部门ID</param>
        public MoveOrg(string _orgId,string _moveToOrgId)
        {
            orgId = _orgId;
            moveToOrgId = _moveToOrgId;
        }

        /// <summary>
        /// 挪动
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _MoveOrg Operate(string _eid,string _token,string _nonce = null)
        {
            return PostData<_MoveOrg>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 跨层次部门挪动|返回
    /// </summary>
    public class _MoveOrg : BaseResult
    {
        /// <summary>
        /// 返回信息
        /// </summary>
        public string data { get; set; }
    }
}
