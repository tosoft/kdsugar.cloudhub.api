﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Orgs
{
    /// <summary>
    /// 组织同步返回信息类
    /// </summary>
    public class MsgInfo
    {
        /// <summary>
        /// 消息id
        /// </summary>
        public string msgId { get; set; }

        /// <summary>
        /// 消息码
        /// </summary>
        public int msgCode { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        public string msg { get; set; }
    }
}
