﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Orgs
{
    /// <summary>
    /// 组织信息
    /// </summary>
    public class OrgInfo
    {
        /// <summary>
        /// 组织名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 组织的id
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 排序码
        /// </summary>
        public int weights { get; set; }

        /// <summary>
        /// 组织长名称
        /// </summary>
        public string department { get; set; }

        /// <summary>
        /// 组织父Id
        /// </summary>
        public string parentId { get; set; }

        /// <summary>
        /// 1：新增 2：更新 3：删除
        /// </summary>
        public string changeType { get; set; }

        public enum ChangeTypeEnum
        {
            /// <summary>
            /// 新增
            /// </summary>
            NewAdd,
            /// <summary>
            /// 更新
            /// </summary>
            Update,
            /// <summary>
            /// 删除
            /// </summary>
            Delete
        }

        [JsonIgnore]
        public ChangeTypeEnum? ChangeType
        {
            get
            {
                ChangeTypeEnum? result = null;
                int _type;
                if (!string.IsNullOrWhiteSpace(changeType) &&
                    int.TryParse(changeType, out _type))
                {
                    switch (_type)
                    {
                        case 1:
                            result = ChangeTypeEnum.NewAdd;
                            break;
                        case 2:
                            result = ChangeTypeEnum.Update;
                            break;
                        case 3:
                            result = ChangeTypeEnum.Delete;
                            break;
                    }
                }
                return result;
            }
        }
    }
}
