﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Orgs
{
    /// <summary>
    /// 根据orgId或department查询组织信息|发起
    /// </summary>
    public class QueryOrgsById : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/dept/get?accessToken={0}";

        /// <summary>
        /// 查询Id
        /// </summary>
        public List<string> array { get; set; } = new List<string>();

        /// <summary>
        /// 查询类型
        /// </summary>
        public int type
        {
            get { return (int)idType; }
        }

        /// <summary>
        /// 查询类型枚举
        /// </summary>
        public enum IdTypeEnum
        {
            /// <summary>
            /// 根据orgId查询
            /// </summary>
            OrgId,
            /// <summary>
            /// 根据department查询
            /// </summary>
            Department
        }

        private IdTypeEnum idType { get; set; }

        /// <summary>
        /// 根据orgId或department查询组织信息|初始化
        /// </summary>
        /// <param name="_type">查询类型</param>
        public QueryOrgsById(IdTypeEnum _type)
        {
            idType = _type;
        }

        /// <summary>
        /// 新增查询Id
        /// </summary>
        /// <param name="_id">查询Id</param>
        public void AddId(string _id)
        {
            array.Add(_id);
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _QueryOrgs Query(string _eid,string _token,string _nonce = null)
        {
            return PostData<_QueryOrgs>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 查询全部组织信息|发起
    /// </summary>
    public class QueryAllOrgs : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/dept/getall?accessToken={0}";

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _QueryOrgs Query(string _eid,string _token,string _nonce = null)
        {
            return PostData<_QueryOrgs>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 查询更新部门信息|发起
    /// </summary>
    public class QueryOrgsByUpdate : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/dept/getAtTime?accessToken={0}";

        /// <summary>
        /// 必填,查询时点，格式：“2014-08-02 01:40:38”
        /// </summary>
        public string time { get; set; }

        /// <summary>
        /// 查询更新部门信息|初始化
        /// </summary>
        /// <param name="_dt"></param>
        public QueryOrgsByUpdate(DateTime _dt)
        {
            time = _dt.ToString("yyyy-MM-dd HH:mm:ss");
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _QueryOrgs Query(string _eid,string _token,string _nonce = null)
        {
            return PostData<_QueryOrgs>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 组织查询结果
    /// </summary>
    public class _QueryOrgs : BaseResult
    {
        /// <summary>
        /// 组织列表
        /// </summary>
        public List<OrgInfo> data { get; set; }
    }
}
