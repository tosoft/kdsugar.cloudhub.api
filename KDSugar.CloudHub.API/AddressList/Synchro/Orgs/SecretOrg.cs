﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Orgs
{
    /// <summary>
    /// 添加/删除机密组织可见部门|发起
    /// </summary>
    public class AddDeleteSecretOrgWhitelist : RequestDataBase
    {
        private const string ADD_URL = "/gateway/openimport/open/dept/addOrgSecretDeptWhitelist?accessToken={0}";

        private const string DEL_URL = "/gateway/openimport/open/dept/deleteOrgSecretDeptWhitelist?accessToken={0}";

        /// <summary>
        /// 机密组织部门id
        /// </summary>
        public string orgId { get; set; }

        /// <summary>
        /// 机密组织部门对应类型
        /// </summary>
        public int secretType
        {
            get
            {
                return (int)_secretType;
            }
        }

        /// <summary>
        /// 设置列表
        /// </summary>
        public List<string> whiteOrgIds { get; set; } = new List<string>();

        /// <summary>
        /// 添加机密组织可见部门
        /// </summary>
        /// <param name="_orgId">机密组织部门id</param>
        public void AddWhiteOrgId(string _orgId)
        {
            whiteOrgIds.Add(_orgId);
        }

        /// <summary>
        /// 机密组织部门对应类型枚举
        /// </summary>
        public enum SecretTypeEnum
        {
            /// <summary>
            /// 隐藏部门
            /// </summary>
            HideDept = 1,
            /// <summary>
            /// 限制该部门人员不可查看其他部门
            /// </summary>
            DontViewOtherDept = 2
        }

        private SecretTypeEnum _secretType { get; set; }

        /// <summary>
        /// 操作类型枚举
        /// </summary>
        public enum OperateTypeEnum
        {
            /// <summary>
            /// 新增
            /// </summary>
            Add,
            /// <summary>
            /// 删除
            /// </summary>
            Delete
        }

        private OperateTypeEnum operateType { get; set; }

        /// <summary>
        /// 添加机密组织可见部门|初始化
        /// </summary>
        /// <param name="_orgId">机密组织部门id</param>
        /// <param name="_type">机密组织部门对应类型</param>
        /// <param name="_operateType"></param>
        public AddDeleteSecretOrgWhitelist(string _orgId,SecretTypeEnum _type,OperateTypeEnum _operateType)
        {
            orgId = _orgId;
            _secretType = _type;
            operateType = _operateType;
        }

        /// <summary>
        /// 操作
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _SecretOrg Operate(string _eid,string _token,string _nonce = null)
        {
            string tmpUrl = null;
            switch (operateType)
            {
                case OperateTypeEnum.Add:
                    tmpUrl = ADD_URL;
                    break;
                case OperateTypeEnum.Delete:
                    tmpUrl = DEL_URL;
                    break;
            }
            return PostData<_SecretOrg>(_eid, tmpUrl, _token, _nonce);
        }
    }

    /// <summary>
    /// 机密组织返回
    /// </summary>
    public class _SecretOrg : BaseResult
    {
        /// <summary>
        /// 返回信息
        /// </summary>
        public string data { get; set; }
    }
}
