﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Orgs
{
    /// <summary>
    /// 更新组织排序码|发起
    /// </summary>
    public class UpdateOrgWeight : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/dept/updateWeightsById?accessToken={0}";

        /// <summary>
        /// 排序信息
        /// </summary>
        public class WeightInfo
        {
            /// <summary>
            /// 部门ID
            /// </summary>
            public string orgId { get; set; }

            /// <summary>
            /// 排序码
            /// </summary>
            public string weights { get; set; }

            /// <summary>
            /// 排序信息初始化
            /// </summary>
            /// <param name="_orgId">部门ID</param>
            /// <param name="_weights">排序码</param>
            public WeightInfo(string _orgId,int _weights)
            {
                orgId = _orgId;
                weights = _weights.ToString();
            }
        }

        /// <summary>
        /// 更新列表
        /// </summary>
        public List<WeightInfo> departments { get; set; } = new List<WeightInfo>();

        /// <summary>
        /// 新增排序信息
        /// </summary>
        /// <param name="_info">排序信息</param>
        public void AddWeightInfo(WeightInfo _info)
        {
            departments.Add(_info);
        }

        /// <summary>
        /// 更新组织排序码
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _UpdateOrgWeight Operate(string _eid,string _token,string _nonce = null)
        {
            return PostData<_UpdateOrgWeight>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 更新组织排序码|返回
    /// </summary>
    public class _UpdateOrgWeight : BaseResult
    {
        /// <summary>
        /// 错误返回信息
        /// </summary>
        public List<MsgInfo> data { get; set; }
    }
}
