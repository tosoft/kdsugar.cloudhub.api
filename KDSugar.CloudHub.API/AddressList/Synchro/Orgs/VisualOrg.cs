﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Orgs
{
    /// <summary>
    /// 设置隐藏部门或部门仅可见|发起
    /// </summary>
    public class SetVisualOrg : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/company/setOrgSecret?accessToken={0}";

        /// <summary>
        /// 设置信息类
        /// </summary>
        public class SetVisualOrgInfo
        {
            /// <summary>
            /// 唯一标识一次提交
            /// <para>可自行选取，保证不重复即可</para>
            /// </summary>
            public string commitId { get; set; }

            /// <summary>
            /// 部门ID
            /// </summary>
            public string orgId { get; set; }

            /// <summary>
            /// 类型
            /// </summary>
            public string type
            {
                get
                {
                    string result = null;
                    switch (visualType)
                    {
                        case VisualTypeEnum.Hide:
                            result = "HIDE";
                            break;
                        case VisualTypeEnum.Visual:
                            result = "VISI";
                            break;
                    }
                    return result;
                }
            }

            /// <summary>
            /// 状态
            /// <para>true:开启；false：关闭</para>
            /// </summary>
            public bool status { get; set; }

            /// <summary>
            /// 类型枚举
            /// </summary>
            public enum VisualTypeEnum
            {
                /// <summary>
                /// 隐藏部门
                /// </summary>
                Hide,
                /// <summary>
                /// 部门仅可见
                /// </summary>
                Visual
            }

            private VisualTypeEnum visualType { get; set; }

            /// <summary>
            /// 初始化设置信息类
            /// </summary>
            /// <param name="_commitId">提交id</param>
            /// <param name="_orgId">部门id</param>
            /// <param name="_type">类型</param>
            /// <param name="_status">
            /// 状态
            /// <para>true:开启；false：关闭</para>
            /// </param>
            public SetVisualOrgInfo(string _commitId,string _orgId,VisualTypeEnum _type,bool _status)
            {
                commitId = _commitId;
                orgId = _orgId;
                visualType = _type;
                status = _status;
            }
        }

        private List<SetVisualOrgInfo> list = new List<SetVisualOrgInfo>();

        /// <summary>
        /// 新增设置信息
        /// </summary>
        /// <param name="_info">设置信息</param>
        public void AddSetVisualOrgInfo(SetVisualOrgInfo _info)
        {
            list.Add(_info);
        }

        /// <summary>
        /// 操作
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _SetVisualOrg Operate(string _eid,string _token,string _nonce = null)
        {
            return PostData<_SetVisualOrg>(_eid, URL, _token, list, _nonce);
        }
    }

    /// <summary>
    /// 设置隐藏部门或部门仅可见|返回
    /// </summary>
    public class _SetVisualOrg : BaseResult
    {
        /// <summary>
        /// 错误信息类
        /// </summary>
        public class ErrorInfo
        {
            /// <summary>
            /// 提交id
            /// </summary>
            public string commitId { get; set; }

            /// <summary>
            /// 错误信息
            /// </summary>
            public string errorMsg { get; set; }
        }

        /// <summary>
        /// 错误信息列表
        /// </summary>
        public List<ErrorInfo> data { get; set; }
    }

    /// <summary>
    /// 查询设置隐藏部门或者部门仅可见部门|发起
    /// </summary>
    public class QueryVisualOrg : RequestDataBaseByPageByNotEID
    {
        private const string URL = "/gateway/openimport/open/company/queryOrgSecret?accessToken={0}";

        /// <summary>
        /// 查询类型枚举
        /// </summary>
        public enum QueryTypeEnum
        {
            /// <summary>
            /// 隐藏部门
            /// </summary>
            Hide,
            /// <summary>
            /// 部门仅可见
            /// </summary>
            Visual
        }

        private QueryTypeEnum queryType { get; set; }

        /// <summary>
        /// 查询类型
        /// </summary>
        public string type
        {
            get
            {
                string result = null;
                switch (queryType)
                {
                    case QueryTypeEnum.Hide:
                        result = "HIDE";
                        break;
                    case QueryTypeEnum.Visual:
                        result = "VISI";
                        break;
                }
                return result;
            }
        }

        /// <summary>
        /// 查询设置隐藏部门或者部门仅可见部门|初始化
        /// </summary>
        /// <param name="_type">查询类型</param>
        public QueryVisualOrg(QueryTypeEnum _type)
        {
            queryType = _type;
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _QueryVisualOrg Query(string _eid,string _token,string _nonce = null)
        {
            return PostData<_QueryVisualOrg>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 查询设置隐藏部门或者部门仅可见部门|返回
    /// </summary>
    public class _QueryVisualOrg : BaseResult
    {
        /// <summary>
        /// 隐藏部门信息
        /// </summary>
        public class QueryVisualInfo
        {
            /// <summary>
            /// id
            /// </summary>
            public string id { get; set; }

            /// <summary>
            /// 组织长名称
            /// </summary>
            public string department { get; set; }

            /// <summary>
            /// 组织id
            /// </summary>
            public string orgId { get; set; }
        }

        /// <summary>
        /// 隐藏部门信息列表
        /// </summary>
        public List<QueryVisualInfo> data { get; set; }
    }
}
