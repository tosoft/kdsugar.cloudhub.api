﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Person
{
    /// <summary>
    /// 新增人员|发起
    /// </summary>
    public class AddPerson : RequestDataBase
    {
        //public string eid { get; set; }

        /// <summary>
        /// 新增人员列表
        /// </summary>
        public List<AddPersonInfo> persons { get; set; } = new List<AddPersonInfo>();

        /// <summary>
        /// 新增人员|初始化
        /// </summary>
        /// <param name="_list">新增人员列表</param>
        public AddPerson(List<AddPersonInfo> _list)
        {
            persons = _list;
        }

        /// <summary>
        /// 旧·新增人员URL
        /// </summary>
        private const string URL = "/gateway/openimport/open/person/add?accessToken={0}";

        /// <summary>
        /// 新·新增人员URL
        /// </summary>
        private const string URL_NEW = "/gateway/openimport/open/person/addNew?accessToken={0}";

        /// <summary>
        /// 新增人员信息
        /// </summary>
        public class AddPersonInfo
        {
            /// <summary>
            /// 姓名
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// 头像URL,可访问的公网URL,默认为空
            /// </summary>
            public string photoUrl { get; set; }

            /// <summary>
            /// 账户信息
            /// </summary>
            public PersonAccount account { get; set; }

            /// <summary>
            /// 手机号码,工作圈内唯一
            /// <para>如果account字段中mobile和email都为空，那这个phone就必须有，否则创建账号不成功</para>
            /// <para>如果account中的mobile字段存在，则优先以mobile作为账号，phone字段忽略。</para>
            /// <para>建议phone和mobile只要其中一个传值即可</para>
            /// </summary>
            public string phone { get; set; }

            /// <summary>
            /// 是否在通讯录中隐藏手机号码,0: 不隐藏; 1: 隐藏,默认为0
            /// </summary>
            public string isHidePhone { get; set; }

            /// <summary>
            /// 部门长名称，格式："一级部门\\二级部门\\三级部门"，如 ： "研发中心\\移动平台产品部\\开发部"
            /// <para>挂在根部门下面，直接传： "\\"</para>
            /// </summary>
            public string department { get; set; }

            /// <summary>
            /// 职位,默认为空
            /// </summary>
            public string jobTitle { get; set; }

            /// <summary>
            /// 企业工号
            /// </summary>
            public string jobNo { get; set; }

            /// <summary>
            /// 性别,0: 不确定; 1: 男; 2: 女,默认为0
            /// </summary>
            public string gender { get; set; }

            /// <summary>
            /// 生日
            /// </summary>
            public string birthday { get; set; }

            /// <summary>
            /// 状态 0: 注销，1: 正常，2: 禁用,默认为1
            /// </summary>
            public string status { get; set; }

            /// <summary>
            /// 排序权重，权重越小，排序越前.
            /// </summary>
            public int weights { get; set; }

            /// <summary>
            /// 是否部门负责人,0表示普通用户，1表示部门负责人
            /// </summary>
            public int orgUserType { get; set; }

            /// <summary>
            /// 自定义的联系方式，可以存储公司自定义的一些个人信息，比如短号、工号等。
            /// <para>contact是公有信息，包括电话，邮箱以及其他三类，每类必须有name,type,value三个属性</para>
            /// <para>type为P支持在手机端直接呼叫打电话</para>
            /// <para>具体格式: [{“name”:”工号”，“type”:”O”, “value”:”GH001”},{“name”:”电话”，“type”:”P”, “value”:”13800000000”},{"name": "邮箱1","type": "E","value": "123aaa@abc.com"}...]</para>
            /// <para>其中name是自定义属性的名称，type是类型，type值只有P,E,O三种类型，分别表示手机号，邮箱和其他，value是对应的值</para>
            /// </summary>
            public string contact { get; set; }

            /// <summary>
            /// 入职日期，格式如："2018-01-01"
            /// </summary>
            public string hireDate { get; set; }

            /// <summary>
            /// 转正日期，格式如："2018-01-01"
            /// </summary>
            public string positiveDate { get; set; }

            /// <summary>
            /// 用户来源，如K3OA则对应"K3OA",EAS对应"EAS",K/3对应"K/3",KIS对应"KIS",微信对应"wechat"，S-HR对应"sHR"
            /// </summary>
            public string regSource { get; set; }
        }

        /// <summary>
        /// 账户信息类
        /// </summary>
        public class PersonAccount
        {
            /// <summary>
            /// 手机账号。如果account中的email为空，且下面phone字段也为空，那mobile字段不能为空，否则创建账号不成功。
            /// <para>如果有mobile字段，也有下面phone字段，则优先以mobile字段作为账号,phone忽略。建议下面phone和mobile只要其中一个传值即可</para>
            /// </summary>
            public string mobile { get; set; }

            /// <summary>
            /// 邮箱账号
            /// </summary>
            public string email { get; set; }

            /// <summary>
            /// 账号密码，如果传了该密码，且是首次导入，则该用户会自动激活，如果没传，则是未激活的账号，需要该用户自行激活
            /// </summary>
            public string password { get; set; }
        }

        /// <summary>
        /// 新增人员|操作
        /// </summary>
        /// <param name="_eid">团队EID</param>
        /// <param name="_token">Token</param>
        /// <param name="isNewUrl">是否使用新接口</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _AddPerson Operate(string _eid,string _token,bool isNewUrl,string _nonce=null)
        {
            if (persons != null)
            {
                for(int i = 0; i < persons.Count; i++)
                {
                    if (string.IsNullOrWhiteSpace(persons[i].phone) &&
                        (persons[i].account == null ||
                        (persons[i].account != null && (string.IsNullOrWhiteSpace(persons[i].account.mobile) ||
                        string.IsNullOrWhiteSpace(persons[i].account.email)))
                        ))
                        throw new Exception(string.Format("{0}|手机号码或账号信息为空，无法创建账号", i.ToString()));
                }
            }
            return PostData<_AddPerson>(_eid, isNewUrl ? URL_NEW : URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 新增人员|返回
    /// </summary>
    public class _AddPerson : BaseResult
    {
        /// <summary>
        /// 新增人员情况列表
        /// </summary>
        public List<AddPersonInfo> data { get; set; }

        /// <summary>
        /// 新增人员情况类
        /// </summary>
        public class AddPersonInfo
        {
            /// <summary>
            /// openId
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 如果创建成功，则与openId相同值，不成功，则是手机号
            /// </summary>
            public string msgId { get; set; }

            /// <summary>
            /// 消息码
            /// </summary>
            public string msgCode { get; set; }

            /// <summary>
            /// 消息
            /// </summary>
            public string msg { get; set; }
        }
    }
}
