﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Person
{
    /// <summary>
    /// 获取手机号变更历史记录|发起
    /// </summary>
    public class ChangePhoneRecords : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/person/getPersonChangePhone?accessToken={0}";

        /// <summary>
        /// 可选,页数
        /// </summary>
        public int begin { get; set; }

        /// <summary>
        /// 可选,每页条数
        /// </summary>
        public int count { get; set; }

        //public string eid { get; set; }

        /// <summary>
        /// 必选,时间戳，形如："2017-04-02 12:00:00"
        /// </summary>
        public string time { get; set; }

        /// <summary>
        /// 获取手机号变更历史记录|初始化
        /// </summary>
        /// <param name="_dateTime">时间戳</param>
        public ChangePhoneRecords(DateTime _dateTime)
        {
            time = _dateTime.ToString("yyyy-MM-dd HH:mm:ss");
        }

        /// <summary>
        /// 获取手机号变更历史记录
        /// </summary>
        /// <param name="_eid">团队EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _ChangePhoneRecords GetChangePhoneRecords(string _eid,string _token,string _nonce)
        {
            return PostData<_ChangePhoneRecords>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 获取手机号变更历史记录|返回
    /// </summary>
    public class _ChangePhoneRecords : BaseResult
    {
        /// <summary>
        /// 手机号变更历史记录列表
        /// </summary>
        public List<ChangePhoneRecord> data { get; set; }

        /// <summary>
        /// 手机号变更历史记录类
        /// </summary>
        public class ChangePhoneRecord
        {
            /// <summary>
            /// 时间
            /// </summary>
            public string createTime { get; set; }

            /// <summary>
            /// 更新之后手机号
            /// </summary>
            public string newPhone { get; set; }

            /// <summary>
            /// 工作圈id
            /// </summary>
            public string eid { get; set; }

            /// <summary>
            /// 更新之前的手机号
            /// </summary>
            public string oldPhone { get; set; }

            /// <summary>
            /// 用户openid
            /// </summary>
            public string openId { get; set; }
        }
    }
}
