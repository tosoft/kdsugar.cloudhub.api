﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Person
{
    /// <summary>
    /// 删除人员|发起
    /// </summary>
    public class DeletePerson : RequestDataBase
    {
        private const string URL = "gateway/openimport/open/person/delete?accessToken={0}";

        //public string eid { get; set; }

        /// <summary>
        /// 用户ID列表
        /// </summary>
        public List<string> openIds { get; set; }

        /// <summary>
        /// 删除人员|初始化
        /// </summary>
        /// <param name="_openIds">用户ID列表</param>
        public DeletePerson(List<string> _openIds)
        {
            openIds = _openIds;
        }

        /// <summary>
        /// 删除人员|操作
        /// </summary>
        /// <param name="_eid"> 必填，企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public MsgInfoList Operate(string _eid,string _token,string _nonce)
        {
            return PostData<MsgInfoList>(_eid, URL, _token, _nonce);
        }
    }

    //public class _DeletePerson : BaseResult
    //{
    //    public List<MsgInfo> data { get; set; }

    //    public class MsgInfo
    //    {
    //        public string msgId { get; set; }

    //        public int msgCode { get; set; }

    //        public string msg { get; set; }
    //    }
    //}
}
