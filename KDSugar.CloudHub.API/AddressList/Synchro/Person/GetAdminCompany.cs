﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Person
{
    /// <summary>
    /// 根据登陆账号和密码获取其是管理员的工作圈列表|发起
    /// </summary>
    public class GetAdminCompany : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/openaccess/input/person/getAdminCompany";

        /// <summary>
        /// 账号，手机号或者是电子邮箱
        /// </summary>
        public string userName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string password { get; set; }

        /// <summary>
        /// 根据登陆账号和密码获取其是管理员的工作圈列表|初始化
        /// </summary>
        /// <param name="_user">账号，手机号或者是电子邮箱</param>
        /// <param name="_pass">密码</param>
        public GetAdminCompany(string _user,string _pass)
        {
            userName = _user;
            password = _pass;
        }

        /// <summary>
        /// 获取管理员的工作圈列表
        /// </summary>
        /// <returns></returns>
        public _GetAdminCompany GetAdminCompanyList()
        {
            return Post<_GetAdminCompany>(URL);
        }
    }

    /// <summary>
    /// 根据登陆账号和密码获取其是管理员的工作圈列表|返回
    /// </summary>
    public class _GetAdminCompany : BaseResult
    {
        /// <summary>
        /// 工作圈列表
        /// </summary>
        public List<AdminCompany> data { get; set; }

        /// <summary>
        /// 工作圈信息
        /// </summary>
        public class AdminCompany
        {
            /// <summary>
            /// 工作圈名字
            /// </summary>
            public string companyName { get; set; }

            /// <summary>
            /// 工作圈eid
            /// </summary>
            public string eid { get; set; }
        }
    }
}
