﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Person
{
    /// <summary>
    /// 查询已更新人员信息|发起
    /// </summary>
    public class GetAtTimePerson : RequestDataBase
    {
        private const string URL = "gateway/openimport/open/person/getAtTime?accessToken={0}";

        //public string eid { get; set; }

        /// <summary>
        /// 时间戳
        /// </summary>
        public string time { get; set; }

        /// <summary>
        /// 查询已更新人员信息|初始化
        /// </summary>
        /// <param name="_dt">时间戳</param>
        public GetAtTimePerson(DateTime _dt)
        {
            time = _dt.ToString("yyyy-MM-dd HH:mm:ss");
        }

        /// <summary>
        /// 可选，计数下标，默认0
        /// </summary>
        public int? begin { get; set; }

        /// <summary>
        /// 可选，计数基数，默认1000
        /// </summary>
        public int? count { get; set; }

        /// <summary>
        /// 查询已更新人员信息
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public PersonInfoList GetPersonList(string _eid, string _token, string _nonce)
        {
            return PostData<PersonInfoList>(_eid, URL, _token, _nonce);
        }
    }
}
