﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Person
{
    /// <summary>
    /// 查询指定人员信息|发起
    /// </summary>
    public class GetPerson : RequestDataBase
    {
        private const string URL = "gateway/openimport/open/person/get?accessToken={0}";

        //public string eid { get; set; }

        /// <summary>
        /// 必填，0：手机号码，1：openId，默认0，注意这里的手机号码是指mobile值
        /// </summary>
        public int type
        {
            get
            {
                return (int)queryPersonType;
            }
        }

        /// <summary>
        /// 必填，手机号码或者openId数组
        /// </summary>
        public List<string> array { get; set; }

        /// <summary>
        /// 查询类型枚举
        /// </summary>
        public enum QueryPersonTypeEnum
        {
            /// <summary>
            /// 手机号码
            /// </summary>
            phone,
            /// <summary>
            /// openId
            /// </summary>
            openId
        }

        /// <summary>
        /// 查询类型
        /// </summary>
        private QueryPersonTypeEnum queryPersonType { get; set; }

        /// <summary>
        /// 查询指定人员信息|初始化
        /// </summary>
        /// <param name="_type">查询类型</param>
        /// <param name="_array">手机号码或者openId数组</param>
        public GetPerson(QueryPersonTypeEnum _type,List<string> _array)
        {
            queryPersonType = _type;
            array = _array;
        }

        /// <summary>
        /// 查询指定人员信息|操作
        /// </summary>
        /// <param name="_eid">必填，企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public PersonInfoList GetPersonInfo(string _eid,string _token,string _nonce)
        {
            return PostData<PersonInfoList>(_eid, URL, _token, _nonce);
        }
    }

    ///// <summary>
    ///// 查询指定人员信息|返回
    ///// </summary>
    //public class _GetPerson : BaseResult
    //{
    //    public List<PersonInfo> data { get; set; }
    //}
}
