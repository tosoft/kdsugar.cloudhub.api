﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Person
{
    /// <summary>
    /// 消息信息
    /// </summary>
    public class MsgInfo
    {
        /// <summary>
        /// 消息Id
        /// </summary>
        public string msgId { get; set; }

        /// <summary>
        /// 消息码
        /// </summary>
        public int msgCode { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        public string msg { get; set; }
    }

    /// <summary>
    /// 消息信息列
    /// </summary>
    public class MsgInfoList : BaseResult
    {
        /// <summary>
        /// 消息信息列
        /// </summary>
        public List<MsgInfo> data { get; set; }
    }
}
