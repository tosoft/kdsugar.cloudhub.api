﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Person
{
    /// <summary>
    /// 机密组织可见成员相关
    /// </summary>
    public class OrgSecretPerson : RequestDataBase
    {
        //public string eid { get; set; }

        /// <summary>
        /// 组织ID
        /// </summary>
        public string orgId { get; set; }

        /// <summary>
        /// 机密组织类型
        /// </summary>
        public int secretType
        {
            get
            {
                return (int)SecretType;
            }
        }

        /// <summary>
        /// 操作人员
        /// </summary>
        public List<string> openIds { get; set; }

        /// <summary>
        /// 操作类型枚举
        /// </summary>
        public enum OperateTypeEnum
        {
            /// <summary>
            /// 新增
            /// </summary>
            Add,
            /// <summary>
            /// 删除
            /// </summary>
            Delete
        }

        private OperateTypeEnum OperateType { get; set; }

        /// <summary>
        /// 机密组织对应类型枚举
        /// </summary>
        public enum SecretTypeEnum
        {
            /// <summary>
            /// 隐藏部门
            /// </summary>
            HideDept = 1,
            /// <summary>
            /// 限制该部门人员不可查看其他部门
            /// </summary>
            DontViewOtherDept = 2
        }

        private SecretTypeEnum SecretType { get; set; }

        /// <summary>
        /// 机密组织可见成员相关|初始化
        /// </summary>
        /// <param name="_orgId">组织ID</param>
        /// <param name="_secretType">机密组织类型</param>
        /// <param name="_operateType">操作类型</param>
        /// <param name="_openIds">操作的人员(用户openId数组)</param>
        public OrgSecretPerson(string _orgId,SecretTypeEnum _secretType,
            OperateTypeEnum _operateType,List<string> _openIds)
        {
            orgId = _orgId;
            SecretType = _secretType;
            OperateType = _operateType;
            openIds = _openIds;
        }

        /// <summary>
        /// 添加机密组织可见成员
        /// </summary>
        private const string ADD_URL = "/gateway/openimport/open/person/addOrgSecretPerson?accessToken={0}";

        /// <summary>
        /// 删除机密组织可见成员
        /// </summary>
        private const string DEL_URL = "/gateway/openimport/open/person/deleteOrgSecretPerson?accessToken={0}";

        /// <summary>
        /// 操作
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public BaseResult Submit(string _eid,string _token,string _nonce)
        {
            string tmpUrl;
            switch (OperateType)
            {
                case OperateTypeEnum.Add:
                    tmpUrl = ADD_URL;
                    break;
                default:
                    tmpUrl = DEL_URL;
                    break;
            }
            return PostData<BaseResult>(_eid, tmpUrl, _token, _nonce);
        }
    }

    //public class _OrgSecretPerson : BaseResult
    //{

    //}
}
