﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Person
{
    /// <summary>
    /// 人员信息
    /// </summary>
    public class PersonInfo
    {
        /// <summary>
        /// 人员的openid
        /// </summary>
        public string openId { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string photoUrl { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        public string phone { get; set; }

        /// <summary>
        /// 是否在通讯录中隐藏手机号,0不隐藏，1隐藏，默认为0
        /// </summary>
        public string isHidePhone { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// 部门长名称
        /// </summary>
        public string department { get; set; }

        /// <summary>
        /// 企业工号
        /// </summary>
        public string jobNo { get; set; }

        /// <summary>
        /// 职位
        /// </summary>
        public string jobTitle { get; set; }

        /// <summary>
        /// 性别,0: 不确定; 1: 男; 2: 女
        /// </summary>
        public int gender { get; set; }

        /// <summary>
        /// 状态 0: 注销，1: 正常，2: 禁用
        /// </summary>
        public int status { get; set; }

        /// <summary>
        /// 是否部门负责人 0:否， 1：是
        /// </summary>
        public int orgUserType { get; set; }
    }

    /// <summary>
    /// 人员信息列表
    /// </summary>
    public class PersonInfoList : BaseResult
    {
        /// <summary>
        /// 人员信息列表
        /// </summary>
        public List<PersonInfo> data { get; set; }
    }
}
