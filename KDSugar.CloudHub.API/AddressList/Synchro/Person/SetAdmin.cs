﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Person
{
    /// <summary>
    /// 设置或取消管理员|发起
    /// </summary>
    public class SetAdmin:RequestDataBase
    {
        private const string URL = "gateway/openimport/open/company/setadmin?accessToken={0}";

        //public string eid { get; set; }

        /// <summary>
        /// 必填，其值为mobile或者email
        /// </summary>
        public string account { get; set; }

        /// <summary>
        /// 必填，操作类型。0:取消 1:设置
        /// </summary>
        public int type
        {
            get
            {
                return (int)OperateType;
            }
        }

        /// <summary>
        /// 操作类型枚举
        /// </summary>
        public enum OperateTypeEnum
        {
            /// <summary>
            /// 取消
            /// </summary>
            Cancel = 0,
            /// <summary>
            /// 设置
            /// </summary>
            Setting = 1
        }

        private OperateTypeEnum OperateType { get; set; }

        /// <summary>
        /// 设置或取消管理员|初始化
        /// </summary>
        /// <param name="_account">必填，其值为mobile或者email</param>
        /// <param name="_operateType">操作类型枚举</param>
        public SetAdmin(string _account,OperateTypeEnum _operateType)
        {
            account = _account;
            OperateType = _operateType;
        }

        /// <summary>
        /// 设置或取消管理员|操作
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public BaseResult Operate(string _eid,string _token,string _nonce)
        {
            return PostData<BaseResult>(_eid, URL, _token, _nonce);
        }
    }
}
