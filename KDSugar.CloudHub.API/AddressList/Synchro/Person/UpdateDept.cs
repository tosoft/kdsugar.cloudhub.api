﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Person
{
    /// <summary>
    /// 更新人员组织|发起
    /// </summary>
    public class UpdateDept : RequestDataBase
    {
        private const string URL = "gateway/openimport/open/person/updateDept?accessToken={0}";

        //public string eid { get; set; }

        /// <summary>
        /// 更新人员组织信息列表
        /// </summary>
        public List<UpdateDeptInfo> persons { get; set; } = new List<UpdateDeptInfo>();

        /// <summary>
        /// 更新人员组织|初始化
        /// </summary>
        /// <param name="_list">更新人员组织信息列表</param>
        public UpdateDept(List<UpdateDeptInfo> _list)
        {
            persons = _list;
        }

        /// <summary>
        /// 更新人员组织信息类
        /// </summary>
        public class UpdateDeptInfo
        {
            /// <summary>
            /// 人员的openId,人员必须在职
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 新的组织长名称,组织须已存在。department=""，则人员移动到根部门;department="0",则人员移动到[无部门人员]
            /// </summary>
            public string department { get; set; }

            /// <summary>
            /// 更新人员组织信息类|初始化
            /// </summary>
            /// <param name="_openId">人员的openId,人员必须在职</param>
            /// <param name="_dept">新的组织长名称,组织须已存在。department=""，则人员移动到根部门;department="0",则人员移动到[无部门人员]</param>
            public UpdateDeptInfo(string _openId,string _dept)
            {
                openId = _openId;
                department = _dept;
            }
        }

        /// <summary>
        /// 更新人员组织
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public MsgInfoList Operate(string _eid, string _token, string _nonce = null)
        {
            return PostData<MsgInfoList>(_eid, URL, _token, _nonce);
        }
    }
}
