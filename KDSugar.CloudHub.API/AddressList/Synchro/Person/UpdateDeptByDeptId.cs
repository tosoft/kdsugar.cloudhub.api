﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Person
{
    /// <summary>
    /// 根据组织id批量更新人员组织|发起
    /// </summary>
    public class UpdateDeptByDeptId : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/person/updateDeptByDeptId?accessToken={0}";

        //public string eid { get; set; }

        /// <summary>
        /// 更新信息列表
        /// </summary>
        public List<UpdateDeptInfo> persons { get; set; } = new List<UpdateDeptInfo>();

        /// <summary>
        /// 根据组织id批量更新人员组织|初始化
        /// </summary>
        /// <param name="_list">更新信息列表</param>
        public UpdateDeptByDeptId(List<UpdateDeptInfo> _list)
        {
            persons = _list;
        }

        /// <summary>
        /// 更新信息类
        /// </summary>
        public class UpdateDeptInfo
        {
            /// <summary>
            /// 人员的openId(注意:人员必须在职)
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 新的组织id,若组织id为空则移动到无部门
            /// </summary>
            public string orgId { get; set; }

            /// <summary>
            /// 更新信息类|初始化
            /// </summary>
            /// <param name="_openId">人员的openId(注意:人员必须在职)</param>
            public UpdateDeptInfo(string _openId)
            {
                openId = _openId;
            }

            /// <summary>
            /// 更新信息类|初始化
            /// </summary>
            /// <param name="_openId">人员的openId(注意:人员必须在职)</param>
            /// <param name="_orgId">新的组织id,若组织id为空则移动到无部门</param>
            public UpdateDeptInfo(string _openId,string _orgId)
                : this(_openId)
            {
                orgId = _orgId;
            }
        }

        /// <summary>
        /// 批量更新人员组织
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public MsgInfoList Operate(string _eid,string _token,string _nonce)
        {
            return PostData<MsgInfoList>(_eid, URL, _token, _nonce);
        }
    }
}
