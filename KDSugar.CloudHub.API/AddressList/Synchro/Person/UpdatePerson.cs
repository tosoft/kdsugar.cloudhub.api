﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Person
{
    /// <summary>
    /// 更新人员信息|发起
    /// </summary>
    public class UpdatePerson : RequestDataBase
    {
        private const string URL = "gateway/openimport/open/person/updateInfo?accessToken={0}";

        //public string eid { get; set; }

        /// <summary>
        /// 更新人员信息列表
        /// </summary>
        public List<UpdatePersonInfo> persons { get; set; } = new List<UpdatePersonInfo>();

        /// <summary>
        /// 更新人员信息|初始化
        /// </summary>
        /// <param name="_list">更新人员信息列表</param>
        public UpdatePerson(List<UpdatePersonInfo> _list)
        {
            persons = _list;
        }

        /// <summary>
        /// 更新人员信息类
        /// </summary>
        public class UpdatePersonInfo
        {
            /// <summary>
            /// 更新人员信息类|初始化
            /// </summary>
            /// <param name="_openId"></param>
            public UpdatePersonInfo(string _openId)
            {
                openId = _openId;
            }

            /// <summary>
            /// 必填,人员的openId
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 可选,姓名
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// 可选,头像URL
            /// </summary>
            public string photoUrl { get; set; }

            /// <summary>
            /// 可选,是否在通讯录中隐藏手机号码
            /// </summary>
            public string isHidePhone { get; set; }

            /// <summary>
            /// 可选,企业工号
            /// </summary>
            public string jobNo { get; set; }

            /// <summary>
            /// 可选,职位
            /// </summary>
            public string jobTitle { get; set; }

            /// <summary>
            /// 可选,性别
            /// </summary>
            public string gender { get; set; }

            /// <summary>
            /// 可选,生日
            /// </summary>
            public string birthday { get; set; }

            /// <summary>
            /// 可选，排序权重，权重越小，排序越前.
            /// </summary>
            public int weights { get; set; }

            /// <summary>
            /// 可选,是否部门负责人,0表示普通用户，1表示设置部门负责人 2 表示取消部门负责人
            /// </summary>
            public int orgUserType { get; set; }

            /// <summary>
            /// 入职日期，格式如："2018-01-01"
            /// </summary>
            public string hireDate { get; set; }

            /// <summary>
            /// 转正日期，格式如："2018-01-01"
            /// </summary>
            public string positiveDate { get; set; }

            /// <summary>
            /// 自定义的联系方式列表
            /// <para>更新此字段，如果是部分更新，也需要将全部字段的值也传过来，因为此字段会覆盖保存</para>
            /// </summary>
            public List<ContactInfo> contact { get; set; }

            /// <summary>
            /// 自定义联系方式类
            /// </summary>
            public class ContactInfo
            {
                /// <summary>
                /// 名称
                /// </summary>
                public string name { get; set; }

                /// <summary>
                /// 类型
                /// </summary>
                public string type { get; set; }

                /// <summary>
                /// 值
                /// </summary>
                public string value { get; set; }
            }
        }

        /// <summary>
        /// 更新人员信息|操作
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public MsgInfoList Operate(string _eid,string _token,string _nonce)
        {
            return PostData<MsgInfoList>(_eid, URL, _token, _nonce);
        }
    }
}
