﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Person
{
    /// <summary>
    /// 修改手机号|发起
    /// </summary>
    public class UpdatePhone : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/person/updatePhone?accessToken={0}";

        /// <summary>
        /// 修改信息列表
        /// </summary>
        public List<UpdatePhoneInfo> persons { get; set; } = new List<UpdatePhoneInfo>();

        /// <summary>
        /// 修改信息类
        /// </summary>
        public class UpdatePhoneInfo
        {
            /// <summary>
            /// 用户OpenId
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 手机号
            /// </summary>
            public string phone { get; set; }

            /// <summary>
            /// 修改信息类|初始化
            /// </summary>
            /// <param name="_openId">用户openId</param>
            /// <param name="_phone">手机号</param>
            public UpdatePhoneInfo(string _openId,string _phone)
            {
                openId = _openId;
                phone = _phone;
            }
        }

        /// <summary>
        /// 修改手机号|操作
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _UpdatePhone ChangePhone(string _eid,string _token,string _nonce)
        {
            return PostData<_UpdatePhone>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 修改手机号|返回
    /// </summary>
    public class _UpdatePhone : BaseResult
    {
        /// <summary>
        /// 返回信息列表
        /// </summary>
        public List<ResultMsg> data { get; set; }

        /// <summary>
        /// 返回信息类
        /// </summary>
        public class ResultMsg
        {
            /// <summary>
            /// 消息
            /// </summary>
            public string msg { get; set; }

            /// <summary>
            /// 消息码
            /// </summary>
            public int msgCode { get; set; }

            /// <summary>
            /// 消息Id
            /// </summary>
            public string msgId { get; set; }
        }
    }
}
