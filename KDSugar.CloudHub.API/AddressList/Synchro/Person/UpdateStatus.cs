﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Person
{
    /// <summary>
    /// 更新人员状态|发起
    /// </summary>
    public class UpdateStatus : RequestDataBase
    {
        private const string URL = "gateway/openimport/open/person/updateStatus?accessToken={0}";

        //public string eid { get; set; }

        /// <summary>
        /// 更新人员状态列表
        /// </summary>
        public List<UpdateInfo> persons { get; set; } = new List<UpdateInfo>();

        /// <summary>
        /// 更新人员状态|初始化
        /// </summary>
        /// <param name="_list">更新人员状态列表</param>
        public UpdateStatus(List<UpdateInfo> _list)
        {
            persons = _list;
        }

        /// <summary>
        /// 更新人员状态类
        /// </summary>
        public class UpdateInfo
        {
            /// <summary>
            /// 用户OpenId
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 更新状态
            /// </summary>
            public string type
            {
                get
                {
                    return ((int)UpdateType).ToString();
                }
            }

            /// <summary>
            /// 更新人员状态类|初始化
            /// </summary>
            /// <param name="_openId">用户OpenId</param>
            /// <param name="_type">更新状态</param>
            public UpdateInfo(string _openId,TypeEnum _type)
            {
                openId = _openId;
                UpdateType = _type;
            }

            /// <summary>
            /// 更新类型枚举
            /// </summary>
            public enum TypeEnum
            {
                /// <summary>
                /// 离职
                /// </summary>
                Quit = 1,
                /// <summary>
                /// 恢复
                /// </summary>
                Recovery = 2,
                /// <summary>
                /// 禁用
                /// </summary>
                Disable = 3,
                /// <summary>
                /// 启用
                /// </summary>
                Enable = 4
            }

            private TypeEnum UpdateType;
        }

        /// <summary>
        /// 更新人员状态|操作
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">Token</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public MsgInfoList Operate(string _eid,string _token,string _nonce)
        {
            return PostData<MsgInfoList>(_eid, URL, _token, _nonce);
        }
    }
}
