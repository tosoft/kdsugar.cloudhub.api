﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Relations
{
    /// <summary>
    /// 批量指定上级|发起
    /// </summary>
    public class AddRelations : RequestDataBaseByNotEID
    {
        private const string URL = "https://www.yunzhijia.com/gateway/openimport/open/company/addRelations?accessToken={0}";
        
        private List<RelationInfo> infos;

        /// <summary>
        /// 批量指定上级|初始化
        /// </summary>
        /// <param name="adds">新增列表</param>
        public AddRelations(List<RelationInfo> adds)
        {
            infos = adds;
        }

        /// <summary>
        /// 批量指定上级
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _AddRelations Operate(string _eid,string _token,string _nonce = null)
        {
            return PostData<_AddRelations>(_eid, URL, _token, infos, _nonce);
        }
    }

    /// <summary>
    /// 批量指定上级|返回
    /// </summary>
    public class _AddRelations : BaseResult
    {
        /// <summary>
        /// 错误信息列表
        /// </summary>
        public List<AddRelationError> data { get; set; }

        /// <summary>
        /// 错误信息类
        /// </summary>
        public class AddRelationError
        {
            /// <summary>
            /// 提交ID 
            /// </summary>
            public string commitId { get; set; }

            /// <summary>
            /// 失败原因
            /// </summary>
            public string errorMsg { get; set; }
        }
    }
}
