﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Relations
{
    /// <summary>
    /// 批量删除上级|发起
    /// </summary>
    public class DeleteRelations : RequestDataBaseByNotEID
    {
        private const string URL = "https://www.yunzhijia.com/gateway/openimport/open/company/deleteRelations?accessToken={0}";

        private List<RelationInfo> infos;

        /// <summary>
        /// 批量删除上级|初始化
        /// </summary>
        /// <param name="adds">删除列表</param>
        public DeleteRelations(List<RelationInfo> adds)
        {
            infos = adds;
        }

        /// <summary>
        /// 批量删除上级
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _DeleteRelations Operate(string _eid,string _token,string _nonce = null)
        {
            return PostData<_DeleteRelations>(_eid, URL, _token, infos, _nonce);
        }
    }

    /// <summary>
    /// 删除所有上级
    /// </summary>
    public class DeleteAllRelations : RequestDataBaseByNotEID
    {
        private const string URL = "https://www.yunzhijia.com/gateway/openimport/open/relationship/deleteAllRelation?accessToken={0}";

        /// <summary>
        /// 是否删除所有,默认值为false。该值传true时，表示删除所有部门负责人（此时list可以不传）；该值为false时，表示删除指定部门负责人，list字段必传。
        /// </summary>
        public bool deleteAll { get; set; }

        /// <summary>
        /// 删除列表
        /// </summary>
        public List<RelationInfo> list { get; set; }

        /// <summary>
        /// 无条件删除所有上级
        /// </summary>
        public DeleteAllRelations()
        {
            deleteAll = true;
        }

        /// <summary>
        /// 根据删除列表删除对应的所有上级
        /// </summary>
        /// <param name="_list">删除列表</param>
        public DeleteAllRelations(List<RelationInfo> _list)
        {
            deleteAll = false;
            list = _list;
        }

        /// <summary>
        /// 删除操作
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _DeleteRelations Operate(string _eid,string _token,string _nonce = null)
        {
            return PostData<_DeleteRelations>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 删除上级|返回
    /// </summary>
    public class _DeleteRelations : BaseResult
    {
        /// <summary>
        /// 错误处理信息列表
        /// </summary>
        public List<DeleteRelationError> data { get; set; }

        /// <summary>
        /// 错误处理信息类
        /// </summary>
        public class DeleteRelationError
        {
            /// <summary>
            /// 错误码
            /// </summary>
            public int errorCode { get; set; }

            /// <summary>
            /// 提交ID
            /// </summary>
            public string commitId { get; set; }

            /// <summary>
            /// 失败原因
            /// </summary>
            public string error { get; set; }
        }
    }
}
