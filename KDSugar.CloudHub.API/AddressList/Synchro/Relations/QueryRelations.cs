﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Relations
{
    /// <summary>
    /// 批量查询上级|发起
    /// </summary>
    public class QueryRelations : RequestDataBaseByNotEID
    {
        private const string URL = "https://www.yunzhijia.com/gateway/openimport/open/company/queryRelations?accessToken={0}";

        private RelationTypeEnum RelationType;

        /// <summary>
        /// 指定上级
        /// </summary>
        public string relationType
        {
            get
            {
                return Enum.GetName(typeof(RelationTypeEnum), RelationType);
            }
        }

        /// <summary>
        /// 起始
        /// </summary>
        public string begin { get; set; }

        /// <summary>
        /// 条数
        /// </summary>
        public string count { get; set; }

        /// <summary>
        /// 批量查询上级|初始化
        /// </summary>
        /// <param name="type">上级关系</param>
        public QueryRelations(RelationTypeEnum type)
        {
            RelationType = type;
        }

        /// <summary>
        /// 批量查询上级
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _QueryRelations Query(string _eid,string _token,string _nonce = null)
        {
            return PostData<_QueryRelations>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 批量查询上级|返回
    /// </summary>
    public class _QueryRelations : BaseResult
    {
        /// <summary>
        /// 上级信息列表
        /// </summary>
        public List<RelationInfo> data { get; set; }
    }
}
