﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Relations
{
    /// <summary>
    /// 上级信息
    /// </summary>
    public class RelationInfo
    {
        /// <summary>
        /// 提交ID（唯一标识）
        /// </summary>
        public string commitId { get; set; }

        /// <summary>
        /// 人员ID
        /// </summary>
        public string openId { get; set; }

        /// <summary>
        /// 上级ID
        /// </summary>
        public string leaderOpenId { get; set; }

        /// <summary>
        /// 指定上级
        /// </summary>
        public string relationType
        {
            get
            {
                return RelationType != null ? Enum.GetName(typeof(RelationTypeEnum), RelationType) : _relationType;
            }
            set
            {
                _relationType = value;
                RelationType = (RelationTypeEnum)Enum.Parse(typeof(RelationTypeEnum), value);
            }
        }

        private string _relationType;

        private RelationTypeEnum? RelationType;
    }

    /// <summary>
    /// 上级关系枚举
    /// </summary>
    public enum RelationTypeEnum
    {
        /// <summary>
        /// 指定上级
        /// </summary>
        LEADER,
        /// <summary>
        /// 汇报上级
        /// </summary>
        REPORT
    }
}
