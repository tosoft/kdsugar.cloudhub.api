﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.AddressList.Synchro
{
    /// <summary>
    /// 同步接口请求基类
    /// </summary>
    public class RequestBase : PostRequest
    {
        /// <summary>
        /// 企业EID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 调用数据
        /// </summary>
        public string data { get; set; }

        /// <summary>
        /// 用于校验重复请求的随机字符串
        /// </summary>
        public string nonce { get; set; }

        /// <summary>
        /// 同步接口请求基类|初始化
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        public RequestBase(string _eid, string _nonce = null)
        {
            eid = _eid;
            nonce = _nonce;
        }

        /// <summary>
        /// 调用数据
        /// </summary>
        /// <typeparam name="T">返回数据基类</typeparam>
        /// <param name="_url">调用URL</param>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public T GetDataPack<T>(string _url,string _token) where T : BaseResult
        {
            return PostByUrlencoded<T>(_url, _token);
        }
    }

    /// <summary>
    /// 同步接口请求-[data基类-含EID](无需另外调整data属性的)
    /// </summary>
    public class RequestDataBase : RequestDataBaseByNotEID
    {
        //public const string ROOT_URL = "https://www.yunzhijia.com{0}";
        
        /// <summary>
        /// 企业EID
        /// </summary>
        public string eid { get; set; }
        #region 预留
        //protected T PostData<T>(string _eid, string _url, string _token, string _nonce = null) where T : BaseResult
        //{
        //    RequestBase requestBase = new RequestBase(_eid, _nonce)
        //    {
        //        data = JsonConvert.SerializeObject(this)
        //    };
        //    string tmpRootUrl = ROOT_URL.Substring(0, ROOT_URL.Length - 4);
        //    if (!_url.Contains(tmpRootUrl))
        //    {
        //        if (!string.IsNullOrWhiteSpace(_url))
        //            if (!_url.Substring(0, 1).Equals("/"))
        //                _url += "/" + _url;
        //        return requestBase.GetDataPack<T>(string.Format(ROOT_URL, _url), _token);
        //    }
        //    else
        //    {
        //        return requestBase.GetDataPack<T>(_url, _token);
        //    }
        //}
        #endregion
    }

    /// <summary>
    /// 同步接口请求-[data基类-不含EID](无需另外调整data属性的)
    /// </summary>
    public class RequestDataBaseByNotEID //: PostRequest
    {
        ///// <summary>
        ///// 请求地址
        ///// </summary>
        //[JsonIgnore]
        //public string RequestUrl { get; set; }

        /// <summary>
        /// 请求数据
        /// </summary>
        [JsonIgnore]
        public PostRequest RequestData { get; set; }

        /// <summary>
        /// 调用接口请求
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_eid"></param>
        /// <param name="_url"></param>
        /// <param name="_token"></param>
        /// <param name="_nonce"></param>
        /// <returns></returns>
        protected T PostData<T>(string _eid, string _url, string _token, string _nonce) where T : BaseResult
        {
            #region 预留
            //RequestBase requestBase = new RequestBase(_eid, _nonce)
            //{
            //    data = JsonConvert.SerializeObject(this)
            //};
            //if (!_url.Contains(Constant.ROOTURL))
            //{
            //    string rootUrl = Constant.ROOTURL + "{0}";
            //    if (!string.IsNullOrWhiteSpace(_url))
            //        if (!_url.Substring(0, 1).Equals("/"))
            //            _url += "/" + _url;
            //    return requestBase.GetDataPack<T>(string.Format(rootUrl, _url), _token);
            //}
            //else
            //{
            //    return requestBase.GetDataPack<T>(_url, _token);
            //}
            #endregion
            return PostData<T>(_eid, _url, _token, this, _nonce);
        }

        /// <summary>
        /// 调用接口请求
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_eid"></param>
        /// <param name="_url"></param>
        /// <param name="_token"></param>
        /// <param name="_content"></param>
        /// <param name="_nonce"></param>
        /// <returns></returns>
        protected T PostData<T>(string _eid, string _url, string _token, object _content, string _nonce)
            where T : BaseResult
        {
            RequestBase requestBase = new RequestBase(_eid, _nonce)
            {
                data = JsonConvert.SerializeObject(_content)
            };
            RequestData = requestBase;
            if (!_url.Contains(Constant.ROOTURL))
            {
                string rootUrl = Constant.ROOTURL + "{0}";
                if (!string.IsNullOrWhiteSpace(_url))
                    if (!_url.Substring(0, 1).Equals("/"))
                        _url += "/" + _url;
                //RequestUrl = string.Format(rootUrl, _url);
                return requestBase.GetDataPack<T>(string.Format(rootUrl, _url), _token);
            }
            else
            {
                //RequestUrl = _url;
                return requestBase.GetDataPack<T>(_url, _token);
            }
        }
    }

    //public class RequestDataOnlyBase : RequestBase
    //{

    //}

    /// <summary>
    /// 同步接口请求-[data基类-含EID]（带分页的）
    /// </summary>
    public class RequestDataBaseByPage : RequestDataBase
    {
        /// <summary>
        /// 起始页码
        /// </summary>
        public int begin { get; set; }

        /// <summary>
        /// 每页数量
        /// </summary>
        public int count { get; set; }
    }

    /// <summary>
    /// 同步接口请求-[data基类-不含EID]（带分页的）
    /// </summary>
    public class RequestDataBaseByPageByNotEID : RequestDataBaseByNotEID
    {
        /// <summary>
        /// 起始页码
        /// </summary>
        public int? begin { get; set; }

        /// <summary>
        /// 每页数量
        /// </summary>
        public int? count { get; set; }
    }
}
