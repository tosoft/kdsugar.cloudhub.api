﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Role
{
    /// <summary>
    /// 添加角色标签|发起
    /// </summary>
    public class AddRoleTag : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/roletag/addRoleTag?accessToken={0}";

        /// <summary>
        /// 角色名称
        /// </summary>
        public string roleName { get; set; }

        /// <summary>
        /// 添加角色标签|初始化
        /// </summary>
        /// <param name="_roleName">角色名称</param>
        public AddRoleTag(string _roleName)
        {
            roleName = _roleName;
        }

        /// <summary>
        /// 添加角色标签
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _AddRoleTag Operate(string _eid,string _token,string _nonce)
        {
            return PostData<_AddRoleTag>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 添加角色标签|返回
    /// </summary>
    public class _AddRoleTag : BaseResult
    {
        //public List<AddRoleInfo> data { get; set; }

        /// <summary>
        /// 添加角色标签信息
        /// </summary>
        public AddRoleInfo data { get; set; }

        /// <summary>
        /// 添加角色标签信息类
        /// </summary>
        public class AddRoleInfo
        {
            /// <summary>
            /// 角色ID
            /// </summary>
            public string id { get; set; }
        }
    }
}
