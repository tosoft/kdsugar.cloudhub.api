﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Role
{
    /// <summary>
    /// 根据用户id批量设置人员角色|发起
    /// </summary>
    public class BatchSetPersonRoleTagByOpenId : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/roletag/batchSetPersonRoleTag?accessToken={0}";

        /// <summary>
        /// 操作类型
        /// </summary>
        public string operate
        {
            get
            {
                return Enum.GetName(typeof(OperateTypeEnum), OperateType);
            }
        }

        /// <summary>
        /// 操作类型枚举
        /// </summary>
        public enum OperateTypeEnum
        {
            /// <summary>
            /// 添加
            /// </summary>
            insert,
            /// <summary>
            /// 删除
            /// </summary>
            delete
        }

        /// <summary>
        /// 根据用户id批量设置人员角色|初始化
        /// </summary>
        /// <param name="_type">操作类型</param>
        /// <param name="_roleTags">角色标签信息</param>
        public BatchSetPersonRoleTagByOpenId(OperateTypeEnum _type, List<RoleTag> _roleTags)
        {
            OperateType = _type;
            roleTags = _roleTags;
        }

        private OperateTypeEnum OperateType;

        /// <summary>
        /// 角色标签信息
        /// </summary>
        public List<RoleTag> roleTags { get; set; }

        /// <summary>
        /// 角色标签类
        /// </summary>
        public class RoleTag
        {
            /// <summary>
            /// 用户OpenId
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 角色作用范围列表
            /// </summary>
            public List<RoleOrg> roleOrgs { get; set; }
        }

        /// <summary>
        /// 角色作用范围
        /// </summary>
        public class RoleOrg
        {
            /// <summary>
            /// 角色ID
            /// </summary>
            public string roleId { get; set; }

            /// <summary>
            /// 作用范围
            /// </summary>
            public List<string> orgids { get; set; }
        }

        /// <summary>
        /// 根据用户id批量设置人员角色
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public BaseResult Operate(string _eid, string _token, string _nonce)
        {
            return PostData<BaseResult>(_eid, URL, _token, _nonce);
        }

    }

    /// <summary>
    /// 根据角色id批量设置人员角色|发起
    /// </summary>
    public class BatchSetPersonRoleTagByRoleId : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/roletag/batchSetPersonRoleTag_other?accessToken={0}";

        /// <summary>
        /// 操作类型
        /// </summary>
        public string operate
        {
            get
            {
                return Enum.GetName(typeof(OperateTypeEnum), OperateType);
            }
        }

        /// <summary>
        /// 操作类型枚举
        /// </summary>
        public enum OperateTypeEnum
        {
            /// <summary>
            /// 添加
            /// </summary>
            insert,
            /// <summary>
            /// 删除
            /// </summary>
            delete
        }

        private OperateTypeEnum OperateType;

        /// <summary>
        /// 操作列表
        /// </summary>
        public List<RoleTag> roleTags { get; set; }

        /// <summary>
        /// 根据角色id批量设置人员角色|初始化
        /// </summary>
        /// <param name="_type">操作类型</param>
        /// <param name="_roleTags">操作列表</param>
        public BatchSetPersonRoleTagByRoleId(OperateTypeEnum _type,List<RoleTag> _roleTags)
        {
            OperateType = _type;
            roleTags = _roleTags;
        }

        /// <summary>
        /// 角色操作信息
        /// </summary>
        public class RoleTag
        {
            /// <summary>
            /// 角色ID
            /// </summary>
            public string roleId { get; set; }

            /// <summary>
            /// 用户作用范围列表
            /// </summary>
            public List<PersonOrg> personOrgs { get; set; }
        }

        /// <summary>
        /// 用户作用范围类
        /// </summary>
        public class PersonOrg
        {
            /// <summary>
            /// 用户OpenID
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 作用范围
            /// </summary>
            public List<string> orgids { get; set; }
        }

        /// <summary>
        /// 根据角色id批量设置人员角色
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public BaseResult Operate(string _eid,string _token,string _nonce)
        {
            return PostData<BaseResult>(_eid, URL, _token, _nonce);
        }
    }
}
