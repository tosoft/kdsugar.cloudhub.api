﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Role
{
    /// <summary>
    /// 删除角色标签|发起
    /// </summary>
    public class DeleteRoleTag : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/roletag/deleteRoleTag?accessToken={0}";

        /// <summary>
        /// 角色ID
        /// </summary>
        public string roleId { get; set; }

        /// <summary>
        /// 删除角色标签|初始化
        /// </summary>
        /// <param name="_roleId">角色ID</param>
        public DeleteRoleTag(string _roleId)
        {
            roleId = _roleId;
        }

        /// <summary>
        /// 删除角色标签
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public BaseResult Operate(string _eid,string _token,string _nonce)
        {
            return PostData<BaseResult>(_eid, URL, _token, _nonce);
        }
    }
}
