﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Role
{
    /// <summary>
    /// 获取工作圈角色标签列表|发起
    /// </summary>
    public class GetCompanyRoleTag : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/roletag/getCompanyRoleTag?accessToken={0}";

        /// <summary>
        /// 获取工作圈角色标签列表
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _GetCompanyRoleTag Operate(string _eid,string _token,string _nonce)
        {
            return PostData<_GetCompanyRoleTag>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 获取工作圈角色标签列表|返回
    /// </summary>
    public class _GetCompanyRoleTag : BaseResult
    {
        /// <summary>
        /// 角色标签信息列表
        /// </summary>
        public List<CompanyRoleTag> data { get; set; }

        /// <summary>
        /// 角色标签信息
        /// </summary>
        public class CompanyRoleTag
        {
            /// <summary>
            /// 角色id
            /// </summary>
            public string id { get; set; }

            /// <summary>
            /// 创建时间
            /// </summary>
            public string createtime { get; set; }

            /// <summary>
            /// 角色名称
            /// </summary>
            public string rolename { get; set; }

            /// <summary>
            /// 上次更新时间
            /// </summary>
            public string lastupdatetime { get; set; }

            /// <summary>
            /// 创建人id
            /// </summary>
            public string createpersonid { get; set; }

            /// <summary>
            /// 如果是第三方应用创建，appid就会有值
            /// </summary>
            public string appid { get; set; }

            /// <summary>
            /// 是否系统常用标签
            /// </summary>
            public int iscommon { get; set; }

            /// <summary>
            /// /标签类型，10系统标签，20 自建应用标签，30 第三方应用标签,40 企业自定义标签
            /// </summary>
            public int type { get; set; }

            /// <summary>
            /// 如果是企业自定义标签，就会有值,工作圈id
            /// </summary>
            public string eid { get; set; }
        }
    }
}
