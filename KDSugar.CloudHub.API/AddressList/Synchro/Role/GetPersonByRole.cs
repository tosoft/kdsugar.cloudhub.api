﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Role
{
    /// <summary>
    /// 根据角色获取人员|发起
    /// </summary>
    [Obsolete("官方文档中已声明不建议使用此接口")]
    public class GetPersonByRole : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/roletag/getPersonByRole?accessToken={0}";

        /// <summary>
        /// 角色id
        /// </summary>
        public string roleId { get; set; }

        /// <summary>
        /// 根据角色获取人员|初始化
        /// </summary>
        /// <param name="_roleId">角色id</param>
        public GetPersonByRole(string _roleId)
        {
            roleId = _roleId;
        }

        /// <summary>
        /// 根据角色获取人员
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _GetPersonByRole Operate(string _eid, string _token, string _nonce)
        {
            return PostData<_GetPersonByRole>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 根据角色Id分页获取人员|发起
    /// </summary>
    public class GetPersonByRoleAndPage : RequestDataBaseByPage
    {
        private const string URL = "/gateway/openimport/open/roletag/getPersonsByRoleAndPage?accessToken={0}";

        /// <summary>
        /// 角色id
        /// </summary>
        public string roleId { get; set; }

        /// <summary>
        /// 根据角色Id分页获取人员
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _GetPersonByRole Operate(string _eid,string _token,string _nonce)
        {
            return PostData<_GetPersonByRole>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 获取人员信息|返回
    /// </summary>
    public class _GetPersonByRole : BaseResult
    {
        /// <summary>
        /// 人员信息列表
        /// </summary>
        public List<PersonRoleInfo> data { get; set; }

        /// <summary>
        /// 人员信息类
        /// </summary>
        public class PersonRoleInfo
        {
            /// <summary>
            /// 作用范围
            /// </summary>
            public string orgIds { get; set; }

            /// <summary>
            /// 用户openid
            /// </summary>
            public string openId { get; set; }
        }
    }
}
