﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Role
{
    /// <summary>
    /// 设置人员角色标签|发起
    /// </summary>
    public class SetPersonRoleTag : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/roletag/setPersonRoleTag?accessToken={0}";

        /// <summary>
        /// 角色ID
        /// </summary>
        public string roleId { get; set; }

        /// <summary>
        /// 用户OpenID
        /// </summary>
        public string openId { get; set; }

        /// <summary>
        /// 作用范围
        /// </summary>
        public string orgIds { get; set; }

        /// <summary>
        /// 设置人员角色标签|初始化
        /// </summary>
        /// <param name="_roleId">角色id</param>
        /// <param name="_openId">用户openId</param>
        /// <param name="_orgIds">作用范围</param>
        public SetPersonRoleTag(string _roleId,string _openId,List<string> _orgIds)
        {
            roleId = _roleId;
            openId = _openId;
            orgIds = null;
            if (_orgIds != null)
            {
                string tmpIds = "";
                foreach (string id in _orgIds)
                    tmpIds += string.Format("{0},", id);
                if (string.IsNullOrWhiteSpace(tmpIds))
                    tmpIds = tmpIds.Substring(0, tmpIds.Length - 1);
                orgIds = tmpIds;
            }
        }

        /// <summary>
        /// 设置人员角色标签
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public BaseResult Operate(string _eid, string _token, string _nonce)
        {
            return PostData<BaseResult>(_eid, URL, _token, _nonce);
        }
    }
}
