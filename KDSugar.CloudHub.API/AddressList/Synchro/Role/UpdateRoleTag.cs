﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.Role
{
    /// <summary>
    /// 更改角色标签名字|发起
    /// </summary>
    public class UpdateRoleTag : RequestDataBase
    {
        private const string URL = "/gateway/openimport/open/roletag/updateRoleTag?accessToken={0}";

        /// <summary>
        /// 角色ID
        /// </summary>
        public string roleId { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        public string roleName { get; set; }

        /// <summary>
        /// 更改角色标签名字|初始化
        /// </summary>
        /// <param name="_roleId"></param>
        /// <param name="_roleName"></param>
        public UpdateRoleTag(string _roleId, string _roleName)
        {
            roleId = _roleId;
            roleName = _roleName;
        }

        /// <summary>
        /// 更改角色标签名字
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public BaseResult Operate(string _eid, string _token, string _nonce)
        {
            return PostData<BaseResult>(_eid, URL, _token, _nonce);
        }
    }
}
