﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.AddressList.Synchro.TimeJobs
{
    /// <summary>
    /// 根据部门id批量设置兼职
    /// </summary>
    public class AddTimeJobsByDept : RequestDataBaseByNotEID
    {
        private const string URL = "https://www.yunzhijia.com/gateway/openimport/open/company/addPartTimeJobs?accessToken={0}";

        private List<AddTimeJobsByDeptInfo> list { get; set; }

        /// <summary>
        /// 兼职设置信息类
        /// </summary>
        public class AddTimeJobsByDeptInfo
        {
            /// <summary>
            /// 提交ID
            /// </summary>
            public string commitId { get; set; }

            /// <summary>
            /// 人员ID
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 部门ID
            /// </summary>
            public string orgId { get; set; }

            /// <summary>
            /// 兼职职位
            /// </summary>
            public string jobTitle { get; set; }
        }

        /// <summary>
        /// 根据部门id批量设置兼职|初始化
        /// </summary>
        /// <param name="_list">兼职信息列表</param>
        public AddTimeJobsByDept(List<AddTimeJobsByDeptInfo> _list)
        {
            list = _list;
        }

        /// <summary>
        /// 根据部门id批量设置兼职
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public BaseResult Operate(string _eid,string _token,string _nonce=null)
        {
            return PostData<BaseResult>(_eid, URL, _token, list, _nonce);
        }
    }

    /// <summary>
    /// 根据部门长名称批量设置兼职
    /// </summary>
    public class AddTimeJobsByFullDept : RequestDataBaseByNotEID
    {
        private const string URL = "https://www.yunzhijia.com/gateway/openimport/open/company/addPartTimeJobsFull?accessToken={0}";

        /// <summary>
        /// 兼职设置信息类
        /// </summary>
        public class AddTimeJobsByFullDeptInfo
        {
            /// <summary>
            /// 提交ID
            /// </summary>
            public string commitId { get; set; }

            /// <summary>
            /// 兼职职位
            /// </summary>
            public string jobTitle { get; set; }

            /// <summary>
            /// 用户openId
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 简直部门长名称
            /// </summary>
            public string department { get; set; }

            /// <summary>
            /// 权重
            /// </summary>
            public int weights { get; set; }

            /// <summary>
            /// 是否设为兼职部门负责人
            /// </summary>
            public int orgUserType { get; set; }
        }

        private List<AddTimeJobsByFullDeptInfo> list { get; set; }

        /// <summary>
        /// 根据部门长名称批量设置兼职|初始化
        /// </summary>
        /// <param name="_list">兼职信息列表</param>
        public AddTimeJobsByFullDept(List<AddTimeJobsByFullDeptInfo> _list)
        {
            list = _list;
        }

        /// <summary>
        /// 根据部门长名称批量设置兼职
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public BaseResult Operate(string _eid, string _token, string _nonce = null)
        {
            return PostData<BaseResult>(_eid, URL, _token, list, _nonce);
        }
    }
}
