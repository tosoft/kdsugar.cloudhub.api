﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.AddressList.Synchro.TimeJobs
{
    /// <summary>
    /// 根据部门id批量删除兼职
    /// </summary>
    public class DeleteTimeJobs : RequestDataBaseByNotEID
    {
        private const string URL = "https://www.yunzhijia.com/gateway/openimport/open/company/deletePartTimeJobs?accessToken={0}";

        private List<DeleteTimeJobInfo> list { get; set; }

        /// <summary>
        /// 兼职删除信息类
        /// </summary>
        public class DeleteTimeJobInfo
        {
            /// <summary>
            /// 提交ID
            /// </summary>
            public string commitId { get; set; }

            /// <summary>
            /// 人员openId
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 部门ID
            /// </summary>
            public string orgId { get; set; }
        }

        /// <summary>
        /// 根据部门id批量删除兼职|初始化
        /// </summary>
        /// <param name="_list">删除兼职信息列表</param>
        public DeleteTimeJobs(List<DeleteTimeJobInfo> _list)
        {
            list = _list;
        }

        /// <summary>
        /// 根据部门id批量删除兼职
        /// </summary>
        /// <param name="_eid">EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public BaseResult Delete(string _eid, string _token, string _nonce = null)
        {
            return PostData<BaseResult>(_eid, URL, _token, list, _nonce);
        }
    }

    /// <summary>
    /// 删除所有兼职
    /// </summary>
    public class DeleteAllTimeJobs : RequestDataBaseByNotEID
    {
        private const string URL = "https://www.yunzhijia.com/gateway/openimport/open/relationship/deleteAllPartTimeJobs?accessToken={0}";

        /// <summary>
        /// 是否删除所有
        /// </summary>
        public bool deleteAll { get; set; } = false;

        /// <summary>
        /// 删除兼职信息类
        /// </summary>
        public class DeleteAllTimeJobInfo
        {
            /// <summary>
            /// 人员openId
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 部门ID
            /// </summary>
            public string orgId { get; set; }
        }

        /// <summary>
        /// 删除兼职列表
        /// </summary>
        public List<DeleteAllTimeJobInfo> list { get; set; }

        /// <summary>
        /// 删除所有兼职
        /// </summary>
        public DeleteAllTimeJobs()
        {
            deleteAll = true;
        }

        /// <summary>
        /// 根据列表所有兼职
        /// </summary>
        /// <param name="_list">删除兼职列表</param>
        public DeleteAllTimeJobs(List<DeleteAllTimeJobInfo> _list)
        {
            deleteAll = false;
            list = _list;
        }

        /// <summary>
        /// 删除所有兼职
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public BaseResult Operate(string _eid,string _token,string _nonce = null)
        {
            return PostData<BaseResult>(_eid, URL, _token, _nonce);
        }
    }
}
