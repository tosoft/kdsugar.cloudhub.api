﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.AddressList.Synchro.TimeJobs
{
    /// <summary>
    /// 批量查询兼职|发起
    /// </summary>
    public class QueryTimeJobs : RequestDataBaseByPageByNotEID
    {
        private const string URL = "https://www.yunzhijia.com/gateway/openimport/open/company/queryPartTimeJobs?accessToken={0}";

        //private Dictionary<string, int> PageInfo;

        //public QueryTimeJobs(int _begin, int _count, string _eid, string _nonce = null)
        //    : base(_eid, _nonce)
        //{
        //    //PageInfo = new Dictionary<string, int>();
        //    //PageInfo.Add("begin", _begin);
        //    //PageInfo.Add("count", _count);
        //}

        /// <summary>
        /// 批量查询兼职|初始化
        /// </summary>
        public QueryTimeJobs()
        {

        }

        /// <summary>
        /// 批量查询兼职|初始化
        /// </summary>
        /// <param name="_page">起始页码</param>
        /// <param name="_count">每页数量</param>
        public QueryTimeJobs(int? _page,int? _count)
        {
            this.begin = _page;
            this.count = _count;
        }

        /// <summary>
        /// 批量查询兼职
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_token">token</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public _QueryTimeJobs Query(string _eid, string _token, string _nonce = null)
        {
            //data = JsonConvert.SerializeObject(PageInfo);
            return PostData<_QueryTimeJobs>(_eid, URL, _token, _nonce);
        }
    }

    /// <summary>
    /// 批量查询兼职|返回
    /// </summary>
    public class _QueryTimeJobs : BaseResult
    {
        /// <summary>
        /// 兼职信息列表
        /// </summary>
        public List<TimeJobInfo> data { get; set; }

        /// <summary>
        /// 兼职信息类
        /// </summary>
        public class TimeJobInfo
        {
            /// <summary>
            /// 人员openId
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 兼职职位
            /// </summary>
            public string jobTitle { get; set; }

            /// <summary>
            /// (未知属性)
            /// </summary>
            public long weights { get; set; }

            /// <summary>
            /// 组织ID
            /// </summary>
            public string orgId { get; set; }
        }
    }
}
