﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API
{
    /// <summary>
    /// 返回信息基类
    /// </summary>
    public class BaseResult
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool success { get; set; } = true;

        /// <summary>
        /// 错误码
        /// </summary>
        public int errorCode { get; set; } = 0;

        /// <summary>
        /// 错误信息
        /// </summary>
        public string error { get; set; } = null;

        /// <summary>
        /// 初始化返回信息基类
        /// </summary>
        public BaseResult()
        {

        }

        /// <summary>
        /// 初始化返回错误信息基类
        /// </summary>
        /// <param name="_errorCode">错误码</param>
        /// <param name="_error">错误信息</param>
        public BaseResult(int _errorCode,string _error)
        {
            success = false;
            errorCode = _errorCode;
            error = _error;
        }
    }
}
