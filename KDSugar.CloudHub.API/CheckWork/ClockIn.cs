﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.CheckWork
{
    /// <summary>
    /// 根据更新时间获取获移动签到明细|发起
    /// </summary>
    public class ClockIn : PostRequest
    {
        private const string URL = "/gateway/attendance-data/v1/clockIn/list?accessToken={0}";

        /// <summary>
        /// 起始时间
        /// </summary>
        public string workDateFrom { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public string workDateTo { get; set; }

        /// <summary>
        /// 开始页（默认第一页从1开始）
        /// </summary>
        public int? start { get; set; }

        /// <summary>
        /// 每页显示条数大小(默认页大小为 :200>=size>=10)
        /// </summary>
        public int? limit { get; set; }

        /// <summary>
        /// 团队ID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 查询指定员工ID (最大不能超过50,多个中间用 , 号隔开)
        /// </summary>
        public string openIds { get; set; }

        /// <summary>
        /// 根据更新时间获取获移动签到明细|初始化
        /// </summary>
        /// <param name="_eid">团队Id</param>
        /// <param name="begTime">起始时间</param>
        /// <param name="endTime">终止时间</param>
        public ClockIn(string _eid,DateTime begTime,DateTime endTime)
        {
            const string FORMAT = "yyyy-MM-dd";
            eid = _eid;
            workDateFrom = begTime.ToString(FORMAT);
            workDateTo = endTime.ToString(FORMAT);
        }

        /// <summary>
        /// 获取移动签到明细
        /// </summary>
        /// <param name="_token">Token</param>
        /// <returns></returns>
        public _ClockIn GetClockInRecord(string _token)
        {
            return PostByUrlencoded<_ClockIn>(Constant.ROOTURL + URL, _token);
        }
    }

    /// <summary>
    /// 按具体日期查询签到明细|发起
    /// </summary>
    public class ClockInDay : PostRequest
    {
        private const string URL = "/gateway/attendance-data/v1/clockIn/day/list?accessToken={0}";

        /// <summary>
        /// 团队id
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 查询日期, 格式如 2017-01-02
        /// </summary>
        public string day { get; set; }

        /// <summary>
        /// 查询页数, 默认为第一页,即 start为1
        /// </summary>
        public int? start { get; set; }

        /// <summary>
        /// 查询数量, 最大200条数据
        /// </summary>
        public int? limit { get; set; }

        /// <summary>
        /// 指定查询用户openId, 以逗号分隔
        /// </summary>
        public string openIds { get; set; }

        /// <summary>
        /// 按具体日期查询签到明细|初始化
        /// </summary>
        /// <param name="_eid">团队EID</param>
        /// <param name="_day">查询日期</param>
        public ClockInDay(string _eid,DateTime _day)
        {
            const string FORMAT = "yyyy-MM-dd";
            eid = _eid;
            day = _day.ToString(FORMAT);
        }

        /// <summary>
        /// 获取签到数据
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _ClockIn GetClockInRecord(string _token)
        {
            return PostByUrlencoded<_ClockIn>(Constant.ROOTURL + URL, _token);
        }
    }

    /// <summary>
    /// 签到数据|返回
    /// </summary>
    public class _ClockIn : BaseResult
    {
        /// <summary>
        /// 签到流水数据总数
        /// </summary>
        public long total { get; set; }

        /// <summary>
        /// 签到数据列表
        /// </summary>
        public List<ClockInInfo> data { get; set; }

        /// <summary>
        /// 签到数据信息类
        /// </summary>
        public class ClockInInfo
        {
            /// <summary>
            /// 签到地点
            /// </summary>
            public string position { get; set; }

            /// <summary>
            /// 无线接入点MAC地址
            /// </summary>
            public string bssid { get; set; }

            /// <summary>
            /// 无线网络名称
            /// </summary>
            public string ssid { get; set; }

            /// <summary>
            /// 签到日期 （格式：yyyy-MM-dd）
            /// </summary>
            public string day { get; set; }

            /// <summary>
            /// 签到时间
            /// </summary>
            public long time { get; set; }

            /// <summary>
            /// 签到人员编号
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 签到位置结果
            /// </summary>
            public string positionResult { get; set; }

            /// <summary>
            /// 签到审核结果
            /// </summary>
            public ApproveResultInfo approveResult { get; set; }

            /// <summary>
            /// 部门名称
            /// </summary>
            public string department { get; set; }

            /// <summary>
            /// 用户名
            /// </summary>
            public string userName { get; set; }

            /// <summary>
            /// 签到拍照文件ID
            /// </summary>
            public string photoId { get; set; }

            /// <summary>
            /// 签到备注
            /// </summary>
            public string remark { get; set; }

            /// <summary>
            /// 签到审核结果类
            /// </summary>
            public class ApproveResultInfo
            {
                /// <summary>
                /// 关联的审批ID
                /// </summary>
                public string approveId { get; set; }

                /// <summary>
                /// 签到审核用户ID
                /// </summary>
                public string approveUserOpenId { get; set; }

                /// <summary>
                /// 审批时间
                /// </summary>
                public long approveTime { get; set; }

                /// <summary>
                /// 审批状态 （未处理:UNHANDLED、已知悉:KNOWEN、已处理:HANDLED）
                /// </summary>
                public string approveStatus { get; set; }

                /// <summary>
                /// 审批状态枚举
                /// </summary>
                public enum ApproveStatusEnum
                {
                    /// <summary>
                    /// 未处理
                    /// </summary>
                    UNHANDLED,
                    /// <summary>
                    /// 已知悉
                    /// </summary>
                    KNOWEN,
                    /// <summary>
                    /// 已处理
                    /// </summary>
                    HANDLED
                }

                /// <summary>
                /// 审批状态枚举类型
                /// </summary>
                [JsonIgnore]
                public ApproveStatusEnum? ApproveStatus
                {
                    get
                    {
                        ApproveStatusEnum? result = null;
                        switch (approveStatus)
                        {
                            case "UNHANDLED":
                                result = ApproveStatusEnum.UNHANDLED;
                                break;
                            case "KNOWEN":
                                result = ApproveStatusEnum.KNOWEN;
                                break;
                            case "HANDLED":
                                result = ApproveStatusEnum.HANDLED;
                                break;
                        }
                        return result;
                    }
                }

                /// <summary>
                /// 签到审批类型 （外勤反馈:OUTWORK、异常反馈:EXCEPTION、迟到:LATE、早退:EARLYLEAVE、缺勤:ABSENCE）
                /// </summary>
                public string approveType { get; set; }

                /// <summary>
                /// 签到审批枚举类型
                /// </summary>
                [JsonIgnore]
                public ApproveTypeEnum? ApproveType
                {
                    get
                    {
                        ApproveTypeEnum? result = null;
                        switch (approveType)
                        {
                            case "OUTWORK":
                                result = ApproveTypeEnum.OUTWORK;
                                break;
                            case "EXCEPTION":
                                result = ApproveTypeEnum.EXCEPTION;
                                break;
                            case "LATE":
                                result = ApproveTypeEnum.LATE;
                                break;
                            case "EARLYLEAVE":
                                result = ApproveTypeEnum.EARLYLEAVE;
                                break;
                            case "ABSENCE":
                                result = ApproveTypeEnum.ABSENCE;
                                break;
                        }
                        return result;
                    }
                }

                /// <summary>
                /// 签到审批类型枚举
                /// </summary>
                public enum ApproveTypeEnum
                {
                    /// <summary>
                    /// 外勤反馈
                    /// </summary>
                    OUTWORK,
                    /// <summary>
                    /// 异常反馈
                    /// </summary>
                    EXCEPTION,
                    /// <summary>
                    /// 迟到
                    /// </summary>
                    LATE,
                    /// <summary>
                    /// 早退
                    /// </summary>
                    EARLYLEAVE,
                    /// <summary>
                    /// 缺勤
                    /// </summary>
                    ABSENCE
                }
            }
        }
    }
}
