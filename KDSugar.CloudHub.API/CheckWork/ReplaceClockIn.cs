﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.CheckWork
{
    /// <summary>
    /// 获取代签到数据|发起
    /// </summary>
    public class ReplaceClockIn : PostRequest
    {
        private const string URL = "/gateway/attendance-data/v1/clockIn/listReplaceClockIn?accessToken={0}";

        /// <summary>
        /// 开始时间
        /// </summary>
        public string workDateFrom { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public string workDateTo { get; set; }

        /// <summary>
        /// 开始页（默认第一页从1开始）
        /// </summary>
        public int? start { get; set; }

        /// <summary>
        ///  每页显示条数大小(默认页大小为 :200>=size>=10)
        /// </summary>
        public int? limit { get; set; }

        /// <summary>
        /// 最大分页条数
        /// </summary>
        public const int MAX_LIMIT = 200;

        /// <summary>
        /// 最小分页条数
        /// </summary>
        public const int MIN_LIMIT = 10;

        /// <summary>
        /// 工作圈ID（EID）
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 初始化代签到数据
        /// </summary>
        /// <param name="_eid">工作圈ID（EID）</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        public ReplaceClockIn(string _eid,DateTime beginTime,DateTime endTime)
        {
            const string FORMAT = "yyyy-MM-dd";
            eid = _eid;
            workDateFrom = beginTime.ToString(FORMAT);
            workDateTo = endTime.ToString(FORMAT);
        }

        /// <summary>
        /// 获取代签到数据
        /// </summary>
        /// <param name="_token"></param>
        /// <returns></returns>
        public _ReplaceClockIn GetReplaceClockIn(string _token)
        {
            return PostByUrlencoded<_ReplaceClockIn>(Constant.ROOTURL + URL, _token);
        }
    }

    /// <summary>
    /// 获取代签到数据|返回
    /// </summary>
    public class _ReplaceClockIn : BaseResult
    {
        /// <summary>
        /// 代签到数据
        /// </summary>
        public List<ReplaceClockInInfo> data { get; set; }

        /// <summary>
        /// 代签到数据类
        /// </summary>
        public class ReplaceClockInInfo
        {
            /// <summary>
            /// 部门信息
            /// </summary>
            public string deptName { get; set; }

            /// <summary>
            /// 序号
            /// </summary>
            public int number { get; set; }

            /// <summary>
            /// 设备拥有人姓名（张三）
            /// </summary>
            public string masterUserName { get; set; }

            /// <summary>
            /// 打卡用户openId
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 备注 例如：张三疑似为李四代签到了
            /// </summary>
            public string remark { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public string clockInAddress { get; set; }

            /// <summary>
            /// 用户名（李四）
            /// </summary>
            public string userName { get; set; }

            /// <summary>
            /// 签到日期
            /// </summary>
            public long day { get; set; }

            /// <summary>
            /// 设备编号
            /// </summary>
            public string deviceId { get; set; }

            /// <summary>
            /// 签到时间为毫秒数
            /// </summary>
            public long clockInTime { get; set; }
        }
    }
}
