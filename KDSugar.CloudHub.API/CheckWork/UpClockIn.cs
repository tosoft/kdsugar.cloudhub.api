﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.CheckWork
{
    /// <summary>
    /// 获取用户打卡地点|发起
    /// </summary>
    public class GetUserPosition : PostRequest
    {
        private const string URL = "/gateway/attendance-data/v1/attSet/getUserPositionList?accessToken={0}";

        /// <summary>
        /// 用户编号
        /// </summary>
        public string openId { get; set; }

        /// <summary>
        /// 获取用户打卡地点|初始化
        /// </summary>
        /// <param name="_openId">用户编号</param>
        public GetUserPosition(string _openId)
        {
            openId = _openId;
        }

        /// <summary>
        /// 获取用户打卡地点信息列表
        /// </summary>
        /// <param name="_token"></param>
        /// <returns></returns>
        public _GetUserPosition GetUserPositionList(string _token)
        {
            return Post<_GetUserPosition>(Constant.ROOTURL + URL, _token);
        }
    }

    /// <summary>
    /// 获取用户打卡地点|返回
    /// </summary>
    public class _GetUserPosition : BaseResult
    {
        /// <summary>
        /// 用户打卡地点列表
        /// </summary>
        public List<AttSetInfo> data { get; set; }

        /// <summary>
        /// 用户打卡地点信息类
        /// </summary>
        public class AttSetInfo
        {
            /// <summary>
            /// 签到点名称
            /// </summary>
            public string position { get; set; }

            /// <summary>
            /// 签到点编号
            /// </summary>
            public string positionId { get; set; }

            /// <summary>
            /// 经度
            /// </summary>
            public double lng { get; set; }

            /// <summary>
            /// 纬度
            /// </summary>
            public double lat { get; set; }
        }
    }

    /// <summary>
    /// 单条打卡记录上传|发起
    /// </summary>
    public class SingleUpClockIn : PostRequest
    {
        private const string URL = "/gateway/attendance-data/v1/clockIn/singleUpload?accessToken={0}";

        /// <summary>
        /// 用户编号
        /// </summary>
        public string openId { get; set; }

        /// <summary>
        /// 考勤类型
        /// </summary>
        public int clockType { get; set; }

        /// <summary>
        /// 考勤时间
        /// </summary>
        public long clockInTime { get; set; }

        /// <summary>
        /// 考勤点编号
        /// </summary>
        public string positionId { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string remark { get; set; }

        /// <summary>
        /// 单条打卡记录上传|初始化
        /// </summary>
        /// <param name="_openId">用户编号</param>
        /// <param name="_clockInTime">考勤时间</param>
        /// <param name="_positionId">考勤点编号</param>
        /// <param name="_type">考勤类型，默认为【1-内勤】</param>
        public SingleUpClockIn(string _openId, DateTime _clockInTime, string _positionId, int _type = 1)
        {
            openId = _openId;
            clockInTime = Helper.ToUnixTime(_clockInTime);
            positionId = _positionId;
            clockType = _type;
        }

        /// <summary>
        /// 上传打卡记录
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _UpClockIn UpClockIn(string _token)
        {
            return Post<_UpClockIn>(Constant.ROOTURL + URL, _token);
        }
    }

    /// <summary>
    /// 多条打卡记录上传|发起
    /// <para>注意：最多一次性200条数据</para>
    /// </summary>
    public class MutiUpClockIn : PostRequest
    {
        private const string URL = "/gateway/attendance-data/v1/clockIn/mutiUpload?accessToken={0}";

        /// <summary>
        /// 多条打卡记录信息列表
        /// </summary>
        public List<MutiUpClockInInfo> list { get; set; }

        /// <summary>
        /// 多条打卡记录信息类
        /// </summary>
        public class MutiUpClockInInfo : SingleUpClockIn
        {
            /// <summary>
            /// 【必填】序号.保证每一次调用的唯一性
            /// </summary>
            public int index { get; set; }

            /// <summary>
            /// 多条打卡记录上传|初始化
            /// </summary>
            /// <param name="_index">序号.保证每一次调用的唯一性</param>
            /// <param name="_openId">用户编号</param>
            /// <param name="_clockInTime">考勤时间</param>
            /// <param name="_positionId">考勤点编号</param>
            /// <param name="_type">考勤类型，默认为【1-内勤】</param>
            public MutiUpClockInInfo(int _index, string _openId, DateTime _clockInTime,
                string _positionId, int _type = 1)
                : base(_openId, _clockInTime, _positionId, _type)
            {
                index = _index;
            }
        }

        /// <summary>
        /// 多条打卡记录上传|初始化
        /// </summary>
        public MutiUpClockIn()
        {
            list = new List<MutiUpClockInInfo>();
        }

        /// <summary>
        /// 新增单条打卡记录信息
        /// </summary>
        /// <param name="_info"></param>
        public void AddClockIn(MutiUpClockInInfo _info)
        {
            list.Add(_info);
        }

        /// <summary>
        /// 新增多条打卡记录信息
        /// </summary>
        /// <param name="_list"></param>
        public void AddClockIn(List<MutiUpClockInInfo> _list)
        {
            list.AddRange(_list);
        }

        /// <summary>
        /// 上传多条打卡考勤记录
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _UpClockIn UpClockIn(string _token)
        {
            string tmpUrl = Constant.ROOTURL + URL;
            string tmpJson = Helper.PostWeb(string.Format(tmpUrl, _token), JsonConvert.SerializeObject(list));
            return JsonConvert.DeserializeObject<_UpClockIn>(tmpJson);
        }
    }

    /// <summary>
    /// 上传打卡记录|返回
    /// </summary>
    public class _UpClockIn : BaseResult
    {
        /// <summary>
        /// 打卡记录结果
        /// </summary>
        public UpClockInResult data { get; set; }

        /// <summary>
        /// 打卡记录结果类
        /// </summary>
        public class UpClockInResult
        {
            /// <summary>
            /// 成功信息
            /// </summary>
            public UpClockInSuccessResult success { get; set; }

            /// <summary>
            /// 失败信息
            /// </summary>
            public UpClockInFailureResult failure { get; set; }
        }

        /// <summary>
        /// 成功信息类
        /// </summary>
        public class UpClockInSuccessResult
        {
            /// <summary>
            /// 处理数量
            /// </summary>
            public int count { get; set; }
        }

        /// <summary>
        /// 失败信息类
        /// </summary>
        public class UpClockInFailureResult : UpClockInSuccessResult
        {
            /// <summary>
            /// 失败详情列表
            /// </summary>
            public List<UpClockInDetail> details { get; set; }
        }

        /// <summary>
        /// 失败详情类
        /// </summary>
        public class UpClockInDetail
        {
            /// <summary>
            /// 序号
            /// </summary>
            public int index { get; set; }

            /// <summary>
            /// 处理失败编码
            /// </summary>
            public string errorCode { get; set; }
        }
    }
}
