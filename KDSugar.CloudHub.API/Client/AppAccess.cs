﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Client
{
    /// <summary>
    /// App级别访问
    /// </summary>
    public class AppAccess : BaseAccess
    {
        /// <summary>
        /// 初始化App级别访问
        /// </summary>
        /// <param name="_appId"></param>
        /// <param name="_secret"></param>
        public AppAccess(string _appId,string _secret)
        {
            base.accessToken = new AccessToken(Constant.ScopeType.app);
            base.accessToken.appId = _appId;
            base.accessToken.secret = _secret;
            base.GetTokenInfo();
            OrgData = new Apps.OrgData(this);
        }

        /// <summary>
        /// 组织信息获取模块
        /// </summary>
        public Apps.OrgData OrgData;

        /// <summary>
        /// 验证用户身份
        /// </summary>
        /// <param name="_appId">轻应用APPID</param>
        /// <param name="_ticket">轻应用Ticket</param>
        /// <returns></returns>
        public _LightAppUser.UserInfo VerifyUser(string _appId,string _ticket)
        {
            _LightAppUser.UserInfo result = null;
            LightAppUser lightAppUser = new LightAppUser(_appId, _ticket);
            var resultData = lightAppUser.GetLightAppUser(TokenStr);
            if (!resultData.success)
                throw new ErrorMessage<LightAppUser, _LightAppUser>(lightAppUser, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 生成待办事项
        /// </summary>
        /// <param name="_title">来自字段的内容显示</param>
        /// <param name="_appId">生成的待办所关联的第三方服务类型ID(第三方应用ID)</param>
        /// <param name="_headImg">待办在客户端显示的图URL</param>
        /// <param name="_url">点击待办跳转的URL</param>
        /// <param name="_openIds">待办接收人列表</param>
        /// <param name="_sourceId">生成的待办所关联的第三方服务业务记录的ID，待办的批次号，对应待办处理中的sourceitemid；
        /// <para>其值都第三方服务自行设定，需保证其唯一性。</para></param>
        /// <param name="_sync">待办生成方式，true表示同步，false表示异步，默认为false；
        /// <para>同步生成待办目前批量最多一次只允许生成100条；</para></param>
        /// <returns></returns>
        public Todo._GenerateTodo GenerateTodo(string _title, string _appId, string _headImg,
            string _url, List<string> _openIds, string _sourceId, bool _sync = false)
        {
            Todo._GenerateTodo result = null;
            Todo.GenerateTodo todo = new Todo.GenerateTodo(_title, _appId, _headImg, _url, _sourceId);
            todo.AddReceiveOpenId(_openIds);
            result = todo.Generate(TokenStr, _sync);
            return result;
        }

        /// <summary>
        /// 生成待办事项
        /// </summary>
        /// <param name="todo">发起生成待办事项</param>
        /// <param name="_sync">待办生成方式，true表示同步，false表示异步，默认为false；
        /// <para>同步生成待办目前批量最多一次只允许生成100条；</para></param>
        /// <returns></returns>
        public Todo._GenerateTodo GenerateTodo(Todo.GenerateTodo todo, bool _sync = false)
        {
            Todo._GenerateTodo result = null;
            if (todo == null)
                throw new Exception("发起待办事项需先初始化");
            result = todo.Generate(TokenStr, _sync);
            return result;
        }

        /// <summary>
        /// 变更处理待办事项
        /// </summary>
        /// <param name="todo">变更处理待办事项发起</param>
        /// <param name="_sync">待办生成方式，true表示同步，false表示异步，默认为false；
        /// <para>同步生成待办目前批量最多一次只允许生成100条；</para></param>
        /// <returns></returns>
        public Todo._HandleTodo HandleTodo(Todo.HandleTodo todo, bool _sync = false)
        {
            Todo._HandleTodo result = null;
            if (todo == null)
                throw new Exception("变更待办事项需先初始化");
            result = todo.Operate(TokenStr, _sync);
            return result;
        }

        /// <summary>
        /// 待办事项状态查询
        /// </summary>
        /// <param name="_sourcetype">应用ID，即appId</param>
        /// <param name="_sourceitemid">即sourceId,生成的待办所关联的第三方服务业务记录的ID，是待办的批次号</param>
        /// <param name="_openId">待办接受人ID</param>
        /// <returns></returns>
        public Todo._CheckTodo.CheckInfo CheckTodo(string _sourcetype, string _sourceitemid, string _openId)
        {
            Todo._CheckTodo.CheckInfo result = null;
            Todo.CheckTodo checkTodo = new Todo.CheckTodo(_sourcetype, _sourceitemid, _openId);
            var resultData = checkTodo.Operate(TokenStr);
            if (!resultData.success)
                throw new ErrorMessage<Todo.CheckTodo, Todo._CheckTodo>(checkTodo, resultData);
            result = resultData.data;
            return result;
        }
    }
}
