﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Client.Apps
{
    /// <summary>
    /// App权限功能实现基类
    /// </summary>
    public class BaseApp
    {
        private AppAccess access { get; set; }

        /// <summary>
        /// App权限功能实现基类构造
        /// </summary>
        /// <param name="_access">App权限访问调用</param>
        public BaseApp(AppAccess _access)
        {
            access = _access;
        }

        /// <summary>
        /// App权限Token
        /// </summary>
        public string Token
        {
            get
            {
                return access.TokenStr;
            }
        }
    }
}
