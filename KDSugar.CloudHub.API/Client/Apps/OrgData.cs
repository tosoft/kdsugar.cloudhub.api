﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OInfo = KDSugar.CloudHub.API.AddressList;
using OD = KDSugar.CloudHub.API.AddressList.OrgData;

namespace KDSugar.CloudHub.API.Client.Apps
{
    /// <summary>
    /// 获取组织数据实现
    /// </summary>
    public class OrgData : BaseApp
    {
        /// <summary>
        /// 获取组织数据实现|初始化
        /// </summary>
        /// <param name="_access">access</param>
       public OrgData(AppAccess _access)
            : base(_access)
        {

        }

        /// <summary>
        /// 获取企业所有组织人员
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_dt">
        /// 查询时间，查询这个时刻之后所有变更的数据
        /// <para>若赋值为null，则视为第一次拉取</para>
        /// </param>
        /// <param name="_begin">起始数</param>
        /// <param name="_count">拉取数量</param>
        /// <returns></returns>
        public OD._GetAllPersons.PersonData GetAllPersons(string _eid, DateTime? _dt, int? _begin, int? _count)
        {
            OD._GetAllPersons.PersonData result = null;
            OD.GetAllPersons persons = new OD.GetAllPersons(_eid, _dt, _begin, _count);
            var resultData = persons.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<OD.GetAllPersons, OD._GetAllPersons>(persons, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 获取个人信息
        /// </summary>
        /// <param name="_openId">人员openId</param>
        /// <param name="_eid">企业EID</param>
        /// <returns></returns>
        public List<OInfo.OPerson> GetPerson(string _openId, string _eid)
        {
            List<OInfo.OPerson> result = null;
            OD.GetPerson person = new OD.GetPerson(_openId, _eid);
            var resultData = person.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<OD.GetPerson, OD._GetPerson>(person, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 获取当前部门基本信息或部门负责人
        /// </summary>
        /// <param name="_orgId">部门ID</param>
        /// <param name="_eid">企业EID</param>
        /// <returns></returns>
        public OD._GetDeptInfoAndChargers.DeptInfoChargers GetDeptInfoAndChargers(string _orgId, string _eid)
        {
            OD._GetDeptInfoAndChargers.DeptInfoChargers result = null;
            OD.GetDeptInfoAndChargers getDeptInfo = new OD.GetDeptInfoAndChargers(_orgId, _eid);
            var resultData = getDeptInfo.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<OD.GetDeptInfoAndChargers, OD._GetDeptInfoAndChargers>(getDeptInfo, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 获取企业基本信息
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <returns></returns>
        public OInfo.OCompany GetCompany(string _eid)
        {
            OInfo.OCompany result = null;
            OD.GetCompany company = new OD.GetCompany(_eid);
            var resultData = company.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<OD.GetCompany, OD._GetCompany>(company, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 查询部门信息
        /// </summary>
        /// <param name="_orgIds">组织ID</param>
        /// <param name="_eid">企业EID</param>
        /// <param name="_hasChild">是否查询子部门信息</param>
        /// <param name="_begin">
        /// 分页开始
        /// <para>为null,默认0</para>
        /// </param>
        /// <param name="_count">
        /// 每页数量
        /// <para>为null,默认1000</para>
        /// </param>
        /// <returns></returns>
        public List<OD._GetDeptInfo.DeptInfo> GetDeptInfo(string _orgIds, string _eid, bool _hasChild, int? _begin, int? _count)
        {
            List<OD._GetDeptInfo.DeptInfo> result = null;
            OD.GetDeptInfo deptInfo = new OD.GetDeptInfo(_orgIds, _eid, _hasChild, _begin, _count);
            var resultData = deptInfo.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<OD.GetDeptInfo, OD._GetDeptInfo>(deptInfo, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 获取所有部门列表
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <returns></returns>
        public List<OInfo.OOrg> GetAllDepts(string _eid)
        {
            List<OInfo.OOrg> result = null;
            OD.GetAllDepts getAllDepts = new OD.GetAllDepts(_eid);
            var resultData = getAllDepts.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<OD.GetAllDepts, OD._GetAllDepts>(getAllDepts, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 获取当前部门的所有上级部门列表
        /// </summary>
        /// <param name="_orgId">组织ID</param>
        /// <param name="_eid">企业EID</param>
        /// <returns></returns>
        public List<OInfo.OOrg> GetAllLastDeptByCurDept(string _orgId,string _eid)
        {
            List<OInfo.OOrg> result = null;
            OD.GetAllLastDeptByCurDept getAllLastDept = new OD.GetAllLastDeptByCurDept(_orgId, _eid);
            var resultData = getAllLastDept.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<OD.GetAllLastDeptByCurDept, OD._GetAllLastDeptByCurDept>(getAllLastDept, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 获取当前部门所有下级部门列表
        /// </summary>
        /// <param name="_orgId">组织ID</param>
        /// <param name="_eid">企业EID</param>
        /// <returns></returns>
        public List<OInfo.OOrg> GetAllNextDeptByCurDept(string _orgId,string _eid)
        {
            List<OInfo.OOrg> result = null;
            OD.GetAllNextDeptByCurDept getAllNextDept = new OD.GetAllNextDeptByCurDept(_orgId, _eid);
            var resultData = getAllNextDept.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<OD.GetAllNextDeptByCurDept, OD._GetAllNextDeptByCurDept>(getAllNextDept, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 获取当前部门下一层级的所有部门基本信息列表
        /// </summary>
        /// <param name="_orgId">组织ID</param>
        /// <param name="_eid">企业EID</param>
        /// <returns></returns>
        public List<OInfo.OOrg> GetNextLevDeptByCurDept(string _orgId,string _eid)
        {
            List<OInfo.OOrg> result = null;
            OD.GetNextLevDeptByCurDept getNextLevDept = new OD.GetNextLevDeptByCurDept(_orgId, _eid);
            var resultData = getNextLevDept.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<OD.GetNextLevDeptByCurDept, OD._GetNextLevDeptByCurDept>(getNextLevDept, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 获取用户的默认上级或默认汇报上级或指定上级
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_openId">人员OID</param>
        /// <returns></returns>
        public OD._GetParentPerson.ParentPersonInfo GetParentPerson(string _eid,string _openId)
        {
            OD._GetParentPerson.ParentPersonInfo result = null;
            OD.GetParentPerson getParentPerson = new OD.GetParentPerson(_eid, _openId);
            var resultData = getParentPerson.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<OD.GetParentPerson, OD._GetParentPerson>(getParentPerson, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 通过工作圈eid获取管理员oid
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <returns></returns>
        public OD._GetAdminOIDsByEID.AdminOIDsInfo GetAdminOIDsByEID(string _eid)
        {
            OD._GetAdminOIDsByEID.AdminOIDsInfo result = null;
            OD.GetAdminOIDsByEID getAdminOIDs = new OD.GetAdminOIDsByEID(_eid);
            var resultData = getAdminOIDs.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<OD.GetAdminOIDsByEID, OD._GetAdminOIDsByEID>(getAdminOIDs, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 根据企业eid查询全部合作伙伴信息
        /// </summary>
        /// <param name="partners">参数</param>
        /// <returns></returns>
        public OD._GetPartners.PartnerInfo GetPartners(OD.GetPartners partners)
        {
            OD._GetPartners.PartnerInfo result = null;
            var resultData = partners.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<OD.GetPartners, OD._GetPartners>(partners, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 根据手机号码和工作圈名称查询工作圈信息
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_phone">手机号</param>
        /// <param name="_name">团队名称</param>
        /// <returns></returns>
        public List<OD._SearchNetwork.NetWorkData.NetWorkInfo> SearchNetwork(string _eid, string _phone, string _name)
        {
            List<OD._SearchNetwork.NetWorkData.NetWorkInfo> result = null;
            OD.SearchNetwork search = new OD.SearchNetwork(_eid, _phone, _name);
            var resultData = search.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<OD.SearchNetwork, OD._SearchNetwork>(search, resultData);
            result = resultData.data.newtorks;
            return result;
        }

        /// <summary>
        /// 添加合作伙伴
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_myOid">本人OID</param>
        /// <param name="_partnerEid">合作伙伴EID</param>
        /// <param name="_partnerPhone">合作伙伴手机号</param>
        /// <param name="_errorMsg">返回为false时的错误信息</param>
        /// <returns></returns>
        public bool CreatePartners(string _eid, string _myOid, string _partnerEid, string _partnerPhone,out string _errorMsg)
        {
            OD.CreatePartners createPartners = new OD.CreatePartners(_eid, _myOid, _partnerEid, _partnerPhone);
            var resultData = createPartners.Create(this.Token);
            _errorMsg = !resultData.success ? string.Format("[{0}]-错误：{1}", resultData.errorCode, resultData.error) : null;
            return resultData.success;
        }

        /// <summary>
        /// 删除合作伙伴联系人
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_partnerEid">合作伙伴EID</param>
        /// <param name="_contactId">联系人ID</param>
        /// <param name="_errorMsg">返回为false时的错误信息</param>
        /// <returns></returns>
        public bool DeletePartContacts(string _eid, string _partnerEid, string _contactId,out string _errorMsg)
        {
            OD.DeletePartContacts deletePartContacts = new OD.DeletePartContacts(_eid, _partnerEid, _contactId);
            var resultData = deletePartContacts.Delete(this.Token);
            _errorMsg = !resultData.success ? string.Format("[{0}]-错误：{1}", resultData.errorCode, resultData.error) : null;
            return resultData.success;
        }

        /// <summary>
        /// 按角色id获取人员信息
        /// </summary>
        /// <param name="_eid">企业ID</param>
        /// <param name="_roleIds">角色ID</param>
        /// <returns></returns>
        public List<OD._GetPersonByRoleIds.RoleInfo> GetPersonByRoleIds(string _eid, List<string> _roleIds)
        {
            List<OD._GetPersonByRoleIds.RoleInfo> result = null;
            OD.GetPersonByRoleIds getPersonByRoleIds = new OD.GetPersonByRoleIds(_eid, _roleIds);
            var resultData = getPersonByRoleIds.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<OD.GetPersonByRoleIds, OD._GetPersonByRoleIds>(getPersonByRoleIds, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 根据orgIds获取人员信息
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_orgIds">部门ID</param>
        /// <param name="_isIncSub">是否包含下级部门人员信息</param>
        /// <returns></returns>
        public List<OInfo.OPerson> GetPersonByOrgIds(string _eid, List<string> _orgIds, bool _isIncSub)
        {
            List<OInfo.OPerson> result = null;
            OD.GetPersonByOrgIds getPersonByOrgIds = new OD.GetPersonByOrgIds(_eid, _orgIds, _isIncSub);
            var resultData = getPersonByOrgIds.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<OD.GetPersonByOrgIds, OD._GetPersonByOrgIds>(getPersonByOrgIds, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 获取当前部门成员或部门负责人信息
        /// </summary>
        /// <param name="_eid">企业EID</param>
        /// <param name="_orgId">组织ID</param>
        /// <returns></returns>
        public OD._GetOrgPersons.OrgPersonInfo GetOrgPersons(string _eid,string _orgId)
        {
            OD._GetOrgPersons.OrgPersonInfo result = null;
            OD.GetOrgPersons getOrgPersons = new OD.GetOrgPersons(_eid, _orgId);
            var resultData = getOrgPersons.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<OD.GetOrgPersons, OD._GetOrgPersons>(getOrgPersons, resultData);
            result = resultData.data;
            return result;
        }
    }
}
