﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Client
{
    /// <summary>
    /// 访问基类
    /// </summary>
    public class BaseAccess
    {
        /// <summary>
        /// AccessToken
        /// </summary>
        public AccessToken accessToken { get; set; }

        /// <summary>
        /// Token字符串
        /// </summary>
        public string TokenStr { get; set; }

        /// <summary>
        /// 刷新Token字符串
        /// </summary>
        public string RefreshTokenStr { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime ExpireDate { get; set; }

        /// <summary>
        /// 获取Token信息
        /// </summary>
        protected void GetTokenInfo()
        {
            var info = accessToken.GetToken();
            if (!info.success)
                throw new ErrorMessage(info.error, info.errorCode, accessToken.RequestUrl, accessToken.RequestData);
            if (info.data != null)
            {
                TokenStr = info.data.accessToken;
                ExpireDate = accessToken._timestamp.AddSeconds(info.data.expireIn - 60);
                //ExpireDate = Helper.ToDateTimeFromUnixTime(accessToken.timestamp.ToString())
                //    .AddSeconds(info.data.expireIn - 60);
                RefreshTokenStr = info.data.refreshToken;
            }
            else
            {
                throw new ErrorMessage("未获取到AccessData数据", Constant.ERROR_UNKNOW,
                    accessToken.RequestUrl, accessToken.RequestData);
            }
        }

        /// <summary>
        /// 刷新Token信息
        /// </summary>
        protected void TokenRefresh()
        {
            if (accessToken != null)
            {
                RefreshToken refreshToken = new RefreshToken(accessToken._scope);
                refreshToken.refreshToken = RefreshTokenStr;
                switch (refreshToken._scope)
                {
                    case Constant.ScopeType.app:
                        refreshToken.appId = accessToken.appId;
                        break;
                    case Constant.ScopeType.resGroupSecret:
                        refreshToken.eid = accessToken.eid;
                        break;
                    case Constant.ScopeType.team:
                        refreshToken.appId = accessToken.appId;
                        refreshToken.eid = accessToken.eid;
                        break;
                }
                var info = refreshToken.GetRefreshToken();
                if (!info.success)
                    throw new ErrorMessage(info.error, info.errorCode,
                        refreshToken.RequestUrl, refreshToken.RequestData);
                if (info.data != null)
                {
                    TokenStr = info.data.accessToken;
                    ExpireDate = refreshToken._timestamp.AddSeconds(info.data.expireIn);
                    RefreshTokenStr = info.data.refreshToken;
                }
                else
                {
                    throw new ErrorMessage("未获取到AccessData数据", Constant.ERROR_UNKNOW,
                        refreshToken.RequestUrl, refreshToken.RequestData);
                }
            }
            else
            {
                throw new ErrorMessage("未设置AccessToken", Constant.ERROR_UNKNOW, "", "");
            }
        }
    }
}
