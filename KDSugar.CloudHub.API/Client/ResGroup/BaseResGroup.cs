﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Client.ResGroup
{
    /// <summary>
    /// ResGroup权限功能实现基类
    /// </summary>
    public class BaseResGroup
    {
        private ResGroupSecretAccess access { get; set; }

        /// <summary>
        /// ResGroup权限功能实现基类构造
        /// </summary>
        /// <param name="_access">ResGroup访问调用</param>
        public BaseResGroup(ResGroupSecretAccess _access)
        {
            access = _access;
        }

        /// <summary>
        /// ResGroup权限Token
        /// </summary>
        public string Token
        {
            get
            {
                return access.TokenStr;
            }
        }

        /// <summary>
        /// 调用企业EID
        /// </summary>
        public string EID
        {
            get
            {
                return !string.IsNullOrWhiteSpace(access.accessToken.eid) ? access.accessToken.eid : null;
            }
        }
    }
}
