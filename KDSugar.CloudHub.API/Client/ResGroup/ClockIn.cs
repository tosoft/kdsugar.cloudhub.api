﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Client.ResGroup
{
    /// <summary>
    /// 考勤
    /// </summary>
    public class ClockIn : BaseResGroup
    {
        /// <summary>
        /// 考勤|初始化
        /// </summary>
        /// <param name="_access">ResGroup权限访问调用</param>
        public ClockIn(ResGroupSecretAccess _access)
            : base(_access)
        {

        }

        /// <summary>
        /// 根据更新事件获取签到数据
        /// </summary>
        /// <param name="_clockIn">调用参数</param>
        /// <returns></returns>
        public List<CheckWork._ClockIn.ClockInInfo> GetClockInRecords(CheckWork.ClockIn _clockIn)
        {
            List<CheckWork._ClockIn.ClockInInfo> result = null;
            //if (_clockIn == null)
            //    throw new Exception("参数未初始化");
            var resultData = _clockIn.GetClockInRecord(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<CheckWork.ClockIn, CheckWork._ClockIn>(_clockIn, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 根据具体日期获取签到数据
        /// </summary>
        /// <param name="_clockIn">调用参数</param>
        /// <returns></returns>
        public List<CheckWork._ClockIn.ClockInInfo> GetClockInRecords(CheckWork.ClockInDay _clockIn)
        {
            List<CheckWork._ClockIn.ClockInInfo> result = null;
            //if (_clockIn == null)
            //    throw new Exception("参数未初始化");
            var resultData = _clockIn.GetClockInRecord(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<CheckWork.ClockInDay, CheckWork._ClockIn>(_clockIn, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 获取代签到数据
        /// </summary>
        /// <param name="_clockIn">调用参数</param>
        /// <returns></returns>
        public List<CheckWork._ReplaceClockIn.ReplaceClockInInfo> GetReplaceClockInRecords(CheckWork.ReplaceClockIn _clockIn)
        {
            List<CheckWork._ReplaceClockIn.ReplaceClockInInfo> result = null;
            //if (_clockIn == null)
            //    throw new Exception("参数未初始化");
            var resultData = _clockIn.GetReplaceClockIn(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<CheckWork.ReplaceClockIn, CheckWork._ReplaceClockIn>(_clockIn, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 获取用户打卡地点
        /// </summary>
        /// <param name="_openId">用户OpenId</param>
        /// <returns></returns>
        public List<CheckWork._GetUserPosition.AttSetInfo> GetUserPositionList(string _openId)
        {
            List<CheckWork._GetUserPosition.AttSetInfo> result = null;
            CheckWork.GetUserPosition position = new CheckWork.GetUserPosition(_openId);
            var resultData = position.GetUserPositionList(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<CheckWork.GetUserPosition, CheckWork._GetUserPosition>(position, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 打卡记录上传
        /// </summary>
        /// <param name="_clockIn">单条打卡记录上传参数</param>
        /// <returns></returns>
        public CheckWork._UpClockIn.UpClockInResult UpClockIn(CheckWork.SingleUpClockIn _clockIn)
        {
            CheckWork._UpClockIn.UpClockInResult result = null;
            //if (_clockIn == null)
            //    throw new Exception("参数未初始化");
            var resultData = _clockIn.UpClockIn(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<CheckWork.SingleUpClockIn, CheckWork._UpClockIn>(_clockIn, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 打卡记录上传
        /// </summary>
        /// <param name="_list">多条打卡记录上传参数</param>
        /// <returns></returns>
        public CheckWork._UpClockIn.UpClockInResult UpClockIn(List<CheckWork.MutiUpClockIn.MutiUpClockInInfo> _list)
        {
            CheckWork._UpClockIn.UpClockInResult result = null;
            CheckWork.MutiUpClockIn clockIn = new CheckWork.MutiUpClockIn();
            clockIn.AddClockIn(_list);
            var resultData = clockIn.UpClockIn(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<CheckWork.MutiUpClockIn, CheckWork._UpClockIn>(clockIn, resultData);
            result = resultData.data;
            return result;
        }
    }
}
