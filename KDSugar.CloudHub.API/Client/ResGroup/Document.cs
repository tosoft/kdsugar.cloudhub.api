﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model = KDSugar.CloudHub.API.Document;

namespace KDSugar.CloudHub.API.Client.ResGroup
{
    /// <summary>
    /// 知识中心
    /// </summary>
    public class Document : BaseResGroup
    {
        /// <summary>
        /// 知识中心|初始化
        /// </summary>
        /// <param name="_access"></param>
        public Document(ResGroupSecretAccess _access)
            : base(_access)
        {

        }

        /// <summary>
        /// 创建目录
        /// </summary>
        /// <param name="_preCatalogId">上级目录ID</param>
        /// <param name="_catalogName">目录名称</param>
        /// <param name="_catalogCode">目录编码</param>
        /// <param name="_catalogOrder">目录顺序</param>
        /// <returns></returns>
        public string CreateDirectory(string _preCatalogId, string _catalogName, string _catalogCode, string _catalogOrder)
        {
            string result = null;
            Model.CreateDirectory createDirectory = new Model.CreateDirectory(_preCatalogId, _catalogName, _catalogCode, _catalogOrder);
            var resultData = createDirectory.Create(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<Model.CreateDirectory, Model._CreateDirectory>(createDirectory, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 获取所有的目录列表
        /// </summary>
        /// <param name="para">调用参数</param>
        /// <returns></returns>
        public List<Model._GetAllDirList.DirInfo> GetAllDirList(Model.GetAllDirList para)
        {
            List<Model._GetAllDirList.DirInfo> result = null;
            var resultData = para.GetDirList(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<Model.GetAllDirList, Model._GetAllDirList>(para, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 导入文档
        /// </summary>
        /// <param name="para">参数</param>
        /// <returns></returns>
        public string CreateDocument(Model.CreateDocument para)
        {
            string result = null;
            var resultData = para.Create(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<Model.CreateDocument, Model._CreateDocument>(para, resultData);
            result = resultData.data.documentId;
            return result;
        }

        /// <summary>
        /// 获取文档详情
        /// </summary>
        /// <param name="_documentId">文档ID</param>
        /// <returns></returns>
        public Model._GetDocumentInfo.DocumentInfo GetDocumentInfo(string _documentId)
        {
            Model._GetDocumentInfo.DocumentInfo result = null;
            Model.GetDocumentInfo getDocumentInfo = new Model.GetDocumentInfo(_documentId);
            var resultData = getDocumentInfo.GetInfo(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<Model.GetDocumentInfo, Model._GetDocumentInfo>(getDocumentInfo, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 获取文档列表
        /// </summary>
        /// <param name="_catalogInfoId">目录ID</param>
        /// <param name="_pageIndex">页码</param>
        /// <param name="_pageSize">每页数量</param>
        /// <returns></returns>
        public Model._GetDocumentList.ResultData GetDocumentList(string _catalogInfoId, int _pageIndex, int _pageSize)
        {
            Model._GetDocumentList.ResultData result = null;
            Model.GetDocumentList getDocumentList = new Model.GetDocumentList(_catalogInfoId, _pageIndex, _pageSize);
            var resultData = getDocumentList.GetList(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<Model.GetDocumentList, Model._GetDocumentList>(getDocumentList, resultData);
            result = resultData.data;
            return result;
        }

    }
}
