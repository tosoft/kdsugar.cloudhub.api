﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Client.ResGroup
{
    /// <summary>
    /// 生态圈
    /// </summary>
    public class LinkSpace : BaseResGroup
    {
        /// <summary>
        /// 生态圈|初始化
        /// </summary>
        /// <param name="_access"></param>
        public LinkSpace(ResGroupSecretAccess _access)
            : base(_access)
        {

        }

        /// <summary>
        /// 查询伙伴对应的主企业业务对接人列表
        /// </summary>
        /// <param name="_spaceId">空间id</param>
        /// <param name="_partnerEid">伙伴企业eid</param>
        /// <returns></returns>
        public List<KDSugar.CloudHub.API.LinkSpace._QueryPartnerCharge.PartnerChargeInfo> QueryPartnerCharge(string _spaceId,string _partnerEid)
        {
            List<KDSugar.CloudHub.API.LinkSpace._QueryPartnerCharge.PartnerChargeInfo> result = null;
            KDSugar.CloudHub.API.LinkSpace.QueryPartnerCharge query = new KDSugar.CloudHub.API.LinkSpace.QueryPartnerCharge(_spaceId, _partnerEid);
            var resultData = query.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<KDSugar.CloudHub.API.LinkSpace.QueryPartnerCharge, KDSugar.CloudHub.API.LinkSpace._QueryPartnerCharge>(query, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 查询该用户在生态圈的身份
        /// </summary>
        /// <param name="_spaceId">生态圈空间Id</param>
        /// <param name="_eid">用户所在企业的eid</param>
        /// <param name="_phone">用户手机号</param>
        /// <returns></returns>
        public KDSugar.CloudHub.API.LinkSpace._QueryUserIdentity.UserIdentityInfo QueryUserIdentity(string _spaceId, string _eid, string _phone)
        {
            KDSugar.CloudHub.API.LinkSpace._QueryUserIdentity.UserIdentityInfo result = null;
            KDSugar.CloudHub.API.LinkSpace.QueryUserIdentity query = new KDSugar.CloudHub.API.LinkSpace.QueryUserIdentity(_spaceId, _eid, _phone);
            var resultData = query.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<KDSugar.CloudHub.API.LinkSpace.QueryUserIdentity, KDSugar.CloudHub.API.LinkSpace._QueryUserIdentity>(query, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 获取伙伴企业的负责人列表
        /// </summary>
        /// <param name="_spaceId">空间id</param>
        /// <param name="_partnerEid">伙伴企业eid</param>
        /// <returns></returns>
        public List<KDSugar.CloudHub.API.LinkSpace._QueryPartnerAdmin.PartnerAdminInfo> QueryPartnerAdmin(string _spaceId, string _partnerEid)
        {
            List<KDSugar.CloudHub.API.LinkSpace._QueryPartnerAdmin.PartnerAdminInfo> result = null;
            KDSugar.CloudHub.API.LinkSpace.QueryPartnerAdmin query = new KDSugar.CloudHub.API.LinkSpace.QueryPartnerAdmin(_spaceId, _partnerEid);
            var resultData = query.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<KDSugar.CloudHub.API.LinkSpace.QueryPartnerAdmin, KDSugar.CloudHub.API.LinkSpace._QueryPartnerAdmin>(query, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 查询生态圈内的主企业和伙伴企业间的授权状态
        /// </summary>
        /// <param name="_spaceId">空间id</param>
        /// <param name="_eid">主企业eid</param>
        /// <param name="_partnerEid">伙伴企业eid</param>
        /// <returns></returns>
        public bool? QueryPartnerIdentity(string _spaceId, string _eid, string _partnerEid)
        {
            bool? result = null;
            KDSugar.CloudHub.API.LinkSpace.QueryPartnerIdentity query = new KDSugar.CloudHub.API.LinkSpace.QueryPartnerIdentity(_spaceId, _eid, _partnerEid);
            var resultData = query.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<KDSugar.CloudHub.API.LinkSpace.QueryPartnerIdentity, KDSugar.CloudHub.API.LinkSpace._QueryPartnerIdentity>(query, resultData);
            result = resultData.data.IsAuth;
            return result;
        }

        /// <summary>
        /// 查询主企业所负责的合作伙伴列表
        /// </summary>
        /// <param name="query">调用参数</param>
        /// <returns></returns>
        public KDSugar.CloudHub.API.LinkSpace._QueryPartner.QueryPartnerResult QueryPartner(KDSugar.CloudHub.API.LinkSpace.QueryPartner query)
        {
            KDSugar.CloudHub.API.LinkSpace._QueryPartner.QueryPartnerResult result = null;
            var resultData = query.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<KDSugar.CloudHub.API.LinkSpace.QueryPartner, KDSugar.CloudHub.API.LinkSpace._QueryPartner>(query, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 批量新增伙伴企业
        /// </summary>
        /// <param name="_spaceId">空间id</param>
        /// <param name="list">批量新增的伙伴企业</param>
        /// <returns></returns>
        public List<string> SaveSpacePartner(string _spaceId,List<KDSugar.CloudHub.API.LinkSpace.SaveSpacePartner.SpacePartnerInfo> list)
        {
            List<string> result = null;
            KDSugar.CloudHub.API.LinkSpace.SaveSpacePartner save = new KDSugar.CloudHub.API.LinkSpace.SaveSpacePartner(_spaceId);
            save.AddSpacePartner(list);
            var resultData = save.Save(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<KDSugar.CloudHub.API.LinkSpace.SaveSpacePartner, KDSugar.CloudHub.API.LinkSpace._SaveSpacePartner>(save, resultData);
            result = resultData.data;
            return result;
        }



    }
}
