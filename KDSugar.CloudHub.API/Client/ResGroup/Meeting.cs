﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Client.ResGroup
{
    /// <summary>
    /// 智能会议
    /// </summary>
    public class Meeting : BaseResGroup
    {
        /// <summary>
        /// 智能会议|初始化
        /// </summary>
        /// <param name="_access">ResGroup权限访问调用</param>
        public Meeting(ResGroupSecretAccess _access)
            : base(_access)
        {

        }

        /// <summary>
        /// 新增单个会议
        /// </summary>
        /// <param name="create">调用参数</param>
        /// <returns></returns>
        public string CreateMeeting(KDSugar.CloudHub.API.Meeting.CreateMeeting create)
        {
            string result = null;
            var resultData = create.Create(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<KDSugar.CloudHub.API.Meeting.CreateMeeting,
                    KDSugar.CloudHub.API.Meeting._CreateMeeting>(create, resultData);
            result = resultData.data.meetingId;
            return result;
        }

        /// <summary>
        /// 修改单个会议
        /// </summary>
        /// <param name="meeting">调用参数</param>
        /// <returns></returns>
        public bool ModifyMeeting(KDSugar.CloudHub.API.Meeting.ModifyMeeting meeting)
        {
            bool result = false;
            var resultData = meeting.Operate(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<KDSugar.CloudHub.API.Meeting.ModifyMeeting, BaseResult>(meeting, resultData);
            result = resultData.success;
            return result;
        }

        /// <summary>
        /// 新增重复会议
        /// </summary>
        /// <param name="meeting">调用参数</param>
        /// <returns></returns>
        public KDSugar.CloudHub.API.Meeting._RepeatMeeting.MeetingData CreateRepeatMeeting(KDSugar.CloudHub.API.Meeting.RepeatMeeting meeting)
        {
            KDSugar.CloudHub.API.Meeting._RepeatMeeting.MeetingData result = null;
            var resultData = meeting.Operate(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<KDSugar.CloudHub.API.Meeting.RepeatMeeting, KDSugar.CloudHub.API.Meeting._RepeatMeeting>(meeting, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 批量修改重复日程
        /// </summary>
        /// <param name="meeting">调用参数</param>
        /// <returns></returns>
        public bool ModifyRepeatMeeting(KDSugar.CloudHub.API.Meeting.ModifyRepeatMeeting meeting)
        {
            bool result = false;
            var resultData = meeting.Operate(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<KDSugar.CloudHub.API.Meeting.ModifyRepeatMeeting, BaseResult>(meeting, resultData);
            result = resultData.success;
            return result;
        }

        /// <summary>
        /// 取消单个会议
        /// </summary>
        /// <param name="_id">会议ID</param>
        /// <param name="_openId">会议创建者的oid</param>
        /// <returns></returns>
        public bool CancelMeeting(string _id,string _openId)
        {
            bool result = true;
            KDSugar.CloudHub.API.Meeting.CancelMeeting cancel =
                new KDSugar.CloudHub.API.Meeting.CancelMeeting(_id, _openId);
            var resultData = cancel.Operate(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<KDSugar.CloudHub.API.Meeting.CancelMeeting, BaseResult>(cancel, resultData);
            result = resultData.success;
            return result;
        }

        /// <summary>
        /// 批量取消重复日程
        /// </summary>
        /// <param name="_id">会议ID</param>
        /// <param name="_openid">会议创建者的oid</param>
        /// <param name="_batchId">批量ID(如果是父会议，则没有批量ID，可以不传)</param>
        /// <returns></returns>
        public bool CancelMeeting(string _id, string _openid, string _batchId)
        {
            bool result = false;
            KDSugar.CloudHub.API.Meeting.BatchCancelMeeting batchCancel =
                new KDSugar.CloudHub.API.Meeting.BatchCancelMeeting(_id, _openid, _batchId);
            var resultData = batchCancel.Operate(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<KDSugar.CloudHub.API.Meeting.BatchCancelMeeting, BaseResult>(batchCancel, resultData);
            result = resultData.success;
            return result;
        }

        /// <summary>
        /// 查询会议与会人
        /// </summary>
        /// <param name="_orderId">会议订单id</param>
        /// <returns></returns>
        public List<KDSugar.CloudHub.API.Meeting._QueryActors.ActorInfo> QueryActors(string _orderId)
        {
            List<KDSugar.CloudHub.API.Meeting._QueryActors.ActorInfo> result = null;
            KDSugar.CloudHub.API.Meeting.QueryActors query = new KDSugar.CloudHub.API.Meeting.QueryActors(_orderId);
            var resultData = query.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<KDSugar.CloudHub.API.Meeting.QueryActors, KDSugar.CloudHub.API.Meeting._QueryActors>(query, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 查询闲置会议室
        /// </summary>
        /// <param name="query">查询参数</param>
        /// <returns></returns>
        public List<KDSugar.CloudHub.API.Meeting._QueryFreeRooms.FreeRoom> QueryFreeRooms(KDSugar.CloudHub.API.Meeting.QueryFreeRooms query)
        {
            List<KDSugar.CloudHub.API.Meeting._QueryFreeRooms.FreeRoom> result = null;
            var resultData = query.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<KDSugar.CloudHub.API.Meeting.QueryFreeRooms, KDSugar.CloudHub.API.Meeting._QueryFreeRooms>(query, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 判断该工作圈是否有新增会议预定信息
        /// </summary>
        /// <param name="_eid">必填, 工作圈eid</param>
        /// <param name="_lastTime">选填, 最后的时间,判断该时间节点之后是否有新增预定信息</param>
        /// <returns></returns>
        public bool HasNew(string _eid, DateTime? _lastTime = null)
        {
            bool result = false;
            KDSugar.CloudHub.API.Meeting.HasNewMeeting hasNew = new KDSugar.CloudHub.API.Meeting.HasNewMeeting(_eid, _lastTime);
            var resultData = hasNew.Operate(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<KDSugar.CloudHub.API.Meeting.HasNewMeeting, KDSugar.CloudHub.API.Meeting._HasNewMeeting>(hasNew, resultData);
            result = resultData.data.hasNew;
            return result;
        }

        /// <summary>
        /// 获取该时间节点之后的预定信息
        /// </summary>
        /// <param name="query">调用参数</param>
        /// <returns></returns>
        public KDSugar.CloudHub.API.Meeting._QueryBookInfo.BookInfoList QueryBookInfo(KDSugar.CloudHub.API.Meeting.QueryBookInfo query)
        {
            KDSugar.CloudHub.API.Meeting._QueryBookInfo.BookInfoList result = null;
            var resultData = query.Query(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<KDSugar.CloudHub.API.Meeting.QueryBookInfo, KDSugar.CloudHub.API.Meeting._QueryBookInfo>(query, resultData);
            result = resultData.data;
            return result;
        }

    }
}
