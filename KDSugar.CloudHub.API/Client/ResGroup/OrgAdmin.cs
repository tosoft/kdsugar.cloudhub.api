﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Synchro = KDSugar.CloudHub.API.AddressList.Synchro.OrgAdmin;

namespace KDSugar.CloudHub.API.Client.ResGroup
{
    /// <summary>
    /// 部门负责人模块
    /// </summary>
    public class OrgAdmin : BaseResGroup
    {
        /// <summary>
        /// 初始化部门负责人模块
        /// </summary>
        /// <param name="_access">access</param>
        public OrgAdmin(ResGroupSecretAccess _access)
            : base(_access)
        {

        }

        /// <summary>
        /// 批量设置部门负责人
        /// </summary>
        /// <param name="_list">新增信息</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.SetOrgAdminsError.SetOrgAdminErrorMsg> SetOrgAdmins(List<Synchro.SetOrgAdmins.SetOrgAdminInfo> _list,string _nonce = null)
        {
            List<Synchro.SetOrgAdminsError.SetOrgAdminErrorMsg> result = null;
            Synchro.SetOrgAdmins setOrgAdmins = new Synchro.SetOrgAdmins(_list, this.EID, _nonce);
            var resultData = setOrgAdmins.Operate(this.Token);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 根据组织id批量设置部门负责人
        /// </summary>
        /// <param name="_list">新增信息</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.SetOrgAdminsError.SetOrgAdminErrorMsg> SetOrgAdmins(List<Synchro.SetOrgAdminsById.SetOrgAdminByIdInfo> _list,string _nonce = null)
        {
            List<Synchro.SetOrgAdminsError.SetOrgAdminErrorMsg> result = null;
            Synchro.SetOrgAdminsById setOrgAdminsById = new Synchro.SetOrgAdminsById(_list, this.EID, _nonce);
            var resultData = setOrgAdminsById.Operate(this.Token);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 查询所有部门负责人
        /// </summary>
        /// <param name="para">调用参数</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro._QueryOrgAdmins.QueryOrgAdminInfo> QueryOrgAdmins(Synchro.QueryOrgAdmins para,string _nonce = null)
        {
            List<Synchro._QueryOrgAdmins.QueryOrgAdminInfo> result = null;
            var resultData = para.GetOrgAdmins(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 删除所有部门负责人
        /// </summary>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.DeleteOrgAdminsError.DeleteOrgAdminMsg> DeleteOrgAdmins(string _nonce = null)
        {
            List<Synchro.DeleteOrgAdminsError.DeleteOrgAdminMsg> result = null;
            Synchro.DeleteOrgAdmins deleteOrgAdmins = new Synchro.DeleteOrgAdmins();
            var resultData = deleteOrgAdmins.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 根据删除信息删除部门负责人
        /// </summary>
        /// <param name="_list">删除信息</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.DeleteOrgAdminsError.DeleteOrgAdminMsg> DeleteOrgAdmins(List<Synchro.DeleteOrgAdmins.DeleteOrgAdminInfo> _list, string _nonce = null)
        {
            List<Synchro.DeleteOrgAdminsError.DeleteOrgAdminMsg> result = null;
            Synchro.DeleteOrgAdmins deleteOrgAdmins = new Synchro.DeleteOrgAdmins(_list);
            var resultData = deleteOrgAdmins.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 根据组织ID批量删除部门负责人
        /// </summary>
        /// <param name="_list">删除信息</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.DeleteOrgAdminsError.DeleteOrgAdminMsg> DeleteOrgAdmins(List<Synchro.DeleteOrgAdminsById.DeleteOrgAdminByIdInfo> _list,string _nonce = null)
        {
            List<Synchro.DeleteOrgAdminsError.DeleteOrgAdminMsg> result = null;
            Synchro.DeleteOrgAdminsById deleteOrgAdminsById = new Synchro.DeleteOrgAdminsById(_list);
            var resultData = deleteOrgAdminsById.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 删除全部组织负责人
        /// </summary>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public bool DeleteOrgAdminsByParent(string _nonce=null)
        {
            bool result = false;
            Synchro.DeleteOrgAdminsByParent deleteOrgAdminsByParent = new Synchro.DeleteOrgAdminsByParent();
            var resultData = deleteOrgAdminsByParent.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.success;
            return result;
        }

        /// <summary>
        /// 根据删除信息删除组织负责人
        /// </summary>
        /// <param name="_list">删除信息</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public bool DeleteOrgAdminsByParent(List<Synchro.DeleteOrgAdminsByParent.DeleteOrgAdminByParentInfo> _list,string _nonce = null)
        {
            bool result = false;
            Synchro.DeleteOrgAdminsByParent deleteOrgAdminsByParent = new Synchro.DeleteOrgAdminsByParent(_list);
            var resultData = deleteOrgAdminsByParent.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.success;
            return result;
        }
    }
}
