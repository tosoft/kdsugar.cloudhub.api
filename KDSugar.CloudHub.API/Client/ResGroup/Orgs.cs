﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Synchro = KDSugar.CloudHub.API.AddressList.Synchro.Orgs;

namespace KDSugar.CloudHub.API.Client.ResGroup
{
    /// <summary>
    /// 组织同步模块
    /// </summary>
    public class Orgs : BaseResGroup
    {
        /// <summary>
        /// 初始化组织同步模块
        /// </summary>
        /// <param name="_access"></param>
        public Orgs(ResGroupSecretAccess _access)
            : base(_access)
        {

        }

        /// <summary>
        /// 新增组织
        /// </summary>
        /// <param name="_addInfo">新增【组织/排序码】键值对</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.MsgInfo> AddOrgs(Dictionary<string, int> _addInfo, string _nonce = null)
        {
            List<Synchro.MsgInfo> result = null;
            Synchro.AddOrgs addOrgs = new Synchro.AddOrgs();
            if (_addInfo != null)
            {
                foreach (KeyValuePair<string, int> kv in _addInfo)
                {
                    addOrgs.AddOrgInfo(kv.Key, kv.Value.ToString());
                }
            }
            var resultData = addOrgs.AddOrgsOperate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, Synchro._AddOrgs>(addOrgs.RequestData, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 修改组织名称
        /// </summary>
        /// <param name="_changeInfo">【旧名称，新名称】或者【组织id,新名称】</param>
        /// <param name="_isByOrgId">是否依据组织id修改名称</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.MsgInfo> ChangeOrgName(Dictionary<string, string> _changeInfo, bool _isByOrgId, string _nonce = null)
        {
            List<Synchro.MsgInfo> result = null;
            if (_isByOrgId)
            {
                Synchro.ChangeOrgNameByOrgId changeOrgNameByOrgId = new Synchro.ChangeOrgNameByOrgId();
                if (_changeInfo != null)
                {
                    foreach (KeyValuePair<string, string> kv in _changeInfo)
                    {
                        changeOrgNameByOrgId.AddChangeInfo(new Synchro.ChangeOrgNameByOrgId.ChangeInfo(kv.Key, kv.Value));
                    }
                    var resultData = changeOrgNameByOrgId.ChangeOrgNamesOperate(this.EID, this.Token, _nonce);
                    if (!resultData.success)
                        throw new ErrorMessage<PostRequest, Synchro._ChangeOrgName>(changeOrgNameByOrgId.RequestData, resultData);
                    result = resultData.data;
                }
            }
            else
            {
                Synchro.ChangeOrgName changeOrgName = new Synchro.ChangeOrgName();
                if (_changeInfo != null)
                {
                    foreach (KeyValuePair<string, string> kv in _changeInfo)
                    {
                        changeOrgName.AddChangeOrgNameInfo(new Synchro.ChangeOrgName.ChangeOrgNameInfo(kv.Key, kv.Value));
                    }
                }
                var resultData = changeOrgName.ChangeOrgNamesOperate(this.EID, this.Token, _nonce);
                if (!resultData.success)
                    throw new ErrorMessage<PostRequest, Synchro._ChangeOrgName>(changeOrgName.RequestData, resultData);
                result = resultData.data;
            }
            return result;
        }

        /// <summary>
        /// 删除组织名称
        /// </summary>
        /// <param name="_deleteInfo">长组织或组织id</param>
        /// <param name="_isByOrgId">是否依据组织id删除组织</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.MsgInfo> DeleteOrgs(List<string> _deleteInfo, bool _isByOrgId, string _nonce = null)
        {
            List<Synchro.MsgInfo> result = null;
            if (_isByOrgId)
            {
                Synchro.DeleteOrgsByOrgId deleteOrgsByOrgId = new Synchro.DeleteOrgsByOrgId();
                if (_deleteInfo != null)
                    foreach (string info in _deleteInfo)
                        deleteOrgsByOrgId.AddOrgId(info);
                var resultData = deleteOrgsByOrgId.DeleteOrgsOperate(this.EID, this.Token, _nonce);
                if (!resultData.success)
                    throw new ErrorMessage<PostRequest, Synchro._DeleteOrgs>(deleteOrgsByOrgId.RequestData, resultData);
                result = resultData.data;
            }
            else
            {
                Synchro.DeleteOrgs deleteOrgs = new Synchro.DeleteOrgs();
                if (_deleteInfo != null)
                    foreach (string info in _deleteInfo)
                        deleteOrgs.AddDeleteOrg(info);
                var resultData = deleteOrgs.DeleteOrgsOperate(this.EID, this.Token, _nonce);
                if (!resultData.success)
                    throw new ErrorMessage<PostRequest, Synchro._DeleteOrgs>(deleteOrgs.RequestData, resultData);
                result = resultData.data;
            }
            return result;
        }

        /// <summary>
        /// 跨层次部门挪动
        /// </summary>
        /// <param name="_orgId">待挪动部门id</param>
        /// <param name="_moveToOrgId">挪动到的部门id</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public string MoveOrg(string _orgId, string _moveToOrgId, string _nonce = null)
        {
            string result = null;
            Synchro.MoveOrg moveOrg = new Synchro.MoveOrg(_orgId, _moveToOrgId);
            var resultData = moveOrg.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success && resultData.errorCode != 0 && !string.IsNullOrWhiteSpace(resultData.error))
                throw new ErrorMessage<PostRequest, Synchro._MoveOrg>(moveOrg.RequestData, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 查询所有组织信息
        /// </summary>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.OrgInfo> QueryInfos(string _nonce = null)
        {
            List<Synchro.OrgInfo> result = null;
            Synchro.QueryAllOrgs queryAllOrgs = new Synchro.QueryAllOrgs();
            var resultData = queryAllOrgs.Query(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, Synchro._QueryOrgs>(queryAllOrgs.RequestData, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 查询更新部门信息
        /// </summary>
        /// <param name="_dt">时间点</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.OrgInfo> QueryInfos(DateTime _dt, string _nonce = null)
        {
            List<Synchro.OrgInfo> result = null;
            Synchro.QueryOrgsByUpdate queryOrgsByUpdate = new Synchro.QueryOrgsByUpdate(_dt);
            var resultData = queryOrgsByUpdate.Query(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, Synchro._QueryOrgs>(queryOrgsByUpdate.RequestData, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 根据orgId或department查询组织信息
        /// </summary>
        /// <param name="_ids">查询数组</param>
        /// <param name="_type">查询类型</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.OrgInfo> QueryInfo(Synchro.QueryOrgsById.IdTypeEnum _type, List<string> _ids, string _nonce = null)
        {
            List<Synchro.OrgInfo> result = null;
            Synchro.QueryOrgsById queryOrgsById = new Synchro.QueryOrgsById(_type);
            queryOrgsById.array = _ids;
            var resultData = queryOrgsById.Query(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, Synchro._QueryOrgs>(queryOrgsById.RequestData, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 设置隐藏部门或部门仅可见
        /// </summary>
        /// <param name="_list">设置列表</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro._SetVisualOrg.ErrorInfo> SetOrgSecret(List<Synchro.SetVisualOrg.SetVisualOrgInfo> _list, string _nonce = null)
        {
            List<Synchro._SetVisualOrg.ErrorInfo> result = null;
            Synchro.SetVisualOrg setVisualOrg = new Synchro.SetVisualOrg();
            if (_list != null)
                foreach (Synchro.SetVisualOrg.SetVisualOrgInfo info in _list)
                    setVisualOrg.AddSetVisualOrgInfo(info);
            var resultData = setVisualOrg.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, Synchro._SetVisualOrg>(setVisualOrg.RequestData, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 查询设置隐藏部门或者部门仅可见部门
        /// </summary>
        /// <param name="_type">查询类型</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro._QueryVisualOrg.QueryVisualInfo> QueryOrgSecret(Synchro.QueryVisualOrg.QueryTypeEnum _type, string _nonce = null)
        {
            List<Synchro._QueryVisualOrg.QueryVisualInfo> result = null;
            Synchro.QueryVisualOrg queryVisualOrg = new Synchro.QueryVisualOrg(_type);
            var resultData = queryVisualOrg.Query(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, Synchro._QueryVisualOrg>(queryVisualOrg.RequestData, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 更新组织排序码
        /// </summary>
        /// <param name="_list">更新列表</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.MsgInfo> UpdateWeight(List<Synchro.UpdateOrgWeight.WeightInfo> _list,string _nonce = null)
        {
            List<Synchro.MsgInfo> result = null;
            Synchro.UpdateOrgWeight updateOrgWeight = new Synchro.UpdateOrgWeight();
            if (_list != null)
                foreach (Synchro.UpdateOrgWeight.WeightInfo info in _list)
                    updateOrgWeight.AddWeightInfo(info);
            var resultData = updateOrgWeight.Operate(this.EID, this.Token, _nonce);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 添加/删除机密组织可见部门
        /// </summary>
        /// <param name="_orgId">机密组织部门id</param>
        /// <param name="_operateType">操作类型</param>
        /// <param name="_secretType">机密组织部门对应类型</param>
        /// <param name="_whiteOrgIds">部门白名单的orgId数组</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public bool AddDeleteSecretOrgWhitelist(string _orgId, Synchro.AddDeleteSecretOrgWhitelist.OperateTypeEnum _operateType,
            Synchro.AddDeleteSecretOrgWhitelist.SecretTypeEnum _secretType, List<string> _whiteOrgIds, string _nonce = null)
        {
            bool result = false;
            Synchro.AddDeleteSecretOrgWhitelist addDeleteSecretOrgWhitelist = new Synchro.AddDeleteSecretOrgWhitelist(_orgId,
                _secretType, _operateType);
            addDeleteSecretOrgWhitelist.whiteOrgIds = _whiteOrgIds;
            var resultData = addDeleteSecretOrgWhitelist.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, Synchro._SecretOrg>(addDeleteSecretOrgWhitelist.RequestData, resultData);
            result = resultData.success;
            return result;
        }

    }
}
