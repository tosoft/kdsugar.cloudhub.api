﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Synchro = KDSugar.CloudHub.API.AddressList.Synchro.Person;

namespace KDSugar.CloudHub.API.Client.ResGroup
{
    /// <summary>
    /// 人员同步模块
    /// </summary>
    public class Person : BaseResGroup
    {
        /// <summary>
        /// 初始化人员同步模块
        /// </summary>
        /// <param name="_access"></param>
        public Person(ResGroupSecretAccess _access)
            : base(_access)
        {

        }

        /// <summary>
        /// 获取手机号变更历史记录
        /// </summary>
        /// <param name="_dt">时间戳</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro._ChangePhoneRecords.ChangePhoneRecord> GetChangePhoneRecords(DateTime _dt,string _nonce=null)
        {
            List<Synchro._ChangePhoneRecords.ChangePhoneRecord> result = null;
            Synchro.ChangePhoneRecords changePhoneRecords = new Synchro.ChangePhoneRecords(_dt);
            var resultData = changePhoneRecords.GetChangePhoneRecords(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, Synchro._ChangePhoneRecords>(changePhoneRecords.RequestData, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 修改手机号
        /// </summary>
        /// <param name="_list">手机号修改信息</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro._UpdatePhone.ResultMsg> UpdatePhone(List<Synchro.UpdatePhone.UpdatePhoneInfo> _list,string _nonce = null)
        {
            List<Synchro._UpdatePhone.ResultMsg> result = null;
            Synchro.UpdatePhone updatePhone = new Synchro.UpdatePhone();
            updatePhone.persons = _list;
            var resultData = updatePhone.ChangePhone(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, Synchro._UpdatePhone>(updatePhone.RequestData, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 设置或取消管理员
        /// </summary>
        /// <param name="_account">设置账户（手机号或者是电子邮箱）</param>
        /// <param name="_type">操作类型</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public bool SetAdmin(string _account,Synchro.SetAdmin.OperateTypeEnum _type,string _nonce = null)
        {
            bool result = false;
            Synchro.SetAdmin setAdmin = new Synchro.SetAdmin(_account, _type);
            var resultData = setAdmin.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, BaseResult>(setAdmin.RequestData, resultData);
            result = resultData.success;
            return result;
        }

        /// <summary>
        /// 根据登陆账号和密码获取其是管理员的工作圈列表
        /// </summary>
        /// <param name="_user">账号</param>
        /// <param name="_pass">密码</param>
        /// <returns></returns>
        public List<Synchro._GetAdminCompany.AdminCompany> GetAdminCompany(string _user,string _pass)
        {
            List<Synchro._GetAdminCompany.AdminCompany> result = null;
            Synchro.GetAdminCompany getAdminCompany = new Synchro.GetAdminCompany(_user, _pass);
            var resultData = getAdminCompany.GetAdminCompanyList();
            if (!resultData.success)
                throw new ErrorMessage<Synchro.GetAdminCompany, Synchro._GetAdminCompany>(getAdminCompany, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 删除人员
        /// </summary>
        /// <param name="_openIds">人员的openId数组</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.MsgInfo> DeletePerson(List<string> _openIds,string _nonce = null)
        {
            List<Synchro.MsgInfo> result = null;
            Synchro.DeletePerson deletePerson = new Synchro.DeletePerson(_openIds);
            var resultData = deletePerson.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, Synchro.MsgInfoList>(deletePerson.RequestData, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 添加/删除机密组织可见成员
        /// </summary>
        /// <param name="_orgId">组织id</param>
        /// <param name="_secretType">机密组织类型</param>
        /// <param name="_operateType">操作类型</param>
        /// <param name="_openId">操作用户</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public bool OrgSecretPerson(string _orgId,Synchro.OrgSecretPerson.SecretTypeEnum _secretType,
            Synchro.OrgSecretPerson.OperateTypeEnum _operateType,List<string> _openId,string _nonce = null)
        {
            bool result = false;
            Synchro.OrgSecretPerson orgSecretPerson = new Synchro.OrgSecretPerson(_orgId, _secretType, _operateType, _openId);
            var resultData = orgSecretPerson.Submit(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, BaseResult>(orgSecretPerson.RequestData, resultData);
            result = resultData.success;
            return result;
        }

        /// <summary>
        /// 查询全部在职人员信息
        /// </summary>
        /// <param name="_begin">可选，计数下标</param>
        /// <param name="_count">可选，计数基数</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.PersonInfo> GetPerson(int? _begin,int? _count,string _nonce = null)
        {
            List<Synchro.PersonInfo> result = null;
            Synchro.GetAllPerson getAllPerson = new Synchro.GetAllPerson();
            getAllPerson.begin = _begin;
            getAllPerson.count = _count;
            var resultData = getAllPerson.GetPersonList(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, Synchro.PersonInfoList>(getAllPerson.RequestData, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 查询已更新人员信息
        /// </summary>
        /// <param name="_dt">时间戳</param>
        /// <param name="_begin">可选，计数下标</param>
        /// <param name="_count">可选，计数基数</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.PersonInfo> GetPerson(DateTime _dt,int? _begin,int? _count,string _nonce = null)
        {
            List<Synchro.PersonInfo> result = null;
            Synchro.GetAtTimePerson getAtTimePerson = new Synchro.GetAtTimePerson(_dt);
            getAtTimePerson.begin = _begin;
            getAtTimePerson.count = _count;
            var resultData = getAtTimePerson.GetPersonList(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, Synchro.PersonInfoList>(getAtTimePerson.RequestData, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 查询指定人员信息
        /// </summary>
        /// <param name="_type">查询类型</param>
        /// <param name="_array">手机号码或者openId数组</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.PersonInfo> GetPerson(Synchro.GetPerson.QueryPersonTypeEnum _type,List<string> _array,string _nonce = null)
        {
            List<Synchro.PersonInfo> result = null;
            Synchro.GetPerson getPerson = new Synchro.GetPerson(_type, _array);
            var resultData = getPerson.GetPersonInfo(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, Synchro.PersonInfoList>(getPerson.RequestData, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 更新人员状态
        /// </summary>
        /// <param name="_list">更新人员状态列表</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.MsgInfo> UpdateStatus(List<Synchro.UpdateStatus.UpdateInfo> _list,string _nonce = null)
        {
            List<Synchro.MsgInfo> result = null;
            Synchro.UpdateStatus updateStatus = new Synchro.UpdateStatus(_list);
            var resultData = updateStatus.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, Synchro.MsgInfoList>(updateStatus.RequestData, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 更新人员组织
        /// </summary>
        /// <param name="_list">更新信息列表</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.MsgInfo> UpdateDept(List<Synchro.UpdateDept.UpdateDeptInfo> _list, string _nonce = null)
        {
            List<Synchro.MsgInfo> result = null;
            Synchro.UpdateDept updateDept = new Synchro.UpdateDept(_list);
            var resultData = updateDept.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, Synchro.MsgInfoList>(updateDept.RequestData, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 根据组织id批量更新人员组织
        /// </summary>
        /// <param name="_list">更新信息列表</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.MsgInfo> UpdateDept(List<Synchro.UpdateDeptByDeptId.UpdateDeptInfo> _list,string _nonce = null)
        {
            List<Synchro.MsgInfo> result = null;
            Synchro.UpdateDeptByDeptId updateDeptByDeptId = new Synchro.UpdateDeptByDeptId(_list);
            var resultData = updateDeptByDeptId.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, Synchro.MsgInfoList>(updateDeptByDeptId.RequestData, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 更新人员信息
        /// </summary>
        /// <param name="_list">更新人员信息列表</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.MsgInfo> UpdatePerson(List<Synchro.UpdatePerson.UpdatePersonInfo> _list,string _nonce = null)
        {
            List<Synchro.MsgInfo> result = null;
            Synchro.UpdatePerson updatePerson = new Synchro.UpdatePerson(_list);
            var resultData = updatePerson.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, Synchro.MsgInfoList>(updatePerson.RequestData, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 新增人员信息
        /// </summary>
        /// <param name="_list">新增人员信息列表</param>
        /// <param name="_isNewAdd">是否调用新版新增人员信息接口</param>
        /// <param name="_nonce">非必填，用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro._AddPerson.AddPersonInfo> AddPerson(List<Synchro.AddPerson.AddPersonInfo> _list,bool _isNewAdd,string _nonce = null)
        {
            List<Synchro._AddPerson.AddPersonInfo> result = null;
            Synchro.AddPerson addPerson = new Synchro.AddPerson(_list);
            var resultData = addPerson.Operate(this.EID, this.Token, _isNewAdd, _nonce);
            if (!resultData.success)
                throw new ErrorMessage<PostRequest, Synchro._AddPerson>(addPerson.RequestData, resultData);
            result = resultData.data;
            return result;
        }
    }
}
