﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Synchro = KDSugar.CloudHub.API.AddressList.Synchro.Relations;

namespace KDSugar.CloudHub.API.Client.ResGroup
{
    /// <summary>
    /// 上下级关系模块
    /// </summary>
    public class Relations : BaseResGroup
    {
        /// <summary>
        /// 初始化上下级关系模块
        /// </summary>
        /// <param name="_access">access</param>
        public Relations(ResGroupSecretAccess _access)
            : base(_access)
        {

        }

        /// <summary>
        /// 批量新增上下级关系
        /// </summary>
        /// <param name="_list">新增列表</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro._AddRelations.AddRelationError> AddRelations(List<Synchro.RelationInfo> _list,string _nonce = null)
        {
            List<Synchro._AddRelations.AddRelationError> result = null;
            Synchro.AddRelations addRelations = new Synchro.AddRelations(_list);
            var resultData = addRelations.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 批量删除上下级关系
        /// </summary>
        /// <param name="_list">删除列表</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro._DeleteRelations.DeleteRelationError> DeleteRelationsByBatch(List<Synchro.RelationInfo> _list,string _nonce = null)
        {
            List<Synchro._DeleteRelations.DeleteRelationError> result = null;
            Synchro.DeleteRelations deleteRelations = new Synchro.DeleteRelations(_list);
            var resultData = deleteRelations.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 无条件删除所有上下级关系
        /// </summary>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro._DeleteRelations.DeleteRelationError> DeleteAllRelations(string _nonce = null)
        {
            List<Synchro._DeleteRelations.DeleteRelationError> result = null;
            Synchro.DeleteAllRelations deleteAllRelations = new Synchro.DeleteAllRelations();
            var resultData = deleteAllRelations.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 根据删除列表删除上下级关系
        /// </summary>
        /// <param name="_list">删除列表</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro._DeleteRelations.DeleteRelationError> DeleteAllRelations(List<Synchro.RelationInfo> _list, string _nonce = null)
        {
            List<Synchro._DeleteRelations.DeleteRelationError> result = null;
            Synchro.DeleteAllRelations deleteAllRelations = new Synchro.DeleteAllRelations(_list);
            var resultData = deleteAllRelations.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 查询上下级关系
        /// </summary>
        /// <param name="_para">调用参数</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro.RelationInfo> QueryRelations(Synchro.QueryRelations _para,string _nonce = null)
        {
            List<Synchro.RelationInfo> result = null;
            var resultData = _para.Query(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.data;
            return result;
        }
    }
}
