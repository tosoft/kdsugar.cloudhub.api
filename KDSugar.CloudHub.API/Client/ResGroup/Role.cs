﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Synchro = KDSugar.CloudHub.API.AddressList.Synchro.Role;

namespace KDSugar.CloudHub.API.Client.ResGroup
{
    /// <summary>
    /// 同步角色数据
    /// </summary>
    public class Role : BaseResGroup
    {
        /// <summary>
        /// 同步角色数据
        /// </summary>
        /// <param name="_access">ResGroup权限访问调用</param>
        public Role(ResGroupSecretAccess _access)
            : base(_access)
        {

        }

        /// <summary>
        /// 添加角色标签
        /// </summary>
        /// <param name="_roleName">角色名称</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public string AddRoleTag(string _roleName,string _nonce = null)
        {
            string result = null;
            Synchro.AddRoleTag addRoleTag = new Synchro.AddRoleTag(_roleName);
            var resultData = addRoleTag.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.data.id;
            return result;
        }

        /// <summary>
        /// 获取工作圈角色标签列表
        /// </summary>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro._GetCompanyRoleTag.CompanyRoleTag> GetCompanyRoleTag(string _nonce = null)
        {
            List<Synchro._GetCompanyRoleTag.CompanyRoleTag> result = null;
            Synchro.GetCompanyRoleTag companyRoleTag = new Synchro.GetCompanyRoleTag();
            var resultData = companyRoleTag.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 删除人员角色标签
        /// </summary>
        /// <param name="_roleId">角色id</param>
        /// <param name="_openId">用户openid</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public bool DeletePersonRoleTag(string _roleId,string _openId, string _nonce = null)
        {
            bool result = false;
            Synchro.DeletePersonRoleTag deletePersonRoleTag = new Synchro.DeletePersonRoleTag(_roleId, _openId);
            var resultData = deletePersonRoleTag.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.success;
            return result;
        }

        /// <summary>
        /// 删除角色标签
        /// </summary>
        /// <param name="_roleId">角色id</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public bool DeleteRoleTag(string _roleId,string _nonce = null)
        {
            bool result = false;
            Synchro.DeleteRoleTag deleteRoleTag = new Synchro.DeleteRoleTag(_roleId);
            var resultData = deleteRoleTag.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error, resultData.errorCode);
            result = resultData.success;
            return result;
        }

        /// <summary>
        /// 更改角色标签名字
        /// </summary>
        /// <param name="_roleId">角色id</param>
        /// <param name="_roleName">角色名称</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public bool UpdateRoleTag(string _roleId, string _roleName,string _nonce = null)
        {
            bool result = false;
            Synchro.UpdateRoleTag updateRoleTag = new Synchro.UpdateRoleTag(_roleId, _roleName);
            var resultData = updateRoleTag.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error, resultData.errorCode);
            result = resultData.success;
            return result;
        }

        /// <summary>
        /// 设置人员角色标签
        /// </summary>
        /// <param name="_roleId">角色id</param>
        /// <param name="_openId">用户openId</param>
        /// <param name="_orgIds">作用范围</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public bool SetPersonRoleTag(string _roleId, string _openId, List<string> _orgIds,string _nonce = null)
        {
            bool result = false;
            Synchro.SetPersonRoleTag setPersonRoleTag = new Synchro.SetPersonRoleTag(_roleId, _openId, _orgIds);
            var resultData = setPersonRoleTag.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error, resultData.errorCode);
            result = resultData.success;
            return result;
        }

        /// <summary>
        /// 根据角色获取人员
        /// </summary>
        /// <param name="_roleId">角色id</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        [Obsolete("本接口实现仍可使用，但官方文档中已声明不建议使用此接口")]
        public List<Synchro._GetPersonByRole.PersonRoleInfo> GetPersonByRole(string _roleId,string _nonce = null)
        {
            List<Synchro._GetPersonByRole.PersonRoleInfo> result = null;
            Synchro.GetPersonByRole getPersonByRole = new Synchro.GetPersonByRole(_roleId);
            var resultData = getPersonByRole.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error, resultData.errorCode);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 根据角色Id分页获取人员
        /// </summary>
        /// <param name="para">调用参数</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro._GetPersonByRole.PersonRoleInfo> GetPersonByRole(Synchro.GetPersonByRoleAndPage para,string _nonce=null)
        {
            List<Synchro._GetPersonByRole.PersonRoleInfo> result = null;
            var resultData = para.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error, resultData.errorCode);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 根据用户id批量设置人员角色
        /// </summary>
        /// <param name="para">调用参数</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public bool BatchSetPersonRoleTag(Synchro.BatchSetPersonRoleTagByOpenId para,string _nonce = null)
        {
            bool result = false;
            var resultData = para.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error, resultData.errorCode);
            result = resultData.success;
            return result;
        }

        /// <summary>
        /// 根据角色id批量设置人员角色
        /// </summary>
        /// <param name="para">调用参数</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public bool BatchSetPersonRoleTag(Synchro.BatchSetPersonRoleTagByRoleId para,string _nonce = null)
        {
            bool result = false;
            var resultData = para.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error, resultData.errorCode);
            result = resultData.success;
            return result;
        }
    }
}
