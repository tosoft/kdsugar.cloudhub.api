﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Synchro = KDSugar.CloudHub.API.AddressList.Synchro.TimeJobs;

namespace KDSugar.CloudHub.API.Client.ResGroup
{
    /// <summary>
    /// 同步兼职数据
    /// </summary>
    public class TimeJobs : BaseResGroup
    {
        /// <summary>
        /// 同步兼职数据|初始化
        /// </summary>
        /// <param name="_access">ResGroup权限访问调用</param>
        public TimeJobs(ResGroupSecretAccess _access)
            : base(_access)
        {

        }

        /// <summary>
        /// 根据部门id批量设置兼职
        /// </summary>
        /// <param name="_list">新增列表</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public bool AddTimeJobs(List<Synchro.AddTimeJobsByDept.AddTimeJobsByDeptInfo> _list, string _nonce = null)
        {
            bool result = false;
            Synchro.AddTimeJobsByDept addTimeJobs = new Synchro.AddTimeJobsByDept(_list);
            var resultData = addTimeJobs.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.success;
            return result;
        }

        /// <summary>
        /// 根据部门长名称批量设置兼职
        /// </summary>
        /// <param name="_list">新增列表</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public bool AddTimeJobs(List<Synchro.AddTimeJobsByFullDept.AddTimeJobsByFullDeptInfo> _list, string _nonce = null)
        {
            bool result = false;
            Synchro.AddTimeJobsByFullDept addTimeJobs = new Synchro.AddTimeJobsByFullDept(_list);
            var resultData = addTimeJobs.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.success;
            return result;
        }

        /// <summary>
        /// 根据部门id批量删除兼职
        /// </summary>
        /// <param name="_list">删除列表</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public bool DeleteTimeJobs(List<Synchro.DeleteTimeJobs.DeleteTimeJobInfo> _list, string _nonce = null)
        {
            bool result = false;
            Synchro.DeleteTimeJobs deleteTimeJobs = new Synchro.DeleteTimeJobs(_list);
            var resultData = deleteTimeJobs.Delete(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.success;
            return result;
        }

        /// <summary>
        /// 根据人员与部门删除所有兼职
        /// </summary>
        /// <param name="_list">删除列表</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public bool DeleteTimeJobs(List<Synchro.DeleteAllTimeJobs.DeleteAllTimeJobInfo> _list,string _nonce = null)
        {
            bool result = false;
            Synchro.DeleteAllTimeJobs deleteAllTimeJobs = new Synchro.DeleteAllTimeJobs(_list);
            var resultData = deleteAllTimeJobs.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.success;
            return result;
        }

        /// <summary>
        /// 无条件删除所有兼职
        /// </summary>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public bool DeleteTimeJobs(string _nonce = null)
        {
            bool result = false;
            Synchro.DeleteAllTimeJobs deleteAllTimeJobs = new Synchro.DeleteAllTimeJobs();
            var resultData = deleteAllTimeJobs.Operate(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.success;
            return result;
        }

        /// <summary>
        /// 批量查询兼职
        /// </summary>
        /// <param name="para">调用参数</param>
        /// <param name="_nonce">用于校验重复请求的随机字符串</param>
        /// <returns></returns>
        public List<Synchro._QueryTimeJobs.TimeJobInfo> QueryTimeJobs(Synchro.QueryTimeJobs para, string _nonce = null)
        {
            List<Synchro._QueryTimeJobs.TimeJobInfo> result = null;
            var resultData = para.Query(this.EID, this.Token, _nonce);
            if (!resultData.success)
                throw new ErrorMessage(resultData.error);
            result = resultData.data;
            return result;
        }

    }
}
