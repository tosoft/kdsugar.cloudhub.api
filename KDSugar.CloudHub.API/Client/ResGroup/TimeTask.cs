﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Client.ResGroup
{
    /// <summary>
    /// 时间助手
    /// </summary>
    public class TimeTask : BaseResGroup
    {
        /// <summary>
        /// 时间助手|初始化
        /// </summary>
        /// <param name="_access">ResGroup权限访问调用</param>
        public TimeTask(ResGroupSecretAccess _access)
            : base(_access)
        {

        }

        /// <summary>
        /// 创建单个日程
        /// </summary>
        /// <param name="task">调用参数</param>
        /// <returns></returns>
        public string CreateTimeTask(KDSugar.CloudHub.API.TimeTask.CreateTimeTask task)
        {
            string result = null;
            var resultData = task.Operate(this.Token);
            if (!resultData.success)
                throw new ErrorMessage<KDSugar.CloudHub.API.TimeTask.CreateTimeTask, KDSugar.CloudHub.API.TimeTask._CreateTimeTask>(task, resultData);
            result = resultData.data.workId;
            return result;
        }


    }
}
