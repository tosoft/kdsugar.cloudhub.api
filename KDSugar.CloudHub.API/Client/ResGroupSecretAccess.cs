﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Client
{
    /// <summary>
    /// ResGroup级别访问
    /// </summary>
    public class ResGroupSecretAccess : BaseAccess
    {
        /// <summary>
        /// 初始化ResGroup级别访问
        /// </summary>
        /// <param name="_eid"></param>
        /// <param name="_secret"></param>
        /// <param name="_dt"></param>
        public ResGroupSecretAccess(string _eid, string _secret, DateTime? _dt = null)
        {
            base.accessToken = new AccessToken(Constant.ScopeType.resGroupSecret, _dt);
            base.accessToken.eid = _eid;
            base.accessToken.secret = _secret;
            base.GetTokenInfo();
            ClockIn = new ResGroup.ClockIn(this);
            Meeting = new ResGroup.Meeting(this);
            LinkSpace = new ResGroup.LinkSpace(this);
            Document = new ResGroup.Document(this);
            Role = new ResGroup.Role(this);
            TimeJobs = new ResGroup.TimeJobs(this);
            Relations = new ResGroup.Relations(this);
            OrgAdmin = new ResGroup.OrgAdmin(this);
            Orgs = new ResGroup.Orgs(this);
            Person = new ResGroup.Person(this);
        }

        /// <summary>
        /// 考勤功能实现
        /// </summary>
        public ResGroup.ClockIn ClockIn;

        /// <summary>
        /// 智能会议功能实现
        /// </summary>
        public ResGroup.Meeting Meeting;

        /// <summary>
        /// 生态圈功能实现
        /// </summary>
        public ResGroup.LinkSpace LinkSpace;

        /// <summary>
        /// 知识中心功能实现
        /// </summary>
        public ResGroup.Document Document;

        /// <summary>
        /// 同步角色数据功能实现
        /// </summary>
        public ResGroup.Role Role;

        /// <summary>
        /// 同步兼职数据功能实现
        /// </summary>
        public ResGroup.TimeJobs TimeJobs;

        /// <summary>
        /// 同步上下级关系功能实现
        /// </summary>
        public ResGroup.Relations Relations;

        /// <summary>
        /// 组织负责人功能实现
        /// </summary>
        public ResGroup.OrgAdmin OrgAdmin;

        /// <summary>
        /// 组织同步功能实现
        /// </summary>
        public ResGroup.Orgs Orgs;

        /// <summary>
        /// 人员同步功能实现
        /// </summary>
        public ResGroup.Person Person;

    }
}
