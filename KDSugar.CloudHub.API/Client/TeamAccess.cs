﻿using System;
using System.Collections.Generic;
using System.Text;
using KDSugar.CloudHub.API.Flows;

namespace KDSugar.CloudHub.API.Client
{
    /// <summary>
    /// Team访问
    /// </summary>
    public class TeamAccess : BaseAccess
    {
        /// <summary>
        /// 初始化Team访问
        /// </summary>
        /// <param name="_appId">轻应用id</param>
        /// <param name="_eid">团队id</param>
        /// <param name="_secret">轻应用secret，即appsecret</param>
        public TeamAccess(string _appId,string _eid,string _secret)
        {
            base.accessToken = new AccessToken(Constant.ScopeType.team);
            base.accessToken.appId = _appId;
            base.accessToken.eid = _eid;
            base.accessToken.secret = _secret;
            base.GetTokenInfo();
        }

        /// <summary>
        /// 获取表单模板
        /// </summary>
        /// <param name="_formCodeId">审批模板ID</param>
        /// <returns></returns>
        public _ViewForm._ViewFormData GetViewFormDef(string _formCodeId)
        {
            _ViewForm._ViewFormData result = null;
            ViewFormDef viewFormDef = new ViewFormDef(_formCodeId);
            var resultData = viewFormDef.GetViewFormDef(TokenStr);
            //if (!resultData.success)
            //    throw new ErrorMessage(resultData.error, resultData.errorCode, viewFormDef.RequestUrl, viewFormDef.RequestData);
            if (!resultData.success)
                throw new ErrorMessage<ViewFormDef, _ViewForm>(viewFormDef, resultData);
            if (resultData.data != null)
            {
                result = resultData.data;
            }
            else
            {
                //throw new ErrorMessage("未获取到表单模板数据", Constant.ERROR_UNKNOW, viewFormDef.RequestUrl, viewFormDef.RequestData);
                throw new ErrorMessage<ViewFormDef>("未获取到表单模板数据", Constant.ERROR_UNKNOW, viewFormDef);
            }
            return result;
        }

        /// <summary>
        /// 获取表单实例
        /// </summary>
        /// <param name="_formCodeId"></param>
        /// <param name="_formInstId"></param>
        /// <returns></returns>
        public _ViewForm._ViewFormData GetViewFormInst(string _formCodeId,string _formInstId)
        {
            _ViewForm._ViewFormData result = null;
            ViewFormInst viewFormInst = new ViewFormInst(_formCodeId, _formInstId);
            var resultData = viewFormInst.GetViewFormInst(TokenStr);
            //if (!resultData.success)
            //    throw new ErrorMessage(resultData.error, resultData.errorCode, viewFormInst.RequestUrl, viewFormInst.RequestData);
            if (!resultData.success)
                throw new ErrorMessage<ViewFormInst, _ViewForm>(viewFormInst, resultData);
            if (resultData.data != null)
            {
                result = resultData.data;
            }
            else
            {
                //throw new ErrorMessage("未获取到表单实例数据", Constant.ERROR_UNKNOW, viewFormInst.RequestUrl, viewFormInst.RequestData);
                throw new ErrorMessage<ViewFormInst>("未获取到表单实例数据", Constant.ERROR_UNKNOW, viewFormInst);
            }
            return result;
        }

        /// <summary>
        /// 获取流程记录
        /// </summary>
        /// <param name="_formCodeId"></param>
        /// <param name="_formInstId"></param>
        /// <returns></returns>
        public List<_FlowRecord._FlowRecordInfo> GetFlowRecords(string _formCodeId,string _formInstId)
        {
            List<_FlowRecord._FlowRecordInfo> result = null;
            FlowRecord flowRecord = new FlowRecord(_formCodeId, _formInstId);
            var resultData = flowRecord.GetFlowRecord(TokenStr);
            //if (!resultData.success)
            //    throw new ErrorMessage(resultData.error, resultData.errorCode, flowRecord.RequestUrl, flowRecord.RequestData);
            if (!resultData.success)
                throw new ErrorMessage<FlowRecord, _FlowRecord>(flowRecord, resultData);
            if (resultData.data != null)
            {
                result = resultData.data;
            }
            else
            {
                //throw new ErrorMessage("未获取到流程审批记录", Constant.ERROR_UNKNOW, flowRecord.RequestUrl, flowRecord.RequestData);
                throw new ErrorMessage<FlowRecord>("未获取到流程审批记录", Constant.ERROR_UNKNOW, flowRecord);
            }
            return result;
        }

        /// <summary>
        /// 获取流程列表
        /// </summary>
        /// <param name="findFlows"></param>
        /// <returns></returns>
        public _FindFlows._FindFlowData GetFindFlows(FindFlows findFlows)
        {
            _FindFlows._FindFlowData result = null;
            var resultData = findFlows.GetFindFlows(TokenStr);
            if (!resultData.success)
                throw new ErrorMessage<FindFlows, _FindFlows>(findFlows, resultData);
            if (resultData.data != null)
            {
                result = resultData.data;
            }
            else
            {
                //throw new ErrorMessage("未获取到流程列表信息", Constant.ERROR_UNKNOW, findFlows.RequestUrl, findFlows.RequestData);
                throw new ErrorMessage<FindFlows>("未获取到流程列表信息", Constant.ERROR_UNKNOW, findFlows);
            }
            return result;
        }

        /// <summary>
        /// 获取流程模板列表
        /// </summary>
        /// <param name="_identifyKey">管理员身份识别秘钥</param>
        /// <returns></returns>
        public List<_FlowTemplates._FlowTemplatesClass> GetFlowTemplates(string _identifyKey)
        {
            List<_FlowTemplates._FlowTemplatesClass> result = null;
            FlowTemplates flowTemplates = new FlowTemplates(_identifyKey);
            var resultData = flowTemplates.GetFlowTemplates(TokenStr);
            if (!resultData.success)
                throw new ErrorMessage<FlowTemplates, _FlowTemplates>(flowTemplates, resultData);
            if (resultData.data != null)
            {
                result = resultData.data;
            }
            else
            {
                throw new ErrorMessage<FlowTemplates>("未获取到流程模板列表信息", Constant.ERROR_UNKNOW, flowTemplates);
            }
            return result;
        }

        /// <summary>
        /// 获取流程状态
        /// </summary>
        /// <param name="_flowInstId">流程实例id</param>
        /// <returns></returns>
        public _FlowStatus.FlowStatusEnum GetFlowStatus(string _flowInstId)
        {
            _FlowStatus.FlowStatusEnum result = _FlowStatus.FlowStatusEnum.UNKNOW;
            FlowStatus flowStatus = new FlowStatus(_flowInstId);
            var resultData = flowStatus.GetFlowStatus(TokenStr);
            if (!resultData.success)
                throw new ErrorMessage<FlowStatus, _FlowStatus>(flowStatus, resultData);
            result = resultData.FlowStatusType;
            return result;
        }

        /// <summary>
        /// 获取流程图接口
        /// </summary>
        /// <param name="_flowInstId">流程实例id</param>
        /// <returns></returns>
        public _FlowChart.FlowChartInfo GetFlowChart(string _flowInstId)
        {
            _FlowChart.FlowChartInfo result = null;
            FlowChart flowChart = new FlowChart(_flowInstId);
            var resultData = flowChart.GetFlowChart(TokenStr);
            if (!resultData.success)
                throw new ErrorMessage<FlowChart, _FlowChart>(flowChart, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 获取审批节点审批人信息
        /// </summary>
        /// <param name="_flowInstId">节点实例ID</param>
        /// <param name="_activityCodeId">节点CodeID</param>
        /// <param name="_activityType">节点类型</param>
        /// <param name="_predictFlag">是否预测节点</param>
        /// <returns></returns>
        public _FlowDetailById.FlowDetailByIdInfo GetFlowDetailById(string _flowInstId, string _activityCodeId,
            string _activityType, bool _predictFlag)
        {
            _FlowDetailById.FlowDetailByIdInfo result = null;
            FlowDetailById flowDetailById = new FlowDetailById(_flowInstId, _activityCodeId, _activityType, _predictFlag);
            var resultData = flowDetailById.GetFlowDetailById(TokenStr);
            if (!resultData.success)
                throw new ErrorMessage<FlowDetailById, _FlowDetailById>(flowDetailById, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 创建单据流程
        /// </summary>
        /// <param name="createInst">创建单据流程信息</param>
        /// <returns></returns>
        public CreateInst._CreateInst._CreateInstInfo CreateFormInst(CreateInst createInst)
        {
            CreateInst._CreateInst._CreateInstInfo result = null;
            var resultData = createInst.Create(TokenStr);
            if (!resultData.success)
                throw new ErrorMessage<CreateInst, CreateInst._CreateInst>(createInst, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 修改单据流程
        /// </summary>
        /// <param name="modifyInst">修改单据流程信息</param>
        /// <returns></returns>
        public BaseResult ModifyFormInst(ModifyInst modifyInst)
        {
            BaseResult result = null;
            var resultData = modifyInst.Modify(TokenStr);
            if (!resultData.success)
                throw new ErrorMessage<ModifyInst>(resultData.error, resultData.errorCode, modifyInst);
            result = resultData;
            return result;
        }

        /// <summary>
        /// 获取外部接口日志
        /// </summary>
        /// <param name="pushLog">外部接口日志发起调用</param>
        /// <returns></returns>
        public _PushLog.PushLogList GetPushLog(PushLog pushLog)
        {
            _PushLog.PushLogList result = null;
            var resultData = pushLog.GetPushLog(TokenStr);
            if (!resultData.success)
                throw new ErrorMessage<PushLog, _PushLog>(pushLog, resultData);
            result = resultData.data;
            return result;
        }

        /// <summary>
        /// 调用流程审批控制接口
        /// </summary>
        /// <param name="flowApproval">流程审批控制信息</param>
        /// <returns></returns>
        public BaseResult GetFlowApproval(FlowApproval flowApproval)
        {
            BaseResult result = null;
            var resultData = flowApproval.GetApprovalStatus(TokenStr);
            if (!resultData.success)
                throw new ErrorMessage<FlowApproval>(resultData.error, resultData.errorCode, flowApproval);
            result = resultData;
            return result;
        }
    }
}
