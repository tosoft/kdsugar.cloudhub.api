﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API
{
    /// <summary>
    /// 常量类
    /// </summary>
    public class Constant
    {
        /// <summary>
        /// 未知错误
        /// </summary>
        public const int ERROR_UNKNOW = -65535;

        /// <summary>
        /// 根URL
        /// </summary>
        public const string ROOTURL = "https://www.yunzhijia.com";

        /// <summary>
        /// AccessToken访问类型枚举
        /// </summary>
        public enum ScopeType
        {
            /// <summary>
            /// app
            /// </summary>
            app,
            /// <summary>
            /// resGroupSecret
            /// </summary>
            resGroupSecret,
            /// <summary>
            /// team
            /// </summary>
            team
        }

        /// <summary>
        /// 调用方式
        /// </summary>
        public enum MethodType
        {
            /// <summary>
            /// 以GET形式调用
            /// </summary>
            GET,
            /// <summary>
            /// 以POST形式调用
            /// </summary>
            POST
        }

        /// <summary>
        /// 单据状态枚举
        /// </summary>
        public enum FlowStatusType
        {
            /// <summary>
            /// 审批中
            /// </summary>
            RUNNING,
            /// <summary>
            /// 已挂起
            /// </summary>
            SUSPEND,
            /// <summary>
            /// 待提交
            /// </summary>
            TOSUBMIT,
            /// <summary>
            /// 已完成
            /// </summary>
            FINISH,
            /// <summary>
            /// 不同意
            /// </summary>
            DISAGREE,
            /// <summary>
            /// 已废弃
            /// </summary>
            ABANDON
        }

        /// <summary>
        /// 常用错误类型
        /// </summary>
        public class ErrorMsg
        {
            /// <summary>
            /// 参数未构造错误
            /// </summary>
            public const string PARA_NOTBUILD = "参数未构造";
        }
    }
}
