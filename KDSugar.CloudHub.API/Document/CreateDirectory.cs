﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Document
{
    /// <summary>
    /// 创建目录|发起
    /// </summary>
    public class CreateDirectory : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/api/yzj-info/directory/mng/addDir?accessToken={0}";

        /// <summary>
        /// 上级目录id
        /// </summary>
        public string preCatalogId { get; set; }

        /// <summary>
        /// 目录名称
        /// </summary>
        public string catalogName { get; set; }

        /// <summary>
        /// 目录编码
        /// </summary>
        public string catalogCode { get; set; }

        /// <summary>
        /// 目录顺序
        /// </summary>
        public string catalogOrder { get; set; }

        /// <summary>
        /// 创建目录|初始化
        /// </summary>
        /// <param name="_preCatalogId">上级目录id</param>
        /// <param name="_catalogName">目录名称</param>
        /// <param name="_catalogCode">目录编码</param>
        /// <param name="_catalogOrder">目录顺序</param>
        public CreateDirectory(string _preCatalogId,string _catalogName,string _catalogCode,string _catalogOrder)
        {
            preCatalogId = _preCatalogId;
            catalogName = _catalogName;
            catalogCode = _catalogCode;
            catalogOrder = _catalogOrder;
        }

        /// <summary>
        /// 创建目录
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _CreateDirectory Create(string _token)
        {
            return PostByUrlencoded<_CreateDirectory>(URL, _token);
        }
    }

    /// <summary>
    /// 创建目录|返回
    /// </summary>
    public class _CreateDirectory : BaseResult
    {
        /// <summary>
        /// 目录ID
        /// </summary>
        public string data { get; set; }
    }
}
