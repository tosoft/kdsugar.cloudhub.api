﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.Document
{
    /// <summary>
    /// 导入文档|发起
    /// </summary>
    public class CreateDocument : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/api/yzj-info/document/createForGateWay?accessToken={0}";

        /// <summary>
        /// 标题
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 正文
        /// </summary>
        public string content { get; set; }

        /// <summary>
        /// 封面大图文件ID
        /// </summary>
        public string logoFileId { get; set; }

        /// <summary>
        /// 轮播图文件ID
        /// </summary>
        public string carouselFileId { get; set; }

        /// <summary>
        /// 文档所属目录
        /// </summary>
        public string catalogInfoId { get; set; }

        /// <summary>
        /// ???(未知)
        /// </summary>
        public string summary { get; set; }

        /// <summary>
        /// 是否通过公众号推送消息
        /// </summary>
        public bool pushByPubacc { get; set; }

        /// <summary>
        /// 是否通过短信推送消息
        /// </summary>
        public bool pushBySms { get; set; }

        /// <summary>
        /// 是否开启文档独立权限
        /// </summary>
        public bool customPermission { get; set; }

        /// <summary>
        /// 自定义权限时用到，可见范围是否为所有人
        /// </summary>
        public bool all { get; set; }

        /// <summary>
        /// 自定义创建时间戳, 单位为毫秒。如果没有此参数或者参数为空，则使用接口调用时间为创建时间
        /// </summary>
        public long? customCreateTimeStamp { get; set; }

        ///// <summary>
        ///// 自定义可见成员personId
        ///// </summary>
        //public object personIds { get; set; }

        ///// <summary>
        ///// 自定义权限时用到，包含下级组织(指定组织)  
        ///// </summary>
        //public object subordinateOrgs { get; set; }

        ///// <summary>
        ///// 自定义权限时用到，不包含下级组织(指定组织) 
        ///// </summary>
        //public object notSubordinateOrgs { get; set; }

        ///// <summary>
        ///// 自定义权限时用到，指定角色
        ///// </summary>
        //public object roles { get; set; }

        /// <summary>
        /// 正文类型枚举
        /// </summary>
        public enum ContentTypeEnum
        {
            /// <summary>
            /// 文本
            /// </summary>
            Text = 0,
            /// <summary>
            /// PDF
            /// </summary>
            PDF = 1
        }

        /// <summary>
        /// 正文类型
        /// </summary>
        [JsonIgnore]
        public ContentTypeEnum ContentType { get; set; }

        /// <summary>
        /// 正文类型,type=0普通文本为正文;type=1 pdf为正文
        /// <para>通过枚举属性【ContentType】设置类型</para>
        /// </summary>
        public int type { get { return (int)ContentType; } }

        ///// <summary>
        ///// pdf作为正文时的文件信息, 格式与附件信息一致
        ///// </summary>
        //public object contentAttachments { get; set; }

        /// <summary>
        /// 附件信息类
        /// </summary>
        public class AttachmentInfo
        {
            /// <summary>
            /// 附件文件id
            /// </summary>
            public string fileId { get; set; }

            /// <summary>
            /// MIME类型
            /// </summary>
            public string mimeType { get; set; }

            /// <summary>
            /// 文件名
            /// </summary>
            public string fileName { get; set; }

            /// <summary>
            /// 文件大小(单位为字节)
            /// </summary>
            public long length { get; set; }
        }

        /// <summary>
        /// 附件列表
        /// </summary>
        public List<AttachmentInfo> attachmentList { get; set; }

        /// <summary>
        /// 导入文档|初始化
        /// <para>PDF类初始化</para>
        /// </summary>
        /// <param name="_title">标题</param>
        /// <param name="_catalogInfoId">文档所属目录</param>
        public CreateDocument(string _title,string _catalogInfoId)
        {
            title = _title;
            catalogInfoId = _catalogInfoId;
            ContentType = ContentTypeEnum.PDF;
        }

        /// <summary>
        /// 导入文档|初始化
        /// <para>文本类初始化</para>
        /// </summary>
        /// <param name="_title">标题</param>
        /// <param name="_catalogInfoId">文档所属目录</param>
        /// <param name="_content">正文</param>
        public CreateDocument(string _title,string _catalogInfoId,string _content)
            : this(_title, _catalogInfoId)
        {
            content = _content;
            ContentType = ContentTypeEnum.Text;
        }

        /// <summary>
        /// 导入文档
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _CreateDocument Create(string _token)
        {
            return Post<_CreateDocument>(URL, _token);
        }
    }

    /// <summary>
    /// 导入文档|返回
    /// </summary>
    public class _CreateDocument : BaseResult
    {
        /// <summary>
        /// 返回数据
        /// </summary>
        public ResultData data { get; set; }

        /// <summary>
        /// 返回数据类
        /// </summary>
        public class ResultData
        {
            /// <summary>
            /// 文档id
            /// </summary>
            public string documentId { get; set; }
        }
    }
}
