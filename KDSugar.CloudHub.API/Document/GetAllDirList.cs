﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Document
{
    /// <summary>
    /// 获取所有的目录列表|发起
    /// </summary>
    public class GetAllDirList : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/api/yzj-info/directory/mng/getDirListByIdForGateway?accessToken={0}";

        /// <summary>
        /// 父目录id, 不传此参数则默认为root，表示获取根目录下所有目录
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 分页参数，当前页码，默认为1
        /// </summary>
        public int? pageIndex { get; set; }

        /// <summary>
        /// 分页参数， 每页数据数量，默认为5000
        /// </summary>
        public int? pageCount { get; set; }

        /// <summary>
        /// 获取所有的目录列表
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _GetAllDirList GetDirList(string _token)
        {
            return Get<_GetAllDirList>(URL, _token);
        }
    }

    /// <summary>
    /// 获取所有的目录列表|返回
    /// </summary>
    public class _GetAllDirList : BaseResult
    {
        /// <summary>
        /// 目录列表
        /// </summary>
        public List<DirInfo> data { get; set; }

        /// <summary>
        /// 目录信息类
        /// </summary>
        public class DirInfo
        {
            /// <summary>
            /// 当前目录id
            /// </summary>
            public string id { get; set; }

            /// <summary>
            /// 上层目录id
            /// </summary>
            public string preCatalogId { get; set; }

            /// <summary>
            /// 上层目录名称
            /// </summary>
            public string preCatalogName { get; set; }

            /// <summary>
            /// 当前目录名称
            /// </summary>
            public string catalogName { get; set; }

            /// <summary>
            /// 当前目录编码
            /// </summary>
            public string catalogCode { get; set; }

            /// <summary>
            /// 当前目录顺序
            /// </summary>
            public string catalogOrder { get; set; }

            /// <summary>
            /// ???(未知属性)
            /// </summary>
            public string commentSwitch { get; set; }

            /// <summary>
            /// 允许下载附件0:不允许;1:允许
            /// </summary>
            public string downloadZip { get; set; }

            /// <summary>
            /// 是否显示水印0:不允许;1:允许
            /// </summary>
            public string watermarkSwitch { get; set; }

            /// <summary>
            /// ???(未知)
            /// </summary>
            public string allPreCatalog { get; set; }

            /// <summary>
            /// ???(未知)
            /// </summary>
            public string dateRole { get; set; }

            /// <summary>
            /// ???(未知)
            /// </summary>
            public string restCycle { get; set; }

            /// <summary>
            /// ???(未知)
            /// </summary>
            public string codeNumb { get; set; }

            /// <summary>
            /// ???(未知)
            /// </summary>
            public string codePreview { get; set; }

            /// <summary>
            /// ???(未知)
            /// </summary>
            public int nextCatalogCount { get; set; }

            /// <summary>
            /// 当前目录文档数量(不包含子目录中文档数量)
            /// </summary>
            public int documentCount { get; set; }

            /// <summary>
            /// 目录创建者名称
            /// </summary>
            public string catalogCreator { get; set; }

            /// <summary>
            /// 目录创建时间戳(毫秒)
            /// </summary>
            public long createDate { get; set; }

            /// <summary>
            /// ???(未知)
            /// </summary>
            public string preCatalogCodes { get; set; }

            /// <summary>
            /// ???(未知)
            /// </summary>
            public bool bulletinCatalog { get; set; }
        }
    }
}
