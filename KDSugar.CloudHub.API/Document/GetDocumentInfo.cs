﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.Document
{
    /// <summary>
    /// 获取文档详情|发起
    /// </summary>
    public class GetDocumentInfo : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/api/yzj-info/document/common/detailForGateWay?accessToken={0}";

        /// <summary>
        /// 文档ID
        /// </summary>
        public string documentId { get; set; }

        /// <summary>
        /// 获取文档详情|初始化
        /// </summary>
        /// <param name="_documentId">文档ID</param>
        public GetDocumentInfo(string _documentId)
        {
            documentId = _documentId;
        }

        /// <summary>
        /// 获取文档详情
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _GetDocumentInfo GetInfo(string _token)
        {
            return Get<_GetDocumentInfo>(URL, _token);
        }
    }

    /// <summary>
    /// 获取文档详情|返回
    /// </summary>
    public class _GetDocumentInfo : BaseResult
    {
        /// <summary>
        /// 文档信息
        /// </summary>
        public DocumentInfo data { get; set; }

        /// <summary>
        /// 文档信息类
        /// </summary>
        public class DocumentInfo
        {
            public string id { get; set; }

            /// <summary>
            /// 目录ID
            /// </summary>
            public string catalogInfoId { get; set; }

            /// <summary>
            /// 企业EID
            /// </summary>
            public string eid { get; set; }

            /// <summary>
            /// 文档标题
            /// </summary>
            public string title { get; set; }

            /// <summary>
            /// 文档正文
            /// </summary>
            public string content { get; set; }

            public string summary { get; set; }

            /// <summary>
            /// 文档编号
            /// </summary>
            public string code { get; set; }

            /// <summary>
            /// 封面大图文件ID
            /// </summary>
            public string logoFileId { get; set; }

            /// <summary>
            /// 轮播图文件ID
            /// </summary>
            public string carouselFileId { get; set; }

            /// <summary>
            /// 创建者personId
            /// </summary>
            public string creatorPersonId { get; set; }

            /// <summary>
            /// 文档创建日期(毫秒)
            /// </summary>
            public long createDate { get; set; }

            public string lastUpdatePersonId { get; set; }

            /// <summary>
            /// 文档修改日期(毫秒)
            /// </summary>
            public long updateDate { get; set; }

            public int status { get; set; }

            /// <summary>
            /// 置顶信息 true:被置顶 默认,false:未被置顶
            /// </summary>
            public bool top { get; set; }

            /// <summary>
            /// 是否删除
            /// </summary>
            public bool delete { get; set; }

            /// <summary>
            /// 是否被禁用
            /// </summary>
            public bool forbidden { get; set; }

            ///// <summary>
            ///// 文档附件
            ///// </summary>
            //public List<object> attachmentList { get; set; }

            public bool oldDocument { get; set; }

            public string creatorName { get; set; }

            public string pictureIds { get; set; }

            public int readNumber { get; set; }

            /// <summary>
            /// 0:普通文档  1:pdf为正文的文档
            /// </summary>
            public int type { get; set; }

            ///// <summary>
            ///// pdf等其他特殊形式作为正文时的文件信息 type =1时存在该值
            ///// </summary>
            //public object contentAttachments { get; set; }

            public int attachmentNumber { get; set; }

            public string lastUpdateName { get; set; }

            public int alreadyReadNumber { get; set; }

            public int unReadNumber { get; set; }

            public bool alreadyPush { get; set; }

            public bool pushByPubacc { get; set; }

            public int visibleNumber { get; set; }

            public int downloadZip { get; set; }

            public int watermarkSwitch { get; set; }

            public bool customPermission { get; set; }

            //public object permissions { get; set; }

            //public object documentPermissionsDetail { get; set; }

            public int from { get; set; }

            /// <summary>
            /// 未知用途属性及其值
            /// </summary>
            [JsonExtensionData]
            public Dictionary<string,object> UnknowValues { get; set; }
        }
    }
}
