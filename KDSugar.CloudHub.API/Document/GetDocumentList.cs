﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Document
{
    /// <summary>
    /// 获取文档列表|发起
    /// </summary>
    public class GetDocumentList : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/api/yzj-info/document/list?accessToken={0}";

        /// <summary>
        /// 目录ID
        /// </summary>
        public string catalogInfoId { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public int pageIndex { get; set; }

        /// <summary>
        /// 每页数量
        /// </summary>
        public int pageSize { get; set; }

        /// <summary>
        /// 获取文档列表|初始化
        /// </summary>
        /// <param name="_catalogInfoId">目录ID</param>
        /// <param name="_pageIndex">页码</param>
        /// <param name="_pageSize">每页数量</param>
        public GetDocumentList(string _catalogInfoId,int _pageIndex,int _pageSize)
        {
            catalogInfoId = _catalogInfoId;
            pageIndex = _pageIndex;
            pageSize = _pageSize;
        }

        /// <summary>
        /// 获取文档列表
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _GetDocumentList GetList(string _token)
        {
            return Get<_GetDocumentList>(URL, _token);
        }
    }

    /// <summary>
    /// 获取文档列表|返回
    /// </summary>
    public class _GetDocumentList : BaseResult
    {
        /// <summary>
        /// 返回数据
        /// </summary>
        public ResultData data { get; set; }

        /// <summary>
        /// 返回数据类
        /// </summary>
        public class ResultData
        {
            /// <summary>
            /// 文档数量
            /// </summary>
            public int total { get; set; }

            /// <summary>
            /// 文档列表
            /// </summary>
            public List<object> documentList { get; set; }
        }
    }
}
