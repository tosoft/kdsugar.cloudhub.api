﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API
{
    /// <summary>
    /// 错误信息类
    /// </summary>
    public class ErrorMessage : Exception
    {
        /// <summary>
        /// 请求数据
        /// </summary>
        public string ResponseData { get; set; }

        /// <summary>
        /// 请求地址
        /// </summary>
        public string ResponseUrl { get; set; }

        /// <summary>
        /// 错误码
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// 初始化错误信息类
        /// </summary>
        /// <param name="msg">错误信息</param>
        public ErrorMessage(string msg)
            :base(msg)
        {
            
        }

        /// <summary>
        /// 初始化错误信息类
        /// </summary>
        /// <param name="msg">错误信息</param>
        /// <param name="code">错误码</param>
        public ErrorMessage(string msg,int code)
            : this(msg)
        {
            ErrorCode = code;
        }

        /// <summary>
        /// 初始化错误信息类
        /// </summary>
        /// <param name="msg">错误信息</param>
        /// <param name="code">错误码</param>
        /// <param name="url">请求地址</param>
        public ErrorMessage(string msg,int code,string url)
            : this(msg, code)
        {
            ResponseUrl = url;
        }

        /// <summary>
        /// 初始化错误信息类
        /// </summary>
        /// <param name="msg">错误信息</param>
        /// <param name="code">错误码</param>
        /// <param name="url">请求地址</param>
        /// <param name="data">请求数据</param>
        public ErrorMessage(string msg,int code,string url,string data)
            : this(msg, code, url)
        {
            ResponseData = data;
        }

    }

    /// <summary>
    /// 带请求与返回的错误信息类
    /// </summary>
    /// <typeparam name="RequestPara"></typeparam>
    /// <typeparam name="ResultPara"></typeparam>
    public class ErrorMessage<RequestPara, ResultPara> : ErrorMessage
        where RequestPara : PostRequest where ResultPara : BaseResult
    {
        /// <summary>
        /// 带请求与返回的错误信息类|初始化
        /// </summary>
        /// <param name="requestPara"></param>
        /// <param name="resultPara"></param>
        public ErrorMessage(RequestPara requestPara, ResultPara resultPara)
            : base(resultPara.error, resultPara.errorCode, requestPara.RequestUrl, requestPara.RequestData)
        {

        }
    }

    /// <summary>
    /// 仅带请求的错误信息类
    /// </summary>
    /// <typeparam name="RequestPara"></typeparam>
    public class ErrorMessage<RequestPara> : ErrorMessage
        where RequestPara : PostRequest
    {
        /// <summary>
        /// 仅带请求的错误信息类|初始化
        /// </summary>
        /// <param name="_error"></param>
        /// <param name="_errorCode"></param>
        /// <param name="requestPara"></param>
        public ErrorMessage(string _error, int _errorCode, RequestPara requestPara)
            : base(_error, _errorCode, requestPara.RequestUrl, requestPara.RequestData)
        {

        }
    }
}
