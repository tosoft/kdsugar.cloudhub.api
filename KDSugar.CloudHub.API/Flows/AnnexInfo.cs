﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Flows
{
    /// <summary>
    /// 附件信息
    /// </summary>
    public class AnnexInfo
    {
        /// <summary>
        /// 文件名
        /// </summary>
        public string fileName { get; set; }

        /// <summary>
        /// 文件大小
        /// </summary>
        public string fileSize { get; set; }

        /// <summary>
        /// 文件扩展名
        /// </summary>
        public string fileExt { get; set; }

        /// <summary>
        /// 文件类型
        /// </summary>
        public string fileType { get; set; }

        /// <summary>
        /// 文件ID
        /// </summary>
        public string fileId { get; set; }
    }
}
