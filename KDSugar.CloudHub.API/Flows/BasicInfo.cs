﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Flows
{
    public class BasicInfo
    {
        /// <summary>
        /// 审批节点名称
        /// </summary>
        public string nodeName { get; set; }

        /// <summary>
        /// 团队EID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 表单模板DefId
        /// </summary>
        public string formDefId { get; set; }

        /// <summary>
        /// 数据类型，0=测试数据，1=正式数据
        /// </summary>
        public int dataType { get; set; }

        /// <summary>
        /// 发起人工作圈信息类
        /// </summary>
        public class NetWorkInfo
        {
            public string eid { get; set; }

            public string name { get; set; }
        }

        /// <summary>
        /// 发起人工作圈信息
        /// </summary>
        public NetWorkInfo myNetworkInfo { get; set; }

        /// <summary>
        /// 审批节点类型
        /// </summary>
        public string nodeType { get; set; }

        /// <summary>
        /// 表单模板标题
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 流程实例Id
        /// </summary>
        public string flowInstId { get; set; }

        /// <summary>
        /// 表单模板CodeId
        /// </summary>
        public string formCodeId { get; set; }

        /// <summary>
        /// <para>推送事件触发的操作类型：reach=节点到达，agree=节点同意，submit=节点提交，</para>
        /// <para>delete=单据删除，abandon=单据废弃，withdraw=节点撤回，disagree=不同意。</para>
        /// <para>备注：只有开始节点有撤回事件，开始节点的到达可以视为退回</para>
        /// </summary>
        public string actionType { get; set; }

        /// <summary>
        /// 发起人信息
        /// </summary>
        public PersonInfo myPersonInfo { get; set; }

        /// <summary>
        /// 表单实例Id
        /// </summary>
        public string formInstId { get; set; }

        /// <summary>
        /// 推送时间(开发者选项触发时间)，unix时间戳
        /// </summary>
        public long eventTime { get; set; }

        /// <summary>
        /// 单据发起人部门信息
        /// </summary>
        public DeptInfo myDeptInfo { get; set; }

        /// <summary>
        /// 开发者选项配置的接口id
        /// </summary>
        public string interfaceId { get; set; }

        /// <summary>
        /// 开发者选项配置的接口名
        /// </summary>
        public string interfaceName { get; set; }

        /// <summary>
        /// 是否退回
        /// </summary>
        public bool returned { get; set; }

        /// <summary>
        /// 节点ID
        /// </summary>
        public string nodeId { get; set; }
    }
}
