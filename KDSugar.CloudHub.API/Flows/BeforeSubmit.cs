﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.Flows
{
    /// <summary>
    /// 提交前拦截相关
    /// </summary>
    public class BeforeSubmit
    {
        /// <summary>
        /// 单据实例
        /// </summary>
        _ViewForm ViewInst { get; set; }

        /// <summary>
        /// 初始化提交前拦截
        /// </summary>
        /// <param name="content">密文</param>
        /// <param name="key">密钥</param>
        public BeforeSubmit(string content,string key)
        {
            string viewInstStr = Helper.AESDecrypt(content, key);
            ViewInst = JsonConvert.DeserializeObject<_ViewForm>(viewInstStr);
        }
    }

    /// <summary>
    /// 拦截接口
    /// </summary>
    public class _BeforeSubmit : BaseResult
    {
        /// <summary>
        /// 主表单字段
        /// </summary>
        public Dictionary<string, object> widgetValue { get; set; }

        public class DetailValue
        {
            public List<Dictionary<string, object>> widgetValue { get; set; }
        }

        /// <summary>
        /// 明细表字段
        /// </summary>
        public Dictionary<string, DetailValue> details { get; set; }

        /// <summary>
        /// 拦截接口返回成功
        /// </summary>
        public _BeforeSubmit()
        {
            success = true;
        }

        /// <summary>
        /// 拦截接口返回失败
        /// </summary>
        /// <param name="_error"></param>
        /// <param name="_code"></param>
        public _BeforeSubmit(string _error, int _code = -1)
        {
            success = false;
            error = _error;
            errorCode = _code;
        }

        /// <summary>
        /// 输出json字符串
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
