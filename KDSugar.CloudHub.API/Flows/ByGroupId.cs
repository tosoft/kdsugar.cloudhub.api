﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Flows
{
    public class ByGroupId : PostRequest
    {
        public string groupId { get; set; }

        /// <summary>
        /// 互联控件ID枚举
        /// </summary>
        public enum GroupIdEnum
        {
            /// <summary>
            /// 旧请假互联控件
            /// </summary>
            _S_INTERNET_LEAVE,
            /// <summary>
            /// 新请假互联控件
            /// </summary>
            _S_INTERNET_LEAVE2,
            /// <summary>
            /// 加班互联控件
            /// </summary>
            _S_INTERNET_OVERTIME,
            /// <summary>
            /// 出差互联控件
            /// </summary>
            _S_INTERNET_ON_BUSINESS,
            /// <summary>
            /// 外出互联控件
            /// </summary>
            _S_INTERNET_OUT,
            /// <summary>
            /// 签到补卡
            /// </summary>
            _S_INTERNET_SIGN_SUPPLEMENT,
            /// <summary>
            /// 发文互联控件
            /// </summary>
            _S_INTERNET_SENDFILE,
            /// <summary>
            /// 收文互联控件
            /// </summary>
            _S_INTERNET_RECEIVEFILE,
            /// <summary>
            /// 电子发票互联控件
            /// </summary>
            _S_INTERNET_INVOICE
        }

        public Pageable pageable { get; set; }

        public ByGroupId(string _groupId,Pageable _pageable)
        {
            groupId = _groupId;
            pageable = _pageable;
        }

        public ByGroupId(GroupIdEnum _groupId,Pageable _pageable)
        {
            groupId = Enum.GetName(typeof(GroupIdEnum), _groupId);
            pageable = _pageable;
        }

        private const string URL = "https://yunzhijia.com/gateway/workflow/form/thirdpart/getByGroupId?accessToken={0}";

        public _ByGroupId GetByGroupId(string token)
        {
            return base.Post<_ByGroupId>(URL, token);
        }
    }

    public class _ByGroupId : BaseResult
    {
        public _DataInfo data { get; set; }

        public class _DataInfo
        {
            public _TemplatesInfo formTemplates { get; set; }

            public _BasicInfo basicInfo { get; set; }
        }

        public class _TemplatesInfo
        {
            public int rowsCount { get; set; }

            public List<_TemplatesList> list { get; set; }

            public class _TemplatesList
            {
                public string codeId { get; set; }

                public List<string> groupIds { get; set; }

                public string description { get; set; }

                public string id { get; set; }

                public string title { get; set; }

                public int version { get; set; }

                public int order { get; set; }
            }
        }

        public class _BasicInfo
        {
            public _NetworkInfo networkInfo { get; set; }

            public class _NetworkInfo
            {
                public string eid { get; set; }
            }
        }
    }
}
