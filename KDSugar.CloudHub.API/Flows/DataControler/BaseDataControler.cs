﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Flows.DataControler
{
    /// <summary>
    /// 数据互联控件基类
    /// </summary>
    public class BaseDataControler
    {
        //[ColumnInfo(colEnName = "id")]
        //public string Id { get; set; }

        /// <summary>
        /// 输出数据列字段信息
        /// </summary>
        /// <returns></returns>
        public List<ColumnInfo> ToColumnInfos()
        {
            List<ColumnInfo> result = new List<ColumnInfo>();
            Type type = GetType();
            foreach(var p in type.GetProperties())
            {
                foreach(Attribute a in p.GetCustomAttributes(false))
                {
                    if (a.GetType().Name.Equals(typeof(ColumnInfo).Name))
                    {
                        ColumnInfo info = (ColumnInfo)a;
                        if (info != null)
                            result.Add(info);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 输出数据列键值信息
        /// </summary>
        /// <returns></returns>
        public Dictionary<string,object> ToKeyValue()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            Type type = GetType();
            foreach(var p in type.GetProperties())
            {
                foreach(Attribute a in p.GetCustomAttributes(false))
                {
                    if (a.GetType().Name.Equals(typeof(ColumnInfo).Name))
                    {
                        ColumnInfo columnInfo = (ColumnInfo)a;
                        if (columnInfo != null)
                            result.Add(columnInfo.colEnName, p.GetValue(this, null));
                    }
                }
            }
            return result;
        }
    }
}
