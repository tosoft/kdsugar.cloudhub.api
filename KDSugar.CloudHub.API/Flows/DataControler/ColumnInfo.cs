﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.Flows.DataControler
{
    /// <summary>
    /// 数据列信息
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = true)]
    public class ColumnInfo : Attribute
    {
        /// <summary>
        /// (系统自带)标识符
        /// </summary>
        [JsonIgnore]
        public override object TypeId => base.TypeId;

        /// <summary>
        /// 列Id
        /// </summary>
        public string colEnName { get; set; }

        /// <summary>
        /// 列名称
        /// </summary>
        public string colZhName { get; set; }

        /// <summary>
        /// 控件类型(默认单行文本框)
        /// </summary>
        [JsonIgnore]
        public WidgetTypes WidgetType = WidgetTypes.textWidget;

        /// <summary>
        /// 控件类型
        /// </summary>
        public string widgetType
        {
            get
            {
                return !colEnName.Equals("id") ? Enum.GetName(typeof(WidgetTypes), WidgetType) : null;
            }
        }

        /// <summary>
        /// 在控件类型为日期控件的时候应显示的日期格式(默认为不设时间格式)
        /// </summary>
        [JsonIgnore]
        public DateFormatEnum DateFormat = DateFormatEnum.NotDateFormat;

        /// <summary>
        /// 在控件类型为日期控件的时候应显示的日期格式
        /// </summary>
        public string dateFormat
        {
            get
            {
                string tmpFormat = null;
                switch (DateFormat)
                {
                    case DateFormatEnum.yyyyMM:
                        tmpFormat = "yyyy-MM";
                        break;
                    case DateFormatEnum.yyyyMMdd:
                        tmpFormat = "yyyy-MM-dd";
                        break;
                    case DateFormatEnum.yyyyMMddHHmm:
                        tmpFormat = "yyyy-MM-dd HH:mm";
                        break;
                    case DateFormatEnum.HHmm:
                        tmpFormat = "HH:mm";
                        break;
                    default:
                        tmpFormat = null;
                        break;
                }
                return tmpFormat;
            }
        }

        /// <summary>
        /// 表单选值是否显示列名；（注意：只能有一个列显示为True）
        /// </summary>
        [JsonIgnore]
        public bool ShowName = false;

        /// <summary>
        /// 表单选值是否显示列名；（注意：只能有一个列显示为True）
        /// </summary>
        public string showName
        {
            get
            {
                return ShowName ? ShowName.ToString().ToLower() : null;
            }
        }

        /// <summary>
        /// 控件类型枚举
        /// </summary>
        public enum WidgetTypes
        {
            /// <summary>
            /// 单行文本框
            /// </summary>
            textWidget,
            /// <summary>
            /// 多行文本框
            /// </summary>
            textAreaWidget,
            /// <summary>
            /// 数字输入框
            /// </summary>
            numberWidget,
            /// <summary>
            /// 金额输入框
            /// </summary>
            moneyWidget,
            /// <summary>
            /// 单选框
            /// </summary>
            radioWidget,
            /// <summary>
            /// 多选框
            /// </summary>
            checkboxWidget,
            /// <summary>
            /// 日期控件
            /// </summary>
            dateWidget,
            /// <summary>
            /// 人员控件
            /// </summary>
            personSelectWidget,
            /// <summary>
            /// 部门控件
            /// </summary>
            departmentSelectWidget
        }

        /// <summary>
        /// 日期格式枚举
        /// </summary>
        public enum DateFormatEnum
        {
            /// <summary>
            /// 年月
            /// </summary>
            yyyyMM,
            /// <summary>
            /// 年月日
            /// </summary>
            yyyyMMdd,
            /// <summary>
            /// 年月日时分
            /// </summary>
            yyyyMMddHHmm,
            /// <summary>
            /// 时分
            /// </summary>
            HHmm,
            /// <summary>
            /// 不设时间格式
            /// </summary>
            NotDateFormat
        }

    }
}
