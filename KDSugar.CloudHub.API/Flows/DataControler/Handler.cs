﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.Flows.DataControler
{
    /// <summary>
    /// 数据互联控件处理类
    /// </summary>
    public class Handler
    {
        /// <summary>
        /// 获取到的实例信息
        /// </summary>
        public _FormInst FormInst { get; set; }

        /// <summary>
        /// 数据互联控件处理化
        /// </summary>
        /// <param name="srcContent">获取到的源内容</param>
        public Handler(string srcContent)
        {
            FormInst = JsonConvert.DeserializeObject<_FormInst>(srcContent);
        }

        /// <summary>
        /// 装载互联控件信息(仅输出字段格式)
        /// <para>注意：该函数仅输出互联控件字段格式信息，不输出实际数据</para>
        /// <para>若要调用输出实际数据，请调用InitDataControl(data,_rowsCount)</para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="para">互联控件类参数</param>
        /// <returns></returns>
        private _ResultPack InitDataControl<T>(T para) where T : BaseDataControler
        {
            PageList pageList = new PageList(FormInst.curPage, 1, 1);
            if (para != null)
            {
                pageList.colList = para.ToColumnInfos();
                pageList.dataList.Add(para.ToKeyValue());
            }
            else
            {
                throw new Exception("互联控件类参数para不得为NULL值");
            }
            return new _ResultPack(pageList);
        }

        /// <summary>
        /// 装载互联控件信息(仅输出字段格式)
        /// <para>注意：该函数仅输出互联控件字段格式信息，不输出实际数据</para>
        /// <para>若要调用输出实际数据，请调用InitDataControl(data,_rowsCount)</para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public _ResultPack InitDataControl<T>() where T : BaseDataControler
        {
            PageList pageList = new PageList(FormInst.curPage, 1, 1);
            T t = default(T);
            pageList.colList = t.ToColumnInfos();
            return new _ResultPack(pageList);
        }

        /// <summary>
        /// 装载数据互联控件信息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data">装载数据信息</param>
        /// <param name="_rowsCount">总条数(可任意填写)</param>
        public _ResultPack InitDataControl<T>(List<T> data,int _rowsCount) where T : BaseDataControler
        {
            int pageCount = _rowsCount / FormInst.pageSize;
            if ((_rowsCount % FormInst.pageSize) > 0)
                pageCount++;
            PageList pageList = new PageList(FormInst.curPage, _rowsCount, pageCount);
            if (data != null && data.Count > 0)
            {
                pageList.colList = data[0].ToColumnInfos();
                foreach (var val in data)
                    pageList.dataList.Add(val.ToKeyValue());
            }
            else
            {
                throw new Exception("data参数不得为NULL或是数量为0");
            }
            _ResultPack result = new _ResultPack(pageList);
            return result;
        }
    }

    /// <summary>
    /// 单据实例信息
    /// </summary>
    public class _FormInst
    {
        /// <summary>
        /// 是否为校验接口调用的,true是校验接口（模板设计时）,false是运行时取值（填单时）
        /// </summary>
        public bool verification { get; set; }

        /// <summary>
        /// 当前页码
        /// </summary>
        public int curPage { get; set; }

        /// <summary>
        /// 搜索关键字
        /// </summary>
        public string keyword { get; set; }

        /// <summary>
        /// 每页记录条数
        /// </summary>
        public int pageSize { get; set; }

        /// <summary>
        /// 数据互联控件唯一标识
        /// </summary>
        public string widgetID { get; set; }

        /// <summary>
        /// 主表单对象
        /// </summary>
        public Dictionary<string,object> basicFormInfo { get; set; }

        /// <summary>
        /// 数据互联控件所在明细当前行的对象（如不在明细行则数据为空）
        /// </summary>
        public Dictionary<string,object> rowInfo { get; set; }

        /// <summary>
        /// 云之家身份对象信息类
        /// </summary>
        public class YZJInfo
        {
            /// <summary>
            /// 团队EID
            /// </summary>
            public string eid { get; set; }

            /// <summary>
            /// 用户OID
            /// </summary>
            public string oid { get; set; }

            /// <summary>
            /// 工号
            /// </summary>
            public string jobNo { get; set; }

            /// <summary>
            /// 姓名
            /// </summary>
            public string name { get; set; }
        }

        /// <summary>
        /// 云之家身份信息
        /// </summary>
        public YZJInfo yzjInfo { get; set; }

        /// <summary>
        /// 明细表单键值类
        /// </summary>
        public class WidgetValue
        {
            /// <summary>
            /// 明细表单具体键值
            /// </summary>
            public List<Dictionary<string,object>> widgetValue { get; set; }
        }

        /// <summary>
        /// 明细表单对象
        /// </summary>
        public Dictionary<string,WidgetValue> detailFormInfo { get; set; }
    }

    /// <summary>
    /// 互联控件返回
    /// </summary>
    public class _ResultPack : BaseResult
    {
        /// <summary>
        /// 分页数据
        /// </summary>
        public PageList pageList { get; set; }

        /// <summary>
        /// 初始化互联控件返回信息
        /// </summary>
        /// <param name="_pageList">分页数据信息</param>
        public _ResultPack(PageList _pageList)
        {
            pageList = _pageList;
        }

        /// <summary>
        /// 初始化互联控件返回错误信息
        /// </summary>
        /// <param name="_errorCode">错误码</param>
        /// <param name="_error">错误信息</param>
        public _ResultPack(int _errorCode,string _error)
            : base(_errorCode, _error)
        {
            pageList = null;
        }

        /// <summary>
        /// 输出json字符串
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
