﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Flows.DataControler
{
    public interface IBaseDataControler
    {
        [ColumnInfo(colEnName = "id")]
        string Id { get; set; }
    }
}
