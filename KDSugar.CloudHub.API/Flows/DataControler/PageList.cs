﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Flows.DataControler
{
    /// <summary>
    /// 互联控件返回数据类
    /// </summary>
    public class PageList
    {
        /// <summary>
        /// 总页数
        /// </summary>
        public int pageCount { get; set; }

        /// <summary>
        /// 当前页
        /// </summary>
        public int curPage { get; set; }

        /// <summary>
        /// 总条数
        /// </summary>
        public int rowsCount { get; set; }

        /// <summary>
        /// 初始化互联控件返回数据信息
        /// </summary>
        /// <param name="_curPage">当前页</param>
        /// <param name="_rowsCount">总条数</param>
        /// <param name="_pageCount">总页数</param>
        public PageList(int _curPage,int _rowsCount, int _pageCount)
        {
            curPage = _curPage;
            rowsCount = _rowsCount;
            pageCount = _pageCount;
            colList = new List<ColumnInfo>();
            dataList = new List<Dictionary<string, object>>();
        }

        /// <summary>
        /// 列信息
        /// </summary>
        public List<ColumnInfo> colList { get; set; }

        /// <summary>
        /// 列表每行及每列的数据
        /// </summary>
        public List<Dictionary<string,object>> dataList { get; set; }

        /// <summary>
        /// 默认列表信息
        /// </summary>
        public List<Dictionary<string,object>> defaultValue
        {
            get
            {
                List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
                if (dataList.Count > 0)
                {
                    result.Add(dataList[0]);
                }
                return result;
            }
        }
    }
}
