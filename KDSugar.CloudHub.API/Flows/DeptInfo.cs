﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Flows
{
    /// <summary>
    /// 部门信息
    /// </summary>
    public class DeptInfo
    {
        /// <summary>
        /// 部门名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 部门Id
        /// </summary>
        public string orgId { get; set; }
    }
}
