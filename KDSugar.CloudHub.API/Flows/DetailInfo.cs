﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Flows
{
    /// <summary>
    /// 明细控件信息
    /// </summary>
    public class DetailInfo
    {
        public string buttonName { get; set; }

        public string codeId { get; set; }

        public string title { get; set; }

        public string type { get; set; }

        public Dictionary<string,ControlInfo> widgetVos { get; set; }

        public List<Dictionary<string,object>> widgetValue { get; set; }

        public object extendFieldMap { get; set; }
    }
}
