﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Flows.FieldControlType
{
    /// <summary>
    /// 金额控件
    /// </summary>
    public class AmountWidget : BaseFieldControl
    {
        /// <summary>
        /// 精度
        /// </summary>
        public int decimalDigit { get; set; }

        /// <summary>
        /// 是否以千分位展示
        /// </summary>
        public bool displayThousand { get; set; }

        /// <summary>
        /// 合计名称
        /// </summary>
        public string detailCountName { get; set; }

        /// <summary>
        /// （未知属性）
        /// </summary>
        public object moneyWords { get; set; }

        /// <summary>
        /// 展示大写
        /// </summary>
        public bool displayAmountWords { get; set; }

        /// <summary>
        /// 金额（字符串的）
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// (未知属性)
        /// </summary>
        public bool detailCount { get; set; }
    }
}
