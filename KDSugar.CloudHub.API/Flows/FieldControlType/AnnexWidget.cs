﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Flows.FieldControlType
{
    /// <summary>
    /// 附件控件
    /// </summary>
    public class AnnexWidget : BaseFieldControl
    {
        /// <summary>
        /// 附件数量
        /// </summary>
        public int maximum { get; set; }

        /// <summary>
        /// 附件数量
        /// </summary>
        public List<AnnexInfo> value { get; set; }
    }
}
