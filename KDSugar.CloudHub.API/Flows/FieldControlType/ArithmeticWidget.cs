﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Flows.FieldControlType
{
    /// <summary>
    /// 运算控件
    /// </summary>
    public class ArithmeticWidget : BaseFieldControl
    {
        /// <summary>
        /// 是否以千分位展示
        /// </summary>
        public bool displayThousand { get; set; }

        /// <summary>
        /// 总运算控件名称
        /// </summary>
        public string detailCountName { get; set; }

        /// <summary>
        /// 运算表达式信息
        /// </summary>
        public class ExpressionInfo
        {
            /// <summary>
            /// 表达式类型
            /// </summary>
            public string type { get; set; }

            /// <summary>
            /// 表达式值
            /// </summary>
            public string value { get; set; }
        }

        /// <summary>
        /// 运算表达式
        /// </summary>
        public List<ExpressionInfo> expressionReality { get; set; }

        /// <summary>
        /// (未知)
        /// </summary>
        public object words { get; set; }

        /// <summary>
        /// 是否展示大写
        /// </summary>
        public bool displayAmountWords { get; set; }

        /// <summary>
        /// 数字精度
        /// </summary>
        public int decimalDigit { get; set; }

        /// <summary>
        /// 运算值
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// 是否明细合计
        /// </summary>
        public bool detailCount { get; set; }
    }
}
