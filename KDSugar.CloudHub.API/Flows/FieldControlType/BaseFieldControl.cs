﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.Flows.FieldControlType
{
    /// <summary>
    /// 字段控件基类
    /// </summary>
    public class BaseFieldControl
    {
        /// <summary>
        /// 控件CodeId
        /// </summary>
        public string codeId { get; set; }

        /// <summary>
        /// 控件标题
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 控件类型
        /// </summary>
        public string type { get; set; }

        ///// <summary>
        ///// 控件值
        ///// </summary>
        //public object value { get; set; }

        /// <summary>
        /// 控件扩展字段
        /// </summary>
        public Dictionary<string, object> extendFieldMap { get; set; }

        /// <summary>
        /// 其他扩展字段
        /// </summary>
        [JsonExtensionData]
        public Dictionary<string, object> ExtendField { get; set; }
    }
}
