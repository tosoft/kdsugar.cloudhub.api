﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.Flows.FieldControlType
{
    /// <summary>
    /// 多选框
    /// </summary>
    public class CheckBoxWidget : BaseFieldControl
    {
        /// <summary>
        /// 选项
        /// </summary>
        public List<CheckOption> options { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public List<string> value { get; set; }

        /// <summary>
        /// 获取当前选项信息
        /// </summary>
        [JsonIgnore]
        public List<CheckOption> CurOptions
        {
            get
            {
                List<CheckOption> result = null;
                if (options != null && options.Count > 0&&value!=null&&value.Count>0)
                {
                    result = new List<CheckOption>();
                    foreach(var item in options)
                    {
                        foreach(string k in value)
                        {
                            if (item.key.Equals(k))
                            {
                                result.Add(item);
                                break;
                            }
                        }
                    }
                }
                return result;
            }
        }
    }
}
