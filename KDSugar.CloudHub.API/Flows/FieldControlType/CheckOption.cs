﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.Flows.FieldControlType
{
    /// <summary>
    /// 选项信息
    /// </summary>
    public class CheckOption
    {
        /// <summary>
        /// 是否选择
        /// </summary>
        [JsonProperty(PropertyName = "checked")]
        public bool Checked { get; set; }

        /// <summary>
        /// 选项值
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// 选项键
        /// </summary>
        public string key { get; set; }
    }
}
