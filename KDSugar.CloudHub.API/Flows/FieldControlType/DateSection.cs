﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.Flows.FieldControlType
{
    /// <summary>
    /// 日期区间控件
    /// </summary>
    public class DateSection : BaseFieldControl
    {
        /// <summary>
        /// 日期格式
        /// </summary>
        public string dateFormat { get; set; }

        /// <summary>
        /// 第二标题（结束时间标题）
        /// </summary>
        public string title2 { get; set; }

        /// <summary>
        /// 起止日期（UNIX时间戳）
        /// </summary>
        public List<long> value { get; set; }

        /// <summary>
        /// 起始时间
        /// </summary>
        [JsonIgnore]
        public DateTime? BegTime
        {
            get
            {
                DateTime? result = null;
                if (value != null && value.Count > 2)
                    result = Helper.ToDateTimeFromUnixTime(value[0].ToString());
                return result;
            }
        }

        /// <summary>
        /// 终止时间
        /// </summary>
        [JsonIgnore]
        public DateTime? EndTime
        {
            get
            {
                DateTime? result = null;
                if (value != null && value.Count > 2)
                    result = Helper.ToDateTimeFromUnixTime(value[1].ToString());
                return result;
            }
        }
    }
}
