﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.Flows.FieldControlType
{
    /// <summary>
    /// 日期控件
    /// </summary>
    public class DateWidget : BaseFieldControl
    {
        /// <summary>
        /// 日期格式
        /// </summary>
        public string dateFormat { get; set; }

        /// <summary>
        /// 日期（UNIX时间戳）
        /// </summary>
        public long value { get; set; }

        /// <summary>
        /// 获取经过UNIX时间戳转换后的日期时间
        /// </summary>
        [JsonIgnore]
        public DateTime? Value
        {
            get
            {
                DateTime? result = null;
                result = Helper.ToDateTimeFromUnixTime(value.ToString());
                return result;
            }
        }
    }
}
