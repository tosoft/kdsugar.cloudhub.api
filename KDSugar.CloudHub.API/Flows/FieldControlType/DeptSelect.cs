﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Flows.FieldControlType
{
    /// <summary>
    /// 部门选择控件
    /// </summary>
    public class DeptSelect : BaseFieldControl
    {
        /// <summary>
        /// 部门信息
        /// </summary>
        public List<DeptInfo> deptInfo { get; set; }

        /// <summary>
        /// 部门orgId
        /// </summary>
        public List<string> value { get; set; }

        /// <summary>
        /// 选择类型，single:单选，multi:多选
        /// </summary>
        public string option { get; set; }
    }
}
