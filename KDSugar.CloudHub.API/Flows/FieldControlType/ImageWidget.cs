﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Flows.FieldControlType
{
    /// <summary>
    /// 图片控件
    /// </summary>
    public class ImageWidget : BaseFieldControl
    {
        /// <summary>
        /// 图片数量
        /// </summary>
        public int maximum { get; set; }

        /// <summary>
        /// 图片文件ID
        /// </summary>
        public List<string> value { get; set; }
    }
}
