﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Flows.FieldControlType
{
    /// <summary>
    /// 数字控件
    /// </summary>
    public class NumberWidget : BaseFieldControl
    {
        /// <summary>
        /// 精度
        /// </summary>
        public int decimalDigit { get; set; }

        /// <summary>
        /// 总数量描述
        /// </summary>
        public string detailCountName { get; set; }

        /// <summary>
        /// 数量值（字符串）
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// (未知属性)
        /// </summary>
        public bool detailCount { get; set; }
    }
}
