﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Flows.FieldControlType
{
    /// <summary>
    /// 人员选择控件
    /// </summary>
    public class PersonSelect : BaseFieldControl
    {
        /// <summary>
        /// 人员信息
        /// </summary>
        public List<PersonInfo> personInfo { get; set; }

        /// <summary>
        /// single:单选，multi：多选
        /// </summary>
        public string option { get; set; }

        /// <summary>
        /// 选中人员的oid
        /// </summary>
        public List<string> value { get; set; }
    }
}
