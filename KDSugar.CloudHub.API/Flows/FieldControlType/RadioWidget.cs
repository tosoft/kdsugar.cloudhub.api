﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.Flows.FieldControlType
{
    /// <summary>
    /// 单选框控件
    /// </summary>
    public class RadioWidget : BaseFieldControl
    {
        /// <summary>
        /// 选项信息
        /// </summary>
        public List<CheckOption> options { get; set; }

        /// <summary>
        /// 选项值
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// 获取当前选项信息
        /// </summary>
        [JsonIgnore]
        public CheckOption CurOption
        {
            get
            {
                CheckOption option = null;
                if (options != null && options.Count > 0)
                    option = options.Find(m => m.key.Equals(value));
                return option;
            }
        }
    }
}
