﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Flows.FieldControlType
{
    /// <summary>
    /// 关联审批流程控件
    /// </summary>
    public class RelaFlow : BaseFieldControl
    {
        /// <summary>
        /// 关联的审批单信息
        /// </summary>
        public List<RelaFlowInfo> value { get; set; }

        /// <summary>
        /// 是否只能选一个审批单进行关联？
        /// </summary>
        public bool singleSelect { get; set; }

        /// <summary>
        /// 关联审批流程信息
        /// </summary>
        public class RelaFlowInfo
        {
            /// <summary>
            /// 关联的流程id
            /// </summary>
            public string flowInstId { get; set; }

            /// <summary>
            /// 关联的表单codeId
            /// </summary>
            public string formCodeId { get; set; }

            /// <summary>
            /// 关联的表单模版Id
            /// </summary>
            public string formDefId { get; set; }

            /// <summary>
            /// 关联的表单实例id
            /// </summary>
            public string formInstId { get; set; }

            /// <summary>
            /// 关联的表单标题
            /// </summary>
            public string title { get; set; }
        }
    }
}
