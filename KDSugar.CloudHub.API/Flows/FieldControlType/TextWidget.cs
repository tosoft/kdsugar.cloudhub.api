﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Flows.FieldControlType
{
    /// <summary>
    /// 单行文本框
    /// </summary>
    public class TextWidget : BaseFieldControl
    {
        /// <summary>
        /// 文本内容
        /// </summary>
        public string value { get; set; }
    }
}
