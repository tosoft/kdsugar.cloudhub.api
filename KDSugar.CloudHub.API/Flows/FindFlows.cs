﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Flows
{
    /// <summary>
    /// 获取流程列表|发起
    /// </summary>
    public class FindFlows : PostRequest
    {
        /// <summary>
        /// 管理员身份识别秘钥
        /// </summary>
        public string identifyKey { get; set; }

        /// <summary>
        /// 页数，默认1
        /// </summary>
        public int? pageNumber { get; set; }

        /// <summary>
        /// 每页条数，默认10，最大500
        /// </summary>
        public int? pageSize { get; set; }

        /// <summary>
        /// 获取流程列表|最大条数
        /// </summary>
        public const int MAX_PAGESIZE = 500;

        /// <summary>
        /// 标题
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 流水号
        /// </summary>
        public string serialNo { get; set; }

        private List<Constant.FlowStatusType> _status;

        /// <summary>
        /// 流程状态
        /// </summary>
        public List<string> status
        {
            get
            {
                List<string> result = null;
                if (_status != null && _status.Count > 0)
                {
                    result = new List<string>();
                    foreach (var item in _status)
                        result.Add(Enum.GetName(typeof(Constant.FlowStatusType), item));
                }
                return result;
            }
        }

        /// <summary>
        /// 设置流程状态
        /// </summary>
        /// <param name="list"></param>
        public void SetStatus(List<Constant.FlowStatusType> list)
        {
            _status = list;
        }

        /// <summary>
        /// 添加流程状态
        /// </summary>
        /// <param name="type"></param>
        public void AddStatus(Constant.FlowStatusType type)
        {
            if (_status == null)
                _status = new List<Constant.FlowStatusType>();
            _status.Add(type);
        }

        /// <summary>
        /// 发起人oid
        /// </summary>
        public List<string> creator { get; set; }

        /// <summary>
        /// 发起部门Id
        /// </summary>
        public List<string> deptIds { get; set; }

        /// <summary>
        /// 模板codeId
        /// </summary>
        public List<string> formCodeIds { get; set; }

        /// <summary>
        /// 起始时间
        /// </summary>
        private DateTime? _begTime;

        /// <summary>
        /// 终止时间
        /// </summary>
        private DateTime? _endTime;

        /// <summary>
        /// 第一个元素表示开始时间， 第二个元素结束时间
        /// </summary>
        public List<long> createTime
        {
            get
            {
                List<long> result = null;
                if (_begTime != null && _endTime != null)
                {
                    result = new List<long>();
                    result.Add(Helper.ToUnixTime(_begTime.Value));
                    result.Add(Helper.ToUnixTime(_endTime.Value));
                }
                return result;
            }
        }

        /// <summary>
        /// 设置起止时间
        /// </summary>
        /// <param name="begTime">起始时间</param>
        /// <param name="endTime">终止时间</param>
        public void SetCreateTime(DateTime begTime,DateTime endTime)
        {
            _begTime = begTime;
            _endTime = endTime;
        }

        /// <summary>
        /// 当前审批人
        /// </summary>
        public List<string> approvers { get; set; }

        /// <summary>
        /// 获取流程列表|初始化
        /// </summary>
        /// <param name="_identifyKey"></param>
        public FindFlows(string _identifyKey)
        {
            identifyKey = _identifyKey;
        }

        private const string URL = "https://yunzhijia.com/gateway/workflow/form/thirdpart/findFlows?accessToken={0}";

        /// <summary>
        /// 获取流程列表
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public _FindFlows GetFindFlows(string token)
        {
            return base.Post<_FindFlows>(URL, token);
        }
    }

    /// <summary>
    /// 获取流程类别|返回
    /// </summary>
    public class _FindFlows : BaseResult
    {
        /// <summary>
        /// 数据格式
        /// </summary>
        public _FindFlowData data { get; set; }

        /// <summary>
        /// 获取流程类别|类
        /// </summary>
        public class _FindFlowData
        {
            /// <summary>
            /// 流程列表
            /// </summary>
            public List<_FindFlowInfo> list { get; set; }

            /// <summary>
            /// 当前页数
            /// </summary>
            public int pageNumber { get; set; }

            /// <summary>
            /// 每页条数
            /// </summary>
            public int pageSize { get; set; }

            /// <summary>
            /// 总页数
            /// </summary>
            public int pages { get; set; }

            /// <summary>
            /// 总记录数
            /// </summary>
            public int total { get; set; }
        }
    }

    /// <summary>
    /// 流程信息类
    /// </summary>
    public class _FindFlowInfo
    {
        /// <summary>
        /// 当前节点
        /// </summary>
        public string activityName { get; set; }

        /// <summary>
        /// 当前节点用时
        /// </summary>
        public string activitySpendTime { get; set; }

        /// <summary>
        /// 当前审批人（废弃字段）
        /// </summary>
        public List<string> approvers { get; set; }

        /// <summary>
        /// 当前审批人
        /// </summary>
        public List<PersonInfo> currentApprovers { get; set; }

        /// <summary>
        /// 发起人OID
        /// </summary>
        public string creator { get; set; }

        /// <summary>
        /// 发起人姓名
        /// </summary>
        public string createName { get; set; }

        /// <summary>
        /// 发起时间（时间戳）
        /// </summary>
        public long createTime { get; set; }

        /// <summary>
        /// 发起人部门
        /// </summary>
        public string deptName { get; set; }

        /// <summary>
        /// 流程实例ID
        /// </summary>
        public string flowInstId { get; set; }

        /// <summary>
        /// 总用时
        /// </summary>
        public string flowSpendTime { get; set; }

        /// <summary>
        /// 模板CodeId
        /// </summary>
        public string formCodeId { get; set; }

        /// <summary>
        /// 模板DefId
        /// </summary>
        public string formDefId { get; set; }

        /// <summary>
        /// 表单实例Id
        /// </summary>
        public string formInstId { get; set; }

        /// <summary>
        /// 流水号
        /// </summary>
        public string serialNo { get; set; }

        /// <summary>
        /// 流程状态
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// 模板名称
        /// </summary>
        public string templateName { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 模板类别
        /// </summary>
        public string flowType { get; set; }
    }

    #region 分页相关（预留）
    ///// <summary>
    ///// 获取流程列表(分页)
    ///// </summary>
    //public class FindFlowsByPageable : FindFlows
    //{
    //    /// <summary>
    //    /// 获取流程列表(分页)|初始化
    //    /// </summary>
    //    /// <param name="_identifyKey"></param>
    //    public FindFlowsByPageable(string _identifyKey)
    //        : base(_identifyKey)
    //    {
            
    //    }
    //}
    #endregion
}
