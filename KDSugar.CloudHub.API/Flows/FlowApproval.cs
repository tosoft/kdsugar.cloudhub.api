﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Flows
{
    /// <summary>
    /// 流程审批控制|发起
    /// </summary>
    public class FlowApproval : PostRequest
    {
        /// <summary>
        /// 审批控制类型枚举
        /// </summary>
        private enum ApprovalTypes
        {
            /// <summary>
            /// 同意
            /// </summary>
            Agree,
            /// <summary>
            /// 不同意
            /// </summary>
            DisAgree,
            /// <summary>
            /// 退回
            /// </summary>
            Return
        }

        /// <summary>
        /// 审批控制类型
        /// </summary>
        private ApprovalTypes approvalType { get; set; }

        /// <summary>
        /// 流程控制|流程类
        /// </summary>
        public class ApprovalFlow
        {
            /// <summary>
            /// 流程实例ID
            /// </summary>
            public string flowInstId { get; set; }

            /// <summary>
            /// 审批意见
            /// </summary>
            public string opinion { get; set; }

            /// <summary>
            /// 附件信息
            /// </summary>
            public List<AnnexInfo> files { get; set; }

            /// <summary>
            /// 上传图片信息
            /// </summary>
            public List<string> imgs { get; set; }

            /// <summary>
            /// 流程控制|流程类|初始化
            /// </summary>
            /// <param name="_flowInstId">流程实例ID</param>
            public ApprovalFlow(string _flowInstId)
            {
                flowInstId = _flowInstId;
            }
        }

        /// <summary>
        /// 流程控制|表单类
        /// </summary>
        public class ApprovalForm
        {
            /// <summary>
            /// 单据流程模板ID
            /// </summary>
            public string formCodeId { get; set; }

            /// <summary>
            /// 单据流程模板版本ID
            /// </summary>
            public string formDefId { get; set; }

            /// <summary>
            /// 单据流程实例ID
            /// </summary>
            public string formInstId { get; set; }

            /// <summary>
            /// 表头
            /// </summary>
            public Dictionary<string,object> widgetValue { get; set; }

            /// <summary>
            /// 流程控制|表单类|初始化-同意
            /// </summary>
            /// <param name="_formCodeId">单据流程模板ID</param>
            /// <param name="_formDefId">单据流程模板版本ID</param>
            /// <param name="_formInstId">单据流程实例ID</param>
            public ApprovalForm(string _formCodeId,string _formDefId,string _formInstId)
            {
                formCodeId = _formCodeId;
                formDefId = _formDefId;
                formInstId = _formInstId;
            }

            /// <summary>
            /// 流程控制|表单类|初始化-不同意
            /// </summary>
            /// <param name="_formCodeId">单据流程模板ID</param>
            /// <param name="_formInstId">单据流程实例ID</param>
            public ApprovalForm(string _formCodeId,string _formInstId)
            {
                formCodeId = _formCodeId;
                formInstId = _formInstId;
            }
        }

        /// <summary>
        /// 操作人openId
        /// </summary>
        public string approver { get; set; }

        /// <summary>
        /// 流程控制信息
        /// </summary>
        public ApprovalFlow flow { get; set; }

        /// <summary>
        /// 流程表单信息
        /// </summary>
        public ApprovalForm form { get; set; }

        private FlowApproval(string _approver)
        {
            approver = _approver;
        }

        /// <summary>
        /// 单据同意
        /// </summary>
        /// <param name="_approver"></param>
        /// <param name="_flow"></param>
        /// <param name="_form"></param>
        public FlowApproval(string _approver,ApprovalFlow _flow,ApprovalForm _form)
            : this(_approver)
        {
            approvalType = ApprovalTypes.Agree;
            flow = _flow;
            form = _form;
        }

        /// <summary>
        /// 单据不同意
        /// </summary>
        /// <param name="_approver"></param>
        /// <param name="_flow"></param>
        /// <param name="_formCodeId"></param>
        /// <param name="_formInstId"></param>
        public FlowApproval(string _approver,ApprovalFlow _flow,string _formCodeId,string _formInstId)
            : this(_approver)
        {
            approvalType = ApprovalTypes.DisAgree;
            flow = _flow;
            form = new ApprovalForm(_formCodeId, _formInstId);
        }

        /// <summary>
        /// 单据退回
        /// </summary>
        /// <param name="_approver"></param>
        /// <param name="_flow"></param>
        public FlowApproval(string _approver, ApprovalFlow _flow)
            : this(_approver)
        {
            approvalType = ApprovalTypes.Return;
            flow = _flow;
        }

        /// <summary>
        /// 调用流程控制接口
        /// </summary>
        /// <param name="token">token</param>
        /// <returns></returns>
        public BaseResult GetApprovalStatus(string token)
        {
            string tmpUrl;
            switch (approvalType)
            {
                case ApprovalTypes.Agree:
                    tmpUrl = "http://www.yunzhijia.com/gateway/workflow/form/thirdpart/agree?accessToken={0}";
                    break;
                case ApprovalTypes.DisAgree:
                    tmpUrl = "http://www.yunzhijia.com/gateway/workflow/form/thirdpart/disagree?accessToken={0}";
                    break;
                case ApprovalTypes.Return:
                    tmpUrl = "http://www.yunzhijia.com/gateway/workflow/form/thirdpart/return?accessToken={0}";
                    break;
                default:
                    tmpUrl = null;
                    break;
            }
            if (string.IsNullOrWhiteSpace(tmpUrl))
                throw new Exception("未指定审批类型");
            return base.Post<BaseResult>(tmpUrl, token);
        }
    }
}
