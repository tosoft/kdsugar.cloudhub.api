﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Flows
{
    /// <summary>
    /// 查询审批流程图|发起
    /// </summary>
    public class FlowChart : PostRequest
    {
        /// <summary>
        /// 流程实例ID
        /// </summary>
        public string flowInstId { get; set; }

        private const string URL = "http://www.yunzhijia.com/gateway/workflow/form/thirdpart/getFlowChart?accessToken={0}";

        /// <summary>
        /// 查询审批流程图|初始化
        /// </summary>
        /// <param name="_flowInstId">流程实例ID</param>
        public FlowChart(string _flowInstId)
        {
            flowInstId = _flowInstId;
        }

        /// <summary>
        /// 获取流程图信息
        /// </summary>
        /// <param name="token">token</param>
        /// <returns></returns>
        public _FlowChart GetFlowChart(string token)
        {
            return base.Post<_FlowChart>(URL, token);
        }
    }

    /// <summary>
    /// 查询审批流程图|返回
    /// </summary>
    public class _FlowChart : BaseResult
    {
        /// <summary>
        /// 审批流程信息
        /// </summary>
        public FlowChartInfo data { get; set; }

        /// <summary>
        /// 审批流程图信息
        /// </summary>
        public class FlowChartInfo
        {
            /// <summary>
            /// 当前抵达节点
            /// </summary>
            public string arrivedActivity { get; set; }

            /// <summary>
            /// 节点
            /// </summary>
            public List<ActivityInfo> activitys { get; set; }

            /// <summary>
            /// 节点信息
            /// </summary>
            public class ActivityInfo
            {
                /// <summary>
                /// 审批人
                /// </summary>
                public List<PredictUserInfo> predictUsers { get; set; }

                /// <summary>
                /// 审批人信息
                /// </summary>
                public class PredictUserInfo
                {
                    /// <summary>
                    /// 姓名
                    /// </summary>
                    public string name { get; set; }

                    /// <summary>
                    /// 用户OID
                    /// </summary>
                    public string oid { get; set; }
                }

                /// <summary>
                /// 圈子EID
                /// </summary>
                public string eid { get; set; }

                /// <summary>
                /// 节点codeId
                /// </summary>
                public string codeId { get; set; }

                /// <summary>
                /// 是否设置隐藏节点
                /// </summary>
                public bool hide { get; set; }

                /// <summary>
                /// 节点名称
                /// </summary>
                public string name { get; set; }

                /// <summary>
                /// 操作组名称
                /// </summary>
                public string operationName { get; set; }

                /// <summary>
                /// 节点类型
                /// </summary>
                public string nodeType { get; set; }

                /// <summary>
                /// Element类型 normal:正常 manual：人工
                /// </summary>
                public string activityType { get; set; }

                /// <summary>
                /// 是否并行
                /// </summary>
                public bool parallelFlag { get; set; }

                /// <summary>
                /// 是否预测节点
                /// </summary>
                public bool predictFlag { get; set; }

                /// <summary>
                /// 节点状态
                /// </summary>
                public string status { get; set; }
            }

            /// <summary>
            /// 节点连线
            /// </summary>
            public List<LinesInfo> lines { get; set; }

            /// <summary>
            /// 节点连线信息
            /// </summary>
            public class LinesInfo
            {
                /// <summary>
                /// 出发codeId
                /// </summary>
                public string from { get; set; }

                /// <summary>
                /// 抵达codeId
                /// </summary>
                public string to { get; set; }
            }
        }
    }
}
