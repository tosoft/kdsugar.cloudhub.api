﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.Flows
{
    /// <summary>
    /// 查询审批节点审批人|发起
    /// </summary>
    public class FlowDetailById : PostRequest
    {
        private const string URL = "http://www.yunzhijia.com/gateway/workflow/form/thirdpart/getFlowDetailById?accessToken={0}";

        /// <summary>
        /// 节点实例Id
        /// </summary>
        public string flowInstId { get; set; }

        /// <summary>
        /// 节点CodeID
        /// </summary>
        public string activityCodeId { get; set; }

        /// <summary>
        /// 节点类型
        /// </summary>
        public string activityType { get; set; }

        /// <summary>
        /// 是否预测节点
        /// </summary>
        public bool predictFlag { get; set; }

        /// <summary>
        /// 查询审批节点审批人|初始化
        /// </summary>
        /// <param name="_flowInstId">节点实例ID</param>
        /// <param name="_activityCodeId">节点CodeID</param>
        /// <param name="_activityType">节点类型</param>
        /// <param name="_predictFlag">是否预测节点</param>
        public FlowDetailById(string _flowInstId,string _activityCodeId,
            string _activityType,bool _predictFlag)
        {
            flowInstId = _flowInstId;
            activityCodeId = _activityCodeId;
            activityType = _activityType;
            predictFlag = _predictFlag;
        }

        /// <summary>
        /// 获取审批节点审批人信息
        /// </summary>
        /// <param name="token">token</param>
        /// <returns></returns>
        public _FlowDetailById GetFlowDetailById(string token)
        {
            return base.Post<_FlowDetailById>(URL, token);
        }
    }

    /// <summary>
    /// 查询审批节点审批人|返回
    /// </summary>
    public class _FlowDetailById : BaseResult
    {
        /// <summary>
        /// 节点审批人信息
        /// </summary>
        public FlowDetailByIdInfo data { get; set; }

        /// <summary>
        /// 节点审批人信息类
        /// </summary>
        public class FlowDetailByIdInfo
        {
            /// <summary>
            /// 异常原因
            /// </summary>
            public string abnormalReason { get; set; }

            /// <summary>
            /// 节点抄送内容展示
            /// </summary>
            public string activityCc { get; set; }

            /// <summary>
            /// 节点名称
            /// </summary>
            public string activityName { get; set; }

            /// <summary>
            /// 剩余时长 单位:分钟
            /// </summary>
            public string activitySdtMins { get; set; }

            /// <summary>
            /// 节点类型
            /// </summary>
            public string activityType { get; set; }

            /// <summary>
            /// 征求意见
            /// </summary>
            public string askOpinions { get; set; }

            /// <summary>
            /// 节点codeId
            /// </summary>
            public string codeId { get; set; }

            /// <summary>
            /// 是否需要倒计时
            /// </summary>
            public bool countdown { get; set; }

            /// <summary>
            /// 创建时间
            /// </summary>
            public string createTime { get; set; }

            /// <summary>
            /// 隐藏节点审批意见开关
            /// </summary>
            public string hideOpinionMode { get; set; }

            /// <summary>
            /// 是否人工节点标示
            /// </summary>
            public bool manualFlag { get; set; }

            /// <summary>
            /// 是否自然日计算规则
            /// </summary>
            public bool natureDay { get; set; }

            /// <summary>
            /// 操作名称
            /// </summary>
            public string operationName { get; set; }

            /// <summary>
            /// 是否(PS:原文如此，官方文档描述不清....)
            /// </summary>
            public bool overtimeActivity { get; set; }

            /// <summary>
            /// 节点超时时长 --timeout=true 时显示
            /// </summary>
            public string overtimeMins { get; set; }

            /// <summary>
            /// 外层并行节点Id
            /// </summary>
            public string parallelActivityId { get; set; }

            /// <summary>
            /// 剩余时长 单位:秒
            /// </summary>
            public string remainingTime { get; set; }

            /// <summary>
            /// (未知属性)
            /// </summary>
            public string remainingTimeMins { get; set; }

            /// <summary>
            /// 加签人
            /// </summary>
            public string signer { get; set; }

            /// <summary>
            /// 状态
            /// </summary>
            public string status { get; set; }

            /// <summary>
            /// 系统标准处理设置
            /// </summary>
            public string sysSdtSet { get; set; }

            /// <summary>
            /// 是否已超时
            /// </summary>
            public bool timeout { get; set; }

            /// <summary>
            /// 更新时间
            /// </summary>
            public string updateTime { get; set; }

            /// <summary>
            /// 节点用时
            /// </summary>
            public string usedTime { get; set; }

            /// <summary>
            /// 审批组
            /// </summary>
            public List<HandleGroupInfo> handleGroups { get; set; }

            /// <summary>
            /// 审批组信息
            /// </summary>
            public class HandleGroupInfo
            {
                /// <summary>
                /// 审批组名称(null时默认显示为节点名称)
                /// </summary>
                public string groupName { get; set; }

                /// <summary>
                /// 审批组状态
                /// </summary>
                public string groupStatus { get; set; }

                /// <summary>
                /// 耗时
                /// </summary>
                public string usedTime { get; set; }

                /// <summary>
                /// 抄送人信息
                /// </summary>
                public class GroupCCInfo
                {

                }

                /// <summary>
                /// 处理人信息
                /// </summary>
                public class HandleInfo
                {
                    /// <summary>
                    /// 部门
                    /// </summary>
                    public string dept { get; set; }

                    /// <summary>
                    /// 处理时间
                    /// </summary>
                    public string handleTime { get; set; }

                    /// <summary>
                    /// 审批人状态  1：正常，0 离职
                    /// </summary>
                    public int handlerStatus { get; set; }

                    /// <summary>
                    /// 处理人名称
                    /// </summary>
                    public string name { get; set; }

                    /// <summary>
                    /// 逐签序号
                    /// </summary>
                    public int orderNo { get; set; }

                    /// <summary>
                    /// 是否超时
                    /// </summary>
                    public bool timeout { get; set; }

                    /// <summary>
                    /// 超时时长 timeout=true 时显示
                    /// </summary>
                    public string overtimeMins { get; set; }

                    /// <summary>
                    /// 头像地址
                    /// </summary>
                    public string photo { get; set; }

                    /// <summary>
                    /// 处理状态
                    /// </summary>
                    public string status { get; set; }
                }
            }
        }
    }
}
