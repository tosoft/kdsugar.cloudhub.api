﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Flows
{
    /// <summary>
    /// 审批记录信息|发起
    /// </summary>
    public class FlowRecord : PostRequest
    {
        /// <summary>
        /// 流程实例ID
        /// </summary>
        public string formInstId { get; set; }

        /// <summary>
        /// 流程模板ID
        /// </summary>
        public string formCodeId { get; set; }

        /// <summary>
        /// 审批记录信息|初始化
        /// </summary>
        /// <param name="_formCodeId"></param>
        /// <param name="_formInstId"></param>
        public FlowRecord(string _formCodeId,string _formInstId)
        {
            formCodeId = _formCodeId;
            formInstId = _formInstId;
        }

        private const string URL = "https://yunzhijia.com/gateway/workflow/form/thirdpart/getFlowRecord?accessToken={0}";

        /// <summary>
        /// 获取审批记录信息
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public _FlowRecord GetFlowRecord(string token)
        {
            return base.Post<_FlowRecord>(URL, token);
        }
    }

    /// <summary>
    /// 审批记录信息|返回
    /// </summary>
    public class _FlowRecord : BaseResult
    {
        /// <summary>
        /// 返回数据
        /// </summary>
        public List<_FlowRecordInfo> data { get; set; }

        /// <summary>
        /// 审批记录信息
        /// </summary>
        public class _FlowRecordInfo
        {
            /// <summary>
            /// 审批流程Id
            /// </summary>
            public string flowInstId { get; set; }

            /// <summary>
            /// 节点名称
            /// </summary>
            public string activityName { get; set; }
            
            /// <summary>
            /// 节点类型
            /// </summary>
            public string activityType { get; set; }

            /// <summary>
            /// 审批状态
            /// </summary>
            public string status { get; set; }

            /// <summary>
            /// 审批时间
            /// </summary>
            public long handleTime { get; set; }

            /// <summary>
            /// 完成时间
            /// </summary>
            public long createTime { get; set; }

            /// <summary>
            /// 审批人名称
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// 审批人ID
            /// </summary>
            public string handler { get; set; }

            /// <summary>
            /// 审批意见
            /// </summary>
            public string opinion { get; set; }

            /// <summary>
            /// 审批图片
            /// </summary>
            public List<string> imgs { get; set; }

            /// <summary>
            /// 附件信息
            /// </summary>
            public List<AnnexInfo> files { get; set; }
        }
    }
}
