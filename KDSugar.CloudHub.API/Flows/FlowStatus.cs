﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.Flows
{
    /// <summary>
    /// 查询审批状态|发起
    /// </summary>
    public class FlowStatus : PostRequest
    {
        /// <summary>
        /// 流程Id
        /// </summary>
        public string flowInstId { get; set; }

        /// <summary>
        /// 查询审批状态|初始化
        /// </summary>
        /// <param name="_flowInstId"></param>
        public FlowStatus(string _flowInstId)
        {
            flowInstId = _flowInstId;
        }

        private const string URL = "http://www.yunzhijia.com/gateway/workflow/form/thirdpart/getFlowStatus?accessToken={0}";

        /// <summary>
        /// 获取流程审批状态
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public _FlowStatus GetFlowStatus(string token)
        {
            return base.Post<_FlowStatus>(URL, token);
        }
    }

    /// <summary>
    /// 查询审批状态|返回
    /// </summary>
    public class _FlowStatus : BaseResult
    {
        /// <summary>
        /// 流程状态
        /// </summary>
        public string data { get; set; }

        /// <summary>
        /// 流程状态类型(枚举)
        /// </summary>
        [JsonIgnore]
        public FlowStatusEnum FlowStatusType
        {
            get
            {
                FlowStatusEnum result = FlowStatusEnum.UNKNOW;
                bool isParse = Enum.TryParse<FlowStatusEnum>(data, out result);
                if (!isParse)
                    result = FlowStatusEnum.UNKNOW;
                return result;
            }
        }

        /// <summary>
        /// 流程状态枚举
        /// </summary>
        public enum FlowStatusEnum
        {
            /// <summary>
            /// 同意已完成
            /// </summary>
            FINISH,
            /// <summary>
            /// 不同意已完成
            /// </summary>
            DISAGREE,
            /// <summary>
            /// 待审批
            /// </summary>
            RUNNING,
            /// <summary>
            /// 挂起
            /// </summary>
            SUSPEND,
            /// <summary>
            /// 待提交(审批人退回到发起节点)
            /// </summary>
            RETURNED,
            /// <summary>
            /// 待提交(发起人自己撤回)
            /// </summary>
            TOSUBMIT,
            /// <summary>
            /// 废弃
            /// </summary>
            ABANDON,
            /// <summary>
            /// 已删除
            /// </summary>
            DELETE,
            /// <summary>
            /// 未知
            /// </summary>
            UNKNOW
        }
    }
}
