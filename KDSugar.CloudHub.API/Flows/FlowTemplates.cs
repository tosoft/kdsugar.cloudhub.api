﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Flows
{
    /// <summary>
    /// 获取模板|发起
    /// </summary>
    public class FlowTemplates : PostRequest
    {
        public string identifyKey { get; set; }

        public FlowTemplates(string _identifyKey)
        {
            identifyKey = _identifyKey;
        }

        private const string URL = "https://yunzhijia.com/gateway/workflow/form/thirdpart/getTemplates?accessToken={0}";

        public _FlowTemplates GetFlowTemplates(string token)
        {
            return base.Post<_FlowTemplates>(URL, token);
        }
    }

    /// <summary>
    /// 获取模板|返回
    /// </summary>
    public class _FlowTemplates : BaseResult
    {
        /// <summary>
        /// 模板分类列表
        /// </summary>
        public List<_FlowTemplatesClass> data { get; set; }

        /// <summary>
        /// 模板分类信息
        /// </summary>
        public class _FlowTemplatesClass
        {
            /// <summary>
            /// 分类名称
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// 分类Id
            /// </summary>
            public string id { get; set; }

            /// <summary>
            /// 序号
            /// </summary>
            public int order { get; set; }

            /// <summary>
            /// 模板列表
            /// </summary>
            public List<_FlowTemplatesInfo> formTemplates { get; set; }
        }

        /// <summary>
        /// 模板信息
        /// </summary>
        public class _FlowTemplatesInfo
        {
            /// <summary>
            /// 模板CodeId
            /// </summary>
            public string codeId { get; set; }

            /// <summary>
            /// 模板标题
            /// </summary>
            public string title { get; set; }

            /// <summary>
            /// 序号
            /// </summary>
            public int order { get; set; }
        }
    }
}
