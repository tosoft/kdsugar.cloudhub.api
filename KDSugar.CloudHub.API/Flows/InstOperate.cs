﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Flows
{
    /// <summary>
    /// 实例操作类
    /// </summary>
    public class InstOperate : PostRequest
    {
        /// <summary>
        /// 选填，是否跳过节点字段权限校验，true表示本次操作不会校验节点字段权限
        /// </summary>
        public bool? skipWidgetAuthorityCheck { get; set; }

        /// <summary>
        /// 选填，是否使用别名，true表示使用别名
        /// </summary>
        public bool? useAlias { get; set; }

        /// <summary>
        /// 选填，退回或撤回后是否允许再次发起，true表示允许退回以及撤回后再次发起
        /// </summary>
        public bool? resubmit { get; set; }

        /// <summary>
        /// 模板ID
        /// </summary>
        public string formCodeId { get; set; }

        /// <summary>
        /// 主表单字段
        /// </summary>
        public Dictionary<string,object> widgetValue { get; set; }

        /// <summary>
        /// 明细类
        /// </summary>
        public class DetailValue
        {
            /// <summary>
            /// 明细字段
            /// </summary>
            public List<Dictionary<string,object>> widgetValue { get; set; }
        }

        /// <summary>
        /// 明细表字段
        /// </summary>
        public Dictionary<string,DetailValue> details { get; set; }

        /// <summary>
        /// 实例操作类|初始化
        /// </summary>
        /// <param name="_formCodeId">流程模板ID</param>
        public InstOperate(string _formCodeId)
        {
            formCodeId = _formCodeId;
        }
    }

    /// <summary>
    /// 创建审批流程|发起
    /// </summary>
    public class CreateInst : InstOperate
    {
        /// <summary>
        /// 选填，自由流审批人id，key为固定值"oids"，value为由审批人id(openid)组成的数组
        /// </summary>
        public List<string> oids { get; set; }

        /// <summary>
        /// 选填，自由流抄送人id，key为固定值"cc"，value为由抄送人id(openid)组成的数组
        /// </summary>
        public List<string> cc { get; set; }

        /// <summary>
        /// 选填，是否保存为草稿，true表示本次提交操作不会创建流程，只是保存为一份草稿
        /// </summary>
        public bool? justDraft { get; set; }

        /// <summary>
        /// 创建审批流程|初始化
        /// </summary>
        /// <param name="_formCodeId">流程模板ID</param>
        public CreateInst(string _formCodeId)
            : base(_formCodeId)
        {

        }

        private const string URL = "https://yunzhijia.com/gateway/workflow/form/thirdpart/createInst?accessToken={0}";

        /// <summary>
        /// 创建审批流程|返回
        /// </summary>
        public class _CreateInst : BaseResult
        {
            /// <summary>
            /// 创建流程
            /// </summary>
            public _CreateInstInfo data { get; set; }

            /// <summary>
            /// 创建流程信息类
            /// </summary>
            public class _CreateInstInfo
            {
                /// <summary>
                /// 流程模板版本ID
                /// </summary>
                public string formDefId { get; set; }

                /// <summary>
                /// 单据流程ID
                /// </summary>
                public string formInstId { get; set; }

                /// <summary>
                /// 流程图实例ID
                /// </summary>
                public string flowInstId { get; set; }
            }
        }

        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="token">token</param>
        /// <returns></returns>
        public _CreateInst Create(string token)
        {
            return base.Post<_CreateInst>(URL, token);
        }
    }

    /// <summary>
    /// 修改审批单据|发起
    /// </summary>
    public class ModifyInst : InstOperate
    {
        /// <summary>
        /// 流程模板版本ID
        /// </summary>
        public string formDefId { get; set; }

        /// <summary>
        /// 单据流程实例ID
        /// </summary>
        public string formInstId { get; set; }

        /// <summary>
        /// (选填)是否起始节点
        /// </summary>
        public bool? startNode { get; set; }

        /// <summary>
        /// 操作人openId
        /// </summary>
        public string creator { get; set; }

        /// <summary>
        /// 修改审批单据|初始化
        /// </summary>
        /// <param name="_formCodeId">单据流程模板ID</param>
        /// <param name="_formDefId">单据流程模板ID</param>
        /// <param name="_formInstId">单据流程实例ID</param>
        /// <param name="_creator">操作人openId</param>
        public ModifyInst(string _formCodeId,string _formDefId,string _formInstId,string _creator)
            : base(_formCodeId)
        {
            formDefId = _formDefId;
            formInstId = _formInstId;
            creator = _creator;
        }

        private const string URL = "https://yunzhijia.com/gateway/workflow/form/thirdpart/modifyInst?accessToken={0}";

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="token">token</param>
        /// <returns></returns>
        public BaseResult Modify(string token)
        {
            return base.Post<BaseResult>(URL, token);
        }
    }
}
