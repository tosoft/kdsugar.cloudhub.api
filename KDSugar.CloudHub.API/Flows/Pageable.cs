﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Flows
{
    /// <summary>
    /// 分页
    /// </summary>
    public class Pageable
    {
        /// <summary>
        /// 记录ID
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 记录条数
        /// </summary>
        public int? pageSize { get; set; }

        /// <summary>
        /// 分页类型
        /// </summary>
        public string type { get; set; }
    }
}
