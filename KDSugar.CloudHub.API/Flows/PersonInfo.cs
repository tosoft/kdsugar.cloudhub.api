﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Flows
{
    /// <summary>
    /// 人员信息
    /// </summary>
    public class PersonInfo
    {
        /// <summary>
        /// 头像地址
        /// </summary>
        public string image { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// OID
        /// </summary>
        public string oid { get; set; }

        /// <summary>
        /// UserId
        /// </summary>
        public string userId { get; set; }
    }
}
