﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Flows
{
    /// <summary>
    /// 获取外部接口日志|发起
    /// </summary>
    public class PushLog : PostRequest
    {
        /// <summary>
        /// 日志类型
        /// </summary>
        public string devType { get; set; } = "user";

        /// <summary>
        /// 推送类型枚举
        /// </summary>
        public enum PushTypes
        {
            /// <summary>
            /// 全部日志
            /// </summary>
            all,
            /// <summary>
            /// 仅获取推送成功的日志
            /// </summary>
            success,
            /// <summary>
            /// 仅获取推送失败的日志
            /// </summary>
            failed
        }

        private PushTypes _pushType;

        /// <summary>
        /// 推送类型
        /// </summary>
        public string pushType
        {
            get
            {
                return Enum.GetName(typeof(PushTypes), _pushType);
            }
        }

        /// <summary>
        /// 起始时间
        /// </summary>
        public long? startTime { get; set; }

        /// <summary>
        /// 终止时间
        /// </summary>
        public long? endTime { get; set; }
        
        /// <summary>
        /// 分页信息
        /// </summary>
        public Pageable pageable { get; set; }

        /// <summary>
        /// 获取外部接口日志|初始化
        /// </summary>
        /// <param name="_pushType">推送类型</param>
        /// <param name="_pageable">分页信息</param>
        public PushLog(PushTypes _pushType,Pageable _pageable)
        {
            this._pushType = _pushType;
            this.pageable = _pageable;
        }

        /// <summary>
        /// 设置起止时间
        /// </summary>
        /// <param name="_begTime">起始时间</param>
        /// <param name="_endTime">中止时间</param>
        public void SetTime(DateTime? _begTime,DateTime? _endTime)
        {
            if (_begTime != null)
                startTime = Helper.ToUnixTime(_begTime.Value);
            if (_endTime != null)
                endTime = Helper.ToUnixTime(_endTime.Value);
        }

        private const string URL = "https://yunzhijia.com/gateway/workflow/form/thirdpart/getPushLog?accessToken={0}";

        /// <summary>
        /// 调用获取外部接口日志
        /// </summary>
        /// <param name="token">token</param>
        /// <returns></returns>
        public _PushLog GetPushLog(string token)
        {
            return base.Post<_PushLog>(URL, token);
        }
    }

    /// <summary>
    /// 获取外部接口日志|返回
    /// </summary>
    public class _PushLog : BaseResult
    {
        /// <summary>
        /// 日志记录列表
        /// </summary>
        public PushLogList data { get; set; }

        /// <summary>
        /// 日志记录列表类
        /// </summary>
        public class PushLogList
        {
            /// <summary>
            /// 总记录数
            /// </summary>
            public int count { get; set; }

            /// <summary>
            /// 日志记录信息
            /// </summary>
            public List<_PushLogInfo> pushLogs { get; set; }
        }

        /// <summary>
        /// 日志记录信息类
        /// </summary>
        public class _PushLogInfo
        {
            /// <summary>
            /// 企业EID
            /// </summary>
            public string eid { get; set; }

            /// <summary>
            /// 流程实例ID
            /// </summary>
            public string flowInstId { get; set; }

            /// <summary>
            /// 单据模板ID
            /// </summary>
            public string formCodeId { get; set; }

            /// <summary>
            /// 流程版本ID
            /// </summary>
            public string formDefId { get; set; }

            /// <summary>
            /// 单据实例ID
            /// </summary>
            public string formInstId { get; set; }

            /// <summary>
            /// 日志ID
            /// </summary>
            public string id { get; set; }

            /// <summary>
            /// 接口ID
            /// </summary>
            public string interfaceId { get; set; }

            /// <summary>
            /// 接口名称
            /// </summary>
            public string interfaceName { get; set; }

            /// <summary>
            /// 节点ID
            /// </summary>
            public string nodeid { get; set; }

            /// <summary>
            /// 节点名称
            /// </summary>
            public string nodeName { get; set; }

            /// <summary>
            /// 是否成功标识
            /// </summary>
            public bool success { get; set; }

            /// <summary>
            /// 审批模板名称
            /// </summary>
            public string tempName { get; set; }

            /// <summary>
            /// 日志记录时间戳
            /// </summary>
            public long time { get; set; }
        }
    }
}
