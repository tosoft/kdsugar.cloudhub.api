﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.Flows
{
    /// <summary>
    /// 获取表单模板|发起调用
    /// </summary>
    public class ViewFormDef : PostRequest
    {
        /// <summary>
        /// 审批模板ID
        /// </summary>
        public string formCodeId { get; set; }

        /// <summary>
        /// 获取表单模板|初始化
        /// </summary>
        /// <param name="_formCodeId"></param>
        public ViewFormDef(string _formCodeId)
        {
            formCodeId = _formCodeId;
        }

        private const string URL = "https://yunzhijia.com/gateway/workflow/form/thirdpart/viewFormDef?accessToken={0}";

        /// <summary>
        /// 获取表单模板
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public _ViewForm GetViewFormDef(string token)
        {
            return base.Post<_ViewForm>(URL, token);
        }
    }

    /// <summary>
    /// 获取单据实例|发起调用
    /// </summary>
    public class ViewFormInst : PostRequest
    {
        /// <summary>
        /// 单据模板ID
        /// </summary>
        public string formCodeId { get; set; }

        /// <summary>
        /// 单据实例ID
        /// </summary>
        public string formInstId { get; set; }

        /// <summary>
        /// 获取单据实例|初始化
        /// </summary>
        /// <param name="_formCodeId">单据模板ID</param>
        /// <param name="_formInstId">单据实例ID</param>
        public ViewFormInst(string _formCodeId,string _formInstId)
        {
            formCodeId = _formCodeId;
            formInstId = _formInstId;
        }

        private const string URL = "https://yunzhijia.com/gateway/workflow/form/thirdpart/viewFormInst?accessToken={0}";

        /// <summary>
        /// 获取单据实例
        /// </summary>
        /// <param name="token">调用token</param>
        /// <returns></returns>
        public _ViewForm GetViewFormInst(string token)
        {
            return base.Post<_ViewForm>(URL, token);
        }
    }

    /// <summary>
    /// 接收推送流程
    /// </summary>
    public class Push
    {
        /// <summary>
        /// 单据实例
        /// </summary>
        public _ViewForm ViewInst { get; set; }

        /// <summary>
        /// 初始化接收推送流程信息
        /// </summary>
        /// <param name="content">密文</param>
        /// <param name="key">密钥</param>
        public Push(string content,string key)
        {
            string viewInstStr = Helper.AESDecrypt(content, key);
            ViewInst = JsonConvert.DeserializeObject<_ViewForm>(viewInstStr);
        }
    }

    /// <summary>
    /// 单据信息
    /// </summary>
    public class _ViewForm : BaseResult
    {
        /// <summary>
        /// 流程单据Data
        /// </summary>
        public _ViewFormData data { get; set; }

        /// <summary>
        /// 流程单据Data类
        /// </summary>
        public class _ViewFormData
        {
            /// <summary>
            /// 流程字段信息
            /// </summary>
            public _ViewFormInfo formInfo { get; set; }

            /// <summary>
            /// 流程通用信息
            /// </summary>
            public BasicInfo basicInfo { get; set; }
        }

        /// <summary>
        /// 流程字段信息类
        /// </summary>
        public class _ViewFormInfo
        {
            /// <summary>
            /// 表头字段
            /// </summary>
            public Dictionary<string, ControlInfo> widgetMap { get; set; }

            /// <summary>
            /// 表体字段
            /// </summary>
            public Dictionary<string,DetailInfo> detailMap { get; set; }
        }
    }
}
