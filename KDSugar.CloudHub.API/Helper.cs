﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Security.Cryptography;
//using System.Web;

namespace KDSugar.CloudHub.API
{
    /// <summary>
    /// 工具类
    /// </summary>
    public class Helper
    {
        /// <summary>
        /// 将UNIX时间戳转换为标准时间(若输出未最小时间，则为转换失败)
        /// </summary>
        /// <param name="unixTime"></param>
        /// <returns></returns>
        public static DateTime ToDateTimeFromUnixTime(string unixTime)
        {
            DateTime result = DateTime.MinValue;
            string tmpUnixTime = "";
            try
            {
                if (unixTime != null)
                {
                    if (unixTime.Length >= 10)
                    {
                        tmpUnixTime = unixTime;
                        if (tmpUnixTime.Length > 10)
                            tmpUnixTime = tmpUnixTime.Substring(0, 10);
                        DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
                        result = startTime.AddSeconds(double.Parse(tmpUnixTime));
                    }
                }
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
            return result;
        }

        /// <summary>
        /// 根据给定时间转换UNIX时间戳
        /// </summary>
        /// <param name="dt">给定的时间</param>
        /// <returns></returns>
        public static long ToUnixTime(DateTime dt)
        {
            long result = -1;
            DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            if (dt != null)
            {
                TimeSpan ts = dt.Subtract(dtStart);
                string resultStr = ts.Ticks.ToString().Substring(0, ts.Ticks.ToString().Length - 4);
                if (!long.TryParse(resultStr, out result))
                    throw new Exception("转换UNIX时间戳失败");
            }
            return result;
        }

        /// <summary>
        /// 发起Json-Web请求
        /// </summary>
        /// <param name="url">Url</param>
        /// <param name="content">请求内容</param>
        /// <param name="type">调用方式</param>
        /// <returns></returns>
        public static string PostWeb(string url, string content, Constant.MethodType type = Constant.MethodType.POST)
        {
            string result = null;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                //request.Method = "POST";
                request.Method = Enum.GetName(typeof(Constant.MethodType), type);
                request.ContentType = "application/json";
                byte[] data = Encoding.UTF8.GetBytes(content);
                //request.ContentLength = content.Length;
                request.ContentLength = data.Length;

                Stream intStream = request.GetRequestStream();
                intStream.Write(data, 0, data.Length);
                intStream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream outStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(outStream, Encoding.UTF8);
                result = reader.ReadToEnd();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return result;
        }

        /// <summary>
        /// 以Urlencoded形式发起WEB请求
        /// </summary>
        /// <param name="url">Url</param>
        /// <param name="requeseData">请求内容</param>
        /// <param name="type">调用方式</param>
        /// <returns></returns>
        public static string PostWeb(string url, Dictionary<string, string> requeseData, Constant.MethodType type = Constant.MethodType.POST)
        {
            string result = null;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                //request.Method = "POST";
                request.Method = Enum.GetName(typeof(Constant.MethodType), type);
                request.ContentType = "application/x-www-form-urlencoded";
                string content = "";
                if (requeseData != null && requeseData.Count > 0)
                    foreach (var item in requeseData)
                        content += string.Format("{0}={1}&", item.Key, item.Value);
                if (content.Length > 0)
                    content = content.Substring(0, content.Length - 1);
                byte[] data = Encoding.UTF8.GetBytes(content);
                //request.ContentLength = content.Length;

                Stream intStream = request.GetRequestStream();
                intStream.Write(data, 0, data.Length);
                intStream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream outStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(outStream, Encoding.UTF8);
                result = reader.ReadToEnd();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return result;
        }

        /// <summary>
        /// 发起GET请求
        /// </summary>
        /// <param name="url">Url</param>
        /// <param name="type">调用方式</param>
        /// <returns></returns>
        public static string GetWeb(string url,Constant.MethodType type = Constant.MethodType.GET)
        {
            string result = null;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                //request.Method = "POST";
                request.Method = Enum.GetName(typeof(Constant.MethodType), type);
                //request.ContentType = "application/json";
                //byte[] data = Encoding.UTF8.GetBytes(content);
                ////request.ContentLength = content.Length;
                //request.ContentLength = data.Length;

                //Stream intStream = request.GetRequestStream();
                //intStream.Write(data, 0, data.Length);
                //intStream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream outStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(outStream, Encoding.UTF8);
                result = reader.ReadToEnd();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return result;
        }
        
        /// <summary>
        /// 发起带参GET请求
        /// </summary>
        /// <param name="url">Url</param>
        /// <param name="getPara">调用参数</param>
        /// <param name="type">调用方式</param>
        /// <returns></returns>
        public static string GetWeb(string url,Dictionary<string,string> getPara,Constant.MethodType type = Constant.MethodType.GET)
        {
            string tmpUrl = url;
            if (getPara != null)
                foreach (var dic in getPara)
                    tmpUrl += string.Format("&{0}={1}", dic.Key, dic.Value);
            return GetWeb(tmpUrl);
        }

        /// <summary>
        /// 解密密文
        /// </summary>
        /// <param name="source">密文</param>
        /// <param name="key">密钥</param>
        /// <returns></returns>
        public static string AESDecrypt(string source,string key)
        {
            if (string.IsNullOrEmpty(source))
                return null;
            byte[] toArray = Convert.FromBase64String(source);
            RijndaelManaged rm = new RijndaelManaged
            {
                Key = Encoding.UTF8.GetBytes(key),
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };
            ICryptoTransform transform = rm.CreateDecryptor();
            byte[] resultByte = transform.TransformFinalBlock(toArray, 0, toArray.Length);

            return Encoding.UTF8.GetString(resultByte);
        }

        /// <summary>
        /// 公共号密钥生成
        /// </summary>
        /// <param name="_token"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public static string PublicAccSHA(string _token,params string[] para)
        {
            string result = null;
            if (!string.IsNullOrWhiteSpace(_token) && para != null && para.Length > 0)
            {
                string[] tmpPara = new string[(para.Length + 1)];
                tmpPara[0] = _token;
                for (int i = 0; i < para.Length; i++)
                    tmpPara[i + 1] = para[i];
                Array.Sort(tmpPara, StringComparer.Ordinal);
                string sortData = String.Join("", tmpPara);
                byte[] src = Encoding.Default.GetBytes(sortData);
                HashAlgorithm sha1 = new SHA1CryptoServiceProvider();
                byte[] target = sha1.ComputeHash(src);
                StringBuilder sb = new StringBuilder();
                foreach (byte b in target)
                {
                    sb.AppendFormat("{0:x2}", b);
                }
                result = sb.ToString();
            }
            else
            {
                throw new Exception("公共号密钥参数不全");
            }
            return result;
        }
    }
}
