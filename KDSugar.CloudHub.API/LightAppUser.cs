﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API
{
    /// <summary>
    /// 轻应用用户信息验证|发起
    /// </summary>
    public class LightAppUser : PostRequest
    {
        /// <summary>
        /// 轻应用AppId
        /// </summary>
        public string appid { get; set; }

        /// <summary>
        /// 访问Ticket
        /// </summary>
        public string ticket { get; set; }

        private const string URL = "https://www.yunzhijia.com/gateway/ticket/user/acquirecontext?accessToken={0}";

        /// <summary>
        /// 初始化轻应用用户信息验证
        /// </summary>
        /// <param name="_appId">轻应用AppId</param>
        /// <param name="_ticket">访问Ticket</param>
        public LightAppUser(string _appId,string _ticket)
        {
            appid = _appId;
            ticket = _ticket;
        }

        /// <summary>
        /// 获取用户上下文信息
        /// </summary>
        /// <param name="token">Token</param>
        /// <returns></returns>
        public _LightAppUser GetLightAppUser(string token)
        {
            return Post<_LightAppUser>(URL, token);
        }
    }

    /// <summary>
    /// 轻应用用户信息验证|返回
    /// </summary>
    public class _LightAppUser : BaseResult
    {
        /// <summary>
        /// 用户信息
        /// </summary>
        public UserInfo data { get; set; }

        /// <summary>
        /// 用户信息类
        /// </summary>
        public class UserInfo
        {
            /// <summary>
            /// 轻应用id
            /// </summary>
            public string appid { get; set; }

            /// <summary>
            /// 团队id,仅供金蝶内部使用
            /// </summary>
            public string xtid { get; set; }

            /// <summary>
            /// 团队用户id,仅供金蝶内部使用
            /// </summary>
            public string oid { get; set; }

            /// <summary>
            /// 团队id
            /// </summary>
            public string eid { get; set; }

            /// <summary>
            /// 用户姓名
            /// </summary>
            public string username { get; set; }

            /// <summary>
            /// 云之家用户id
            /// </summary>
            public string userid { get; set; }

            /// <summary>
            /// 云平台用户id,仅供金蝶内部使用
            /// </summary>
            public string uid { get; set; }

            /// <summary>
            /// 云平台工作圈id
            /// </summary>
            public string tid { get; set; }

            /// <summary>
            /// 工号
            /// </summary>
            public string jobNo { get; set; }

            /// <summary>
            /// 工作圈id
            /// </summary>
            public string networkid { get; set; }

            /// <summary>
            /// 设备id
            /// </summary>
            public string deviceId { get; set; }

            /// <summary>
            /// 团队用户id
            /// </summary>
            public string openid { get; set; }

            /// <summary>
            /// 云之家APP会传递ticket参数给轻应用（把ticket参数追加到轻应用对应的url中）
            /// </summary>
            public string ticket { get; set; }
        }
    }
}
