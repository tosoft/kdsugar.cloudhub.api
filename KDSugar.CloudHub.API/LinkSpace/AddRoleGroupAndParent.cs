﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.LinkSpace
{
    /// <summary>
    /// 新增业务范围|发起
    /// </summary>
    public class AddRoleGroupAndParent : PostRequest
    {
        private const string URL = "/gateway/open/linkspace/spaceRoleGroup/addRoleGroupAndParent?accessToken={0}";

        /// <summary>
        /// 生态圈空间Id
        /// </summary>
        public string spaceId { get; set; }

        /// <summary>
        /// 用户所在企业的eid
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 用户手机号
        /// </summary>
        public string phone { get; set; }

        public List<RoleGroupAndParentInfo> data { get; set; }

        public class RoleGroupAndParentInfo
        {
            public string name { get; set; }

            public RoleGroupAndParentInfo(string _name)
            {
                name = _name;
            }
        }

        public AddRoleGroupAndParent(string _spaceId,string _eid,string _phone)
        {
            spaceId = _spaceId;
            eid = _eid;
            phone = _phone;
            data = new List<RoleGroupAndParentInfo>();
        }

        public void AddRoleGroupAndParentInfo(RoleGroupAndParentInfo info)
        {
            data.Add(info);
        }

        public _AddRoleGroupAndParent Operate(string _token)
        {
            return Post<_AddRoleGroupAndParent>(Constant.ROOTURL + URL, _token);
        }
    }

    /// <summary>
    /// 新增业务范围|结束
    /// </summary>
    public class _AddRoleGroupAndParent : BaseResult
    {
        public List<MessageInfo> data { get; set; }

        public class MessageInfo
        {
            public string id { get; set; }

            public string name { get; set; }

            public string message { get; set; }

            public long? errCode { get; set; }
        }
    }
}
