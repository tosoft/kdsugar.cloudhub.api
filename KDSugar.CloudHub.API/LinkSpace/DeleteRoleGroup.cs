﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.LinkSpace
{
    /// <summary>
    /// 删除业务范围|发起
    /// </summary>
    public class DeleteRoleGroup : PostRequest
    {
        private const string URL = "/gateway/open/linkspace/spaceRoleGroup/deleteRoleGroup?accessToken={0}";

        /// <summary>
        /// 生态圈空间Id
        /// </summary>
        public string spaceId { get; set; }

        /// <summary>
        /// 用户所在企业的eid
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 用户手机号
        /// </summary>
        public string phone { get; set; }

        /// <summary>
        /// 业务范围id
        /// </summary>
        public List<string> data { get; set; }

        public DeleteRoleGroup(string _spaceId,string _eid,string _phone)
        {
            spaceId = _spaceId;
            eid = _eid;
            phone = _phone;
            data = new List<string>();
        }

        public void AddDeleteRoleGroupId(string id)
        {
            data.Add(id);
        }

        public _DeleteRoleGroup Delete(string _token)
        {
            return Post<_DeleteRoleGroup>(Constant.ROOTURL + URL, _token);
        }
    }

    /// <summary>
    /// 删除业务范围|返回
    /// </summary>
    public class _DeleteRoleGroup : BaseResult
    {
        public List<ErrorMessage> data { get; set; }

        public class ErrorMessage
        {
            public long errCode { get; set; }

            public string id { get; set; }

            public string message { get; set; }
        }
    }
}
