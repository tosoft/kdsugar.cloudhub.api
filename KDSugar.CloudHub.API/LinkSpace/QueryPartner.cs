﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.LinkSpace
{
    /// <summary>
    /// 查询主企业所负责的合作伙伴列表|发起
    /// </summary>
    public class QueryPartner : PostRequest
    {
        private const string URL = "/gateway/open/linkspace/spacePartner/search?accessToken={0}";

        /// <summary>
        /// 空间ID，对应空间列表接口的ID值
        /// </summary>
        public string spaceId { get; set; }

        /// <summary>
        /// 搜索关键字，只支持伙伴名称查找
        /// </summary>
        public string key { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public int page { get; set; }

        /// <summary>
        /// 每页条数
        /// </summary>
        public int size { get; set; }

        /// <summary>
        /// 查询主企业所负责的合作伙伴列表|初始化
        /// </summary>
        /// <param name="_spaceId">空间ID，对应空间列表接口的ID值</param>
        public QueryPartner(string _spaceId)
        {
            spaceId = _spaceId;
        }

        /// <summary>
        /// 查询主企业所负责的合作伙伴列表|返回
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _QueryPartner Query(string _token)
        {
            return Post<_QueryPartner>(Constant.ROOTURL + URL, _token);
        }
    }

    /// <summary>
    /// 查询主企业所负责的合作伙伴列表|返回
    /// </summary>
    public class _QueryPartner : BaseResult
    {
        /// <summary>
        /// 主企业所负责的合作伙伴列表
        /// </summary>
        public QueryPartnerResult data { get; set; }

        /// <summary>
        /// 主企业所负责的合作伙伴列表-类
        /// </summary>
        public class QueryPartnerResult
        {
            public int milliseconds { get; set; }

            public int pageNumber { get; set; }

            public int totalPages { get; set; }

            public int count { get; set; }

            public int pageSize { get; set; }

            public List<PartnerInfo> list { get; set; }
        }

        /// <summary>
        /// 合作伙伴信息
        /// </summary>
        public class PartnerInfo
        {
            /// <summary>
            /// 空间ID
            /// </summary>
            public string spaceId { get; set; }

            /// <summary>
            /// 主企业EID
            /// </summary>
            public string eid { get; set; }

            /// <summary>
            /// 伙伴企业的可见人数
            /// </summary>
            public int userCount { get; set; }

            /// <summary>
            /// 伙伴企业公司名称
            /// </summary>
            public string partnerName { get; set; }

            /// <summary>
            /// 伙伴企业EID
            /// </summary>
            public string partnerEid { get; set; }

            /// <summary>
            /// 伙伴企业ID,用于查询具体的伙伴企业信息
            /// </summary>
            public string id { get; set; }

            /// <summary>
            /// 伙伴企业工作圈名称
            /// </summary>
            public string partnerEname { get; set; }
        }
    }
}
