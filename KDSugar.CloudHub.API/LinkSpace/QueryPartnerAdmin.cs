﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.LinkSpace
{
    /// <summary>
    /// 获取伙伴企业的负责人列表|发起
    /// </summary>
    public class QueryPartnerAdmin : PostRequest
    {
        private const string URL = "/gateway/open/linkspace/spaceUser/partnerAdminList?accessToken={0}";

        /// <summary>
        /// 空间id
        /// </summary>
        public string spaceId { get; set; }

        /// <summary>
        /// 伙伴企业eid
        /// </summary>
        public string partnerEid { get; set; }

        /// <summary>
        /// 获取伙伴企业的负责人列表|初始化
        /// </summary>
        /// <param name="_spaceId">空间id</param>
        /// <param name="_partnerEid">伙伴企业eid</param>
        public QueryPartnerAdmin(string _spaceId,string _partnerEid)
        {
            spaceId = _spaceId;
            partnerEid = _partnerEid;
        }

        /// <summary>
        /// 获取伙伴企业的负责人列表|返回
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _QueryPartnerAdmin Query(string _token)
        {
            return Post<_QueryPartnerAdmin>(Constant.ROOTURL + URL, _token);
        }
    }

    /// <summary>
    /// 获取伙伴企业的负责人列表|返回
    /// </summary>
    public class _QueryPartnerAdmin : BaseResult
    {
        /// <summary>
        /// 伙伴企业负责人信息
        /// </summary>
        public List<PartnerAdminInfo> data { get; set; }

        /// <summary>
        /// 伙伴企业负责人信息类
        /// </summary>
        public class PartnerAdminInfo
        {
            /// <summary>
            /// 伙伴企业eid
            /// </summary>
            public string partnerEid { get; set; }

            /// <summary>
            /// 伙伴企业圈名
            /// </summary>
            public string partnerEname { get; set; }

            /// <summary>
            /// 空间id
            /// </summary>
            public string spaceId { get; set; }

            /// <summary>
            /// 用户组织长ID
            /// </summary>
            public string userLongOrgId { get; set; }

            /// <summary>
            /// 人员类型，是否为主企业或者伙伴企业
            /// </summary>
            public string userType { get; set; }

            /// <summary>
            /// 用户信息
            /// </summary>
            public AdminInfo user { get; set; }

            /// <summary>
            /// 用户信息类
            /// </summary>
            public class AdminInfo
            {
                /// <summary>
                /// 部门名称
                /// </summary>
                public string department { get; set; }

                /// <summary>
                /// 用户所属企业EID
                /// </summary>
                public string eid { get; set; }

                /// <summary>
                /// 用户OID
                /// </summary>
                public string oid { get; set; }

                /// <summary>
                /// 用户openId
                /// </summary>
                public string openId { get; set; }

                /// <summary>
                /// 组织ID
                /// </summary>
                public string orgId { get; set; }

                /// <summary>
                /// 手机号
                /// </summary>
                public string phone { get; set; }

                /// <summary>
                /// 头像URL
                /// </summary>
                public string photoUrl { get; set; }

                /// <summary>
                /// 用户姓名
                /// </summary>
                public string userName { get; set; }
            }
        }
    }
}
