﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.LinkSpace
{
    /// <summary>
    /// 查询伙伴对应的主企业业务对接人列表|发起
    /// </summary>
    public class QueryPartnerCharge : PostRequest
    {
        private const string URL = "/gateway/open/linkspace/spacePartnerCharge/partnerChargeList?accessToken={0}";

        /// <summary>
        /// 空间id
        /// </summary>
        public string spaceId { get; set; }

        /// <summary>
        /// 伙伴企业eid
        /// </summary>
        public string partnerEid { get; set; }

        /// <summary>
        /// 查询伙伴对应的主企业业务对接人列表|初始化
        /// </summary>
        /// <param name="_spaceId">空间id</param>
        /// <param name="_partnerEid">伙伴企业eid</param>
        public QueryPartnerCharge(string _spaceId,string _partnerEid)
        {
            spaceId = _spaceId;
            partnerEid = _partnerEid;
        }

        /// <summary>
        /// 查询伙伴对应的主企业业务对接人列表|返回
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _QueryPartnerCharge Query(string _token)
        {
            return Post<_QueryPartnerCharge>(Constant.ROOTURL + URL, _token);
        }
    }

    /// <summary>
    /// 查询伙伴对应的主企业业务对接人列表|返回
    /// </summary>
    public class _QueryPartnerCharge : BaseResult
    {
        /// <summary>
        /// 伙伴对应的主企业业务对接人列表
        /// </summary>
        public List<PartnerChargeInfo> data { get; set; }

        /// <summary>
        /// 伙伴对应的主企业业务对接人列表-类
        /// </summary>
        public class PartnerChargeInfo
        {

            public string longOrgId { get; set; }
            
            public string orgId { get; set; }

            /// <summary>
            /// 伙伴企业eid
            /// </summary>
            public string partnerEid { get; set; }

            /// <summary>
            /// 空间id
            /// </summary>
            public string spaceId { get; set; }

            /// <summary>
            /// 接口人信息
            /// </summary>
            public ChargeUserInfo chargeUser { get; set; }

            /// <summary>
            /// 接口人信息类
            /// </summary>
            public class ChargeUserInfo
            {
                public string department { get; set; }

                public string eid { get; set; }

                public string oid { get; set; }

                public string openId { get; set; }

                public string orgId { get; set; }

                public string phone { get; set; }

                public string photoUrl { get; set; }

                public string userName { get; set; }
            }
        }
    }
}
