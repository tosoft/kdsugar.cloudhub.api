﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.LinkSpace
{
    /// <summary>
    /// 查询生态圈内的主企业和伙伴企业间的授权状态|发起
    /// </summary>
    public class QueryPartnerIdentity : PostRequest
    {
        private const string URL = "/gateway/open/linkspace/spacePartner/getPartnerIdentity?accessToken={0}";

        /// <summary>
        /// 空间id
        /// </summary>
        public string spaceId { get; set; }

        /// <summary>
        /// 主企业eid
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 伙伴企业eid
        /// </summary>
        public string partnerEid { get; set; }

        /// <summary>
        /// 查询生态圈内的主企业和伙伴企业间的授权状态|初始化
        /// </summary>
        /// <param name="_spaceId">空间id</param>
        /// <param name="_eid">主企业eid</param>
        /// <param name="_partnerEid">伙伴企业eid</param>
        public QueryPartnerIdentity(string _spaceId,string _eid,string _partnerEid)
        {
            spaceId = _spaceId;
            eid = _eid;
            partnerEid = _partnerEid;
        }

        /// <summary>
        /// 查询生态圈内的主企业和伙伴企业间的授权状态|返回
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _QueryPartnerIdentity Query(string _token)
        {
            return Post<_QueryPartnerIdentity>(Constant.ROOTURL + URL, _token);
        }
    }

    /// <summary>
    /// 查询生态圈内的主企业和伙伴企业间的授权状态|返回
    /// </summary>
    public class _QueryPartnerIdentity : BaseResult
    {
        /// <summary>
        /// 授权状态信息
        /// </summary>
        public AuthInfo data { get; set; }

        /// <summary>
        /// 授权状态信息类
        /// </summary>
        public class AuthInfo
        {
            /// <summary>
            /// 授权状态
            /// </summary>
            public int status { get; set; }

            /// <summary>
            /// 是否处于授权状态
            /// </summary>
            [JsonIgnore]
            public bool IsAuth
            {
                get
                {
                    return status == 1 ? true : false;
                }
            }
        }
    }
}
