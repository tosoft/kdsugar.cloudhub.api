﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.LinkSpace
{
    /// <summary>
    /// 查询业务范围|发起
    /// </summary>
    public class QueryRoleGroup : PostRequest
    {
        private const string URL = "/gateway/open/linkspace/spaceRoleGroup/listRoleGroup?accessToken={0}";

        /// <summary>
        /// 生态圈空间Id
        /// </summary>
        public string spaceId { get; set; }

        /// <summary>
        /// 用户所在企业的eid
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 用户手机号
        /// </summary>
        public string phone { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_spaceId"></param>
        /// <param name="_eid"></param>
        /// <param name="_phone"></param>
        public QueryRoleGroup(string _spaceId,string _eid,string _phone)
        {
            spaceId = _spaceId;
            eid = _eid;
            phone = _phone;
        }

        public _QueryRoleGroup Query(string _token)
        {
            return Post<_QueryRoleGroup>(Constant.ROOTURL + URL, _token);
        }
    }

    /// <summary>
    /// 查询业务范围|返回
    /// </summary>
    public class _QueryRoleGroup : BaseResult
    {
        public List<RoleGroupInfo> data { get; set; }

        public class RoleGroupInfo
        {
            public string spaceId { get; set; }

            public string name { get; set; }

            public string weight { get; set; }

            public string pid { get; set; }

            public string id { get; set; }

            public string longName { get; set; }

            public RoleGroupInfo children { get; set; }
        }
    }
}
