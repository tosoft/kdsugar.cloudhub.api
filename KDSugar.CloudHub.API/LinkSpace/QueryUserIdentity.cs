﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.LinkSpace
{
    /// <summary>
    /// 查询该用户在生态圈的身份|发起
    /// </summary>
    public class QueryUserIdentity : PostRequest
    {
        private const string URL = "/gateway/open/linkspace/spaceUser/getUserIdentity?accessToken={0}";

        /// <summary>
        /// 生态圈空间Id
        /// </summary>
        public string spaceId { get; set; }

        /// <summary>
        /// 用户所在企业的eid
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 用户手机号
        /// </summary>
        public string phone { get; set; }

        /// <summary>
        /// 查询该用户在生态圈的身份|初始化
        /// </summary>
        /// <param name="_spaceId">生态圈空间Id</param>
        /// <param name="_eid">用户所在企业的eid</param>
        /// <param name="_phone">用户手机号</param>
        public QueryUserIdentity(string _spaceId,string _eid,string _phone)
        {
            spaceId = _spaceId;
            eid = _eid;
            phone = _phone;
        }

        /// <summary>
        /// 查询该用户在生态圈的身份|返回
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _QueryUserIdentity Query(string _token)
        {
            return Post<_QueryUserIdentity>(Constant.ROOTURL + URL, _token);
        }
    }

    /// <summary>
    /// 查询该用户在生态圈的身份|返回
    /// </summary>
    public class _QueryUserIdentity : BaseResult
    {
        /// <summary>
        /// 用户生态圈信息
        /// </summary>
        public UserIdentityInfo data { get; set; }

        /// <summary>
        /// 用户生态圈信息类
        /// </summary>
        public class UserIdentityInfo
        {
            /// <summary>
            /// 是否为伙伴负责人
            /// </summary>
            public bool partnerAdmin { get; set; }

            /// <summary>
            /// 是否为主企业空间管理员
            /// </summary>
            public bool spaceAdmin { get; set; }

            /// <summary>
            /// 是否为业务对接人
            /// </summary>
            public bool partnerCharge { get; set; }

            /// <summary>
            /// 是否为主企业普通成员
            /// </summary>
            public bool mainUserFlag { get; set; }

            /// <summary>
            /// 是否为伙伴企业接口人
            /// </summary>
            public bool partnerContact { get; set; }
        }
    }
}
