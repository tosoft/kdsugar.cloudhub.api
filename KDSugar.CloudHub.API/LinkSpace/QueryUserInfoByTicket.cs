﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.LinkSpace
{
    /// <summary>
    /// 通过ticket获取生态圈成员身份信息|发起
    /// </summary>
    public class QueryUserInfoByTicket : PostRequest
    {
        private const string URL = "/gateway/open/linkspace/linkSpace/getUserInfoByTicket?accessToken={0}";

        public string userTicket { get; set; }

        public QueryUserInfoByTicket(string _userTicket)
        {
            userTicket = _userTicket;
        }

        public _QueryUserInfoByTicket Query(string _token)
        {
            return Post<_QueryUserInfoByTicket>(Constant.ROOTURL + URL, _token);
        }
    }

    /// <summary>
    /// 通过ticket获取生态圈成员身份信息|返回
    /// </summary>
    public class _QueryUserInfoByTicket : BaseResult
    {
        public UserInfo data { get; set; }

        public class UserInfo
        {
            public string eid { get; set; }

            public string gender { get; set; }

            public string openId { get; set; }

            public string jobTitle { get; set; }

            public string oid { get; set; }

            public string userName { get; set; }

            public string userId { get; set; }

            public string orgId { get; set; }

            public string photoUrl { get; set; }

            public string phone { get; set; }

            public string name { get; set; }

            public string personId { get; set; }

            public string department { get; set; }

            public string email { get; set; }

            public int status { get; set; }
        }
    }
}
