﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.LinkSpace
{
    /// <summary>
    /// 批量新增伙伴企业|发起
    /// </summary>
    public class SaveSpacePartner : PostRequest
    {
        private const string URL = "/gateway/open/linkspace/spacePartner/batchSaveSpacePartner?accessToken={0}";

        /// <summary>
        /// 空间ID，对应空间列表接口的ID值
        /// </summary>
        public string spaceId { get; set; }

        /// <summary>
        /// 批量新增信息
        /// </summary>
        public List<SpacePartnerInfo> data { get; set; }

        /// <summary>
        /// 伙伴企业信息
        /// </summary>
        public class SpacePartnerInfo
        {
            public string partnerEid { get; set; }

            public string partnerEname { get; set; }

            public string address { get; set; }

            public class UserInfo
            {
                public string userName { get; set; }

                public string phone { get; set; }
            }

            public List<UserInfo> partnerUsers { get; set; }

            public List<UserInfo> chargeUsers { get; set; }

            public List<object> roleGroup { get; set; }
        }

        /// <summary>
        /// 批量新增伙伴企业|初始化
        /// </summary>
        /// <param name="_spaceId">空间ID</param>
        public SaveSpacePartner(string _spaceId)
        {
            spaceId = _spaceId;
            data = new List<SpacePartnerInfo>();
        }

        /// <summary>
        /// 新增伙伴企业
        /// </summary>
        /// <param name="item"></param>
        public void AddSpacePartner(SpacePartnerInfo item)
        {
            data.Add(item);
        }

        /// <summary>
        /// 批量新增伙伴企业
        /// </summary>
        /// <param name="items"></param>
        public void AddSpacePartner(List<SpacePartnerInfo> items)
        {
            data.AddRange(items);
        }

        /// <summary>
        /// 批量新增伙伴企业|返回
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _SaveSpacePartner Save(string _token)
        {
            return Post<_SaveSpacePartner>(Constant.ROOTURL + URL, _token);
        }
    }

    /// <summary>
    /// 批量新增伙伴企业|返回
    /// </summary>
    public class _SaveSpacePartner : BaseResult
    {
        /// <summary>
        /// 出错的伙伴列表
        /// </summary>
        public List<string> data { get; set; }
    }
}
