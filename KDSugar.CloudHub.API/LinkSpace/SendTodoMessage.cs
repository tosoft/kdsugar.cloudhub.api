﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.LinkSpace
{
    /// <summary>
    /// 生态圈发送待办消息|发起
    /// </summary>
    public class SendTodoMessage : PostRequest
    {
        private const string URL = "/gateway/open/linkspace/spaceApp/sendToDoMessage?accessToken={0}";

        public string sourceId { get; set; }

        public string senderId { get; set; }

        public string appId { get; set; }

        public string headImg { get; set; }

        public string itemtitle { get; set; }

        public string title { get; set; }

        public string content { get; set; }

        public string url { get; set; }

        [JsonProperty(PropertyName = "params")]
        public List<ParamInfo> Params { get; set; }

        public class ParamInfo
        {
            public string openId { get; set; }

            public class ParamStatus
            {
                public int READ { get; set; }

                public int DO { get; set; }
            }
        }

        public _SendTodoMessage Operate(string _token)
        {
            return Post<_SendTodoMessage>(Constant.ROOTURL + URL, _token);
        }
    }

    /// <summary>
    /// 生态圈发送待办消息|返回
    /// </summary>
    public class _SendTodoMessage : BaseResult
    {
        public bool data { get; set; }
    }
}
