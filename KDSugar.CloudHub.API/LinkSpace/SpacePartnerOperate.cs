﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.LinkSpace
{
    /// <summary>
    /// 伙伴企业合作状态操作|发起
    /// </summary>
    public class SpacePartnerOperate : PostRequest
    {
        private const string RESUME_URL = "/gateway/open/linkspace/spacePartner/resumeSpacePartner?accessToken={0}";

        private const string CANCEL_URL = "/gateway/open/linkspace/spacePartner/cancelSpacePartner?accessToken={0}";

        private const string DELETE_URL = "/gateway/open/linkspace/spacePartner/delSpacePartner?accessToken={0}";

        public string spaceId { get; set; }

        public string phone { get; set; }

        public string eid { get; set; }

        public List<string> partnerEids { get; set; }

        /// <summary>
        /// 伙伴企业合作状态操作类型
        /// </summary>
        public enum OperateTypeEnum
        {
            /// <summary>
            /// 启用伙伴企业合作
            /// </summary>
            RESUME,
            /// <summary>
            /// 解除伙伴企业合作
            /// </summary>
            CANCEL,
            /// <summary>
            /// 删除伙伴企业
            /// </summary>
            DELETE
        }

        private OperateTypeEnum OperateType { get; set; }

        public SpacePartnerOperate(string _spaceId,OperateTypeEnum _operateType)
        {
            spaceId = _spaceId;
            OperateType = _operateType;
        }

        public _SpacePartnerOperate Resume(string _token)
        {
            string tmpUrl = "";
            switch (OperateType)
            {
                case OperateTypeEnum.RESUME:
                    tmpUrl = RESUME_URL;
                    break;
                case OperateTypeEnum.CANCEL:
                    tmpUrl = CANCEL_URL;
                    break;
                case OperateTypeEnum.DELETE:
                    tmpUrl = DELETE_URL;
                    break;
            }
            return Post<_SpacePartnerOperate>(Constant.ROOTURL + tmpUrl, _token);
        }
    }

    /// <summary>
    /// 伙伴企业合作状态操作|返回
    /// </summary>
    public class _SpacePartnerOperate : BaseResult
    {
        public List<string> data { get; set; }
    }
}
