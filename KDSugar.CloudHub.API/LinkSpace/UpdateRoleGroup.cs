﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.LinkSpace
{
    /// <summary>
    /// 更新业务范围|发起
    /// </summary>
    public class UpdateRoleGroup : PostRequest
    {
        private const string URL = "/gateway/open/linkspace/spaceRoleGroup/updateRoleGroup?accessToken={0}";

        /// <summary>
        /// 生态圈空间Id
        /// </summary>
        public string spaceId { get; set; }

        /// <summary>
        /// 用户所在企业的eid
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 用户手机号
        /// </summary>
        public string phone { get; set; }

        public List<RoleGroupInfo> data { get; set; }

        public class RoleGroupInfo
        {
            /// <summary>
            /// 父节点id,没有可不传
            /// </summary>
            public string id { get; set; }

            public string name { get; set; }

            public RoleGroupInfo(string _id,string _name)
            {
                id = _id;
                name = _name;
            }
        }

        public UpdateRoleGroup(string _spaceId,string _eid,string _phone)
        {
            spaceId = _spaceId;
            eid = _eid;
            phone = _phone;
            data = new List<RoleGroupInfo>();
        }

        public void AddRoleGroupInfo(RoleGroupInfo info)
        {
            data.Add(info);
        }

        public _UpdateRoleGroup Operate(string _token)
        {
            return Post<_UpdateRoleGroup>(Constant.ROOTURL + URL, _token);
        }
    }

    /// <summary>
    /// 更新业务范围|返回
    /// </summary>
    public class _UpdateRoleGroup : BaseResult
    {
        public List<ErrorMessage> data { get; set; }

        public class ErrorMessage
        {
            public long errCode { get; set; }

            public string name { get; set; }

            public string weight { get; set; }

            public string id { get; set; }

            public string message { get; set; }
        }
    }
}
