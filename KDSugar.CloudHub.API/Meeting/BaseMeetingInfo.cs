﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.Meeting
{
    /// <summary>
    /// 会议信息查询结果(基类)
    /// </summary>
    public class BaseMeetingInfo
    {
        /// <summary>
        /// 会议ID
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 会议创建者的oid
        /// </summary>
        public string openid { get; set; }

        /// <summary>
        /// 会议类型(null:普通会议、sign:线下签到类会议、voice:语音类会议)
        /// </summary>
        [Obsolete("建议使用枚举属性MeetingType")]
        public string type { get; set; }

        /// <summary>
        /// 会议类型枚举
        /// </summary>
        public enum MeetingTypeEnum
        {
            /// <summary>
            /// 普通会议
            /// </summary>
            General,
            /// <summary>
            /// 线下签到类会议
            /// </summary>
            Sign,
            /// <summary>
            /// 语音类会议
            /// </summary>
            Voice,
            /// <summary>
            /// 未知
            /// </summary>
            Unknow
        }

        /// <summary>
        /// 会议类型
        /// </summary>
        [JsonIgnore]
        public MeetingTypeEnum MeetingType
        {
            get
            {
                MeetingTypeEnum result = MeetingTypeEnum.Unknow;
                if (!string.IsNullOrWhiteSpace(type))
                {
                    switch (type.ToLower())
                    {
                        case "null":
                            result = MeetingTypeEnum.General;
                            break;
                        case "sign":
                            result = MeetingTypeEnum.Sign;
                            break;
                        case "voice":
                            result = MeetingTypeEnum.Voice;
                            break;
                        default:
                            result = MeetingTypeEnum.Unknow;
                            break;
                    }
                }
                else
                {
                    if (type == null)
                        result = MeetingTypeEnum.General;
                }
                return result;
            }
        }

        /// <summary>
        /// 会议标题
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 会议内容
        /// </summary>
        public string content { get; set; }

        /// <summary>
        /// 会议地址
        /// </summary>
        public string meetingPlace { get; set; }

        /// <summary>
        /// 会议状态(0:未完成、1:完成、2:删除)
        /// </summary>
        [Obsolete("建议使用枚举属性MeetingStatus")]
        public int meetingStatus { get; set; }

        /// <summary>
        /// 会议状态枚举
        /// </summary>
        public enum MeetingStatusEnum
        {
            /// <summary>
            /// 未完成
            /// </summary>
            NotFinish,
            /// <summary>
            /// 完成
            /// </summary>
            Finish,
            /// <summary>
            /// 删除
            /// </summary>
            Delete
        }

        /// <summary>
        /// 会议状态
        /// </summary>
        [JsonIgnore]
        public MeetingStatusEnum MeetingStatus
        {
            get
            {
                MeetingStatusEnum result = MeetingStatusEnum.NotFinish;
                switch (meetingStatus)
                {
                    case 0:
                        result = MeetingStatusEnum.NotFinish;
                        break;
                    case 1:
                        result = MeetingStatusEnum.Finish;
                        break;
                    case 2:
                        result = MeetingStatusEnum.Delete;
                        break;
                }
                return result;
            }
        }

        /// <summary>
        /// 是否已读(0:未读、1:已读)
        /// </summary>
        [Obsolete("可使用布尔属性ReadStatus")]
        public int readStatus { get; set; }

        /// <summary>
        /// 是否已读
        /// </summary>
        [JsonIgnore]
        public bool ReadStatus { get { return readStatus == 1 ? true : false; } }

        /// <summary>
        /// 接受状态(0:未响应、1:不接受、2:接受）
        /// </summary>
        public int acceptStatus { get; set; }

        /// <summary>
        /// 创建者姓名
        /// </summary>
        public string personName { get; set; }

        /// <summary>
        /// 完成时间戳
        /// </summary>
        public long doneTime { get; set; }

        /// <summary>
        /// 提醒时间(-1:不提醒、0:开始时间提醒、15:开始时间前15分钟提醒、60:开始时间前1小时提醒)
        /// </summary>
        public int noticeTime { get; set; }

        /// <summary>
        /// 会议开始时间戳
        /// </summary>
        public long startDate { get; set; }

        /// <summary>
        /// 会议结束时间戳
        /// </summary>
        public long endDate { get; set; }

        /// <summary>
        /// 会议创建时间
        /// </summary>
        public long createDate { get; set; }

        /// <summary>
        /// 会议室ID
        /// </summary>
        public string roomId { get; set; }

        /// <summary>
        /// 会议室订单ID
        /// </summary>
        public string roomOrderId { get; set; }
    }

}
