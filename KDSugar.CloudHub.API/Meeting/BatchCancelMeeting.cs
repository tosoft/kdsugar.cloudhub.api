﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Meeting
{
    /// <summary>
    /// 批量取消重复日程|发起
    /// </summary>
    public class BatchCancelMeeting : PostRequest
    {
        private const string URL = "http://www.yunzhijia.com/gateway/cloudwork/meeting/batchCancel?accessToken={0}";

        /// <summary>
        /// 会议ID
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 会议创建者的oid
        /// </summary>
        public string openid { get; set; }

        /// <summary>
        /// 批量ID(如果是父会议，则没有批量ID，可以不传)
        /// </summary>
        public string batchId { get; set; }

        /// <summary>
        /// 批量取消重复日程|构造
        /// </summary>
        /// <param name="_id">会议ID</param>
        /// <param name="_openid">会议创建者的oid</param>
        /// <param name="_batchId">批量ID(如果是父会议，则没有批量ID，可以不传)</param>
        public BatchCancelMeeting(string _id,string _openid,string _batchId)
        {
            id = _id;
            openid = _openid;
            batchId = _batchId;
        }

        /// <summary>
        /// 批量取消重复日程|返回
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public BaseResult Operate(string _token)
        {
            return Post<BaseResult>(URL, _token);
        }
    }
}
