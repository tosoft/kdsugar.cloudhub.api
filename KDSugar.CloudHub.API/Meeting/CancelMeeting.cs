﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Meeting
{
    /// <summary>
    /// 取消会议|发起
    /// </summary>
    public class CancelMeeting : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/gateway/cloudwork/meeting/cancel?accessToken={0}";

        /// <summary>
        /// 会议ID
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 会议创建者的oid
        /// </summary>
        public string openid { get; set; }

        /// <summary>
        /// 取消会议|构造
        /// </summary>
        /// <param name="_id">会议ID</param>
        /// <param name="_openId">会议创建者的oid</param>
        public CancelMeeting(string _id,string _openId)
        {
            id = _id;
            openid = _openId;
        }

        /// <summary>
        /// 取消会议|返回
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public BaseResult Operate(string _token)
        {
            return Post<BaseResult>(URL, _token);
        }
    }
}
