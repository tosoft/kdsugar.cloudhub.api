﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Meeting
{
    /// <summary>
    /// 新增会议|发起
    /// </summary>
    public class CreateMeeting : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/gateway/cloudwork/meeting/create?accessToken={0}";

        /// <summary>
        /// 会议创建者的oid
        /// </summary>
        public string openid { get; set; }

        /// <summary>
        /// 会议标题
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 会议内容
        /// </summary>
        public string content { get; set; }

        /// <summary>
        /// 会议地址
        /// </summary>
        public string meetingPlace { get; set; }

        /// <summary>
        /// 会议开始时间戳
        /// </summary>
        public long startDate { get; set; }

        /// <summary>
        /// 会议结束时间戳
        /// </summary>
        public long endDate { get; set; }

        /// <summary>
        /// 会议室ID
        /// </summary>
        public string roomId { get; set; }

        /// <summary>
        /// 提醒时间
        /// <para>-1:不提醒、0:开始时间提醒、15:开始时间前15分钟提醒、60:开始时间前1小时提醒</para>
        /// </summary>
        public List<int> noticeTime { get; set; }

        /// <summary>
        /// 是否标记为重要
        /// <para>0:否、1:是</para>
        /// </summary>
        public int topState { get; set; }

        /// <summary>
        /// 协作人oid的集合
        /// </summary>
        public List<string> actors { get; set; }

        /// <summary>
        /// 会议类型
        /// <para>null:普通会议、sign:线下签到类会议、voice:语音类会议</para>
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// 是否需要提交会议纪要(新的体会、总结)
        /// </summary>
        public bool submitExperience { get; set; }

        /// <summary>
        /// 新增会议
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _CreateMeeting Create(string _token)
        {
            return Post<_CreateMeeting>(URL, _token);
        }
    }

    /// <summary>
    /// 新增会议|返回
    /// </summary>
    public class _CreateMeeting : BaseResult
    {
        /// <summary>
        /// 返回数据
        /// </summary>
        public MeetingData data { get; set; }

        /// <summary>
        /// 返回数据类
        /// </summary>
        public class MeetingData
        {
            /// <summary>
            /// 添加成功后会议id
            /// </summary>
            public string meetingId { get; set; }
        }
    }
}
