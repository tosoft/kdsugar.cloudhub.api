﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Meeting
{
    /// <summary>
    /// 判断该工作圈是否有新增会议预定信息|发起
    /// </summary>
    public class HasNewMeeting : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/api/roomBook/third/hasNew?accessToken={0}";

        /// <summary>
        /// 工作圈eid
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 最后的时间
        /// </summary>
        public long? lastTime { get; set; } = null;

        /// <summary>
        /// 判断该工作圈是否有新增会议预定信息|初始化
        /// </summary>
        /// <param name="_eid">必填, 工作圈eid</param>
        /// <param name="_lastTime">选填, 最后的时间,判断该时间节点之后是否有新增预定信息</param>
        public HasNewMeeting(string _eid,DateTime? _lastTime = null)
        {
            eid = _eid;
            if (_lastTime != null)
                lastTime = Helper.ToUnixTime(_lastTime.Value);
        }

        /// <summary>
        /// 判断该工作圈是否有新增会议预定信息|返回
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _HasNewMeeting Operate(string _token)
        {
            return Post<_HasNewMeeting>(URL, _token);
        }
    }

    /// <summary>
    /// 判断该工作圈是否有新增会议预定信息|返回
    /// </summary>
    public class _HasNewMeeting : BaseResult
    {
        /// <summary>
        /// 判定数据
        /// </summary>
        public HasNew data { get; set; }

        /// <summary>
        /// 判定数据类
        /// </summary>
        public class HasNew
        {
            /// <summary>
            /// 是否有新的会议预定信息
            /// </summary>
            public bool hasNew { get; set; }
        }
    }
}
