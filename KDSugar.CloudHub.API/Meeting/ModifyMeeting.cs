﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Meeting
{
    /// <summary>
    /// 修改会议|发起
    /// </summary>
    public class ModifyMeeting : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/gateway/cloudwork/meeting/modify?accessToken={0}";

        /// <summary>
        /// 会议ID
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 会议创建者的oid
        /// </summary>
        public string openid { get; set; }

        /// <summary>
        /// 会议标题
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 会议内容
        /// </summary>
        public string content { get; set; }

        /// <summary>
        /// 会议地址
        /// </summary>
        public string meetingPlace { get; set; }

        /// <summary>
        /// 会议开始时间戳
        /// </summary>
        public long startDate { get; set; }

        /// <summary>
        /// 会议结束时间戳
        /// </summary>
        public long endDate { get; set; }

        /// <summary>
        /// 会议室ID
        /// </summary>
        public string roomId { get; set; }

        /// <summary>
        /// 提醒时间
        /// <para>-1:不提醒、0:开始时间提醒、15:开始时间前15分钟提醒、60:开始时间前1小时提醒</para>
        /// </summary>
        public List<int> noticeTime { get; set; }

        /// <summary>
        /// 是否标记为重要
        /// <para>0:否、1:是</para>
        /// </summary>
        public int topState { get; set; }

        /// <summary>
        /// 协作人oid的集合
        /// </summary>
        public List<string> actors { get; set; }

        /// <summary>
        /// 新加的协作人oid集合
        /// </summary>
        public List<string> addActors { get; set; }

        /// <summary>
        /// 删除的协作人oid集合
        /// </summary>
        public List<string> delActors { get; set; }

        /// <summary>
        /// 是否需要提交会议纪要(新的体会、总结)
        /// </summary>
        public bool submitExperience { get; set; }

        /// <summary>
        /// 修改会议
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public BaseResult Operate(string _token)
        {
            return Post<BaseResult>(URL, _token);
        }
    }
}
