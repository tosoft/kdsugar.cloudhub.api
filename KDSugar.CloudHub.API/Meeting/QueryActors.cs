﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Meeting
{
    /// <summary>
    /// 查询某个会议的与会人|发起
    /// </summary>
    public class QueryActors : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/api/roomBook/third/getActors?accessToken={0}";

        /// <summary>
        /// 会议订单id
        /// </summary>
        public string orderId { get; set; }

        /// <summary>
        /// 查询某个会议的与会人|初始化
        /// </summary>
        /// <param name="_orderId">必填，会议订单id</param>
        public QueryActors(string _orderId)
        {
            orderId = _orderId;
        }

        /// <summary>
        /// 查询某个会议的与会人|返回
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _QueryActors Query(string _token)
        {
            return Post<_QueryActors>(URL, _token);
        }
    }

    /// <summary>
    /// 查询某个会议的与会人|返回
    /// </summary>
    public class _QueryActors : BaseResult
    {
        /// <summary>
        /// 与会人信息
        /// </summary>
        public List<ActorInfo> data { get; set; }

        /// <summary>
        /// 与会人信息类
        /// </summary>
        public class ActorInfo
        {
            /// <summary>
            /// 用户openId
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 用户头像url
            /// </summary>
            public string headerUrl { get; set; }

            /// <summary>
            /// 用户姓名
            /// </summary>
            public string userName { get; set; }
        }
    }
}
