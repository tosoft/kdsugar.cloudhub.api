﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.Meeting
{
    /// <summary>
    /// 获取该时间节点之后的预定信息|发起
    /// </summary>
    public class QueryBookInfo : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/api/roomBook/third/bookInfo?accessToken={0}";

        /// <summary>
        /// 工作圈eid
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 最后的时间戳, 不填则返回所有会议预定信息
        /// </summary>
        public long? lastTime { get; set; }

        /// <summary>
        /// 页码，默认为1
        /// </summary>
        public int? pageIndex { get; set; }

        /// <summary>
        /// 分页数，默认50
        /// </summary>
        public int? pageSize { get; set; }

        /// <summary>
        /// 获取该时间节点之后的预定信息|初始化
        /// </summary>
        /// <param name="_eid">工作圈eid</param>
        public QueryBookInfo(string _eid)
        {
            eid = _eid;
        }

        /// <summary>
        /// 设置最后时间
        /// </summary>
        /// <param name="dt">最后时间</param>
        public void SetLastTime(DateTime dt)
        {
            lastTime = Helper.ToUnixTime(dt);
        }

        /// <summary>
        /// 获取预定信息
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _QueryBookInfo Query(string _token)
        {
            return Post<_QueryBookInfo>(URL, _token);
        }
    }

    /// <summary>
    /// 获取该时间节点之后的预定信息|返回
    /// </summary>
    public class _QueryBookInfo : BaseResult
    {
        /// <summary>
        /// 预定信息数据
        /// </summary>
        public BookInfoList data { get; set; }

        /// <summary>
        /// 预定信息数据类
        /// </summary>
        public class BookInfoList
        {
            /// <summary>
            /// 新增预定信息
            /// </summary>
            public List<BookInfo> add { get; set; }

            /// <summary>
            /// 已删除预定信息
            /// </summary>
            public List<BookInfo> delete { get; set; }
        }

        /// <summary>
        /// 
        /// </summary>
        public class BookInfo
        {
            /// <summary>
            /// 会议预定订单id
            /// </summary>
            public string orderId { get; set; }

            /// <summary>
            /// 会议室名称
            /// </summary>
            public string roomName { get; set; }

            /// <summary>
            /// 会议室地址
            /// </summary>
            public string roomDetail { get; set; }

            /// <summary>
            /// 会议日期
            /// </summary>
            public string day { get; set; }

            /// <summary>
            /// 预定人姓名
            /// </summary>
            public string userName { get; set; }

            /// <summary>
            /// 工作圈eid
            /// </summary>
            public string eid { get; set; }

            /// <summary>
            /// 会议开始时间
            /// </summary>
            public long startTime { get; set; }

            /// <summary>
            /// 会议结束时间
            /// </summary>
            public long endTime { get; set; }

            /// <summary>
            /// 预定时间
            /// </summary>
            public long createTime { get; set; }

            /// <summary>
            /// 修改时间
            /// </summary>
            public long updateTime { get; set; }

            /// <summary>
            /// 会议主题
            /// </summary>
            public string meetingTopic { get; set; }

            /// <summary>
            /// 会议内容
            /// </summary>
            public string meetingContent { get; set; }

            /// <summary>
            /// 订单的审批状态
            /// <para>0-不用审批;1-待审批状态;2-审批通过;3-审批不通过</para>
            /// </summary>
            [Obsolete("订单审批状态使用ApproveStatus属性会方便快捷一些")]
            public string approve { get; set; }

            /// <summary>
            /// 订单审批状态枚举
            /// </summary>
            public enum ApproveStatusEnum
            {
                /// <summary>
                /// 不用审批
                /// </summary>
                NoNeedApprove,
                /// <summary>
                /// 待审批
                /// </summary>
                WaitApprove,
                /// <summary>
                /// 审批通过
                /// </summary>
                Approve,
                /// <summary>
                /// 审批不通过
                /// </summary>
                NotApprove,
                /// <summary>
                /// 未知状态
                /// </summary>
                Unknow
            }

            /// <summary>
            /// 订单审批状态
            /// </summary>
            [JsonIgnore]
            public ApproveStatusEnum ApproveStatus
            {
                get
                {
                    ApproveStatusEnum result = ApproveStatusEnum.Unknow;
                    int _status;
                    bool isParse = int.TryParse(approve, out _status);
                    if (isParse)
                    {
                        switch (_status)
                        {
                            case 0:
                                result = ApproveStatusEnum.NoNeedApprove;
                                break;
                            case 1:
                                result = ApproveStatusEnum.WaitApprove;
                                break;
                            case 2:
                                result = ApproveStatusEnum.Approve;
                                break;
                            case 3:
                                result = ApproveStatusEnum.NotApprove;
                                break;
                            default:
                                result = ApproveStatusEnum.Unknow;
                                break;
                        }
                    }
                    return result;
                }
            }
        }
    }
}
