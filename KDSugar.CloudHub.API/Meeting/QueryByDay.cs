﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Meeting
{
    /// <summary>
    /// 按天查询工作圈下会议|发起
    /// </summary>
    public class QueryByDay : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/gateway/cloudwork/meeting/queryByDay?accessToken={0}";

        /// <summary>
        /// 待查询的日期时间戳（当天任意时间戳都可以）
        /// </summary>
        public long day
        {
            get
            {
                return Helper.ToUnixTime(_day);
            }
        }

        private DateTime _day;

        /// <summary>
        /// 按天查询工作圈下会议|构造
        /// </summary>
        /// <param name="dt">待查询日期</param>
        public QueryByDay(DateTime dt)
        {
            _day = dt;
        }

        /// <summary>
        /// 按天查询工作圈下会议|返回
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _QueryByDay Query(string _token)
        {
            return Post<_QueryByDay>(URL, _token);
        }
    }

    /// <summary>
    /// 按天查询工作圈下会议|返回
    /// </summary>
    public class _QueryByDay : BaseResult
    {
        /// <summary>
        /// 会议列表
        /// </summary>
        public List<BaseMeetingInfo> data { get; set; }
    }
}
