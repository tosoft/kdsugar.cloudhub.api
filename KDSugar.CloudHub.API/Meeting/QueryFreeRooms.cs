﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.Meeting
{
    /// <summary>
    /// 查询该工作圈空闲的会议室|发起
    /// </summary>
    public class QueryFreeRooms : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/api/roomBook/third/freeRooms?accessToken={0}";

        /// <summary>
        /// 预约人员信息openId
        /// </summary>
        public string openId { get; set; }

        /// <summary>
        /// 起始时间戳
        /// </summary>
        public long? startTime { get; set; }
        
        /// <summary>
        /// 终止时间戳
        /// </summary>
        public long? endTime { get; set; }

        /// <summary>
        /// 页码
        /// <para>默认1</para>
        /// </summary>
        public int pageIndex { get; set; } = 1;

        /// <summary>
        /// 每页数量
        /// <para>默认50</para>
        /// </summary>
        public int pageSize { get; set; } = 50;

        /// <summary>
        /// 查询该工作圈空闲的会议室|初始化
        /// </summary>
        /// <param name="_openId">预约人员信息openId</param>
        public QueryFreeRooms(string _openId)
        {
            openId = _openId;
        }

        /// <summary>
        /// 设置起止时间
        /// </summary>
        /// <param name="_begTime">起始时间，为null则不设置</param>
        /// <param name="_endTime">终止时间，为null则不设置</param>
        public void SetTime(DateTime? _begTime,DateTime? _endTime)
        {
            if (_begTime != null)
            {
                startTime = Helper.ToUnixTime(_begTime.Value);
            }
            else
            {
                startTime = null;
            }
            if (_endTime != null)
            {
                endTime = Helper.ToUnixTime(_endTime.Value);
            }
            else
            {
                endTime = null;
            }
        }

        /// <summary>
        /// 查询该工作圈空闲的会议室|返回
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _QueryFreeRooms Query(string _token)
        {
            return Post<_QueryFreeRooms>(URL, _token);
        }
    }

    /// <summary>
    /// 查询该工作圈空闲的会议室|返回
    /// </summary>
    public class _QueryFreeRooms : BaseResult
    {
        /// <summary>
        /// 闲置会议室列表
        /// </summary>
        public List<FreeRoom> data { get; set; }

        /// <summary>
        /// 闲置会议室信息类
        /// </summary>
        public class FreeRoom
        {
            /// <summary>
            /// 
            /// </summary>
            public string note { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public string roomDetail { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public string tagId { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public long limitCount { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public bool approve { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public string roomId { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public string roomName { get; set; }
        }
    }
}
