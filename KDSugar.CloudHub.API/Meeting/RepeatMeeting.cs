﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.Meeting
{
    /// <summary>
    /// 新增重复会议|发起
    /// </summary>
    public class RepeatMeeting : PostRequest
    {
        private const string URL = "https://yunzhijia.com/gateway/cloudwork/meeting/repeatMeeting?accessToken={0}";

        /// <summary>
        /// 会议创建者的oid
        /// </summary>
        public string openid { get; set; }

        /// <summary>
        /// 会议标题
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 会议内容
        /// </summary>
        public string content { get; set; }

        /// <summary>
        /// 会议地址
        /// </summary>
        public string meetingPlace { get; set; }

        /// <summary>
        /// 是否加密
        /// </summary>
        public bool encryption { get; set; }

        /// <summary>
        /// 会议开始时间戳
        /// </summary>
        public long startDate { get; set; }

        /// <summary>
        /// 会议结束时间戳
        /// </summary>
        public long endDate { get; set; }

        /// <summary>
        /// 提醒时间(-1:不提醒、0:开始时间提醒、15:开始时间前15分钟提醒、60:开始时间前1小时提醒)
        /// </summary>
        public List<int> noticeTimes { get; set; }

        /// <summary>
        /// 是否标记为重要(0:否、1:是)
        /// </summary>
        public int topState { get; set; }

        /// <summary>
        /// 协作人oid的集合
        /// </summary>
        public List<string> actors { get; set; }

        /// <summary>
        /// 是否需要提交会议纪要(新的体会、总结)
        /// </summary>
        public bool submitExperience { get; set; }

        /// <summary>
        /// 重复周期(0:不重复、1:每工作日、2:每日、3:每周、4:每两周、5:每月)
        /// </summary>
        public int repeat { get; set; }

        /// <summary>
        /// 重复截止时间
        /// </summary>
        public long repeatEndDate { get; set; }

        /// <summary>
        /// 新增重复会议
        /// </summary>
        /// <param name="_token">token</param>
        /// <returns></returns>
        public _RepeatMeeting Operate(string _token)
        {
            return Post<_RepeatMeeting>(URL, _token);
        }
    }

    /// <summary>
    /// 新增重复会议|返回
    /// </summary>
    public class _RepeatMeeting : BaseResult
    {
        /// <summary>
        /// 返回数据
        /// </summary>
        public MeetingData data { get; set; }

        /// <summary>
        /// 返回数据类
        /// </summary>
        public class MeetingData
        {
            /// <summary>
            /// 会议id集合,其中第一个是父会议ID
            /// </summary>
            public List<string> meetingIds { get; set; }

            /// <summary>
            /// 父会议ID
            /// </summary>
            [JsonIgnore]
            public string ParentMeetingId
            {
                get
                {
                    string result = null;
                    if (meetingIds != null && meetingIds.Count > 0)
                        result = meetingIds[0];
                    return result;
                }
            }
        }
    }
}
