﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API
{
    /// <summary>
    /// 请求调用基类
    /// </summary>
    public class PostRequest
    {
        /// <summary>
        /// 请求地址
        /// </summary>
        [JsonIgnore]
        public string RequestUrl { get; set; }

        /// <summary>
        /// 请求数据内容
        /// </summary>
        [JsonIgnore]
        public string RequestData { get; set; }

        /// <summary>
        /// 以GET形式调用接口并直接输出返回数据（不带token）
        /// </summary>
        /// <param name="_url">调用地址</param>
        /// <returns></returns>
        protected string GetToString(string _url)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            Type type = this.GetType();
            foreach(var p in type.GetProperties())
            {
                bool isIgnore = false;
                try
                {
                    foreach(var attr in p.GetCustomAttributes(false))
                    {
                        JsonIgnoreAttribute ignoreAttribute = (JsonIgnoreAttribute)attr;
                        isIgnore = true;
                    }
                }
                catch
                {
                    isIgnore = false;
                }
                if (!isIgnore)
                {
                    object tmpVal = p.GetValue(this, null);
                    //dic.Add(p.Name, p.GetValue(this, null).ToString());
                    //dic.Add(p.Name, tmpVal != null ? tmpVal.ToString() : null);
                    if (tmpVal != null)
                        dic.Add(p.Name, tmpVal.ToString());
                }
            }
            string tmpPara = "";
            foreach (var d in dic)
                tmpPara += string.Format("&{0}={1}", d.Key, d.Value);
            RequestUrl = _url;
            RequestData = tmpPara;
            return Helper.GetWeb(_url, dic);
        }

        /// <summary>
        /// 以GET形式调用接口并直接输出返回数据（带token）
        /// </summary>
        /// <param name="_url">调用地址</param>
        /// <param name="_token">token</param>
        /// <returns></returns>
        protected string GetToString(string _url,string _token)
        {
            return GetToString(string.Format(_url, _token));
        }

        /// <summary>
        /// 以GET形式调用接口（不带token）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_url"></param>
        /// <returns></returns>
        protected T Get<T>(string _url) where T : BaseResult
        {
            return JsonConvert.DeserializeObject<T>(GetToString(_url));
        }

        /// <summary>
        /// 以GET形式调用接口（带token）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_url"></param>
        /// <param name="_token"></param>
        /// <returns></returns>
        protected T Get<T>(string _url,string _token) where T : BaseResult
        {
            return JsonConvert.DeserializeObject<T>(GetToString(_url, _token));
        }

        /// <summary>
        /// 调用接口并直接输出返回数据(不带token)
        /// </summary>
        /// <param name="_url"></param>
        /// <returns></returns>
        protected string PostToString(string _url)
        {
            RequestUrl = _url;
            RequestData = JsonConvert.SerializeObject(this);
            return Helper.PostWeb(_url, JsonConvert.SerializeObject(this));
        }

        /// <summary>
        /// 调用接口并直接输出返回数据(带token)
        /// </summary>
        /// <param name="_url"></param>
        /// <param name="_token"></param>
        /// <returns></returns>
        protected string PostToString(string _url,string _token)
        {
            return PostToString(string.Format(_url, _token));
        }

        /// <summary>
        /// 调用接口(不带token)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_url"></param>
        /// <returns></returns>
        protected T Post<T>(string _url) where T : BaseResult
        {
            //string tmpContent = Helper.PostWeb(_url, JsonConvert.SerializeObject(this));
            //return JsonConvert.DeserializeObject<T>(tmpContent);
            return JsonConvert.DeserializeObject<T>(PostToString(_url));
        }

        /// <summary>
        /// 调用接口(带token)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_url"></param>
        /// <param name="_token"></param>
        /// <returns></returns>
        protected T Post<T>(string _url,string _token) where T : BaseResult
        {
            return Post<T>(string.Format(_url, _token));
        }

        /// <summary>
        /// 以Urlencoded形式调用接口并直接输出返回数据(不带token)
        /// </summary>
        /// <param name="_url"></param>
        /// <returns></returns>
        protected string PostToStringByUrlencoded(string _url)
        {
            Dictionary<string, string> kv = new Dictionary<string, string>();
            Type type = GetType();
            foreach (var p in type.GetProperties())
            {
                bool isAdd = true;
                foreach(Attribute a in p.GetCustomAttributes(false))
                {
                    if (a.GetType().Name.Equals(typeof(JsonIgnoreAttribute).Name))
                    {
                        isAdd = false;
                    }
                }
                if (isAdd)
                    kv.Add(p.Name, p.GetValue(this, null) != null ? p.GetValue(this, null).ToString() : null);
            }
            RequestUrl = _url;
            RequestData = JsonConvert.SerializeObject(kv);
//#if DEBUG
//            string debugStr = Helper.PostWeb(_url, kv);
//            return debugStr;
//#endif
//#if !DEBUG
//            return Helper.PostWeb(_url, kv);
//#endif
            return Helper.PostWeb(_url, kv);
        }

        /// <summary>
        /// 以Urlencoded形式调用接口并直接输出返回数据(带token)
        /// </summary>
        /// <param name="_url"></param>
        /// <param name="_token"></param>
        /// <returns></returns>
        protected string PostToStringByUrlencoded(string _url,string _token)
        {
            return PostToStringByUrlencoded(string.Format(_url, _token));
        }

        /// <summary>
        /// 以Urlencoded形式调用接口(不带token)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_url"></param>
        /// <returns></returns>
        protected T PostByUrlencoded<T>(string _url) where T : BaseResult
        {
            //Dictionary<string, string> kv = new Dictionary<string, string>();
            //Type type = GetType();
            //foreach(var p in type.GetProperties())
            //    kv.Add(p.Name, p.GetValue(this).ToString());
            //string tmpContent = Helper.PostWeb(_url, kv);
            //return JsonConvert.DeserializeObject<T>(tmpContent);
            return JsonConvert.DeserializeObject<T>(PostToStringByUrlencoded(_url));
        }

        /// <summary>
        /// 以Urlencoded形式调用接口(带token)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_url"></param>
        /// <param name="_token"></param>
        /// <returns></returns>
        protected T PostByUrlencoded<T>(string _url,string _token) where T : BaseResult
        {
            return PostByUrlencoded<T>(string.Format(_url, _token));
        }
    }
}
