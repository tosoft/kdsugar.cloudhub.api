﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// 有关程序集的一般信息由以下
// 控制。更改这些特性值可修改
// 与程序集关联的信息。
[assembly: AssemblyTitle("KDSugar.CloudHub.API")]
[assembly: AssemblyDescription("针对云之家WebAPI接口调用实现的开源解决方案")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("汕头市至软信息技术有限公司")]
[assembly: AssemblyProduct("KDSugar.CloudHub.API")]
[assembly: AssemblyCopyright("版权所有 © 汕头市至软信息技术有限公司 2023")]
[assembly: AssemblyTrademark("至软-Tosoft")]
[assembly: AssemblyCulture("")]

// 将 ComVisible 设置为 false 会使此程序集中的类型
//对 COM 组件不可见。如果需要从 COM 访问此程序集中的类型
//请将此类型的 ComVisible 特性设置为 true。
[assembly: ComVisible(false)]

// 如果此项目向 COM 公开，则下列 GUID 用于类型库的 ID
[assembly: Guid("f3ce6c82-25f0-4796-b6de-43a4ae7b0cb8")]

// 程序集的版本信息由下列四个值组成: 
//
//      主版本
//      次版本
//      生成号
//      修订号
//
// 可以指定所有值，也可以使用以下所示的 "*" 预置版本号和修订号
//通过使用 "*"，如下所示:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: NeutralResourcesLanguage("zh-CN")]
