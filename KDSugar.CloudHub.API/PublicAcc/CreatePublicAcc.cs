﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.PublicAcc
{
    /// <summary>
    /// 创建公共号|发起
    /// </summary>
    public class CreatePublicAcc : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/pubacc/api/pubcreate";

        /// <summary>
        /// 在mid企业管理中心->系统集成->资源授权，用资源授权密钥对"name公共号名称"签名后,BASE64编码字符串
        /// </summary>
        public string sign { get; set; }

        /// <summary>
        /// 团队号eid
        /// </summary>
        public string mid { get; set; }

        /// <summary>
        /// 企业名称
        /// </summary>
        public string midname { get; set; }

        /// <summary>
        /// 公共号名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 图标名称
        /// </summary>
        public string photoname { get; set; }

        /// <summary>
        /// 图标base64
        /// </summary>
        public string photo64 { get; set; }

        /// <summary>
        /// 可订阅
        /// </summary>
        [JsonIgnore]
        public bool IsSubscribe { get; set; }

        /// <summary>
        /// 可订阅[1是、0否]
        /// <para>请通过IsSubscribe属性赋值</para>
        /// </summary>
        public int cssb
        {
            get { return IsSubscribe ? 1 : 0; }
        }

        /// <summary>
        /// 是否全部订阅
        /// </summary>
        [JsonIgnore]
        public bool IsAllSubscribe { get; set; }

        /// <summary>
        /// 全部订阅[1是、0否]
        /// <para>请通过IsAllSubscribe赋值</para>
        /// </summary>
        public int allssb
        {
            get { return IsAllSubscribe ? 1 : 0; }
        }

        /// <summary>
        /// 可回复
        /// </summary>
        [JsonIgnore]
        public bool IsReply { get; set; }

        /// <summary>
        /// 可回复[1是、0否]
        /// <para>请通过IsReply赋值</para>
        /// </summary>
        public int? reply { get { return IsReply ? 1 : 0; } }

        /// <summary>
        /// 可选, 不传默认为false, 为不需要消息推送； 传true为需要消息推送
        /// </summary>
        public bool remind { get; set; } = false;

        /// <summary>
        /// 创建公众号
        /// </summary>
        /// <returns></returns>
        public string Create()
        {
            return base.PostToStringByUrlencoded(URL);
        }
    }

}
