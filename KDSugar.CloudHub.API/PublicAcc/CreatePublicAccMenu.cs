﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.PublicAcc
{
    /// <summary>
    /// 创建公共号菜单|发起
    /// </summary>
    public class CreatePublicAccMenu : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/pubacc/api/pubmenu";

        /// <summary>
        /// 用mid企业密钥对"pid"签名后,BASE64编码字符串
        /// </summary>
        public string sign { get; set; }

        /// <summary>
        /// 团队号eid
        /// </summary>
        public string mid { get; set; }

        /// <summary>
        /// 公共号pubid
        /// </summary>
        public string pid { get; set; }

        /// <summary>
        /// 公共号菜单
        /// </summary>
        public List<PublicAccMenu> menu { get; set; }

        /// <summary>
        /// 创建公共号菜单|初始化
        /// </summary>
        /// <param name="_sign">用mid企业密钥对"pid"签名后,BASE64编码字符串</param>
        /// <param name="_mid">团队号eid</param>
        /// <param name="_pid">公共号pubid</param>
        /// <param name="_list">公共号菜单</param>
        public CreatePublicAccMenu(string _sign,string _mid,string _pid,List<PublicAccMenu> _list)
        {
            sign = _sign;
            mid = _mid;
            pid = _pid;
            menu = _list;
        }

        /// <summary>
        /// 创建公共号菜单
        /// </summary>
        /// <returns></returns>
        public _CreatePublicAccMenu Create()
        {
            return JsonConvert.DeserializeObject<_CreatePublicAccMenu>(base.PostToStringByUrlencoded(URL));
        }
    }

    /// <summary>
    /// 创建公共号菜单|返回
    /// </summary>
    public class _CreatePublicAccMenu
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool success { get; set; }
    }
}
