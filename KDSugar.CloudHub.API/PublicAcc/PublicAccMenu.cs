﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDSugar.CloudHub.API.PublicAcc
{
    /// <summary>
    /// 公共号菜单类
    /// </summary>
    public class PublicAccMenu
    {
        /// <summary>
        /// id
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// Android 应用:安装地址或本地调用协议
        /// </summary>
        public string android { get; set; }

        /// <summary>
        /// IOS 应用 :安装地址或本地调用协议
        /// </summary>
        public string ios { get; set; }

        /// <summary>
        /// 菜单名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 应用id
        /// </summary>
        public string appid { get; set; }

        /// <summary>
        /// 类型：menu:菜单,click:按钮,url:链接,view:轻应用,app:移动原生应用
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// 跳转地址
        /// </summary>
        public string url { get; set; }

        /// <summary>
        /// 键值:用于唯一识别此菜单项的标识符
        /// </summary>
        public string key { get; set; }
    }
}
