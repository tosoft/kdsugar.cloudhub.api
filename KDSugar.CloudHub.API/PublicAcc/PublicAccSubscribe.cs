﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.PublicAcc
{
    /// <summary>
    /// 公共号是否订阅|发起
    /// </summary>
    public class PublicAccIsSubscribe : PostRequest
    {
        /// <summary>
        /// URL地址
        /// </summary>
        public const string URL = "https://www.yunzhijia.com/pubacc/api/pubssb";

        /// <summary>
        /// 公共号id
        /// </summary>
        public string pubid { get; set; }

        /// <summary>
        /// 团队号eid
        /// </summary>
        public string mid { get; set; }

        /// <summary>
        /// 公共号是否订阅|初始化
        /// </summary>
        /// <param name="_pubId">公共号id</param>
        /// <param name="_mid">团队号eid</param>
        public PublicAccIsSubscribe(string _pubId,string _mid)
        {
            pubid = _pubId;
            mid = _mid;
        }

        /// <summary>
        /// 查询公共还是否订阅
        /// </summary>
        /// <returns></returns>
        protected _PublicAccIsSubscribe Query()
        {
            return JsonConvert.DeserializeObject<_PublicAccIsSubscribe>(PostToStringByUrlencoded(URL));
        }
    }

    /// <summary>
    /// 公共号是否订阅|返回
    /// </summary>
    public class _PublicAccIsSubscribe
    {
        /// <summary>
        /// 1是订阅；0是没有订阅
        /// </summary>
        public string ssb { get; set; }

        /// <summary>
        /// 公共号是否订阅
        /// </summary>
        [JsonIgnore]
        public bool IsSubscribe
        {
            get
            {
                bool result = false;
                if (!string.IsNullOrWhiteSpace(ssb))
                    result = ssb.Equals("1") ? true : false;
                return result;
            }
        }
    }

    /// <summary>
    /// 设置公共号企业订阅
    /// </summary>
    public class PublicAccEnterpriseSubscribe : PublicAccIsSubscribe
    {
        private bool IsSubscribe { get; set; }

        private string Token { get; set; }

        /// <summary>
        /// 1是订阅；0是取消
        /// </summary>
        public string ssb { get { return IsSubscribe ? "1" : "0"; } }

        /// <summary>
        /// 10位unix时间戳
        /// </summary>
        public long time { get; set; }

        /// <summary>
        /// SHA1密钥(自动生成)
        /// </summary>
        public string pubtoken { get { return Helper.PublicAccSHA(Token, mid, pubid, time.ToString()); } }

        /// <summary>
        /// 设置公共号企业订阅初始化(时间默认为调用时间)
        /// </summary>
        /// <param name="_pubId">公共号id</param>
        /// <param name="_mid">团队号eid</param>
        /// <param name="_sub">true为订阅，false为取消</param>
        public PublicAccEnterpriseSubscribe(string _pubId, string _mid, bool _sub)
            : base(_pubId, _mid)
        {
            //Token = _token;
            IsSubscribe = _sub;
            time = Helper.ToUnixTime(DateTime.Now);
        }

        /// <summary>
        /// 设置公共号企业订阅初始化(时间自定义)
        /// </summary>
        /// <param name="_pubId">公共号id</param>
        /// <param name="_mid">团队号eid</param>
        /// <param name="_sub">true为订阅，false为取消</param>
        /// <param name="_dt">给定时间</param>
        public PublicAccEnterpriseSubscribe(string _pubId, string _mid, bool _sub, DateTime _dt)
            : this(_pubId, _mid, _sub)
        {
            time = Helper.ToUnixTime(_dt);
        }

        /// <summary>
        /// 订阅
        /// </summary>
        /// <param name="_token">访问Token</param>
        /// <returns></returns>
        public _PublicAccSubscribe Subscribe(string _token)
        {
            Token = _token;
            return JsonConvert.DeserializeObject<_PublicAccSubscribe>(PostToStringByUrlencoded(URL));
        }
    }

    /// <summary>
    /// 设置公共号个人订阅
    /// </summary>
    public class PublicAccUserSubscribe : PublicAccEnterpriseSubscribe
    {
        /// <summary>
        /// 用户openid
        /// </summary>
        public string userid { get; set; }

        /// <summary>
        /// 公共号个人订阅初始化(时间默认为调用时间)
        /// </summary>
        /// <param name="_pubId">公共号id</param>
        /// <param name="_mid">团队号eid</param>
        /// <param name="_userId">用户openid</param>
        /// <param name="_sub">true为订阅，false为取消</param>
        public PublicAccUserSubscribe(string _pubId,string _mid,string _userId,bool _sub)
            : base(_pubId, _mid, _sub)
        {
            userid = _userId;
        }

        /// <summary>
        /// 公共号个人订阅初始化(时间自定义)
        /// </summary>
        /// <param name="_pubId">公共号id</param>
        /// <param name="_mid">团队号eid</param>
        /// <param name="_userId">用户openid</param>
        /// <param name="_sub">true为订阅，false为取消</param>
        /// <param name="_dt">给定时间</param>
        public PublicAccUserSubscribe(string _pubId, string _mid, string _userId, bool _sub, DateTime _dt)
            : base(_pubId, _mid, _sub, _dt)
        {
            userid = _userId;
        }
    }

    /// <summary>
    /// 设置公共号发言人
    /// </summary>
    public class PublicAccSetSpokesman : PublicAccEnterpriseSubscribe
    {
        /// <summary>
        /// 用户openid
        /// </summary>
        public string sayid { get; set; }

        /// <summary>
        /// 公共号发言人设置初始化(时间默认为调用时间)
        /// </summary>
        /// <param name="_pubid">公共号id</param>
        /// <param name="_mid">团队号eid</param>
        /// <param name="_sayid">用户openid</param>
        /// <param name="_sub">true为订阅，false为取消</param>
        public PublicAccSetSpokesman(string _pubid,string _mid,string _sayid,bool _sub)
            : base(_pubid, _mid, _sub)
        {
            sayid = _sayid;
        }

        /// <summary>
        /// 公共号发言人设置初始化(时间自定义)
        /// </summary>
        /// <param name="_pubid">公共号id</param>
        /// <param name="_mid">团队号eid</param>
        /// <param name="_sayid">用户openid</param>
        /// <param name="_sub">true为订阅，false为取消</param>
        /// <param name="_dt">给定时间</param>
        public PublicAccSetSpokesman(string _pubid, string _mid, string _sayid, bool _sub,DateTime _dt)
            : base(_pubid, _mid, _sub, _dt)
        {
            sayid = _sayid;
        }
    }

    /// <summary>
    /// 公共号订阅结果返回
    /// </summary>
    public class _PublicAccSubscribe
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool success { get; set; }
    }
}
