﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.PublicAcc
{
    /// <summary>
    /// 查询公共号列表|发起
    /// </summary>
    public class QueryPublicAccList : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/pubacc/api/xtpubs";

        /// <summary>
        /// 团队号eid
        /// </summary>
        public string mid { get; set; }

        /// <summary>
        /// 查询公共号列表-初始化
        /// </summary>
        /// <param name="_mid">团队号eid</param>
        public QueryPublicAccList(string _mid)
        {
            mid = _mid;
        }

        /// <summary>
        /// 查询公共号列表
        /// </summary>
        /// <returns></returns>
        public _QueryPublicAccList Query()
        {
            return JsonConvert.DeserializeObject<_QueryPublicAccList>(base.PostToStringByUrlencoded(URL));
        }
    }

    /// <summary>
    /// 查询公共号列表|返回
    /// </summary>
    public class _QueryPublicAccList
    {
        /// <summary>
        /// 公共号信息列表
        /// </summary>
        [JsonProperty("")]
        public List<PublicAccInfo>  AccInfoList { get; set; }
        
        /// <summary>
        /// 公共号信息类
        /// </summary>
        public class PublicAccInfo
        {
            public string pid { get; set; }

            public string name { get; set; }

            public long time { get; set; }

            public string photourl { get; set; }

            public string note { get; set; }

            public string mid { get; set; }

            public string midname { get; set; }

            public string account { get; set; }

            public bool reply { get; set; }

            public bool cssb { get; set; }

            public bool allssb { get; set; }

            public bool mssb { get; set; }

            public bool selfssb { get; set; }

            public string orgId { get; set; }

            public string orgName { get; set; }

            public bool fold { get; set; }

            public bool remind { get; set; }

            public bool tmpl { get; set; }

            public bool share { get; set; }

            public string app { get; set; }

            public string type { get; set; }

            public string state { get; set; }

            public string states { get; set; }

            public bool auto { get; set; }

            public bool sendMsgByPubPlatform { get; set; }

            public bool sendMsg2AllByApi { get; set; }

            public bool sendMsg2SomeByApi { get; set; }

            public bool replyType { get; set; }

            public bool grayType { get; set; }

            public bool safeShare { get; set; }

            public bool openAd { get; set; }

            public bool ecosphere { get; set; }

            /// <summary>
            /// 其他未列明的属性
            /// </summary>
            [JsonExtensionData]
            public Dictionary<string,object> OtherVal { get; set; }
        }
    }
}
