﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.PublicAcc
{
    public class QueryPublicSubUser : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/pubacc/api/pubssbusers";

        /// <summary>
        /// 公共号id
        /// </summary>
        public string pubid { get; set; }

        /// <summary>
        /// 页码(0是第一页)
        /// </summary>
        public int page { get; set; }

        /// <summary>
        /// 每页记录数(默认10)
        /// </summary>
        public int count { get; set; } = 10;

        /// <summary>
        /// 10位unix时间戳
        /// </summary>
        public long time { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string pubtoken { get; set; }
    }
}
