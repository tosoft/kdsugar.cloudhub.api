﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API
{
    /// <summary>
    /// 群组机器人
    /// </summary>
    public class Robot
    {
        /// <summary>
        /// WebHook地址
        /// </summary>
        private string url;

        /// <summary>
        /// 初始化群主机器人
        /// </summary>
        /// <param name="_webHook">群主机器人WebHook地址</param>
        public Robot(string _webHook)
        {
            url = _webHook;
        }

        /// <summary>
        /// 发送文本类消息
        /// </summary>
        /// <param name="msg">文本消息</param>
        /// <param name="isAll">是否提醒所有人</param>
        /// <returns></returns>
        public string SendTextMsg(string msg,bool isAll)
        {
            if (!string.IsNullOrWhiteSpace(msg) && isAll)
                msg += "@ALL";
            Dictionary<string, string> requestData = new Dictionary<string, string>();
            requestData.Add("content", msg);
            return Helper.PostWeb(url, JsonConvert.SerializeObject(requestData));
        }

        /// <summary>
        /// 发送应用类消息
        /// </summary>
        /// <param name="msg">应用类消息</param>
        /// <returns></returns>
        public string SendAppMsg(AppMessage msg)
        {
            return Helper.PostWeb(url, JsonConvert.SerializeObject(msg));
        }

        /// <summary>
        /// 应用类消息类
        /// </summary>
        public class AppMessage
        {
            /// <summary>
            /// 消息列表处的预览内容
            /// </summary>
            public string content { get; set; }

            /// <summary>
            /// 消息类型：应用类（默认为1-应用类）
            /// </summary>
            public int msgType { get; set; } = 1;

            /// <summary>
            /// 消息参数信息
            /// </summary>
            public AppMsgParam param { get; set; }

            /// <summary>
            /// 初始化应用类消息类
            /// </summary>
            /// <param name="_content">预览内容</param>
            /// <param name="_param">参数</param>
            public AppMessage(string _content,AppMsgParam _param)
            {
                content = _content;
                param = _param;
            }
        }

        /// <summary>
        /// 应用类消息参数类
        /// </summary>
        public class AppMsgParam
        {
            /// <summary>
            /// 应用名称
            /// </summary>
            public string appName { get; set; }

            /// <summary>
            /// 标题
            /// </summary>
            public string title { get; set; }

            /// <summary>
            /// 轻应用id
            /// </summary>
            public string lightAppId { get; set; }

            /// <summary>
            /// 缩略图url
            /// </summary>
            public string thumbUrl { get; set; }

            /// <summary>
            /// 跳转地址
            /// </summary>
            public string webpageUrl { get; set; }

            /// <summary>
            /// 消息显示样式
            /// </summary>
            public int customStyle
            {
                get
                {
                    return (int)CustomStyleType;
                }
            }

            /// <summary>
            /// 消息显示样式类型-枚举
            /// </summary>
            public enum CustomStyleTypeEnum
            {
                /// <summary>
                /// 默认样式
                /// </summary>
                Default = 0,
                /// <summary>
                /// 主次样式
                /// </summary>
                Master_Slave = 1,
                /// <summary>
                /// 图表样式
                /// </summary>
                Chart = 2
            }

            /// <summary>
            /// 消息显示样式类型
            /// </summary>
            private CustomStyleTypeEnum CustomStyleType;

            /// <summary>
            /// 消息显示内容
            /// </summary>
            public string content { get; set; }

            /// <summary>
            /// 初始化为默认或主次类样式消息
            /// </summary>
            /// <param name="_appName">应用名称</param>
            /// <param name="_title">标题</param>
            /// <param name="_lightAppId">轻应用id</param>
            /// <param name="_thumbUrl">缩略图url</param>
            /// <param name="_webpageUrl">跳转地址</param>
            /// <param name="_content">消息内容</param>
            /// <param name="_isDefault">是否为默认的样式消息，若为Flase则为主次类样式消息</param>
            public AppMsgParam(string _appName, string _title, string _lightAppId,
                string _thumbUrl, string _webpageUrl, string _content, bool _isDefault)
                : this(_appName, _title, _lightAppId, _thumbUrl, _webpageUrl)
            {
                content = _content;
                CustomStyleType = _isDefault ? CustomStyleTypeEnum.Default : CustomStyleTypeEnum.Master_Slave;
            }

            /// <summary>
            /// 初始化为图表样式消息
            /// </summary>
            /// <param name="_appName">应用名称</param>
            /// <param name="_title">标题</param>
            /// <param name="_lightAppId">轻应用id</param>
            /// <param name="_thumbUrl">缩略图url</param>
            /// <param name="_webpageUrl">跳转地址</param>
            public AppMsgParam(string _appName, string _title, string _lightAppId,
                string _thumbUrl, string _webpageUrl)
            {
                appName = _appName;
                title = _title;
                lightAppId = _lightAppId;
                thumbUrl = _thumbUrl;
                webpageUrl = _webpageUrl;
                CustomStyleType = CustomStyleTypeEnum.Chart;
            }
        }
    }
}
