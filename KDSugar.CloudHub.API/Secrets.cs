﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API
{
    /// <summary>
    /// 根据管理员账号信息获取资源授权密钥|发起
    /// </summary>
    public class Secrets : PostRequest
    {
        /// <summary>
        /// 团队EID
        /// </summary>
        public string eid { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string userName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string password { get; set; }

        /// <summary>
        /// 根据管理员账号信息获取资源授权密钥|初始化
        /// </summary>
        /// <param name="_eid">团队EID</param>
        /// <param name="_userName">用户名</param>
        /// <param name="_password">密码</param>
        public Secrets(string _eid,string _userName,string _password)
        {
            eid = _eid;
            userName = _userName;
            password = _password;
        }

        private const string URL = "https://yunzhijia.com/opencloud/rest/getSecretByUser";

        /// <summary>
        /// 获取资源收起密钥
        /// </summary>
        /// <returns></returns>
        public _Secrets GetSecrets()
        {
            return Post<_Secrets>(URL);
        }
    }

    /// <summary>
    /// 根据管理员账号信息获取资源授权密钥|返回
    /// </summary>
    public class _Secrets : BaseResult
    {
        /// <summary>
        /// 资源授权密钥列表
        /// </summary>
        public List<SecretInfo> data { get; set; }

        /// <summary>
        /// 资源授权密钥信息
        /// </summary>
        public class SecretInfo
        {
            /// <summary>
            /// 别名
            /// </summary>
            public string alias { get; set; }

            /// <summary>
            /// 名称
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// 密钥
            /// </summary>
            public string secret { get; set; }
        }
    }
}
