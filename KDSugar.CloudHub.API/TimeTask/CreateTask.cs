﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.TimeTask
{
    public class CreateTask : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/gateway/cloudwork/worktask/create?accessToken={0}";


    }

    public class _CreateTask : BaseResult
    {
        public List<WorkTaskInfo> data { get; set; }

        public class WorkTaskInfo
        {
            public string workTaskId { get; set; }
        }
    }
}
