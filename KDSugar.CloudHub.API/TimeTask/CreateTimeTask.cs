﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.TimeTask
{
    /// <summary>
    /// 创建日程|发起
    /// </summary>
    public class CreateTimeTask : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/gateway/cloudwork/newwork/create?accessToken={0}";

        /// <summary>
        /// 日程创建者的oid
        /// </summary>
        public string openid { get; set; }

        /// <summary>
        /// 日程内容
        /// </summary>
        public string content { get; set; }

        /// <summary>
        /// 日程开始时间戳
        /// </summary>
        public long startDate { get; set; }

        /// <summary>
        /// 日程结束时间戳
        /// </summary>
        public long endDate { get; set; }

        /// <summary>
        /// 提醒时间，以分钟为单位，于起始时间之前的指定分钟开始提醒
        /// <para>若为0，则为开始时间提醒</para>
        /// <para>若为-1.则为不提醒</para>
        /// </summary>
        public int noticeTime { get; set; }

        /// <summary>
        /// 是否标记为重要
        /// </summary>
        public int topState { get { return TopState ? 1 : 0; } }

        private bool TopState;

        /// <summary>
        /// 协作人oid的集合
        /// </summary>
        public List<string> actors { get; set; }

        /// <summary>
        /// 是否需要提交日程纪要(新的体会、总结)
        /// </summary>
        public bool submitExperience { get; set; }

        /// <summary>
        /// 默认0，重复类型(0不重复, 1每工作日, 2每日, 3每周, 4每两周, 5每月)
        /// </summary>
        public int repeat { get; set; } = 0;

        /// <summary>
        /// 重复截止时间，毫秒时间戳，repeat不为0时必填
        /// </summary>
        public long repeatEndDate { get; set; }

        /// <summary>
        /// 共享组id集合
        /// </summary>
        public List<string> shareIds { get; set; }

        /// <summary>
        /// (未知数据类型)图片列表
        /// </summary>
        public object images { get; set; } = null;

        /// <summary>
        /// (未知数据类型)文件列表
        /// </summary>
        public object files { get; set; } = null;

        /// <summary>
        /// 日程回调接口
        /// </summary>
        public string callBackUrl { get; set; }

        /// <summary>
        /// 日程创建初始化
        /// </summary>
        /// <param name="_openid">日程创建者的oid</param>
        /// <param name="_content">日程内容</param>
        /// <param name="_begTime">日程开始时间</param>
        /// <param name="_endTime">日程结束时间</param>
        /// <param name="_noticeTime">
        /// 提醒时间，以分钟为单位，于起始时间之前的指定分钟开始提醒
        /// <para>若为0，则为开始时间提醒</para>
        /// <para>若为-1.则为不提醒</para>
        /// </param>
        /// <param name="_topState">是否标记为重要</param>
        /// <param name="_actors">协作人oid的集合</param>
        /// <param name="_submitExperience">是否需要提交日程纪要(新的体会、总结)</param>
        public CreateTimeTask(string _openid,string _content,DateTime _begTime,DateTime _endTime,int _noticeTime,
            bool _topState,List<string> _actors,bool _submitExperience)
        {
            openid = _openid;
            content = _content;
            startDate = Helper.ToUnixTime(_begTime);
            endDate = Helper.ToUnixTime(_endTime);
            noticeTime = _noticeTime;
            TopState = _topState;
            actors = _actors;
            submitExperience = _submitExperience;
        }

        /// <summary>
        /// 创建日程|操作
        /// </summary>
        /// <param name="_token">Token</param>
        /// <returns></returns>
        public _CreateTimeTask Operate(string _token)
        {
            return Post<_CreateTimeTask>(URL, _token);
        }
    }

    /// <summary>
    /// 创建日程|返回
    /// </summary>
    public class _CreateTimeTask : BaseResult
    {
        /// <summary>
        /// 日程Id列表
        /// </summary>
        public WorkIds data { get; set; }
        //public List<WorkIds> data { get; set; }

        /// <summary>
        /// 日程Id记录类
        /// </summary>
        public class WorkIds
        {
            /// <summary>
            /// 添加成功后日程id
            /// </summary>
            public string workId { get; set; }
        }
    }
}
