﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.TimeTask
{
    /// <summary>
    /// 批量删除日程接口|发起
    /// </summary>
    public class DeleteTimeTask : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/gateway/cloudwork/newwork/delete?accessToken={0}";

        /// <summary>
        /// 日程创建者的oid
        /// </summary>
        public string openid { get; set; }

        /// <summary>
        /// 日程id的集合
        /// </summary>
        public List<string> ids { get; set; }

        /// <summary>
        /// 批量删除日程接口初始化
        /// </summary>
        /// <param name="_openId">日程创建者的oid</param>
        /// <param name="_ids">日程id的集合</param>
        public DeleteTimeTask(string _openId,List<string> _ids)
        {
            openid = _openId;
            ids = _ids;
        }

        public BaseResult Operate(string _token)
        {
            return Post<BaseResult>(URL, _token);
        }
    }
}
