﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.TimeTask
{
    public class ModifyTimeTask : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/gateway/cloudwork/newwork/modify?accessToken={0}";

        /// <summary>
        /// 日程id
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 日程创建者的oid
        /// </summary>
        public string openid { get; set; }

        /// <summary>
        /// 日程内容
        /// </summary>
        public string content { get; set; }

        /// <summary>
        /// 日程开始时间戳
        /// </summary>
        public long startDate { get; set; }

        /// <summary>
        /// 日程结束时间戳
        /// </summary>
        public long endDate { get; set; }

        /// <summary>
        /// 提醒时间，以分钟为单位，于起始时间之前的指定分钟开始提醒
        /// <para>若为0，则为开始时间提醒</para>
        /// <para>若为-1.则为不提醒</para>
        /// </summary>
        public int noticeTime { get; set; }

        /// <summary>
        /// 是否标记为重要
        /// </summary>
        public int topState { get { return TopState ? 1 : 0; } }

        private bool TopState;

        /// <summary>
        /// 新加协作人oid的集合
        /// </summary>
        public List<string> addActors { get; set; }

        /// <summary>
        /// 被删除协作人oid的集合
        /// </summary>
        public List<string> delActors { get; set; }

        /// <summary>
        /// 是否需要提交日程纪要(新的体会、总结)
        /// </summary>
        public bool submitExperience { get; set; }

        public ModifyTimeTask(string _id,string _openid,string _content,DateTime _startDate,DateTime _endDate,
            int _noticeTime,bool _topState,bool _submitExperience)
        {
            id = _id;
            openid = _openid;
            content = _content;
            startDate = Helper.ToUnixTime(_startDate);
            endDate = Helper.ToUnixTime(_endDate);
            noticeTime = _noticeTime;
            TopState = _topState;
            submitExperience = _submitExperience;
        }

        private void AddActor()
        {
            if (addActors == null)
                addActors = new List<string>();
        }

        public void AddActor(string _id)
        {
            AddActor();
            if (!string.IsNullOrWhiteSpace(_id))
                addActors.Add(_id);
        }

        public void AddActor(List<string> _ids)
        {
            AddActor();
            if (_ids != null)
                addActors.AddRange(_ids);
        }

        private void DeleteActor()
        {
            if (delActors == null)
                delActors = new List<string>();
        }

        public void DeleteActor(string _id)
        {
            DeleteActor();
            if (!string.IsNullOrWhiteSpace(_id))
                delActors.Add(_id);
        }

        public void DeleteActor(List<string> _ids)
        {
            DeleteActor();
            if (_ids != null)
                delActors.AddRange(_ids);
        }

        public BaseResult Operate(string _token)
        {
            return Post<BaseResult>(URL, _token);
        }
    }
}
