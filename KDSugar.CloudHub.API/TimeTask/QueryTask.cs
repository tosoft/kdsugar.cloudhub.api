﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.TimeTask
{
    /// <summary>
    /// 查询任务详情接口
    /// </summary>
    public class QueryTask : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/gateway/cloudwork/worktask/find?accessToken={0}";

        /// <summary>
        /// 任务创建者的oid
        /// </summary>
        public string openId { get; set; }

        /// <summary>
        /// 任务id
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 初始化查询任务详情
        /// </summary>
        /// <param name="_openId">任务创建者的oid</param>
        /// <param name="_id">任务id</param>
        public QueryTask(string _openId,string _id)
        {
            openId = _openId;
            id = _id;
        }

        public _QueryTask Query(string _token)
        {
            return Post<_QueryTask>(URL, _token);
        }
    }

    /// <summary>
    /// 分页查询任务列表接口
    /// </summary>
    public class QueryTaskByPageable : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/gateway/cloudwork/worktask/page?accessToken={0}";

        /// <summary>
        /// 每页最大条数
        /// </summary>
        public const int MAX_SIZE = 100;

        /// <summary>
        /// 任务创建者的oid
        /// </summary>
        public string openId { get; set; }

        /// <summary>
        /// 第几页(从1开始)
        /// </summary>
        public int pageNum { get; set; }

        /// <summary>
        /// 每页多少条(最多100条)
        /// </summary>
        public int pageSize { get; set; }

        /// <summary>
        /// 初始化查询任务列表
        /// </summary>
        /// <param name="_openId">任务创建者的oid</param>
        /// <param name="_page">第几页(从1开始)</param>
        /// <param name="_size">每页多少条(最多100条)</param>
        public QueryTaskByPageable(string _openId,int _page,int _size = MAX_SIZE)
        {
            openId = _openId;
            pageNum = _page;
            pageSize = _size;
        }

        public _QueryTask Query(string _token)
        {
            return Post<_QueryTask>(URL, _token);
        }
    }

    public class _QueryTask : BaseResult
    {

        public class TaskInfo
        {
            /// <summary>
            /// 任务相关人的集合
            /// </summary>
            public List<ActorInfo> actors { get; set; }

            /// <summary>
            /// 任务内容
            /// </summary>
            public string content { get; set; }

            /// <summary>
            /// 任务创建时间
            /// </summary>
            public long createDate { get; set; }

            /// <summary>
            /// 任务结束时间戳
            /// </summary>
            public long endDate { get; set; }

            /// <summary>
            /// 附件
            /// </summary>
            public object files { get; set; }

            /// <summary>
            /// 主键ID
            /// </summary>
            public string id { get; set; }

            /// <summary>
            /// 图片
            /// </summary>
            public object images { get; set; }

            /// <summary>
            /// 是否重要(0:不重要、1:重要)
            /// </summary>
            public int important { get; set; }

            /// <summary>
            /// 提醒时间(-1:不提醒、0:开始时间提醒、15:开始时间前15分钟提醒、60:开始时间前1小时提醒)
            /// </summary>
            public int noticeTime { get; set; }

            /// <summary>
            /// 任务创建者的oid
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 任务创建者姓名
            /// </summary>
            public string personName { get; set; }

            /// <summary>
            /// 任务状态(0:未完成、1:完成、2:已关闭)
            /// </summary>
            public int status { get; set; }

            /// <summary>
            /// 任务定时提醒时间(0:不提醒、1:每天、2:每工作日、3:每周、4:每两周、5:每月)
            /// </summary>
            public int timingNoticeTime { get; set; }
        }

        public class ActorInfo
        {
            public string department { get; set; }

            public string eid { get; set; }

            public string openId { get; set; }

            public string personName { get; set; }

            public string photoUrl { get; set; }

            public int progress { get; set; }

            public int readStatus { get; set; }

            public int source { get; set; }

            public int status { get; set; }

            public string userId { get; set; }
        }
    }
}
