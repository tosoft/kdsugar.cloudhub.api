﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.TimeTask
{
    /// <summary>
    /// 
    /// </summary>
    public class QueryTimeTask : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/gateway/cloudwork/newwork/teamWorks?accessToken={0}";

        /// <summary>
        /// 
        /// </summary>
        public const int MAX_SIZE = 1000;

        /// <summary>
        /// 最后一条日程id(默认为空,第一页是为空)
        /// </summary>
        public string lastId { get; set; }

        /// <summary>
        /// 每次查多少条(最多不能超过1000条)
        /// </summary>
        public int size { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_lastId"></param>
        /// <param name="_size"></param>
        public QueryTimeTask(string _lastId,int _size = MAX_SIZE)
        {
            lastId = _lastId;
            size = _size;
        }

        public _QueryTimeTask Query(string _token)
        {
            return Post<_QueryTimeTask>(URL, _token);
        }
    }

    public class QueryTimeTaskByDay : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/gateway/cloudwork/newwork/queryByDay?accessToken={0}";

        /// <summary>
        /// 需查询当天任意时间点的时间戳(只能查询当日创建的日程)
        /// </summary>
        public long day { get; set; }

        public QueryTimeTaskByDay(DateTime _day)
        {
            day = Helper.ToUnixTime(_day);
        }

        public _QueryTimeTask Query(string _token)
        {
            return Post<_QueryTimeTask>(URL, _token);
        }
    }

    public class _QueryTimeTask : BaseResult
    {
        public List<TimeTaskInfo> data { get; set; }

        public class TimeTaskInfo
        {
            public int workStatus { get; set; }

            public int readStatus { get; set; }

            public long endDate { get; set; }

            public string openid { get; set; }

            public string channel { get; set; }

            public int acceptStatus { get; set; }

            public string type { get; set; }

            public string title { get; set; }

            public string content { get; set; }

            public string url { get; set; }

            public string personName { get; set; }

            public bool record { get; set; }

            public string workType { get; set; }

            public long doneTime { get; set; }

            public string id { get; set; }

            public int noticeTime { get; set; }

            public long startDate { get; set; }

            public long createDate { get; set; }
        }
    }
}
