﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KDSugar.CloudHub.API.TimeTask
{
    /// <summary>
    /// 日程设置为完成/未完成
    /// </summary>
    public class TimeTaskStatus : PostRequest
    {
        private const string FINISH_URL = "https://www.yunzhijia.com/gateway/cloudwork/newwork/finish?accessToken={0}";

        private const string UNFINISH_URL = "https://www.yunzhijia.com/gateway/cloudwork/newwork/unfinish?accessToken={0}";

        /// <summary>
        /// 日程id
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 日程创建者的oid
        /// </summary>
        public string openid { get; set; }

        public enum StatusTypeEnum
        {
            /// <summary>
            /// 完成
            /// </summary>
            Finish,
            /// <summary>
            /// 未完成
            /// </summary>
            UnFinish
        }

        private StatusTypeEnum StatusType;

        /// <summary>
        /// 日程设置初始化
        /// </summary>
        /// <param name="_id">日程id</param>
        /// <param name="_openId">日程创建者的oid</param>
        /// <param name="_type">完成状态</param>
        public TimeTaskStatus(string _id,string _openId,StatusTypeEnum _type)
        {
            id = _id;
            openid = _openId;
            StatusType = _type;
        }

        public BaseResult Operate(string _token)
        {
            string tmpUrl = null;
            switch (StatusType)
            {
                case StatusTypeEnum.Finish:
                    tmpUrl = FINISH_URL;
                    break;
                case StatusTypeEnum.UnFinish:
                    tmpUrl = UNFINISH_URL;
                    break;
            }
            return tmpUrl != null ? Post<BaseResult>(tmpUrl, _token) : null;
        }
    }
}
