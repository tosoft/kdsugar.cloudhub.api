﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.Todo
{
    /// <summary>
    /// 待办消息确认|发起
    /// </summary>
    public class CheckTodo : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/gateway/newtodo/open/checkcreatetodo.json?accessToken={0}";

        /// <summary>
        /// 应用ID，即appId
        /// </summary>
        public string sourcetype { get; set; }

        /// <summary>
        /// 即sourceId,生成的待办所关联的第三方服务业务记录的ID，是待办的批次号
        /// </summary>
        public string sourceitemid { get; set; }

        /// <summary>
        /// 待办接受人ID
        /// </summary>
        public string openId { get; set; }

        /// <summary>
        /// 初始化待办消息确认
        /// </summary>
        /// <param name="_sourcetype">应用ID，即appId</param>
        /// <param name="_sourceitemid">即sourceId,生成的待办所关联的第三方服务业务记录的ID，是待办的批次号</param>
        /// <param name="_openId">待办接受人ID</param>
        public CheckTodo(string _sourcetype,string _sourceitemid,string _openId)
        {
            sourcetype = _sourcetype;
            sourceitemid = _sourceitemid;
            openId = _openId;
        }

        /// <summary>
        /// 待办消息确认|发起
        /// </summary>
        /// <param name="_token"></param>
        /// <returns></returns>
        public _CheckTodo Operate(string _token)
        {
            return Post<_CheckTodo>(URL, _token);
        }
    }

    /// <summary>
    /// 待办消息确认|返回
    /// </summary>
    public class _CheckTodo : BaseResult
    {
        /// <summary>
        /// 
        /// </summary>
        public CheckInfo data { get; set; }

        /// <summary>
        /// 待办消息确认信息
        /// </summary>
        public class CheckInfo
        {
            /// <summary>
            /// 对应请求参数中的sourcetype
            /// </summary>
            public string appid { get; set; }

            /// <summary>
            /// 对应请求参数中的sourceitemid
            /// </summary>
            public string sourceid { get; set; }

            /// <summary>
            /// 检查结果，true：成功，false：失败
            /// </summary>
            public bool check { get; set; }

            /// <summary>
            /// 表示生成已办成功的数量
            /// </summary>
            public int dealCount { get; set; }

            /// <summary>
            /// 表示变待办成功的数量
            /// </summary>
            public int undelCount { get; set; }

            /// <summary>
            /// 待办状态枚举
            /// </summary>
            public enum TodoStatusEnum
            {
                /// <summary>
                /// 待办生成失败
                /// </summary>
                GenerateTodoError,
                /// <summary>
                /// 待办生成成功但未变成已办状态
                /// </summary>
                GenerateTodoSuccess,
                /// <summary>
                /// 待办已办
                /// </summary>
                TodoIsHandle
            }

            /// <summary>
            /// 待办状态
            /// </summary>
            [JsonIgnore]
            public TodoStatusEnum TodoStatus
            {
                get
                {
                    TodoStatusEnum result = TodoStatusEnum.GenerateTodoError;
                    if (dealCount == 0 && undelCount == 0)
                        result = TodoStatusEnum.GenerateTodoError;
                    if (dealCount == 0 && undelCount == 1)
                        result = TodoStatusEnum.GenerateTodoSuccess;
                    if (dealCount == 1 && undelCount == 0)
                        result = TodoStatusEnum.TodoIsHandle;
                    return result;
                }
            }
        }
    }
}
