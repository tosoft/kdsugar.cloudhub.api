﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.Todo
{
    /// <summary>
    /// 生成待办消息|发起
    /// </summary>
    public class GenerateTodo : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/gateway/newtodo/open/generatetodo.json?accessToken={0}";

        /// <summary>
        /// 点击待办跳转的URL
        /// </summary>
        public string url { get; set; }

        /// <summary>
        /// 生成的待办所关联的第三方服务业务记录的ID，待办的批次号，对应待办处理中的sourceitemid
        /// <para>其值都第三方服务自行设定，需保证其唯一性。</para>
        /// </summary>
        public string sourceId { get; set; }

        /// <summary>
        /// 待办内容
        /// </summary>
        public string content { get; set; }

        /// <summary>
        /// 来自字段的内容显示
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 待办项标题内容显示,选填，如不填，则默认为title值
        /// </summary>
        public string itemtitle { get; set; }

        /// <summary>
        /// 待办在客户端显示的图URL
        /// </summary>
        public string headImg { get; set; }

        /// <summary>
        /// 生成的待办所关联的第三方服务类型ID(第三方应用ID)
        /// <para>appId和sourceId组合在一起标识云之家唯一的一条待办</para>
        /// </summary>
        public string appId { get; set; }

        /// <summary>
        /// 待办的发送人的openId
        /// </summary>
        public string senderId { get; set; }

        /// <summary>
        /// 待办生成方式，true表示同步，false表示异步，默认为false；
        /// <para>同步生成待办目前批量最多一次只允许生成100条；</para>
        /// </summary>
        public bool sync { get; set; } = false;

        /// <summary>
        /// 待办接收人处理类
        /// </summary>
        public class ReceiveOpenId
        {
            /// <summary>
            /// 待办接收人OpenId
            /// </summary>
            public string openId { get; set; }

            /// <summary>
            /// 待办接收人处理类
            /// </summary>
            /// <param name="_openId">待办接收人OpenId</param>
            public ReceiveOpenId(string _openId)
            {
                openId = _openId;
            }
        }

        /// <summary>
        /// 待办接收人
        /// </summary>
        [JsonProperty(PropertyName = "params")]
        public List<ReceiveOpenId> ReceiveOpenIds { get; set; } = new List<ReceiveOpenId>();

        /// <summary>
        /// 初始化待办消息
        /// </summary>
        /// <param name="_title">来自字段的内容显示</param>
        /// <param name="_appId">生成的待办所关联的第三方服务类型ID(第三方应用ID)</param>
        /// <param name="_headImg">待办在客户端显示的图URL</param>
        /// <param name="_url">点击待办跳转的URL</param>
        /// <param name="_sourceId">生成的待办所关联的第三方服务业务记录的ID，待办的批次号，对应待办处理中的sourceitemid；
        /// <para>其值都第三方服务自行设定，需保证其唯一性。</para>
        /// </param>
        public GenerateTodo(string _title, string _appId, string _headImg, string _url, string _sourceId)
        {
            title = _title;
            appId = _appId;
            headImg = _headImg;
            url = _url;
            sourceId = _sourceId;
        }

        /// <summary>
        /// 新增待办接收人
        /// </summary>
        /// <param name="_openId">待办接收人OpenId</param>
        public void AddReceiveOpenId(string _openId)
        {
            if (ReceiveOpenIds.FindAll(m => m.openId.Equals(_openId)).Count == 0)
            {
                ReceiveOpenId item = new ReceiveOpenId(_openId);
                ReceiveOpenIds.Add(item);
            }
        }

        /// <summary>
        /// 批量新增待办接收人
        /// </summary>
        /// <param name="_openIds">待办接收人OpenId</param>
        public void AddReceiveOpenId(List<string> _openIds)
        {
            if (_openIds != null)
            {
                foreach(string openId in _openIds)
                {
                    AddReceiveOpenId(openId);
                }
            }
        }

        /// <summary>
        /// 生成待办信息
        /// </summary>
        /// <param name="_token"></param>
        /// <param name="_sync">待办生成方式，true表示同步，false表示异步，默认为false；
        /// <para>同步生成待办目前批量最多一次只允许生成100条；</para>
        /// </param>
        /// <returns></returns>
        public _GenerateTodo Generate(string _token, bool _sync = false)
        {
            sync = _sync;
            return Post<_GenerateTodo>(URL, _token);
        }
    }

    /// <summary>
    /// 生成待办消息|接收
    /// </summary>
    public class _GenerateTodo : BaseResult
    {
        /// <summary>
        /// 接收数据
        /// </summary>
        [JsonExtensionData]
        public Dictionary<string,object> Values { get; set; }
    }
}
