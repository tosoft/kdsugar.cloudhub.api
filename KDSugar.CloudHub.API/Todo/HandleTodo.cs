﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace KDSugar.CloudHub.API.Todo
{
    /// <summary>
    /// 变更待办消息的处理状态、读状态、删除状态。
    /// </summary>
    public class HandleTodo : PostRequest
    {
        private const string URL = "https://www.yunzhijia.com/gateway/newtodo/open/action.json?accessToken={0}";

        /// <summary>
        /// 应用ID，即appId
        /// </summary>
        public string sourcetype { get; set; }

        /// <summary>
        /// 即发送待办的sourceId,生成的待办所关联的第三方服务业务记录的ID，是待办的批次号
        /// </summary>
        public string sourceitemid { get; set; }

        /// <summary>
        /// 目标处理状态，0表示未办，1表示已办，默认为0
        /// </summary>
        public int? deal { get; set; }

        /// <summary>
        /// 目标读状态，0表示未读，1表示已读，默认为0
        /// </summary>
        public int? read { get; set; }

        /// <summary>
        /// 目标删除状态，0表示未删除，1表示已删除
        /// </summary>
        public int? delete { get; set; }

        /// <summary>
        /// 可填多人，不填则更改sourceitemid下所有人员的待办状态
        /// </summary>
        public List<string> openids { get; set; }

        /// <summary>
        /// 待办处理方式，true表示同步，false表示异步，默认为false
        /// </summary>
        public bool sync { get; set; } = false;

        /// <summary>
        /// 初始化待办处理方式
        /// </summary>
        /// <param name="_sourcetype">应用ID，即appId</param>
        /// <param name="_sourceitemid">即发送待办的sourceId,生成的待办所关联的第三方服务业务记录的ID，是待办的批次号</param>
        public HandleTodo(string _sourcetype,string _sourceitemid)
        {
            sourcetype = _sourcetype;
            sourceitemid = _sourceitemid;
        }

        /// <summary>
        /// 新增待办处理接收人
        /// </summary>
        /// <param name="_openId"></param>
        public void AddOpenId(string _openId)
        {
            if (openids != null)
            {
                if (!openids.Contains(_openId))
                    openids.Add(_openId);
            }
            else
            {
                openids = new List<string>();
                openids.Add(_openId);
            }
        }

        /// <summary>
        /// 变更待办操作发起
        /// </summary>
        /// <param name="_token"></param>
        /// <param name="_sync">待办生成方式，true表示同步，false表示异步，默认为false；同步生成待办目前批量最多一次只允许生成100条；</param>
        /// <returns></returns>
        public _HandleTodo Operate(string _token, bool _sync = false)
        {
            sync = _sync;
            return Post<_HandleTodo>(URL, _token);
        }
    }

    /// <summary>
    /// 变更待办消息|返回
    /// </summary>
    public class _HandleTodo : BaseResult
    {
        /// <summary>
        /// 待办消息信息
        /// </summary>
        [JsonExtensionData]
        public Dictionary<string,object> Values { get; set; }
    }
}
